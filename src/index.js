import ReactDOM from 'react-dom';
import promiseFinally from 'promise.prototype.finally';
import React from 'react';
import { BrowserRouter, HashRouter, Link } from 'react-router-dom';
import { useStrict } from 'mobx';
import { Provider } from 'mobx-react';

import App from './App';

import apiStore from './store/api-store'
import stateStore from './store/state-store'
import dataStore from './store/data-store'
import './static/scss/base.scss'
import "react-responsive-carousel/lib/styles/carousel.min.css"
const stores = {
  apiStore,
  stateStore,
  dataStore
};

// For easier debugging
window._____APP_STATE_____ = stores;

promiseFinally.shim();
useStrict(true);
document.addEventListener("DOMContentLoaded", function(event) {
  ReactDOM.render((
    <Provider {...stores}>
      <BrowserRouter>
        <App />
      </BrowserRouter>
    </Provider>
  ), document.getElementById('root'));
})
