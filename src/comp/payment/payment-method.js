import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'


@inject('stateStore', 'apiStore', 'dataStore')
@observer
class PaymentMethod extends Component {

  onClickSelectPayment = (e, position) => {
    this.props.stateStore.setPaymentState("paymentMethod", position)
  }
  renderPaymentMethod() {
    const data = this.props.dataStore.paymentMethod
    const paymentState = this.props.stateStore.paymentState

    let view = []
    for(var i=0; i<data.length; i++) {

      let position = i
      let className = paymentState.paymentMethod === position ? 'selected' : 'not-selected'
      view.push(
        <li className="payment-method-item" key={i}>
          <span
            className={className}
            onClick={(e) => this.onClickSelectPayment(e, position)}>{data[i]}</span>
        </li>
      )
    }
    return (
      <div className="payment-method-con">
        <span className="payment-method-title">{this.props.dataStore.str.W0068}</span>
        <ul className="payment-all-method-container">{view}</ul>
      </div>
    )
  }

  render() {
    return (
      <div className="payment-method-entity-container">
        {this.renderPaymentMethod()}
      </div>
    )

  }
}
export default PaymentMethod
