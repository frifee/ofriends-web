import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
import Link from "react-router-dom/Link";

@inject('stateStore', 'apiStore', 'dataStore')
@observer
class FinishPayment extends Component {

  onClickArrow = (e) => {
    const paymentState = this.props.stateStore.paymentState
    this.props.stateStore.setPaymentState("isOpenSelectedOfFinish", !paymentState.isOpenSelectedOfFinish)
  }

  onClickFinish = (e) => {
  }
  renderItemInfo() {
    const paymentState = this.props.stateStore.paymentState
    const imgDUrl = this.props.dataStore.productsImgUrl + this.props.productId + "_0.jpg"
    return (
      <div className="finish-payment-container">
        <span className="finish-payment-title">{this.props.dataStore.str.W0092}</span>
        <div className="finish-payment-selected-ofriends-con">
          <span className="finish-payment-selected-ofriends-title">{this.props.dataStore.str.W0082}</span>
          <span
            className={paymentState.isOpenSelectedOfFinish ? "arrow-down" : "arrow-up"}
            onClick = {(e) => this.onClickArrow(e)}></span>
        </div>
        {
          paymentState.isOpenSelectedOfFinish
          &&
          <div className="finish-payment-image-title-date-option-price-container">
            <img
              className="finish-payment-image"
              src={imgDUrl === undefined || imgDUrl === null ? "" : imgDUrl}
              onError={(e)=>e.target.setAttribute("src", this.props.dataStore.imageUrl + "06_detailpage_review_photo_error.png")}/>
            <div className="finish-payment-title-date-option-con">
              <span className="finish-payment-title">{this.props.title}</span>
              <span className="finish-payment-date">{this.props.date}</span>
              <span className="finish-payment-option">
                {this.props.option}&nbsp;x&nbsp;{this.props.count}{this.props.dataStore.str.W0086}</span>
            </div>
            <span className="finish-payment-price">
              {this.props.apiStore.getFormatWon(this.props.price)}
            </span>
          </div>
        }
        <ul className="finish-payment-final-price-priceCut-method-container">
          <li className="finish-payment-final-price-title-con">
            <span className="finish-payment-final-price-title">{this.props.dataStore.str.W0083}</span>
            <span className="finish-payment-final-price">
              {this.props.apiStore.getFormatWon(this.props.price)}
            </span>
          </li>
          <li className="finish-payment-final-priceCut-con">
            <span className="finish-payment-final-price-priceCut-title">{this.props.dataStore.str.W0066}</span>
            <span className="finish-payment-final-price-priceCut">
              -{this.props.apiStore.getFormatWon(this.props.priceCut)}
            </span>
          </li>
          <li className="finish-payment-final-price-payment-con">
            <span className="finish-payment-final-price-paymentPrice-title">{this.props.dataStore.str.W0067}</span>
            <span className="finish-payment-final-price-paymentPrice">
              {this.props.apiStore.getFormatWon(this.props.paymentPrice)}
            </span>
          </li>
          <li className="finish-payment-final-price-method-con">
            <span className="finish-payment-final-price-method-title">{this.props.dataStore.str.W0068}</span>
            <span className="finish-payment-final-price-method">{this.props.method}</span>
          </li>
        </ul>
      </div>
    )
  }

  render() {
    return (
      <div className="finish-payment-entity-container">
        {this.renderItemInfo()}
        <Link to="/my/ofriends/history" className="finish-payment-confirm-button">{this.props.dataStore.str.W0091}</Link>
      </div>
    )
  }
}
export default FinishPayment
