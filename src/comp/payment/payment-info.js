import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'

@inject('stateStore', 'apiStore', 'dataStore')
@observer
class PaymentInfo extends Component {

  onClickArrow = (e) => {
    // const samplePayment = this.props.stateStore.samplePayment
    // this.props.stateStore.setPaymentState("isOpenSelectedOf", !samplePayment.isOpenSelectedOf)
    const paymentState = this.props.stateStore.paymentState
    this.props.stateStore.setPaymentState("isOpenSelectedOf", !paymentState.isOpenSelectedOf)
  }

  onClickCouponSelect = (e) => {
    this.props.stateStore.setPopupOn('couponSelect')
  }

  renderItemInfo() {
    const paymentState = this.props.stateStore.paymentState
    const imgDUrl = this.props.dataStore.productsImgUrl + this.props.productId + "_0.jpg"
    return (
      <div className="payment-info-item-information-container">
        <span className="payment-info-title">{this.props.dataStore.str.W0060}</span>
        <div className="payment-info-selected-ofriends-arrow-con">
          <span className="payment-info-selected-title">{this.props.dataStore.str.W0082}</span>
          <span
            className={paymentState.isOpenSelectedOf ? "arrow-down" : "arrow-up"}
            onClick = {(e) => this.onClickArrow(e)}></span>
        </div>
        {
          paymentState.isOpenSelectedOf
          &&
          <div className="payment-info-item-detail-container">
            <div className="payment-info-image-con">
              <img
                className="payment-info-detail-image"
                src={imgDUrl === undefined || imgDUrl === null ? "" : imgDUrl}
                onError={(e)=>e.target.setAttribute("src", this.props.dataStore.imageUrl + "06_detailpage_no_image.png")}/>
            </div>
            <div className="payment-info-title-date-option-con">
              <div className="payment-info-detail-title-con">
                <span className="payment-info-detail-title">{this.props.title}</span>
              </div>
              <div className="payment-info-date-option-price-con">
                <div className="payment-info-detail-option-con">
                  <span className="payment-info-detail-date">{this.props.date}</span>
                  <span className="payment-info-option">
                    {this.props.option}&nbsp;x&nbsp;{this.props.count}{this.props.dataStore.str.W0086}
                  </span>
                </div>
                <div className="payment-info-price-con">
                  <span className="payment-info-price">
                  {this.props.apiStore.getFormatWon(this.props.price)}
                  </span>
                </div>
              </div>
            </div>
          </div>
        }
        <div className="payment-info-item-price-con">
          <span className="payment-info-item-price-title">{this.props.dataStore.str.W0083}</span>
          <span className="payment-info-item-price">
            {this.props.apiStore.getFormatWon(this.props.price)}
          </span>
        </div>
        <div className="payment-info-coupon-priceCut-con">
          <span className="payment-info-coupon-title">{this.props.dataStore.str.W0034}</span>
          <span className="payment-info-priceCut">
            -{this.props.apiStore.getFormatWon(paymentState.priceCut)}
          </span>
          <span className="payment-info-select-coupon" onClick={(e) => {this.onClickCouponSelect(e)}}>{this.props.dataStore.str.W0087}</span>
        </div>
      </div>
    )

  }

  renderPriceInfo() {
    const paymentState = this.props.stateStore.paymentState
    return (
      <div className="payment-info-price-container">
        <div className="payment-info-price-icon-title-priceCut-con">
          <span className="payment-info-price-icon"></span>
          <span className="payment-info-price-title">{this.props.dataStore.str.W0084}</span>
          <span className="payment-info-price-priceCut">
            -{this.props.apiStore.getFormatWon(paymentState.priceCut)}
          </span>
        </div>
        <div className="payment-info-price-finally-price-con">
          <span className="payment-info-price-final-price-title">{this.props.dataStore.str.W0085}</span>
          <span className="payment-info-finally-price">
            {this.props.apiStore.getFormatWon(paymentState.price - paymentState.priceCut)}
          </span>
        </div>
      </div>
    )
  }

  render() {
    return (
      <div className="payment-info-entity-container">
        {this.renderItemInfo()}
        {this.renderPriceInfo()}
      </div>
    )

  }
}
export default PaymentInfo
