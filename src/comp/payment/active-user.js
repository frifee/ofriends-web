import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'

@inject('stateStore', 'apiStore', 'dataStore')
@observer
class ActiveUser extends Component {

  onClickActiveUser = (e) => {
    // this.props.stateStore.setSamplePayment("activeUser", true)
    // this.props.stateStore.setRegActiveState('regCase', 1)
    this.props.stateStore.setRegActiveState('regCase', 2)
    this.props.stateStore.setRegActiveState('isOn', true)
    this.props.stateStore.setRegActiveState('title', this.props.dataStore.str.W1025)
    this.props.stateStore.setRegActiveState('idTitle', this.props.dataStore.str.W0134)
    this.props.stateStore.setRegActiveState('mktTitle', this.props.dataStore.str.W0135)
  }

  render() {
    return (
      <div className="active-user-entity-container">
        <span className="active-user-register-title">{this.props.dataStore.str.W1034}</span>
        <div className="active-user-warning-register-container">
          <span className="active-user-warning-message" dangerouslySetInnerHTML={{__html: this.props.dataStore.str.S0013}}></span>
          <span
            className="active-user-register-btn"
            onClick={(e) => this.onClickActiveUser(e)}>{this.props.dataStore.str.W0050}</span>
        </div>
      </div>
    )
  }
}
export default ActiveUser
