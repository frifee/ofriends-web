import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
import { Link } from 'react-router-dom'
@inject('stateStore', 'apiStore', 'dataStore')
@observer
class UserInfo extends Component {

  componentDidMount() {

  }
  onChangeImages = (e) => {
    // let thumbnails = []
    // for(var i=0; i<this.props.stateStore.myReviewEditState.thumbnails.length; i++) {
    //   let row = this.props.stateStore.myReviewEditState.thumbnails[i]
    //   thumbnails.push(row)
    // }
    // thumbnails.push(e.target.files[0])
    // this.props.stateStore.setMyReviewEditState("thumbnails", thumbnails)
    let files = e.target.files
    if(files !=null) {
      this.postImageUpload(files[0])
    }
  }

  postImageUpload(file) {
    let formData = new FormData()
    formData.append('photos', file)
    let token = this.props.apiStore.getJwtToken()
    this.props.apiStore.postImageUploadUsers(token, formData)
      .then(res => {
        this.putUserSetting(res.data.data[0].url)

      })
      .catch(err => {
      })
  }

  putUserSetting(url) {
    let jwtToken = this.props.apiStore.getJwtToken()
    if(jwtToken === null) {
      this.props.history.push('/login')
      return
    }
    let payload = {
      'image_url' : url
    }
    this.props.apiStore.putUserSetting(localStorage.getItem('jwt'), payload)
      .then(res => {
        this.getUserInfo()
        this.props.stateStore.setPopupOn('uploadFinish')
      })
      .catch(err => {
      })
  }
  getUserInfo() {
    this.props.apiStore.getUserInfo(localStorage.getItem('jwt'))
      .then(res=>{
        this.props.dataStore.setDataUserInfo(res.data)
      })
      .catch(err=> {
      })
  }
  render() {
    const dataUserInfo = this.props.dataStore.dataUserInfo
    let nickName =
        (typeof dataUserInfo.nickname === "undefined"
            || dataUserInfo.nickname === null
            || dataUserInfo.nickname === "")
            ? this.props.dataStore.str.W0051
            : this.props.dataStore.dataUserInfo.nickname
    let pf = ''
    switch(dataUserInfo.pf) {
      case 'email':
        pf = this.props.dataStore.str.W0155
        break
      case 'facebook':
        pf = this.props.dataStore.str.W0156
        break
      case 'kakao':
        pf = this.props.dataStore.str.W0157
        break
    }
    return (
      <div className="userinfo-entity-container">
        <img
          src={
            (dataUserInfo.image_url != undefined && dataUserInfo.image_url.length > 0)
            ? this.props.dataStore.userImgUrl + dataUserInfo.image_url : this.props.dataStore.imageUrl + "07_profile_user.png"
          }
          alt=''
          onError={(e)=>e.target.setAttribute("src", this.props.dataStore.imageUrl + "07_profile_user.png")}/>
        <div className="user-img-upload">
          <input
            className="user-img-upload-input"
            type="file" onChange={(e)=>this.onChangeImages(e)}
            id="user-img-uploader"
            accept="image/jpeg, image/png"/>
          <label htmlFor="user-img-uploader">
            <div className="user-img-click"></div>
          </label>
        </div>

        <div className="user-info-profile-desc-con">
          <div className="user-info-profile-name-con">
            <span className="user-info-profile-name">{nickName}</span>
          </div>
          <div className="user-info-interest-con">
            <Link className="user-info-interest" to="/my/interest">{this.props.dataStore.str.W0031}</Link>
          </div>
          <div className="user-info-register-email-con">
            <span className="user-info-register-email">{pf}</span>
          </div>
        </div>
      </div>
    )

  }
}
export default UserInfo
