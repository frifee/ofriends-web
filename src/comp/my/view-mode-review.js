import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
import StarRatingComponent from 'react-star-rating-component'

@inject('stateStore', 'apiStore', 'dataStore')
@observer
class ViewModeReview extends Component {

  onClickEdit = (e) => {
    this.props.stateStore.setMyReviewEditState("state", 1)
  }

  renderImg() {
    const pageId = parseInt(this.props.stateStore.myReviewEditState.id)
    const data = this.props.stateStore.myReviewEditState.thumbnails
    if(data != undefined && data[0] != null) {
      let images = []
      let key = 0
      for(var i=0; i<data.length; i++) {
        let row = data[i]
        images.push(
          <li className="view-mode-review-image-con" key={key}>
            <img
              src={row === undefined || row === null ? "" : this.props.dataStore.reviewImgUrl + row}
              onError={(e)=>e.target.setAttribute("src", this.props.dataStore.imageUrl + "06_detailpage_review_photo_error.png")}/>
          </li>
        )
        key = key + 1
      }
      return (<ul className="view-mode-review-images-container">{images}</ul>)
    }

  }

  render() {


    const pageId = parseInt(this.props.stateStore.myReviewEditState.id)
    // const data = this.props.dataStore.sampleMyReviews.data[pageId]
    const myReviewEditState = this.props.stateStore.myReviewEditState

    return (
      <div className="view-mode-review-entity-container">
        <div className="view-mode-review-save-btn">
          <span onClick={(e)=>{this.onClickEdit()}}>{this.props.dataStore.str.W0079}</span>
        </div>
        <div className="view-mode-review-name-rate-content-container">
          <div className="view-mode-review-infoName-con">
            <div className="view-mode-review-infoName">
              <span>{myReviewEditState.productTitle}</span>
            </div>
          </div>
          <div className="view-mode-review-rating-container">
            <ul className="view-mode-review-rating-star-con">
              <li>
                <span className="view-mode-review-rating-title">{this.props.dataStore.str.W0076}</span>
              </li>
              <li className="view-mode-review-star-rating-component">
                <StarRatingComponent
                  name="rate1"
                  starCount={5}
                  value={myReviewEditState.rate}
                  renderStarIcon={() => <span className="view-mode-review-star"></span>}
                  editing={false}>
                </StarRatingComponent>
              </li>
            </ul>
          </div>
          <div className="view-mode-review-content-con">
            <div className="view-mode-review-content-title">
              <span>{this.props.dataStore.str.W0077}</span>
            </div>
            <div className="view-mode-review-content-area-con">
              <span
                className="view-mode-review-content-area"
                dangerouslySetInnerHTML={{__html: myReviewEditState.inputFormatText}}></span>
            </div>
          </div>
        </div>
        <div className="view-mode-review-picture-con">
          <span className="view-mode-review-picture">{this.props.dataStore.str.W0078}</span>
        </div>
        {this.renderImg()}
      </div>
    )

  }
}
export default ViewModeReview
