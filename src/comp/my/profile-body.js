import React, { Component, Fragment } from 'react'
import { inject, observer } from 'mobx-react'
import {withRouter} from 'react-router-dom'
import TwoBtn from "../dialog/two-btn";
import OneBtn from "../dialog/one-btn";
@inject('stateStore', 'apiStore', 'dataStore')
@withRouter
@observer
class ProfileBody extends Component {

  constructor(props) {
    super(props);
    this.state = {
      infoHoverPopup: false
    }
  }

  componentDidMount() {
    this.onRouteChanged()
  }

  componentDidUpdate(prevProps) {
    if (this.props.location !== prevProps.location) {
      this.onRouteChanged()
    }
  }

  onRouteChanged() {
    this.setInitState()
  }

  setInitState() {
    const myProfileState = this.props.stateStore.myProfileState
    const dataUserInfo = this.props.dataStore.dataUserInfo
    let nickName = this.props.dataStore.dataUserInfo.nickname
    if(nickName === undefined || nickName === null)
      nickName = ''

    let inputRecomCode = this.props.dataStore.dataUserInfo.recommender
    let recomCode = 2
    if(inputRecomCode === undefined || inputRecomCode === null || inputRecomCode === "") {
      recomCode = 0
    }

    let activeCustomer = dataUserInfo.level > 100 ? 2 : 0

    let userLevel = dataUserInfo.level

    this.props.stateStore.setMyProfileState('nickChange', 0)
    this.props.stateStore.setMyProfileState('activeCustomer', activeCustomer)
    this.props.stateStore.setMyProfileState('userLevel', userLevel)
    this.props.stateStore.setMyProfileState('pwChange', 0)
    this.props.stateStore.setMyProfileState('recomCode', recomCode)
    this.props.stateStore.setMyProfileState('inputNick', nickName)
    this.props.stateStore.setMyProfileState('inputRecomCode', recomCode)
    this.props.stateStore.setMyProfileState('checkPw', 0)

    this.setState({infoHoverPopup: false})

  }



  onChangeNick = (e) => {
    let nickName = e.target.value
    if(nickName.trim().length <= 10) {
      this.props.stateStore.setMyProfileState('inputNick', e.target.value)
    }
  }
  onChangeRecomCode = (e) => {
    this.props.stateStore.setMyProfileState('inputRecomCode', e.target.value)
  }

  onClickRecomInput = (e) => {
    const myProfileState = this.props.stateStore.myProfileState
    this.props.stateStore.setMyProfileState('inputRecomCode', '')
    this.props.stateStore.setMyProfileState('recomCode', 1)
  }

  onClickRecomSave = (e) => {
    const sampleMyProfile = this.props.dataStore.sampleMyProfile
    const myProfileState = this.props.stateStore.myProfileState

    let payload = {
      'recommender' : myProfileState.inputRecomCode
    }
    let jwtToken = this.props.apiStore.getJwtToken()
    if(jwtToken === null) {
      this.props.history.push('/login')
      return
    }

    this.props.apiStore.putUserSetting(jwtToken, payload)
      .then(res => {
        this.props.stateStore.setMyProfileState('recomCode', 2)
        this.props.getUserInfo()
      })
      .catch(err => {
      })
  }

  onClickRecomDel = (e) => {
    // const sampleMyProfile = this.props.dataStore.sampleMyProfile
    // const myProfileState = this.props.stateStore.myProfileState
    // this.props.stateStore.setMyProfileState('recomCode', 3)
    // this.props.dataStore.setSampleMyProfile('recomCode', null)

    this.props.stateStore.setMyProfileState('recomCode', 3)
  }

  onClickNickChange = (e) => {
    const myProfileState = this.props.stateStore.myProfileState
    const dataUserInfo = this.props.dataStore.dataUserInfo
    let nickName =
      (typeof dataUserInfo.nickname == "undefined"
        || dataUserInfo.nickname == null
        || dataUserInfo.nickname == "")
        ? ''
        : dataUserInfo.nickname

    this.props.stateStore.setMyProfileState('inputNick', nickName)
    this.props.stateStore.setMyProfileState('nickChange', 1)

  }
  onClickNickSave = (e) => {
    const myProfileState = this.props.stateStore.myProfileState
    let nickName = myProfileState.inputNick
    if(nickName.trim().length < 1) {
      alert(this.props.dataStore.str.S1054)
      return
    }
    if(nickName.trim().length > 10) {
      nickName = nickName.substring(0,10)
    }
    let payload = {
      'nickname' : nickName
    }

    let jwtToken = this.props.apiStore.getJwtToken()

    if(jwtToken === null) {
      this.props.history.push('/login')
      return
    }

    this.props.apiStore.putUserSetting(jwtToken, payload)
      .then(res => {
        this.props.stateStore.setMyProfileState('recomCode', 2)
        this.props.getUserInfo()
      })
      .catch(err => {
      })

    this.props.dataStore.setSampleMyProfile('nickName', myProfileState.inputNick)
    this.props.stateStore.setMyProfileState('nickChange', 0)
  }
  onClickActive = (e) => {
    let userLevel = this.props.stateStore.myProfileState.userLevel
    // this.props.stateStore.setRegActiveState('regCase', userLevel < 20 ? 1 : 2)
    this.props.stateStore.setRegActiveState('regCase', 2)
    this.props.stateStore.setRegActiveState('isOn', true)
    this.props.stateStore.setRegActiveState('title', this.props.dataStore.str.W1025)
    this.props.stateStore.setRegActiveState('idTitle', this.props.dataStore.str.W0134)
    this.props.stateStore.setRegActiveState('mktTitle', this.props.dataStore.str.W0135)
  }
  onClickPw = (e) => {
    this.props.stateStore.setMyProfileState('pwChange', 1)
  }

  onClickWithdrawal= (e) => {
    this.props.stateStore.setPopupOn('withdrawal')
  }

  onMouserOverFriendsInfo = (e) => {
    this.setState({infoHoverPopup: true})
  }

  onMouseOutFriendsInfo = (e) => {
    this.setState({infoHoverPopup: false})
  }

  onClickSuperFriends = (e) => {

  }

  renderNickName() {
    const myProfileState = this.props.stateStore.myProfileState
    const dataUserInfo = this.props.dataStore.dataUserInfo
    let inputView
    let btnClassName = myProfileState.nickChange === 0 ? 'nick-default' : 'nick-change'
    let nickName =
      (typeof dataUserInfo.nickname == "undefined"
        || dataUserInfo.nickname == null
        || dataUserInfo.nickname == "")
        ? this.props.dataStore.str.W0051
        : dataUserInfo.nickname

    if(myProfileState.nickChange === 0) {
      inputView = (
        <ul>
          <li className="profile-body-input-nickname-con">
            <span>{nickName}</span>
          </li>
          <li className="profile-body-input-nickname-modify-con">
            <span
              className={btnClassName}
              onClick={(e)=>this.onClickNickChange(e)}>{this.props.dataStore.str.W0044}</span>
          </li>
        </ul>
      )
    } else {
      inputView = (
        <ul>
          <li className="profile-body-input-nickname-change-con">
            <input
              value={myProfileState.inputNick}
              onChange={(e) => this.onChangeNick(e)}/>
          </li>
          <li className="profile-body-input-nickname-modify-change-con">
            <span
              className={btnClassName}
              onClick={(e)=>this.onClickNickSave(e)}>{this.props.dataStore.str.W0045}</span>
          </li>
        </ul>
      )
    }

    return (
      <div className="profile-body-nickname-modify-container">
        <div className="profile-body-nickname-con">
          <span>{this.props.dataStore.str.W0035}</span>
        </div>
        <div className="profile-body-inputview-con">{inputView}</div>
      </div>
    )
  }
  renderActive() {
    const myProfileState = this.props.stateStore.myProfileState
    let buttonClassName = myProfileState.userLevel<120 ? 'not-active' : 'active'
    let buttonText = myProfileState.userLevel<120
      ?
      (
        <span
          className="non-activated-user"
          onClick={(e)=>{this.onClickActive(e)}}>
          {this.props.dataStore.str.W1027}
        </span>
      )
      :
      (
        <span className="activated-user">{this.props.dataStore.str.W1028}</span>
      )

    let button = (
      <div className={buttonClassName}>
        {buttonText}
      </div>
    )
    return (
      <div className="profile-body-active-container">
        <div className="profile-body-active-customer-con">
          <span className="profile-body-active-customer-title">{this.props.dataStore.str.W1022}</span>
          <a
            className="profile-body-superfriends-info"
            href="https://static.ofriends.co.kr/images/details/ofriends_benefit/index.html" target="_blank">
          </a>
        </div>
        <div className="profile-body-button-con">{button}</div>
      </div>
    )

  }

  renderActiveUserInfo() {
    const dataUserInfo = this.props.dataStore.dataUserInfo
    let birthday = ""
    try {
      // birthday = dataUserInfo.birthday.replace('-', '년 ').replace('-', '월 ') + '일'
      birthday = dataUserInfo.birthday.split('-')[0] + '년 '
        + dataUserInfo.birthday.split('-')[1]
        + '월 ' + dataUserInfo.birthday.split('-')[2] + '일'
    } catch (e) {
      birthday = dataUserInfo.birthday
    }
    return (
      <Fragment>
        {/*이름*/}
        <div className="profile-body-name-con">
          <div className="profile-body-name-title">
            <span>{this.props.dataStore.str.W0037}</span>
          </div>
          <div className="profile-body-name">
            <span>{dataUserInfo.name}</span>
          </div>
        </div>
        {/*생년월일*/}
        <div className="profile-body-birthday-con">
          <div className="profile-body-name-birthday-title">
            <span>{this.props.dataStore.str.W0038}</span>
          </div>
          <div className="profile-body-name-birthday">
            <span>{birthday}</span>
          </div>
        </div>
        {/*성별*/}
        <div className="profile-body-gender-con">
          <div className="profile-body-gender-title">
            <span>{this.props.dataStore.str.W0039}</span>
          </div>
          <div className="profile-body-gender">
            <span>
              {dataUserInfo.gender === 'male' && this.props.dataStore.str.W1030}
              {dataUserInfo.gender === 'female' && this.props.dataStore.str.W1031}
              {dataUserInfo.gender !== 'male' && dataUserInfo.gender !== 'female' && this.props.dataStore.str.W1032}
            </span>
          </div>
        </div>
        {/*휴대폰번호*/}
        <div className="profile-body-phone-con">
          <div className="profile-body-phone-title">
            <span>{this.props.dataStore.str.W0040}</span>
          </div>
          <div className="profile-body-phone">
            <ul>
              <li className="profile-body-phone-cellNum">
                <span>{dataUserInfo.phone}</span>
              </li>
              <li className="profile-body-phone-auth">
                <span>{this.props.dataStore.str.W0047}</span>
              </li>
            </ul>
          </div>
        </div>
      </Fragment>

    )

  }

  renderEmail() {
    const dataUserInfo = this.props.dataStore.dataUserInfo
    return (
      <div className="profile-body-email-con">
        <div className="profile-body-email-title">
          <span>{this.props.dataStore.str.W0041}</span>
        </div>
        <div className="profile-body-email">
          <span>{dataUserInfo.email}</span>
        </div>
      </div>
    )
  }

  renderPw() {
    const sampleMyProfile = this.props.dataStore.sampleMyProfile
    return (
      <div className="profile-body-password-con">
        <div className="profile-body-password-change-con">
          <span>{this.props.dataStore.str.W0042}</span>
        </div>
        <div className="profile-body-password-change-box-con">
          <span onClick={(e) => this.onClickPw(e)}>{this.props.dataStore.str.W0048}</span>
        </div>
      </div>
    )
  }

  renderRecomCode() {
    const myProfileState = this.props.stateStore.myProfileState
    const dataUserInfo = this.props.dataStore.dataUserInfo
    let inputView
    let btnClassName
    switch(myProfileState.recomCode) {
      case 0:
        btnClassName = 'recomcode-input'
        inputView = (
          <ul className="profile-body-recomcode-input-con">
            <li className="profile-body-recomcode-input-text">
              <span></span>
            </li>
            <li className="profile-body-recomcode-input-button">
              <span onClick={(e) => this.onClickRecomInput(e)}>{this.props.dataStore.str.W0049}</span>
            </li>
          </ul>
        )
        break
      case 1:
        btnClassName = 'recomcode-save'
        inputView = (
          <ul className="profile-body-recomcode-save-con">
            <li className="profile-body-recomcode-save-input-con">
              <input
                className="profile-body-recomcode-save-input-text"
                onChange={(e) => this.onChangeRecomCode(e)}
                value={myProfileState.inputRecomCode}/>
            </li>
            <li className="profile-body-recomcode-input-save-btn-con">
              <span
                className="profile-body-recomcode-save-btn"
                onClick={(e) => this.onClickRecomSave(e)}>{this.props.dataStore.str.W0045}</span>
            </li>
          </ul>
        )
        break
      case 2:
      case 3:
        btnClassName = 'recomcode-delete'
        inputView = (
          <ul className="profile-body-recomcode-delete-con">
            <li className="profile-body-recomcode-delete-text-con">
              <span className="profile-body-recomcode-delete-text">{dataUserInfo.recommender}</span>
            </li>
            <li className="profile-body-recomcode-delete-btn-con">
              <span
                className="profile-body-recomcode-delete-btn"
                onClick={(e) => this.onClickRecomDel(e)}>{this.props.dataStore.str.W0052}</span>
            </li>
          </ul>
        )
        break
      default:
        break
    }
    return (
      <div className="profile-body-recomcode-con">
        <div className="profile-body-recomcode-title-con">
          <span>{this.props.dataStore.str.W0043}</span>
        </div>
        <div className="profile-body-recomcode-input-entity-con">{inputView}</div>
      </div>
    )
  }


  renderQuit() {
    return (
      <div className="profile-body-quit-con">
        <span className="profile-body-quit-title">{this.props.dataStore.str.W1014}</span>
        <span className="profile-body-quit-btn" onClick={(e) => {this.onClickWithdrawal(e)}}>{this.props.dataStore.str.W1015}</span>
      </div>
    )
  }

  // 로그아웃
  logout() {
    const userPf = this.props.dataStore.dataUserInfo.pf
    try {
      if (userPf === 'kakao') {
        if (window.Kakao.isInitialized() === true) {
          window.Kakao.cleanup()
        }
        window.Kakao.init(process.env.REACT_APP_KAKAO_APP_ID)
        window.Kakao.Auth.logout(() => {
        })
      } else if (userPf === 'facebook') {
        window.FB.logout((response) => {
        })
      }
    } catch (e) {}
    localStorage.removeItem('exp')
    localStorage.removeItem('jwt')
    this.props.stateStore.setLoginState(0)
    this.props.history.push('/')
  }

  onClickDeny = (e) => {
    this.props.stateStore.setPopupOn("")
  }

  onClickProcess = (e) => {
    this.props.stateStore.setPopupOn("")
    // const loginState = this.props.stateStore.loginState
    const jwtToken = this.props.apiStore.getJwtToken()
    if(jwtToken === null) {
      this.props.history.push('/login')
      return
    }
    this.props.apiStore.postWithdrawal(jwtToken)
      .then((res) => {
        this.logout()
    })
      .catch((err) => {

    })
  }

  renderQuitDialog() {
    const popupOn = this.props.stateStore.popupOn
    if(popupOn === 'withdrawal') {
      return (
        <TwoBtn
          content={this.props.dataStore.str.S1024}
          onClickBtn1={(e) => this.onClickDeny(e)}
          onClickBtn2={(e) => this.onClickProcess(e)}
          btn1 = {this.props.dataStore.str.W0053}
          btn2 = {this.props.dataStore.str.W1016}></TwoBtn>
      )
    }
  }

  renderMyProfile() {
    const dataUserInfo = this.props.dataStore.dataUserInfo
    return (
      <Fragment>
        {/*닉네임*/}
        <div className="profile-body-nickname-entity">
          {this.renderNickName()}
        </div>
        {/*활동고객등록*/}
        <div className="profile-body-active-enroll-entity">
          {this.renderActive()}
          {this.state.infoHoverPopup === true &&
          <div className="profile-body-superfriends-info-con">
            <span className="superfriends-info-title">{this.props.dataStore.str.W1038}</span>
            <span className="superfriends-info-desc">{this.props.dataStore.str.S1056}</span>
          </div>
          }
        </div>
        {/*활동고객일때만 보이는 정보*/}
        {
          this.props.stateStore.myProfileState.activeCustomer === 2
          &&
          <div className="profile-body-activated-member">{this.renderActiveUserInfo()}</div>

        }
        {/*이메일*/}
        <div className="profile-body-email-entity">
          {this.renderEmail()}
        </div>
        {/*비밀번호 변경*/}
        {dataUserInfo.pf === 'email' &&
          <div className="profile-body-password-entity">
          {this.renderPw()}
          </div>
        }
        {/*추천인코드*/}
        <div className="profile-body-recomcode-entity">
          {this.renderRecomCode()}
        </div>
        <div className="profile-body-quit-entity">
        {/*회원 탈퇴*/}
          {this.renderQuit()}
        </div>
        {this.renderQuitDialog()}
      </Fragment>
    )
  }

  renderRegEndMsg() {
    if(this.props.stateStore.popupOn === 'regEndMsg') {
      return (
        <OneBtn
          msg={this.props.stateStore.regActiveState.regEndMsg}
          onClickClose={(e) => {
            this.props.stateStore.setPopupOn("")
          }}></OneBtn>
      )
    }
    if(this.props.stateStore.popupOn === 'needSmsAgree') {
      return (
        <OneBtn
          msg={this.props.dataStore.str.S0167}
          onClickClose={(e) => {
            this.props.stateStore.setPopupOn("")
          }}></OneBtn>
      )
    }
  }


  render() {
    return (
      <div className="profile-body-entity-container">
        <div className="profile-body-my-profile-con">{this.renderMyProfile()}</div>
        {this.renderRegEndMsg()}
      </div>
    )

  }
}
export default ProfileBody
