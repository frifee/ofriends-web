import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'

@inject('stateStore', 'apiStore', 'dataStore')
@observer
class RowMyInterest extends Component {

  onClickTag = (e, id) => {
    this.props.stateStore.setMyInterestSelected(id.toString())

  }

  renderTags() {
    let tags = []
    const myInterestSelected = this.props.selected
    for(var i=0; i<this.props.item.length; i++) {
      let row = this.props.item[i]
      let className = myInterestSelected.includes(row.id.toString()) ? 'selected' : 'not-selected'
      tags.push(
        <li key={row.id}>
          <span className={className} onClick={(e) => this.onClickTag(e, row.id)}>{row.name}</span>
        </li>
      )
    }
    return (<ul className="row-my-interest-tags-con">{tags}</ul>)
  }

  render() {
    return (
      <div className="row-my-interest-category-container">
        <div className="row-my-interest-category">
          <span>{this.props.category}</span>
        </div>
        <div className="row-my-interest-tags">{this.renderTags()}</div>
      </div>
    )

  }
}
export default RowMyInterest
