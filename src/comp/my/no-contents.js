import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'

@inject('stateStore', 'apiStore', 'dataStore')
@observer
class NoContents extends Component {

  render() {
    return (
      <div className="no-contents-entity-container">
        <div className="no-contents-icon-con">
          <span className={this.props.iconClass}></span>
        </div>
        <div className="no-contents-text-con">
          <span className="no-contents-text">{this.props.text}</span>
        </div>
      </div>
    )

  }
}
export default NoContents
