import React, { Component, Fragment } from 'react'
import { inject, observer } from 'mobx-react'
import { Link } from 'react-router-dom'
@inject('stateStore', 'apiStore', 'dataStore')
@observer
class ItemReviews extends Component {
  renderStar(point) {
    let star = []
    let key = 0
    for(var i=0; i<point; i++) {
      star.push(
        <li className="item-review-star-con" key={key}><span className="star"></span></li>
      )
      key = key + 1
    }

    for (var i=0; i<5-point; i++) {
      star.push(
        <li className="item-review-not-star-con" key={key}><span className="not-star"></span></li>
      )
      key = key + 1
    }
    return (
      <ul className="item-review-star-container">{star}</ul>
    )
  }

  renderImg(imageList) {
    let images = []
    let key = 0
    let arrImg = JSON.parse(imageList)
    if(imageList.length > 0 && arrImg[0] != null) {
      for(var i=0; i<arrImg.length; i++) {
        let image = arrImg[i]
        let position = i
        images.push(
          <li className="item-review-images-item" key={position}>
            <img
              src={this.props.dataStore.reviewImgUrl + image === undefined ||
              this.props.dataStore.reviewImgUrl + image === null
                ? "" : this.props.dataStore.reviewImgUrl + image}
              onError={(e)=>e.target.setAttribute("src", this.props.dataStore.imageUrl + "06_detailpage_review_photo_error.png")}/>
          </li>
        )
        key = key + 1
      }
      return (
        <ul className="item-review-images-item-list">{images}</ul>
      )
    }

  }

  renderNoneReview() {
    const optionNumber = this.props.optionNumber == null ? 1 : this.props.optionNumber
    return (
      <div className="item-review-none-review-entity-con">
        <div className="item-review-none-review-name">
          <span>{this.props.title}</span>
        </div>
        <div className="item-review-none-review-date-join-option-con">
          <span className="item-review-none-review-date-join-option">
            {
              this.props.optionDate
              &&
              <Fragment>
                {this.props.apiStore.getFullTime(this.props.optionDate)}
                &nbsp;
                {this.props.dataStore.str.W0070}
              </Fragment>
            }
            ({this.props.optionName}({optionNumber}{this.props.dataStore.str.W0160}))
          </span>
        </div>
        <Link
          className="item-review-none-review-edit-link"
          to={
            "/my/ofriends/review/edit/"
            + 'new' + '-'
            + this.props.hostId + '-'
            + this.props.productId
            + '-' + this.props.optionId}>
          <div className="item-review-none-review-edit-con">
            <span className="item-review-none-review-edit-icon"></span>
            <span className="item-review-none-review-edit">{this.props.dataStore.str.W0071}</span>
          </div>
        </Link>
      </div>
    )
  }

  renderDoneReview() {
    const optionNumber = this.props.optionNumber == null ? 1 : this.props.optionNumber
    return(
      <Fragment>
        <div className="item-review-done-review-entity-con">
          <div className="item-review-done-review-name">
            <span className="item-review-done-review-title" style={{'WebkitBoxOrient': 'vertical'}}>{this.props.title}</span>
          </div>
          <div className="item-review-done-review-option">
            {
              this.props.optionDate
              &&
              <Fragment>
                {this.props.apiStore.getFullTime(this.props.optionDate)}
                &nbsp;
                {this.props.dataStore.str.W0070}
              </Fragment>
            }
            ({this.props.optionName}({optionNumber}{this.props.dataStore.str.W0160}))
          </div>
          <ul className="item-review-done-review-rate-con">
            <li className="item-review-done-review-rate">
              {this.renderStar(this.props.rate)}
            </li>
            <li className="item-review-done-review-date">
              <span>
                {this.props.apiStore.getFullTime(this.props.date)}
                &nbsp;
                {this.props.dataStore.str.W0073}
              </span>
            </li>
          </ul>
          <div className="item-review-done-review-text">
            <span dangerouslySetInnerHTML={{__html: this.props.text}}></span>
          </div>
          <div className="item-review-done-review-img">
            {this.renderImg(this.props.images)}
          </div>
        </div>
        <Link className="item-review-done-review-edit-link"
          to={"/my/ofriends/review/edit/"
            + this.props.reviewId
            + '-' + this.props.hostId
            + '-' + this.props.productId
            + '-' + this.props.optionId}>
          <div className="item-review-done-review-edit-con">
            <span className="item-review-done-review-edit-icon"></span>
            <span className="item-review-done-review-edit">{this.props.dataStore.str.W0072}</span>
          </div>
        </Link>
      </Fragment>
    )
  }

  renderReview() {
    if(this.props.isReview === true){
      return (<Fragment>{this.renderDoneReview()}</Fragment>)
    } else {
      return (<Fragment>{this.renderNoneReview()}</Fragment>)
    }
  }

  render() {
    return (
      <div className="item-review-entity-container">
        {this.renderReview()}
      </div>
    )

  }
}
export default ItemReviews
