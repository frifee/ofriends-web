import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
import StarRatingComponent from 'react-star-rating-component'
@inject('stateStore', 'apiStore', 'dataStore')
@observer
class EditModeReview extends Component {
  onClickSave = (e) => {
    const myReviewEditState = this.props.stateStore.myReviewEditState
    if(myReviewEditState.reviewId<=0) {
      this.postHostReview()
    }
    else {
      this.putHostReview()
    }
    //
  }

  onStarClick = (nextValue, prevValue, name) => {
    this.props.stateStore.setMyReviewEditState("rate", nextValue)
  }

  onClickRemoveImg = (e, position) => {
    let thumbnails = []
    let index = 0
    for(var i=0; i<this.props.stateStore.myReviewEditState.thumbnails.length; i++) {
      let row = this.props.stateStore.myReviewEditState.thumbnails[i]
      if(i != position) {
        thumbnails.push(row)
      }
    }
    this.props.stateStore.setMyReviewEditState("thumbnails", thumbnails)
  }

  onChangeReview = (e) => {
    this.props.stateStore.setMyReviewEditState("inputText", e.target.value)
    let formatText = e.target.value
    formatText = formatText.replace(/(?:\r\n|\r|\n)/g, '<br>')
                  .replace(/"/g, "&#34;")
                  .replace(/'/g, "&#39;")
    this.props.stateStore.setMyReviewEditState("inputFormatText", formatText)
  }

  onChangeImages = (e) => {
    // let thumbnails = []
    // for(var i=0; i<this.props.stateStore.myReviewEditState.thumbnails.length; i++) {
    //   let row = this.props.stateStore.myReviewEditState.thumbnails[i]
    //   thumbnails.push(row)
    // }
    // thumbnails.push(e.target.files[0])
    // this.props.stateStore.setMyReviewEditState("thumbnails", thumbnails)
    let files = e.target.files
    if(files !=null) {
      this.postImageUpload(files[0])
    }
  }

  postImageUpload(file) {
    let formData = new FormData()
    formData.append('photos', file)
    let token = this.props.apiStore.getJwtToken()
    this.props.apiStore.postImageUpload(token, formData)
      .then(res => {
        let thumbnails = []
        const myReviewEditState = this.props.stateStore.myReviewEditState
        for(let row of myReviewEditState.thumbnails) {
          thumbnails.push(row)
        }
        for(let row of res.data.data) {
          thumbnails.push(row.url)
        }
        setTimeout(()=> {
          this.props.stateStore.setMyReviewEditState('thumbnails', thumbnails)
        }, 1000)
      })
      .catch(err => {
      })
  }

  postHostReview() {
    let token = this.props.apiStore.getJwtToken()
    const myReviewEditState = this.props.stateStore.myReviewEditState
    let payload = {
      'product' : myReviewEditState.productId,
      'product_option' : myReviewEditState.optionId,
      'body' : myReviewEditState.inputFormatText,
      'rating' : myReviewEditState.rate,
      'image_files' : myReviewEditState.thumbnails
    }
    this.props.apiStore.postHostReview(token, myReviewEditState.id, payload)
      .then(res=>{
        this.props.stateStore.setMyReviewEditState("state", 0)
      })
      .catch(err=>{
      })
  }

  putHostReview() {
    let token = this.props.apiStore.getJwtToken()
    const myReviewEditState = this.props.stateStore.myReviewEditState
    let payload = {
      'product' : myReviewEditState.productId,
      'product_option' : myReviewEditState.optionId,
      'body' : myReviewEditState.inputFormatText,
      'rating' : myReviewEditState.rate,
      'image_files' : myReviewEditState.thumbnails
    }
    this.props.apiStore.putHostReview(token, myReviewEditState.id, myReviewEditState.reviewId,payload)
      .then(res=>{
        this.props.stateStore.setMyReviewEditState("state", 0)
      })
      .catch(err=>{
      })
  }

  renderImg() {
    const data = this.props.stateStore.myReviewEditState
    let images = []
    let key = 0
    if(data.thumbnails !== undefined) {
      for(let i=0; i<data.thumbnails.length; i++) {
        let row = data.thumbnails[i]
        let position = key
        images.push(
          <li className="edit-mode-review-image-con" key={position}>
            <div>
              <img
                alt = ''
                src={row === undefined || row == null ? "" : this.props.dataStore.reviewImgUrl + row}
                onError={(e)=>e.target.setAttribute("src", this.props.dataStore.imageUrl + "06_detailpage_review_photo_error.png")}/>
              <span className="edit-mode-review-image-remove" onClick={(e)=>{this.onClickRemoveImg(e, position)}}></span>
            </div>
          </li>
        )
        key = key + 1
      }
    }

    images.push(
      <li className="edit-mode-review-image-upload-con" key={key}>
        <div className="edit-mode-review-image-upload">
          <input
            className="edit-mode-review-image-upload-btn"
            type="file" onChange={(e)=>this.onChangeImages(e)}
            id="image-uploader"
            accept="image/jpeg, image/png"/>
          <label htmlFor="image-uploader"></label>
        </div>

      </li>
    )
    return (<ul className="edit-mode-review-images-container">{images}</ul>)
  }

  render() {
    const pageId = parseInt(this.props.stateStore.myReviewEditState.id)
    // const data = this.props.dataStore.sampleMyReviews.data[pageId]
    const myReviewEditState = this.props.stateStore.myReviewEditState
    return (
      <div className="edit-mode-review-entity-container">
        <div className="edit-mode-review-save-btn">
          <span onClick={(e)=>{this.onClickSave()}}>{this.props.dataStore.str.W0075}</span>
        </div>
        <div className="edit-mode-review-name-rate-content-container">
          <div className="edit-mode-review-infoName-con">
            <div className="edit-mode-review-infoName">
              <span>{myReviewEditState.productTitle}</span>
            </div>
          </div>
        <div className="edit-mode-review-rating-container">
          <ul className="edit-mode-review-rating-star-con">
            <li>
              <span className="edit-mode-review-rating-title">{this.props.dataStore.str.W0076}</span>
            </li>
            <li className="edit-mode-review-star-rating-component">
              <StarRatingComponent
                name="rate1"
                starCount={5}
                renderStarIcon={() => <span className="edit-mode-review-star"></span>}
                value={this.props.stateStore.myReviewEditState.rate}
                editing={true}
                onStarClick={this.onStarClick.bind(this)}>
              </StarRatingComponent>
            </li>
          </ul>
        </div>
          <div className="edit-mode-review-content-con">
            <div className="edit-mode-review-content-title">
              <span>{this.props.dataStore.str.W0077}</span>
            </div>
            <div className="edit-mode-review-content-area-con">
              <textarea
                className="edit-mode-review-content-area"
                onChange={(e)=>this.onChangeReview(e)}
                value={this.props.stateStore.myReviewEditState.inputText}/>
            </div>
          </div>
        </div>
        <div className="edit-mode-review-picture-con">
          <span className="edit-mode-review-picture">{this.props.dataStore.str.W0078}</span>
        </div>
        {this.renderImg()}
      </div>
    )

  }
}
export default EditModeReview
