import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
import { Link } from 'react-router-dom'

@inject('stateStore', 'apiStore', 'dataStore')
@observer
class MyOfriendsTab extends Component {

  componentDidMount() {
    this.props.stateStore.setMyOfriendsTab(this.props.tabPos)
  }

  renderTabs() {
    const tabPos = this.props.tabPos
    const myOfriendsTabs = this.props.dataStore.myOfriendsTab
    let tabs = []
    for(var i=0; i<myOfriendsTabs.length; i++) {
      let row = myOfriendsTabs[i]
      tabs.push(
        <li key={row.id} className={row.id === this.props.tabPos ? "tab-selected" : "tab-default"}>
          <Link to={row.url}>{row.title}</Link>
        </li>
      )
    }
    return (
      <ul className="my-ofriends-tabs">{tabs}</ul>
    )
  }

  render() {
    return (
      <div className="my-ofriends-tab-entity-container">
        <div className="my-ofriends-tab-con">{this.renderTabs()}</div>
      </div>
    )

  }
}
export default MyOfriendsTab
