import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'

@inject('stateStore', 'apiStore', 'dataStore')
@observer
class ItemCoupon extends Component {

  render() {

    const dueDate = this.props.apiStore.getTime(this.props.dueDate)
    const type = this.props.option.replace('_', '-')
    return (
      <div className={"item-coupon-entity-container " + type}>
        <div className="item-coupon-title-con">
          <span className="item-coupon-title">{this.props.title}</span>
        </div>
        <div className="item-coupon-content-con">
          <span className="item-coupon-content">
            {this.props.dataStore.str.W0058}
            &nbsp;
            {dueDate}
            {this.props.dataStore.str.W0159}
          </span>
        </div>
        <div className="item-coupon-expired-con">
          <span className="item-coupon-expired"></span>
        </div>
      </div>
    )

  }
}
export default ItemCoupon
