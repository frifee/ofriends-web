import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
import { Link } from 'react-router-dom'


@inject('stateStore', 'apiStore', 'dataStore')
@observer
class ProfileTab extends Component {

  componentDidMount() {
    this.props.stateStore.setMyProfileTab(this.props.tabPos)
  }

  renderTabs() {
    const tabPos = this.props.tabPos
    const myProfileTabs = this.props.dataStore.myProfileTabs
    let tabs = []
    for(var i=0; i<myProfileTabs.length; i++) {
      let row = myProfileTabs[i]
      tabs.push(
        <li key={row.id} className={row.id === this.props.tabPos ? "tab-selected" : "tab-default"}>
          <Link to={row.url}>{row.title}</Link>
        </li>
      )
    }
    return (
      <ul className="profile-tab-entity-con">{tabs}</ul>
    )
  }

  render() {
    return (
      <div className="profile-tab-entity-container">
        <div className="profile-tab-con">{this.renderTabs()}</div>
      </div>
    )

  }
}
export default ProfileTab
