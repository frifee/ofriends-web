import React, { Component, Fragment } from 'react'
import { inject, observer } from 'mobx-react'
import ItemRisingOfriends from '../main/item-rising-ofriends'
import Pagination from "react-js-pagination"
import NoContents from '../my/no-contents'
@inject('stateStore', 'apiStore', 'dataStore')
@observer
class LikeBody extends Component {

  componentDidMount() {
    this.props.stateStore.setLikeBodyState('from', 0)
    this.props.stateStore.setLikeBodyState('to', 0)
    this.props.stateStore.setLikeBodyState('count', 0)
    this.props.stateStore.setLikeBodyState('page', 1)
    this.props.stateStore.setLikeBodyState('done', false)
    this.getProductList(1)
  }

  getProductList(page) {
    const likeBodyState = this.props.stateStore.likeBodyState
    let fromIdx = (page - 1) * 8
    let toIdx = fromIdx + 8 - 1
    let range = '[' + fromIdx + ',' + toIdx + ']'
    let params = []
    params.push({
      key: 'range',
      value: range
    })
    params.push({
      key: 'filter',
      value: "{\"like\":\"" + 1 + "\"}"
    })
    this.props.apiStore.getProductList(localStorage.getItem('jwt'), params).then(res=>{
      let range = res.headers['content-range'].split(' ')[1]
      let fromIdx = range.split('/')[0].split('-')[0]
      let toIdx = range.split('/')[0].split('-')[1]
      let count = range.split('/')[1]
      this.props.stateStore.setLikeBodyState('from', parseInt(fromIdx))
      this.props.stateStore.setLikeBodyState('to', parseInt(toIdx))
      this.props.stateStore.setLikeBodyState('count', parseInt(count))
      this.props.stateStore.setLikeBodyState('page', parseInt(page))
      this.props.stateStore.setLikeBodyState('done', true)
      this.props.dataStore.setDataProductList([])
      this.props.dataStore.setDataProductList(res.data)
    })
    .catch(err=> {
    })
  }

  handlePageChange(pageNumber) {
    const likeBodyState = this.props.stateStore.likeBodyState
    if(likeBodyState.page != pageNumber) {
      this.getProductList(pageNumber)
    }

  }

  handleCallback() {
    const likeBodyState = this.props.stateStore.likeBodyState
    this.getProductList(likeBodyState.page)
  }

  renderItems() {

    const data = this.props.dataStore.dataProductList
    const likeBodyState = this.props.stateStore.likeBodyState
    if(data != null) {
      if(likeBodyState.done === false) {
        return (<Fragment></Fragment>)
      }
      if(data.length > 0) {
        let view = []
        for(let item of data) {
          let tags = []
          if(item.is_hot === 1) {
            tags.push(this.props.dataStore.str.W0125)
          }
          if(item.is_new === 1) {
            tags.push(this.props.dataStore.str.W0126)
          }
          if(item.is_only === 1) {
            tags.push(this.props.dataStore.str.W0127)
          }
          view.push(
            <ItemRisingOfriends
              id = {item.id}
              key = {item.id}
              loc = {item.loc}
              isLike = {item.like}
              subTitle = {item.catch_phrase}
              title = {item.title}
              price = {item.original_price}
              discount = {item.price}
              rating = {item.rating}
              tags = {tags}
              callback = {() => this.handleCallback()}
              ></ItemRisingOfriends>
          )
        }
        return (<div className="like-body-items-con">{view}</div>)
      } else {
        return (
          <NoContents
            iconClass="icon-none-available"
            text={this.props.dataStore.str.S1001}></NoContents>
        )

      }
    }
  }
  renderPagination() {
    const likeBodyState = this.props.stateStore.likeBodyState
    const data = this.props.dataStore.dataProductList
    let activePage = likeBodyState.page
    let totalItemsCount = likeBodyState.count
    let pageRangeDisplayed = (likeBodyState.count/5) >= 5 ? 5 : parseInt(likeBodyState.count/5 + 1)
    if(data == undefined || data.length===0) {
      return(
        <Fragment></Fragment>
      )
    } else {
      return (
        <Pagination
          activePage={activePage}
          itemsCountPerPage={8}
          totalItemsCount={totalItemsCount}
          pageRangeDisplayed={pageRangeDisplayed+1}
          onChange={this.handlePageChange.bind(this)}
          firstPageText={""}
          prevPageText={""}
          nextPageText={""}
          lastPageText={""}
          itemClassFirst="first-link"
          itemClassPrev="prev-link"
          itemClassNext="next-link"
          itemClassLast="last-link"
          linkClass="page-link"
          innerClass="pagination"
          itemClass="pg"
          activeClass="current"
        />
      )
    }
  }
  render() {
    return (
      <div className="like-body-entity-container">
        {this.renderItems()}
        {this.renderPagination()}
      </div>
    )

  }
}
export default LikeBody
