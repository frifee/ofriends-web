import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
import { Link } from 'react-router-dom'
@inject('stateStore', 'apiStore', 'dataStore')
@observer
class ItemMyOfriends extends Component {

  onClickCancelSched = (e) => {
    if(this.props.onClickCancelSched != undefined) {
      this.props.onClickCancelSched(e)
    }
  }

  renderInfo() {
    if(this.props.type === "history") {
      return (
        <div className="item-my-ofriends-history-quantity-detail-container">
          <div className="item-my-ofriends-history-quantity">
            <span>{this.props.dataStore.str.W0061}:&nbsp;{this.props.count}</span>
          </div>
          <div className="item-my-ofriends-history-detail">
            <Link
              to={"/my/ofriends/history/"+this.props.itemId}>
              {this.props.dataStore.str.W0062}
            </Link>
          </div>
        </div>
      )
    } else if(this.props.type === 'detail') {
      return (
        <div className="item-my-ofriends-detail-container">
          <div className="item-my-ofriends-detail-count">
            <span>{this.props.dataStore.str.W0061}:&nbsp;{this.props.count}</span>
          </div>
          {
            this.props.available>0 &&
            <div className="item-my-ofriends-detail-canceled-schedule">
              <span onClick={(e)=>{this.onClickCancelSched(e)}}>{this.props.dataStore.str.W0069}</span>
            </div>
          }
        </div>
      )
    }
  }

  renderState() {
    if(this.props.type === "history") {
      if(this.props.available>0) {
        return (
          <div className="available">
            <span>{this.props.dataStore.str.W0055}:&nbsp;{this.props.available}</span>
          </div>
        )
      } else if(this.props.cancel>0) {
        return (
          <div className="canceled">
            <span>{this.props.dataStore.str.W0063}:&nbsp;{this.props.cancel}</span>
          </div>
        )
      }
    } else if(this.props.type === 'detail') {
      switch(this.props.status) {
        case 'cancelled' :
          return (
            <div className="canceled">
              <span>{this.props.dataStore.str.W0063}</span>
            </div>
          )
          break
        case 'confirmed' :
          return (
            <div className="available">
              <span>{this.props.dataStore.str.W0055}</span>
            </div>
          )
          break
        case 'finished':
        case 'reviewed':
          return (
            <div className="canceled">
              <span>{this.props.dataStore.str.W0172}</span>
            </div>
          )
          break
      }
    }
  }
  /*
  --props--
  imgUrl
  title
  location
  materials
  dueDate
  date
  option
  count
  available
  cancel
  */
  render() {
    const imgUrl = this.props.dataStore.productsImgUrl + this.props.imgUrl + "_0.jpg"
    return (
      <div className="item-my-ofriends-entity-container">
        <div className="item-my-ofriends-title-container">
          <img
            src={imgUrl === undefined || imgUrl === null ? "" : imgUrl}
            onError={(e)=>e.target.setAttribute("src", this.props.dataStore.imageUrl + "06_detailpage_review_photo_error.png")}/>
          <div className="item-my-ofriends-title-loc-mat-duedate-container">
            <div className="item-my-ofriends-title">
              <span style={{'WebkitBoxOrient': 'vertical'}}>{this.props.title}</span>
            </div>
            <ul className="item-my-ofriends-loc-mat-container">
              <li className="item-my-ofriends-loc-icon">
                <span></span>
              </li>
              <li className="item-my-ofriends-loc">
                <span>{this.props.location}</span>
              </li>
            </ul>
            <ul className="item-my-ofriends-mat-icon-container">
              <li className="item-my-ofriends-mat-icon">
                <span></span>
              </li>
              <li className="item-my-ofriends-mat">
                <span>{this.props.materials}</span>
              </li>
            </ul>
            <ul className="item-my-ofriends-duedate-container">
              <li className="item-my-ofriends-duedate-icon">
                <span></span>
              </li>
              <li className="item-my-ofriends-duedate">
                {
                  this.props.dueDate != null && this.props.dueDate != undefined
                  &&
                  <span>
                    {this.props.dataStore.str.W0058}
                    &nbsp;
                    {this.props.apiStore.getFullTime(this.props.dueDate)}</span>

                }

              </li>
            </ul>
          </div>
          <div className="item-my-ofriends-date">
            <span>{this.props.apiStore.getTime(this.props.date)}&nbsp;{this.props.dataStore.str.W0060}</span>
          </div>
          {this.renderInfo()}
        </div>
        <div className="item-my-ofriends-option-container">
          <ul className="item-my-ofriends-option-menu-con">
            <li className="item-my-ofriends-option-title">
              <span>{this.props.dataStore.str.W0059}</span>
            </li>
            <li className="item-my-ofriends-option">
              <span>{this.props.option}</span>
            </li>
            <li className="item-my-ofriends-status-con">
              <span
                className={this.props.status === 'cancelled' ? "item-my-ofriends-status cancelled" : "item-my-ofriends-status available"}
              >{this.props.status === 'cancelled' ? this.props.dataStore.str.W0063 : this.props.dataStore.str.W0055}</span>
            </li>
          </ul>
          {this.renderState()}
        </div>
      </div>
    )

  }
}
export default ItemMyOfriends
