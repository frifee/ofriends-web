import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
import { Link, withRouter } from 'react-router-dom'
import Info from "../product/info";
@inject('stateStore', 'apiStore', 'dataStore')
@withRouter
@observer
class Footer extends Component {

  constructor(props) {
    super(props)
    this.state = {
      'isMobileFooterOpen' : false
    }
  }

  onClickMobileFooter(e) {
    this.setState({isMobileFooterOpen : !this.state.isMobileFooterOpen})
  }

  render() {
    const nowPath = this.props.location.pathname.split('/')[1]
    return (
      <div className="footer">
        <div className="footer-container-d">
          <div className="footer-top-container">
            <div className="footer-top-logo-container">
              {/*링크영역*/}
              <div>
                <span className="footer-logo" />
              </div>
              <a href="#" target="_blank">
                <span className="footer-facebook" />
              </a>
              <a href="#" target="_blank">
                <span className="footer-instagram" id="sns-icon"/>
              </a>
              <a href="#" target="_blank">
                <span className="footer-twitter" id="sns-icon"/>
              </a>
              <a href="#" target="_blank">
                <span className="footer-linkedin" id="sns-icon"/>
              </a>
            </div>
            <div className="footer-top-desc-container">
              {/*정보영역*/}
              <div className="footer-desc-loc-info">
                <div className="footer-friendtrip">{this.props.dataStore.str.W1012}</div>
                <div>대표: 임수열</div>
                <div>개인정보 관리 책임자: 양대현</div>
                <div>사업자 등록번호: 261-81-04385</div>
                <div>통신판매업신고번호: 2016-서울성동-01088</div>
              </div>
              <div className="footer-contract-info">
                <div className="footer-empty-space"> </div>
                <div>이메일:<a className="footer-mail" href="mailto:cs@ofriends.co.kr"> cs@ofriends.co.kr</a></div>
                <div>대표전화: 070-5175-6637</div>
                <div>주소: 서울특별시 성동구 왕십리로 115 헤이그라운드 서울숲점 G601</div>
              </div>
            </div>
          </div>
          <div className="footer-bottom-container">
            {/*하단영역*/}
            <div className="footer-terms-container">
              <Link className="footer-terms" to="/terms">{this.props.dataStore.str.W0014}</Link>
              <Link className="footer-personal-information" to="/privacy">{this.props.dataStore.str.W0131}</Link>
              {/*일단 제외
              <Link className="footer-marketing" to="/">마케팅 동의신청</Link>
              */}
              <span className="footer-reserved">{this.props.dataStore.str.S1010}</span>
            </div>
          </div>
        </div>
        <div className="footer-mobile-desc-popup-con" onClick={(e)=> {this.onClickMobileFooter(e)}}>
          <span className="footer-mobile-desc-title">{this.props.dataStore.str.W1012}</span>
          <span className={this.state.isMobileFooterOpen ?
              "footer-mobile-desc-arrow-open" : "footer-mobile-desc-arrow-close"}></span>
        </div>
        <div
          className="footer-container-m"
          style={{'paddingBottom' : nowPath === 'product' || nowPath === 'event' ? '60px' : '0'}}>
          <div className="footer-top-container" style={{'display':this.state.isMobileFooterOpen ? 'flex' : 'none'}}>
            <div className="footer-top-logo-container">
              {/*링크영역*/}
              <div>
                <span className="footer-logo" />
              </div>
              <a href="#" target="_blank">
                <span className="footer-facebook" />
              </a>
              <a href="#" target="_blank">
                <span className="footer-instagram" id="sns-icon"/>
              </a>
              <a href="#" target="_blank">
                <span className="footer-twitter" id="sns-icon"/>
              </a>
              <a href="#" target="_blank">
                <span className="footer-linkedin" id="sns-icon"/>
              </a>
            </div>
            <div className="footer-top-desc-container">
              {/*정보영역*/}
              <div className="footer-desc-loc-info">
                <div>대표: 임수열</div>
                <div>개인정보 관리 책임자: 양대현</div>
                <div>사업자 등록번호: 261-81-04385</div>
                <div>통신판매업신고번호: 2016-서울성동-01088</div>
              </div>
              <div className="footer-contract-info">
                <div>이메일: <a className="footer-mail" href="mailto:cs@ofriends.co.kr">cs@ofriends.co.kr</a></div>
                <div>대표전화: 070-5175-6637</div>
                <div>주소: 서울특별시 성동구 왕십리로 115 헤이그라운드 서울숲점 G601</div>
              </div>
            </div>
          </div>
          <div className="footer-bottom-container">
            {/*하단영역*/}
            <div className="footer-terms-container">
              <Link className="footer-terms" to="/terms">{this.props.dataStore.str.W0014}</Link>
              <Link className="footer-personal-information" to="/privacy">{this.props.dataStore.str.W0131}</Link>
              {/*일단 제외
              <Link className="footer-marketing" to="/">마케팅 동의신청</Link>
              */}
              <span className="footer-reserved">{this.props.dataStore.str.S1010}</span>
            </div>
          </div>
        </div>



      </div>
    )
  }
}
export default Footer
