import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
import {withRouter, Link} from 'react-router-dom'

@inject('stateStore', 'apiStore', 'dataStore')
@withRouter
@observer
class MHeader extends Component {

  constructor(props) {
    super(props)
    this.state = {
      searchActivity: ''
    }
  }


  onClickMenu = (e) => {
    this.props.stateStore.setMMenuOpen(!this.props.stateStore.mMenuOpen)
  }

  onClickSearch = (e) => {
    const headerState = this.props.stateStore.headerState
    this.props.stateStore.setHeaderState('isOnSearch', !headerState.isOnSearch)
  }

  onChangeSearch = (e) => {
    this.props.stateStore.setHeaderState('inputSearch', e.target.value)
  }

  onKeyPressSearch = (e) => {
    if (e.key === 'Enter') {
      this.props.history.push('/search/' + this.props.stateStore.headerState.inputSearch)
      this.props.stateStore.setHeaderState('isOnSearch', false)
    }
  }

  onClickRemoveSearch= (e) => {
    this.props.stateStore.setHeaderState('inputSearch', '')
  }

  renderSearch() {

    const headerState = this.props.stateStore.headerState
    if(headerState.isOnSearch === true) {
      return (
        <div className="m-header-search-container">
          <div className="m-header-search-title-backbtn-con">
            <span className="m-header-search-backbtn" onClick={(e)=>this.onClickSearch()}></span>
            <span className="m-header-search-title">{this.props.dataStore.str.W0128}</span>
          </div>
          <div className="m-header-search-desc-input-icon-con">
            <span className="m-header-search-desc" dangerouslySetInnerHTML={{__html: this.props.dataStore.str.S0028}}></span>
            <span className="m-header-search-icon"></span>
            <input
              className="input-search"
              type="text"
              onChange={(e) => this.onChangeSearch(e)}
              value={headerState.inputSearch}
              onKeyPress={(e) => this.onKeyPressSearch(e)}
            />
            {
              (headerState.inputSearch != undefined && headerState.inputSearch.length>0)
              &&
              <span
                className="header-search-remove-btn"
                onClick={() => this.onClickRemoveSearch()}></span>
            }
          </div>
        </div>
      )
    }
  }

  render() {
    return (
      <div className="mobile">
        <div className="m-header-entity-container">
          <div className="m-header-menu-wrapper" onClick={(e)=>this.onClickMenu(e)}>
            <span className="m-header-menu"></span>
          </div>
          <Link className="m-header-logo" to="/"></Link>
          <div className="m-header-search-wrapper" onClick={(e)=>this.onClickSearch(e)}>
            <span className="m-header-search"></span>
          </div>
        </div>
        { this.renderSearch() }
      </div>
    )

  }
}
export default MHeader
