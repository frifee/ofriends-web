import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
import { Link } from 'react-router-dom'
import {withRouter} from 'react-router-dom';

@inject('stateStore', 'apiStore', 'dataStore')
@withRouter
@observer
class DHeader extends Component {

  // 고객 센터 눌렀을 때 뜨는 창
  renderHelpMenu() {
    if(this.props.stateStore.isHelpMenuOpen === true) {
      return (
        <ul className="header-right-menus-help-dropdown">
          <li><Link className="header-right-menus-notice" to="/notice">{this.props.dataStore.str.W0004}</Link></li>
          <li><Link className="header-right-menus-faq" to="/help">{this.props.dataStore.str.W0005}</Link></li>
          <li><Link className="header-right-menus-terms" to="/terms">{this.props.dataStore.str.W0014}</Link></li>
        </ul>
      )
    }

  }

  // 내 아이디 눌렀을 때 뜨는 창
  renderProfileMenu() {
    if(this.props.stateStore.isProfileMenuOpen === true) {
      return (
        <ul className="header-right-menus-profile-dropdown">
          <li className="d-header-right-menus-profile"><Link className="d-header-profile-btn" to="/my/profile">{this.props.dataStore.str.W0008}</Link></li>
          <li
            onClick = {(e) => this.onClickLogout(e)}
            className="d-header-right-menus-logout">{this.props.dataStore.str.W0007}</li>
        </ul>
      )
    }
  }

  //login 상태에 따라 menu 렌더링
  renderRightHeader() {
    if(this.props.stateStore.loginState <= 0) {
      return (
        <ul className="header-right-menus">
          <li className="header-register-con"><Link className="header-register-btn" to="/register">{this.props.dataStore.str.W0001}</Link></li>
          <span className="header-vertical-line" />
          <li className="header-login-con"><Link className="header-login-btn" to="/login">{this.props.dataStore.str.W0002}</Link></li>
          <span className="header-vertical-line" />
          <li className="header-help-menu-default-con" onClick={(e) => this.onClickHelpMenu(e)}>{this.props.dataStore.str.W0003}
            <span className= {this.props.stateStore.isHelpMenuOpen === true ? "center-arrow-open" : "center-arrow-close"}></span>
            {this.renderHelpMenu()}
          </li>
        </ul>
      )
    } else {
      const dataUserInfo = this.props.dataStore.dataUserInfo
      let nickName =
          (typeof dataUserInfo.nickname === "undefined"
              || dataUserInfo.nickname === null
              || dataUserInfo.nickname === "")
              ? this.props.dataStore.str.W0051
              : this.props.dataStore.dataUserInfo.nickname
      return (
        <ul className="header-right-menus">
          <li className="header-user-info-con" onClick={(e) => this.onClickUserInfo(e)}>
              {nickName}
              &nbsp;
              {this.props.dataStore.str.W0154}
            {this.renderProfileMenu()}
          </li>
          <span className="header-vertical-line" />
          <li className="header-help-menu-login-con" onClick={(e) => this.onClickHelpMenu(e)}>{this.props.dataStore.str.W0003}
            <span className= {this.props.stateStore.isHelpMenuOpen === true ? "center-arrow-open" : "center-arrow-close"}></span>
            {this.renderHelpMenu()}
          </li>
        </ul>
      )
    }
  }

  onClickHelpMenu = (e) => {
    e.stopPropagation(e)
    this.props.stateStore.setIsHelpMenuOpen(!this.props.stateStore.isHelpMenuOpen)
  }


  onClickUserInfo = (e) => {
    e.stopPropagation(e)
    this.props.stateStore.setIsProfileMenuOpen(!this.props.stateStore.isProfileMenuOpen)
  }

  onFocusSearch = (e) => {
    this.props.stateStore.setHeaderState('isOnSearch', true)
  }

  onBlurSearch = (e) => {
  }

  onClickLogout = (e) => {
    e.stopPropagation()
    this.props.stateStore.setLoginState(0)
    const userPf = this.props.dataStore.dataUserInfo.pf
    try {
      if (userPf === 'kakao') {
        if (window.Kakao.isInitialized() === true) {
          window.Kakao.cleanup()
        }
        window.Kakao.init(process.env.REACT_APP_KAKAO_APP_ID)
        window.Kakao.Auth.logout(() => {
        })
      } else if (userPf === 'facebook') {
        window.FB.logout((response) => {
        })
      }
    } catch (e) {}
    localStorage.removeItem('exp')
    localStorage.removeItem('jwt')
    this.props.history.push('/')
  }

  onChangeSearch = (e) => {
    this.props.stateStore.setHeaderState('inputSearch', e.target.value)
  }

  onKeyPressSearch = (e) => {
    if (e.key === 'Enter') {
      this.props.history.push('/search/' + this.props.stateStore.headerState.inputSearch)
      this.props.stateStore.setHeaderState('isOnSearch', false)
    }
  }

  onClickRemoveSearch = (e) => {
    e.stopPropagation()
    this.props.stateStore.setHeaderState('inputSearch', '')
  }

  render() {
    const headerState = this.props.stateStore.headerState
    let headerSearchState = (headerState.inputSearch != undefined && headerState.inputSearch.length>0);
    return (
      <div className="desktop">
        <Link className="logo-box" to="">
          <span className="logo" />
        </Link>
        <div
          className="search-box"
          id={ headerSearchState ? 'active' : 'inactive'}>
          <span className="ico-search" />
          <input
            className="input-search"
            type="text"
            placeholder = {this.props.dataStore.str.S0002}
            onFocus = {(e) => this.onFocusSearch(e)}
            onBlur = {(e) => this.onBlurSearch(e)}
            onChange={(e) => this.onChangeSearch(e)}
            onKeyPress={(e) => this.onKeyPressSearch(e)}
            value={this.props.stateStore.headerState.inputSearch}/>
          {
            headerSearchState
            &&
            <span
                className="header-search-remove-btn"
                onClick={(e) => this.onClickRemoveSearch(e)}></span>
          }
        </div>
        {
          this.renderRightHeader()
        }
      </div>
    )

  }
}
export default DHeader
