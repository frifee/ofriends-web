import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
import { Link, withRouter } from 'react-router-dom'
import { Carousel } from 'react-responsive-carousel'
const listMenu = ['일상', 'special', '매거진', '미정']

@inject('stateStore', 'apiStore', 'dataStore')
@withRouter
@observer
class MHeaderMenu extends Component {

  constructor(props) {
    super(props)
    this.state = {
      mounted : false
    }
  }
  componentDidMount() {
    this.mounted = true
    this.setState({
      mounted : true
    })
  }

  componentWillUnmount() {
    this.mounted = false
    this.setState({
      mounted: false
    })
  }

  onClickMenu = (e) => {
  }

  onClickSearch = (e) => {
  }

  onClickLogout = (e) => {
    const userPf = this.props.dataStore.dataUserInfo.pf
    try {
      if (userPf === 'kakao') {
        if (window.Kakao.isInitialized() === true) {
          window.Kakao.cleanup()
        }
        window.Kakao.init(process.env.REACT_APP_KAKAO_APP_ID)
        window.Kakao.Auth.logout(() => {
        })
      } else if (userPf === 'facebook') {
        window.FB.logout((response) => {
        })
      }
    } catch (e) {}
    localStorage.removeItem('exp')
    localStorage.removeItem('jwt')
    this.props.stateStore.setLoginState(0)
    this.props.stateStore.setMMenuOpen(false)
    this.props.history.push('/')
  }

  onClickMyProfile = (e) => {
    this.props.history.push('/my/profile')
  }


  onClickClose = (e) => {
    this.props.stateStore.setMMenuOpen(false)
  }

  onClickBody = (e) => {
    e.stopPropagation()
  }

  onClickGallery = (e, position) => {
    const dataMain = this.props.dataStore.dataMain
    if(dataMain.banners != undefined) {
      // this.props.history.push(dataMain.banners[position].link)
      window.open(dataMain.banners[position].link)
    }
  }
  onChangeImages = (e) => {
    // let thumbnails = []
    // for(var i=0; i<this.props.stateStore.myReviewEditState.thumbnails.length; i++) {
    //   let row = this.props.stateStore.myReviewEditState.thumbnails[i]
    //   thumbnails.push(row)
    // }
    // thumbnails.push(e.target.files[0])
    // this.props.stateStore.setMyReviewEditState("thumbnails", thumbnails)
    let files = e.target.files
    if(files !=null) {
      this.postImageUpload(files[0])
    }
  }

  postImageUpload(file) {
    let formData = new FormData()
    formData.append('photos', file)
    let token = this.props.apiStore.getJwtToken()
    this.props.apiStore.postImageUploadUsers(token, formData)
      .then(res => {
        this.putUserSetting(res.data.data[0].url)

      })
      .catch(err => {
      })
  }

  putUserSetting(url) {
    let jwtToken = this.props.apiStore.getJwtToken()
    if(jwtToken === null) {
      this.props.history.push('/login')
      return
    }
    let payload = {
      'image_url' : url
    }
    this.props.apiStore.putUserSetting(localStorage.getItem('jwt'), payload)
      .then(res => {
        this.getUserInfo()
        this.props.stateStore.setPopupOn('uploadFinish')
      })
      .catch(err => {
      })
  }
  getUserInfo() {
    this.props.apiStore.getUserInfo(localStorage.getItem('jwt'))
      .then(res=>{
        this.props.dataStore.setDataUserInfo(res.data)
      })
      .catch(err=> {
      })
  }
  renderUserInfo = () => {
    if(this.props.stateStore.loginState <= 0) {
      return (
        <div className="sidenav-userinfo">
          <Link to="/login">
            <img
              className="side-nav-default-user-image"
              src=""
              onError={(e)=>e.target.setAttribute("src", this.props.dataStore.imageUrl + "07_profile_user.png")}/>
          </Link>
          <div className="side-nav-default-container">
            <Link className="side-nav-default-login" to="/login">{this.props.dataStore.str.S1047}</Link>
          </div>
          <ul className="side-nav-default-login-or-register">
            <li className="side-nav-default-or-con"><span>{this.props.dataStore.str.W0006}</span></li>
            <li className="side-nav-default-register-con"><Link to="/register">&nbsp;{this.props.dataStore.str.W0001}</Link></li>
          </ul>
        </div>
      )
    } else {
      const dataUserInfo = this.props.dataStore.dataUserInfo
      let nickName =
        (typeof dataUserInfo.nickname === "undefined"
          || dataUserInfo.nickname === null
          || dataUserInfo.nickname === "")
          ? this.props.dataStore.str.W0051
          : dataUserInfo.nickname
      return (
        <div className="sidenav-userinfo">
          <img
            className="side-nav-login-user-image"
            alt=""
            src={
              (dataUserInfo.image_url != undefined && dataUserInfo.image_url.length > 0)
              ? this.props.dataStore.userImgUrl + dataUserInfo.image_url : this.props.dataStore.imageUrl + "07_profile_user.png"
            }
            onError={(e)=>e.target.setAttribute("src", this.props.dataStore.imageUrl + "07_profile_user.png")}/>
          <div className="user-img-upload">
            <input
              className="user-img-upload-input"
              type="file" onChange={(e)=>this.onChangeImages(e)}
              id="user-img-uploader"
              accept="image/jpeg, image/png"/>
            <label htmlFor="user-img-uploader">
              <div className="user-img-click"></div>
            </label>
          </div>

          <div className="side-nav-login-container">
            <span className="side-nav-login-user-name">
              {nickName}
              &nbsp;
              {this.props.dataStore.str.W0154}
            </span>
          </div>
          <div className="side-nav-login-logout-clickPf-con">
            <button className="side-nav-logout-button" onClick={(e)=> this.onClickLogout(e)}>{this.props.dataStore.str.W0007}</button>
            <button className="side-nav-click-my-profile-button" onClick={(e)=> this.onClickMyProfile(e)}>{this.props.dataStore.str.W0008}</button>
          </div>
        </div>

      )
    }
  }

  renderMenu() {
    const imgUrl = this.props.dataStore.imageUrl;
    let menus = []
    const mainCate = this.props.dataStore.categories.mainCate
    if(mainCate != undefined) {
      for(var i=0; i<mainCate.length; i++) {
        let row = mainCate[i]
        menus.push(
          <li className="side-nav-menu-con" key={row.id}>
            <div className="side-nav-wrapper">
            <Link to={row.link}>
              <img
                id={"side-nav-menu-"+row.id}
                className="side-nav-menu-image"
                src={imgUrl + 'm_menu_category_0' + row.id + '.png'}
                onError={(e)=>e.target.setAttribute("src", this.props.dataStore.imageUrl + "m_menu_category.png")}/>
              <span className="side-nav-menu-desc">{row.name}</span>
            </Link>
            </div>
          </li>
        )
      }
      return (<ul className="side-nav-menus-container">{menus}</ul>)
    }

  }

  renderCarousel() {
    const dataMain = this.props.dataStore.dataMain
    if(this.state.mounted === true) {
      let gallery = []
      if(dataMain.banners != undefined) {
        for(var i=0; i<dataMain.banners.length; i++) {
          let position = i
          let row = dataMain.banners[i]
          gallery.push(
            <div key={row.id} onClick={(e) => {this.onClickGallery(e, position)}}>
              <img
                className="m-header-menu-carousel-image"
                src={row.image_desktop === undefined || row.image_desktop === null? "" : row.image_desktop}
                onError={(e)=>e.target.setAttribute("src", this.props.dataStore.imageUrl + "06_detailpage_no_image.png")}/>
            </div>
          )
        }

        return (
          <Carousel
            className="recom-carousel-container"
            autoPlay
            showThumbs={false}
            swipeable={true}
            emulateTouch
            showStatus={false}
            infiniteLoop={true}>
            {gallery}
          </Carousel>
        )
      }

    }
  }

  render() {
    if(this.props.stateStore.mMenuOpen === true) {
      window.document.body.style.position = 'fixed'
      // window.document.body.style.overflow = 'hidden'
    } else {
      window.document.body.style.position = 'static'
      window.document.body.style.overflow = 'auto'
    }
    return (
      <div className={this.props.stateStore.mMenuOpen? "mobile sidenav sidenav-open" : "mobile sidenav sidenav-close"}
           onClick={(e)=>this.onClickClose(e)}>
        <div className="sidenav-body"
             onClick={(e)=>this.onClickBody(e)}>
          {this.renderUserInfo()}
          {this.renderMenu()}
          {this.renderCarousel()}
          <div className="sidenav-help-notice-faq-email-container">
            {/*
              <div className="side-nav-help-con">
                <span>{this.props.dataStore.str.W0003}</span>
              </div>
            */}
            {/*
              <div className="side-nav-img"></div>

            */}
            <a href="https://static.ofriends.co.kr/images/details/ofriends_introduce/index.html" target="_blank" className="side-nav-intro">
              <div className="sidenav-intro-con">
                <span className="sidenav-intro-icon"></span>
                <span className="side-nav-intro-title">{this.props.dataStore.str.W1039}</span>
                <span className="sidenav-intro-arrow"></span>
              </div>
            </a>
            <a href="https://static.ofriends.co.kr/images/details/ofriends_benefit/index.html" target="_blank" className="side-nav-intro">
              <div className="sidenav-intro-con">
                <span className="sidenav-benefit-icon"></span>
                <span className="side-nav-intro-title">{this.props.dataStore.str.W1048}</span>
                <span className="sidenav-intro-arrow"></span>
              </div>
            </a>
            <Link to="/events" className="side-nav-event">
              <div className="sidenav-event-con">
                <span className="sidenav-event-icon"></span>
                <span className="side-nav-event-title">{this.props.dataStore.str.W1049}</span>
                <span className="sidenav-event-arrow"></span>
              </div>
            </Link>
            <Link to="/notice" className="sidenav-notice-link">
              <div className="side-nav-notice-con">
                <span className="sidenav-notice-icon"></span>
                <span className="sidenav-notice-title">{this.props.dataStore.str.W0004}</span>
                <span className="sidenav-notice-arrow"></span>
              </div>
            </Link>
            <Link to="/help" className="sidenav-faq-link">
              <div className="side-nav-faq-con">
                <span className="sidenav-faq-icon"></span>
                <span className="sidenav-faq-title">{this.props.dataStore.str.W0005}</span>
                <span className="sidenav-faq-arrow"></span>
              </div>
            </Link>
            <a className="sidenav-email-help" href="mailto:cs@ofriends.co.kr">
              <div className="sidenav-email-con">
                <span className="sidenav-email-icon"></span>
                <span className="side-nav-email-title">{this.props.dataStore.str.W0124}</span>
                <span className="sidenav-email-arrow"></span>
              </div>
            </a>

          </div>
        </div>
      </div>
    )

  }
}
export default MHeaderMenu
