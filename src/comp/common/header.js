import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'

import DHeader from './header/d-header'
import MHeader from './header/m-header'
import MHeaderMenu from './header/m-header-menu'
import SearchPopTag from './header/search-pop-tag'

@inject('stateStore', 'apiStore', 'dataStore')
@observer
class Header extends Component {

  componentDidMount() {
    this.onRouteChanged()
  }

  componentDidUpdate(prevProps) {
    if (this.props.location !== prevProps.location) {
      this.onRouteChanged()
    }
  }

  onRouteChanged() {
    this.props.stateStore.setHeaderState('isOnSearch', false)
  }

  render() {
    return (
      <div className="header">
        <DHeader></DHeader>
        <MHeader></MHeader>
        <MHeaderMenu></MHeaderMenu>
        <SearchPopTag></SearchPopTag>
      </div>
    )

  }
}
export default Header
