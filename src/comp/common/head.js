import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
import { Helmet } from "react-helmet"



@inject('stateStore', 'apiStore', 'dataStore')
@observer
class Head extends Component {

  render() {
    const imgUrl = this.props.dataStore.imageUrl
    return (
      <div>
        <Helmet>
          <link
            rel="stylesheet"
            href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
            integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
            crossorigin="anonymous"/>
          <link rel="stylesheet" href="https://fonts.googleapis.com/earlyaccess/notosanskr.css"></link>
          <link rel="icon" type="image/png" sizes="192x192"  href={imgUrl + "android-icon-192x192.png"}></link>
          <link rel="icon" type="image/png" sizes="32x32" href={imgUrl + "favicon-32x32.png"}></link>
          <link rel="icon" type="image/png" sizes="96x96" href={imgUrl + "favicon-96x96.png"}></link>
          <link rel="icon" type="image/png" sizes="16x16" href={imgUrl + "favicon-16x16.png"}></link>
          <link rel="apple-touch-icon-precomposed" href={imgUrl + "apple-icon-152x152.png"}></link>
          <script src="//developers.kakao.com/sdk/js/kakao.min.js"></script>
          <script async defer crossOrigin="anonymous"
                  src="https://connect.facebook.net/ko_KR/sdk.js#xfbml=1&version=v5.0&appId=1705550742909762&autoLogAppEvents=1"></script>
          <script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.min.js" ></script>
          <script type="text/javascript" src="https://cdn.iamport.kr/js/iamport.payment-1.1.4.js"></script>
        </Helmet>
      </div>
    )

  }
}
export default Head
