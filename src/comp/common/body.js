import React, { Component, Fragment } from 'react'
import { inject, observer } from 'mobx-react'

import {Switch, Route, withRouter, BrowserRouter, Link, Redirect} from 'react-router-dom';
import Main from '../route/main'
import DailyActivity from '../route/daily-activity'
import Product from '../route/product'
import MyCoupon from '../route/profile/my-coupon'
import MyLike from '../route/profile/my-like'
import MyProfile from '../route/profile/my-profile'
import MyInterest from '../route/profile/my-interest'
import MyOfriendsAvailable from '../route/profile/my-ofriends-available'
import MyOfriendsHistory from '../route/profile/my-ofriends-history'
import MyOfriendsReview from '../route/profile/my-ofriends-review'
import MyOfriendsHistoryDetail from '../route/profile/my-ofriends-history-detail'
import MyOfriendsReviewEdit from '../route/profile/my-ofriends-review-edit'
import Login from '../route/user/login'
import Register from '../route/user/register'
import LoginEmail from '../route/user/login-email'
import PwReset from '../route/user/pw-reset'
import RegisterEmail from '../route/user/register-email'
import Help from '../route/customer/help'
import Notice from '../route/customer/notice'
import Terms from '../route/customer/terms'
import Privacy from '../route/customer/privacy'
import Recommend from '../route/recommend'
import Exhibit from '../route/exhibit'
import ReviewHost from '../route/review-host'
import Payment from '../route/payment'
import Search from '../route/search'
import Err from '../route/err'
import Magazine from '../route/magazine'
import MagazineList from '../route/magazine-list'
import Events from '../route/events'
import TwoBtn from '../dialog/two-btn'
import OneBtn from '../dialog/one-btn'
import RegActive from '../dialog/reg-active'
import jwt from 'jwt-decode'
import HelpCenter from '../help/help-center'
import FinishPaymentMobile from "../route/finish-payment-mobile";
import PhoneVerify from "../dialog/phone-verify";
@inject('stateStore', 'apiStore', 'dataStore')
@withRouter
@observer
class Body extends Component {
  componentDidMount() {
    this.onRouteChanged()
    this.preventMobileScrolling()
  }
  componentDidUpdate(prevProps) {
    if (this.props.location !== prevProps.location) {
      this.onRouteChanged()
    }
  }
  onRouteChanged() {
    window.scrollTo(0, 0)
    this.props.stateStore.setDefaultOpen()
    this.props.stateStore.setPopupOn("")
    this.props.stateStore.setMMenuOpen(false)
    this.props.stateStore.setRegActiveState('isOn', false)
    this.props.stateStore.setProductOption('mobileSelOpt', false)
    this.props.stateStore.setRegActiveState('regEndMsg', "")
    this.props.stateStore.setIsLoading(true)

    let exp = localStorage.getItem('exp')
    let jwtToken = localStorage.getItem('jwt')

    if(exp == null || jwtToken == null) {//login안함
      this.props.stateStore.setLoginState(0)
      this.props.stateStore.setIsLoading(false)
    } else if(parseInt(exp) < Math.floor(Date.now() / 1000)) {
      this.props.stateStore.setLoginState(-1)
      this.props.stateStore.setIsLoading(false)
      localStorage.removeItem('jwt')
    } else {
      this.props.stateStore.setLoginState(1)
      this.props.dataStore.setDataAuthenticate(jwt(jwtToken))
      this.props.dataStore.setDataJwt(jwtToken)
      if(this.props.dataStore.dataUserInfo.username === undefined ||
        this.props.dataStore.dataUserInfo.level === undefined) {
        this.getUserInfo()
        return
      }
      this.props.stateStore.setIsLoading(false)
    }
  }

  getUserInfo() {
    this.props.apiStore.getUserInfo(localStorage.getItem('jwt')).then(res=>{
      this.props.dataStore.setDataUserInfo(res.data)
      this.setInitState()
      this.props.stateStore.setIsLoading(false)
    })
      .catch(err=> {
      })
  }

  setInitState() {
    const dataUserInfo = this.props.dataStore.dataUserInfo
    let nickName = this.props.dataStore.dataUserInfo.nickname
    if(nickName === undefined || nickName === null)
      nickName = ''

    let inputRecomCode = this.props.dataStore.dataUserInfo.recommender
    let recomCode = 2
    if(inputRecomCode === undefined || inputRecomCode === null || inputRecomCode === "")
      recomCode = 0

    let activeCustomer = dataUserInfo.level > 100 ? 2 : 0

    let userLevel = dataUserInfo.level

    this.props.stateStore.setMyProfileState('nickChange', 0)
    this.props.stateStore.setMyProfileState('activeCustomer', activeCustomer)
    this.props.stateStore.setMyProfileState('userLevel', userLevel)
    this.props.stateStore.setMyProfileState('pwChange', 0)
    this.props.stateStore.setMyProfileState('recomCode', recomCode)
    this.props.stateStore.setMyProfileState('inputNick', nickName)
    this.props.stateStore.setMyProfileState('inputRecomCode', recomCode)
    this.props.stateStore.setMyProfileState('checkPw', 0)
  }

  onClickCancel = (e) => {
    this.props.stateStore.setPopupOn('')
  }
  onClickLogin = (e) => {
    this.props.history.push('/login')
    this.props.stateStore.setPopupOn('')
  }

  renderNotiDialog() {
    const popupOn = this.props.stateStore.popupOn
    if(popupOn === 'needLogin') {
      return (
        <TwoBtn
          content={this.props.dataStore.str.S0027}
          btn1={this.props.dataStore.str.W0053}
          btn2={this.props.dataStore.str.W0091}
          onClickBtn1={(e) => this.onClickCancel(e)}
          onClickBtn2={(e) => this.onClickLogin(e)}></TwoBtn>
      )
    }
    if(popupOn === 'uploadFinish') {
      return (
        <OneBtn
          msg={this.props.dataStore.str.S0169}
          onClickClose={(e) => this.onClickCancel(e)}>
        </OneBtn>
      )
    }
    
  }



  renderRegActive() {
    const regActiveState = this.props.stateStore.regActiveState
    if(regActiveState.isOn) {
      window.document.body.style.position = window.innerWidth > 480 ? 'static' : 'fixed'
      return (
        <RegActive
          getUserInfo={() => this.getUserInfo()}></RegActive>
      )
    } else {
      window.document.body.style.position = 'static'
    }
  }

  renderPhoneVerify() {
    const phoneVerifyState = this.props.stateStore.phoneVerifyState
    if(phoneVerifyState.isOn) {
      return (
        <PhoneVerify></PhoneVerify>
      )
    }
  }

  renderHelp() {
    let pathname = this.props.location.pathname
    if(pathname.indexOf('/help') == 0) {
      return (
        <HelpCenter></HelpCenter>
      )
    }
  }


  preventMobileScrolling() {
    window.addEventListener('resize', () => {
      if(window.innerWidth > 480) {
        window.document.body.style.overflow = 'auto'
        window.document.body.style.position = 'static'
      } else {
        if(this.props.stateStore.productOption.mobileSelOpt ||
          this.props.stateStore.mMenuOpen ||
          this.props.stateStore.regActiveState.isOn) {
          window.document.body.style.position = 'fixed'
        }
      }
    }, true)
  }


  render() {
    const nowPath = this.props.location.pathname.split('/')[1]
    return (
      <Fragment>
        <div className="body" style={{minHeight:nowPath === 'help' ? '40vh' : '100vh'}}>
          <Switch>
            <Route exact path="/" component={Main} />
            <Route exact path="/search/:keyword" component={Search} />
            <Route exact path="/search/:keyword/page/:page" component={Search} />
            <Route exact path="/recommend" component={Recommend} />
            <Route exact path="/recommend/:page" component={Recommend} />
            <Route exact path="/exhibit/:id" component={Exhibit} />
            <Route exact path="/exhibit/:id/:page" component={Exhibit} />
            <Route exact path="/daily/:cate1/:cate2" component={Main} />
            <Route exact path="/daily/:cate1/:cate2/page/:page" component={Main} />
            <Route exact path="/daily/:cate1" component={Main} />
            <Route exact path="/daily/:cate1/page/:page" component={Main} />
            <Route exact path="/special/:cate1" component={Main} />
            <Route exact path="/special" component={Main} />
            <Route exact path="/events" component={Events} />
            <Route exact path="/magazine" component={MagazineList} />
            <Route path="/magazine/:id" component={Magazine} />
            <Route path="/product/:id" component={Product} />
            <Route path="/event/:id" component={Product} />
            <Route path="/my/interest" component={MyInterest} />
            <Route path="/my/coupon" component={MyCoupon} />
            <Route path="/my/like" component={MyLike} />
            <Route path="/my/profile" component={MyProfile}/>
            <Route path="/my/ofriends/available" component={MyOfriendsAvailable} />
            <Route exact path="/my/ofriends/history" component={MyOfriendsHistory} />
            <Route path="/my/ofriends/history/:id" component={MyOfriendsHistoryDetail} />
            <Route exact path="/my/ofriends/review" component={MyOfriendsReview} />
            <Route path="/my/ofriends/review/edit/:id" component={MyOfriendsReviewEdit} />
            <Route exact path="/register" component={Register} />
            <Route exact path="/login" component={Login} />
            <Route exact path="/login/email" component={LoginEmail} />
            <Route exact path="/pw-reset" component={PwReset} />
            <Route exact path="/register/email" component={RegisterEmail} />
            <Route exact path="/help" component={Help} />
            <Route exact path="/notice" component={Notice} />
            <Route exact path="/terms" component={Terms} />
            <Route exact path="/privacy" component={Privacy} />
            <Route path="/review/:id" component={ReviewHost} />
            <Route path="/payment/:id" component={Payment} />
            <Route exact path="/mobilepay" component={FinishPaymentMobile} />
            <Route exact path="/error" component={Err} />
            <Route component={Err} />
          </Switch>
          {this.renderNotiDialog()}
          {this.renderRegActive()}
          {this.renderPhoneVerify()}
        </div>
        {this.renderHelp()}
      </Fragment>

    )

  }
}
export default Body
