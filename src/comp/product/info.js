import React, { Component, Fragment } from 'react'
import { inject, observer } from 'mobx-react'

@inject('stateStore', 'apiStore', 'dataStore')
@observer
class Host extends Component {

  htmlDecode(content) {
    content.replace('')
  }

  onClickMoreSched() {
    this.props.stateStore.setProductOption("isOpenSched", true)
    this.props.stateStore.setProductOption('mobileSelOpt', true)
  }

  renderTimeTable() {
    let timeTables = JSON.parse(this.props.dataStore.dataProduct.product.timetables)
    let key = 0
    let view = []
    for(let item of timeTables) {
      view.push(
        <li key={key} className={item.type === 'text' ? "info-body-timeTable-type-text" : "info-body-timeTable-type-detail"}>
          <span className="info-body-timeTable-time" style={{display : item.time === '' ? 'none' : 'static'}}>{item.time}</span>
          <span className="info-body-timeTable-description">{item.description}</span>
        </li>
      )
      key = key + 1
    }
    return(<ul className="info-body-timeTable-contents">{view}</ul>)
  }

  renderFAQ() {
    let faq = JSON.parse(this.props.dataStore.dataProduct.product.faq)
    let key = 0
    let view = []
    for(let item of faq) {
      view.push(
        <li key = {key}>
          <div className="info-body-faq-q-con">
            <span className="info-body-faq-icon-q">Q</span>
            <span className="info-body-faq-question">{item.Q}</span>
          </div>
          <div className="info-body-faq-a-con">
            <span className="info-body-faq-icon-a">A</span>
            <span className="info-body-faq-answer">{item.A}</span>
          </div>
        </li>
      )
      key = key + 1
    }
    return(<ul className="info-body-faq-container">{view}</ul>)
  }

  render() {
    let product = this.props.dataStore.dataProduct.product
    if(product == undefined) {
      return (<Fragment></Fragment>)
    }

    return (
      <div className="info-entity-container">
        <div className="info-header-title">{product.frip_type === 'event' ? this.props.dataStore.str.W1033 : this.props.dataStore.str.W0023}</div>
        <div className="info-body-con">
          <span dangerouslySetInnerHTML={{__html: product.body.replace('/\"', "\"").replace(/\\"/g, '"')}}></span>
        </div>
        {
          (product.include != undefined && product.include != null)
          &&
          <div className="info-body-included-con">
            <span className="info-body-included-title">{this.props.dataStore.str.W1002}</span>
            <span className="info-body-included" dangerouslySetInnerHTML={{__html: product.include.replace('/\"', "\"").replace(/\\"/g, '"')}}></span>
          </div>
        }
        {
          (product.exclude != undefined && product.exclude != null)
          &&
          <div className="info-body-notIncluded-con">
            <span className="info-body-notIncluded-title">{this.props.dataStore.str.W1003}</span>
            <span className="info-body-notIncluded" dangerouslySetInnerHTML={{__html: product.exclude.replace('/\"', "\"").replace(/\\"/g, '"')}}></span>
          </div>
        }
        {
          (product.timetables != undefined && product.timetables != null)
          &&
          <div className="info-body-timeTable-con">
            <span className="info-body-timetable-title">{this.props.dataStore.str.W1004}</span>
            {this.renderTimeTable()}
          </div>
        }
        {
          (product.stuffs_to_prepare != undefined && product.stuffs_to_prepare != null)
          &&
          <div className="info-body-prepare-con">
            <span className="info-body-prepare-title">{this.props.dataStore.str.W1005}</span>
            <span className="info-body-prepare">{product.stuffs_to_prepare}</span>
          </div>
        }
        {
          (product.note != undefined && product.note != null)
          &&
          <div className="info-body-note-con">
            <span className="info-body-note-title">{this.props.dataStore.str.W1006}</span>
            <span className="info-body-note" dangerouslySetInnerHTML={{__html: product.note.replace('/\"', "\"").replace(/\\"/g, '"')}}></span>
          </div>
        }
        {
          (product.venue != undefined && product.venue != null)
          &&
          <div className="info-body-location-con">
            <span className="info-body-location-title">{this.props.dataStore.str.W1007}</span>
            <div className="info-body-location-map-con">
              {this.props.isMobile === true ? <div id="venue_map_m"></div> : <div id="venue_map"></div>}
            </div>
            {this.props.isMobile === true ? <div id="gathering_map_m"></div> : <div id="gathering_map"></div>}
            <span className="info-body-location">{product.venue}</span>
          </div>
        }
        {
          (product.faq != undefined && product.faq != null)
          &&
          <div className="info-body-faq-con">
            <span className="info-body-faq-title">{this.props.dataStore.str.W1008}</span>
            {this.renderFAQ()}
          </div>
        }
        {
          product.refund_information != undefined
          &&
          <div className="info-body-refund-con">
            <span className="info-body-refund-title">{this.props.dataStore.str.W1010}</span>
            <span
              className="info-body-refund"
              dangerouslySetInnerHTML={{__html: product.refund_information.replaceAll('  ','</br></br>')}}></span>
          </div>
        }
      </div>
    )
  }
}
export default Host
