﻿﻿import React, { Component, Fragment } from 'react'
import { inject, observer } from 'mobx-react'
import { Carousel } from 'react-responsive-carousel'
import { Link, withRouter } from 'react-router-dom'
import OneBtn from "../dialog/one-btn";

@inject('stateStore', 'apiStore', 'dataStore')
@withRouter
@observer
class Frip extends Component {

  constructor(props) {
    super(props)
    this.state = {
      mounted : false
    }
  }
  componentDidMount() {
    this.props.stateStore.setProductOption("isShareOn", false)
    this.mounted = true
    this.setState({
      mounted : true
    })
  }

  componentWillUnmount() {
    this.mounted = false
    this.setState({
      mounted: false
    })
  }


  onClickMoreSched() {
    if(this.props.stateStore.productOption.isEventProduct) {
      const productOpt = this.props.stateStore.productOption.eventProductOpt
      const loginState = this.props.stateStore.loginState
      const dataUserInfo = this.props.dataStore.dataUserInfo
      let userLevel =
        (typeof  dataUserInfo.level === "undefined"
          || dataUserInfo.level === null
          || dataUserInfo.level === "")
          ? 0 : dataUserInfo.level

      if(productOpt.remaining_quantity <= 0) {
        this.props.stateStore.setPopupOn('soldOut')
        return
      }

      if(loginState <= 0) {
        this.props.stateStore.setPopupOn('needLogin')
        return
      }

      if(userLevel < 120) {
        this.props.stateStore.setPopupOn('needAgree')
        return
      }

      this.mobileAutoPurchase(productOpt)
    }
    this.props.stateStore.setProductOption("mobileSelOpt", true)
    this.props.stateStore.setProductOption("isOpenSched", true)
  }

  onClickCloseMarketing = (e) => {
    this.props.stateStore.setPopupOn("")
    this.props.stateStore.setRegActiveState('regCase', 2)
    this.props.stateStore.setRegActiveState('isOn', true)
    this.props.stateStore.setRegActiveState('title', this.props.dataStore.str.W1024)
    this.props.stateStore.setRegActiveState('idTitle', this.props.dataStore.str.W1035)
    this.props.stateStore.setRegActiveState('mktTitle', this.props.dataStore.str.W1036)
  }

  renderDialog() {
    const popupOn = this.props.stateStore.popupOn
    if(popupOn === 'needAgree') {
      return (
        <OneBtn
          msg={this.props.dataStore.str.S0036}
          onClickClose={(e) => this.onClickCloseMarketing(e)}>
        </OneBtn>
      )
    }
    if(popupOn === 'soldOut') {
      return (
        <OneBtn
          msg={this.props.dataStore.str.S1063}
          onClickClose={(e) => this.props.stateStore.setPopupOn("")}>
        </OneBtn>
      )
    }
    if(popupOn === 'participateEvent') {
      return (
        <OneBtn
          msg={this.props.dataStore.str.S0037}
          onClickClose={(e) => {
            this.props.stateStore.setPopupOn("")
            this.props.history.push('/')
          }}>
        </OneBtn>
      )
    }
  }

  renderNoti() {
    const productOption = this.props.stateStore.productOption
    if(productOption.isOnNoti) {
      return (
        <span className="select-option-notice-msg">{productOption.callErrMsg}</span>
      )
    }
  }

  renderRegEndMsg() {
    if(this.props.stateStore.popupOn === 'regEndMsg') {
      return (
        <OneBtn
          msg={this.props.stateStore.regActiveState.regEndMsg}
          onClickClose={(e) => {
            this.props.stateStore.setPopupOn("")
          }}></OneBtn>
      )
    }
  }

  purchaseFailedPopup() {
    this.props.stateStore.setProductOption("isOnNoti", true)
    setTimeout(() => {
      this.props.stateStore.setProductOption("isOnNoti", false)
      this.props.stateStore.setProductOption("callErrMsg", "")
    }, 2000)
  }

  mobileAutoPurchase(product) {
    if(product !== undefined) {
      let jwtToken = this.props.apiStore.getJwtToken()
      if(jwtToken === null) {
        this.props.history.push('/login')
        return
      }
      let payload = {}
      payload = {
        'product' : product.product,
        'product_option' : product.id,
        'amount' : 1
      }
      this.props.apiStore.postPurchases(jwtToken, payload)
        .then(res => {
          this.props.stateStore.setPopupOn("participateEvent")
        })
        .catch(err => {
          if(err.response.status === 400) {
            this.props.stateStore.setProductOption("callErrMsg", this.props.dataStore.str.S1022)
          } else if (err.response.status === 409) {
            this.props.stateStore.setProductOption("callErrMsg", this.props.dataStore.str.S1021)
          } else if (err.response.status === 412) {
            this.props.stateStore.setProductOption("callErrMsg", this.props.dataStore.str.S1057)
          } else {
            this.props.stateStore.setProductOption("callErrMsg", this.props.dataStore.str.S1006)
          }
          this.purchaseFailedPopup()
        })
    }
    return
  }

  onClickShare() {
    this.props.stateStore.setProductOption("isShareOn", true)
  }

  onClickCloseShare() {
    this.props.stateStore.setProductOption("isShareOn", false)
  }

  onClickFacebookShare() {
    const product = this.props.dataStore.dataProduct.product
    window.FB.ui({
      method: 'share',
      href: 'https://ofriends.co.kr/product/' + product.id, // 링크 페이지의 메타 태그에 있는 og:title, og:image 등등을 읽음
    }, (response)=>{
    });
  }

  onClickKakaoShare() {
    const product = this.props.dataStore.dataProduct.product
    if(window.Kakao.isInitialized() === true) {
      window.Kakao.cleanup()
    }
    window.Kakao.init(process.env.REACT_APP_KAKAO_APP_ID)
    window.Kakao.Link.sendDefault({
      objectType: 'feed',
      content: {
        title: product.title,
        description: '', // 해시태그 #가츠산도, #오프렌즈 ...
        imageUrl: 'https://static.ofriends.co.kr/images/products/' + product.id + '_0.jpg', // 현재 사이트의 대표 이미지
        link: {
          mobileWebUrl: 'http://ofriends.co.kr/product/' + product.id,
          webUrl: 'http://ofriends.co.kr/product/' + product.id
        }
      },
    });
  }

  onClickLike() {
    const loginState = this.props.stateStore.loginState
    const product = this.props.dataStore.dataProduct.product
    if(loginState <= 0) {
      this.props.stateStore.setPopupOn('needLogin')
      return
    }

    if(product.like === 1) {
      let token = this.props.apiStore.getJwtToken()
      this.props.apiStore.delLike(token, product.id).then((data) => {
        this.likeChange()
      })
        .catch(err=> {
        })
    } else {
      let token = this.props.apiStore.getJwtToken()
      this.props.apiStore.putLike(token, product.id).then((data) => {
        this.likeChange()
      })
        .catch(err=> {
        })
    }
  }
  likeChange() {
    const dataProduct = this.props.dataStore.dataProduct
    if(dataProduct.product != undefined) {
      this.props.dataStore.setDataProductLike(1 - dataProduct.product.like)
    }
  }
  renderCarousel() {
    let nowPath = this.props.location.pathname
    nowPath = nowPath.endsWith('/') ? nowPath : nowPath + '/'
    let productId = parseInt(nowPath.split('/')[2])
    let data = (productId%2) === 0 ? this.props.dataStore.sampleItemData1 : this.props.dataStore.sampleItemData2

    let images = []
    const product = this.props.dataStore.dataProduct.product
    if(product!=undefined && product.no_image!=undefined) {
      let key = 0
      for(let i=0; i<product.no_image; i++) {
        let position = key
        const imgDUrl = this.props.dataStore.productsImgUrl + product.id + "_"+i+".jpg"
        images.push(
          <div className="frip-carousel-image-container" key={position}>
            <img
              src={imgDUrl === undefined || imgDUrl === null? "" : imgDUrl}
              alt={''}
              onError={(e)=>e.target.setAttribute("src", this.props.dataStore.imageUrl + "06_detailpage_no_image.png")}/>
          </div>
        )
        key = key + 1
      }
    }

    if(this.state.mounted === true) {
      return (
        <div className="frip-carousel-container">
          <Carousel
            className="frip-carousel"
            autoPlay
            showThumbs={false}
            swipeable={true}
            emulateTouch
            showStatus={true}
            showIndicators={false}
            statusFormatter={(current, total) => {
              return current + "/" + total
            }}
            infiniteLoop={true}>
            {images}
          </Carousel>
          <div className="frip-carousel-image-container"></div>
        </div>

      )
    } else {
      return <Fragment></Fragment>
    }
  }
  renderInfo() {
    let nowPath = this.props.location.pathname
    nowPath = nowPath.endsWith('/') ? nowPath : nowPath + '/'
    let productId = parseInt(nowPath.split('/')[2])

    let data = (productId%2) === 0 ? this.props.dataStore.sampleItemData1 : this.props.dataStore.sampleItemData2
    let optionCount = data.option_date.length

    const dataProduct = this.props.dataStore.dataProduct
    let product = dataProduct.product

    let productOptions = dataProduct.product_options
    let fripType = product.frip_type

    if(product == undefined) {
      return (<Fragment></Fragment>)
    }

    let discount = ((product.original_price-product.price)*100/product.original_price)
    if(discount > 0) {
      discount = discount.toFixed(0) + "%"
    } else {
      discount = ""
    }
    return (
      <Fragment>
        <div className="frip-product-container">
          <div className="frip-product-likeshare-con">
            <span className="frip-product-share" onClick={(e) => this.onClickShare(e)}></span>
            <span
              className={product.like==1? "frip-product-like" : "frip-product-unlike"}
              onClick={(e) => this.onClickLike(e)}>
              </span>
          </div>
          <div className="frip-product-title-con">
            <span className="frip-product-title">{product.catch_phrase}</span>
          </div>
          <div className="frip-product-subtitle">
            <span>{product.title}</span>
          </div>
          {
            product.frip_type != 'event'
            &&
            <Fragment>
              <div className="frip-product-price-con">
                <span className="frip-product-price">
                  {this.props.apiStore.getFormatWon(product.price)}
                </span>
                {
                  product.original_price != product.price
                  &&
                  <span className="frip-product-discount">
                    {this.props.apiStore.getFormatWon(product.original_price)}
                  </span>
                }

                <span className="frip-product-discount-rate">
                  {discount}
                </span>
              </div>
              <div className="frip-product-desc-con">
                <ul>
                  <li className="frip-product-desc-like-icon"></li>
                  <li>{product.likes}{this.props.dataStore.str.S0003}</li>
                </ul>
                <ul>
                  <li className="frip-product-desc-schedule-icon"></li>
                  <li>
                    <span className="m-frip-product-desc-other-schedule" onClick={(e) => this.onClickMoreSched()}>
                      {this.props.dataStore.str.W0080}
                    </span>
                  </li>
                  {this.renderProductSchedule()}
                </ul>
                <ul>
                  <li className="frip-product-desc-location-icon"></li>
                  <li>{product.location}</li>
                </ul>
                <ul>
                  <li className="frip-product-desc-star-icon"></li>
                  <li>{this.props.dataStore.str.W0018}&nbsp;{product.rating}</li>
                </ul>
              </div>
            </Fragment>
          }

        </div>
      </Fragment>
    )
  }

  renderProductSchedule() {
    const product = this.props.dataStore.dataProduct.product
    const productOption = this.props.dataStore.dataProduct.product_options
    let fripType = product.frip_type
    let optionCount = 0
    let startDate = ""
    for(let item of productOption) {
      if(item.type === 'option') {
        if(item.start_date !== undefined && item.start_date !== null && item.start_date !== "") {
          startDate = this.props.apiStore.getScheTime(item.start_date)
          break
        }
      }
    }
    for(let item of productOption) {
      if(item.type === 'option') {
        optionCount += 1
      }
    }
    if(fripType === 'frip') {
      return (
        optionCount === 1
          ?
          <li>{startDate}</li>
          :
          <li>
            {startDate}&nbsp;
            <span className="frip-product-desc-other-schedule" onClick={(e) => this.onClickMoreSched()}>
               {this.props.dataStore.str.W0017}
              &nbsp;
              {optionCount -1}&nbsp;
              {this.props.dataStore.str.S0004}
             </span>
          </li>
      )
    } else {
      return (
        <span className="frip-product-expiration-date">{this.props.dataStore.str.S1042}</span>
      )
    }
  }

  render() {
    return (
      <Fragment>
        <div className="frip-entity-container">
          {this.renderCarousel()}
          {this.renderInfo()}
        </div>
        {
          this.props.stateStore.productOption.isShareOn &&
          <div className="frip-product-share-entity-container">
            <div className="frip-product-share-modal-container">
              <div className="frip-product-share-title-close-btn-con">
                <span className="frip-product-share-title">{this.props.dataStore.str.W1001}</span>
                <span className="frip-product-close-btn" onClick={(e)=>{this.onClickCloseShare(e)}}></span>
              </div>
              <div className="frip-product-share-contents-con">
                <span className="frip-product-share-facebook" onClick={(e)=>{this.onClickFacebookShare(e)}}></span>
                <span className="frip-product-share-kakao" onClick={(e)=>{this.onClickKakaoShare(e)}}></span>
              </div>
            </div>
          </div>
        }
        {this.renderDialog()}
        {this.renderNoti()}
        {this.renderRegEndMsg()}
        {
          this.props.isEvent === true
          &&
          <div className="info-event-participate-btn" onClick={(e) =>{this.onClickMoreSched()}}>
            {this.props.dataStore.str.W0080}
          </div>
        }
      </Fragment>
    )

  }
}
export default Frip
