import React, { Component, Fragment } from 'react'
import { inject, observer } from 'mobx-react'
import { Link, withRouter } from 'react-router-dom'

import ItemReviewHost from './item-review-host'

@inject('stateStore', 'apiStore', 'dataStore')
@withRouter
@observer
class ReviewHost extends Component {

  renderReviewHost() {
    let data = this.props.dataStore.sampleReviewHost.data[0]
    let hostReviews = this.props.dataStore.dataProduct.host_reviews
    if(hostReviews != undefined && hostReviews.length > 0) {
      let row = hostReviews[0]
      return (
        <Fragment>
          <ItemReviewHost
            key = {row.id}
            profileId = {row.user}
            name = {row.user}
            rate = {row.rating}
            date = {row.ct}
            text = {row.body}
            fripTitle = {row.product_title}
            fripDate = {""}
            fripOption = {""}
            thumnails = {[]}>
          </ItemReviewHost>
        </Fragment>
      )
    }

  }

  render() {
    const dataProduct = this.props.dataStore.dataProduct
    let product = dataProduct.product
    let hostlink = product != undefined ? ("/review/" + product.host) : ""
    return (
      <div className="review-host-entity-container">
        <div className="review-host-host-name-con">
          <span className="review-host-host-name">{this.props.dataStore.str.W0022}</span>
        </div>
        {this.renderReviewHost()}
        {
          (dataProduct.host_reviews != undefined && dataProduct.host_reviews.length >= 0)
          &&
          <Link
            className="review-host-all-reviews"
            to={hostlink}>
            {dataProduct.host_reviews.length}
            {this.props.dataStore.str.S0005}
          </Link>
        }

      </div>
    )


  }
}
export default ReviewHost
