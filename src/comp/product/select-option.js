import React, { Component, Fragment } from 'react'
import { inject, observer } from 'mobx-react'
import { Link, withRouter } from 'react-router-dom'
import OneBtn from '../dialog/one-btn'
@inject('stateStore', 'apiStore', 'dataStore')
@withRouter
@observer
class SelectOption extends Component {

  componentDidMount() {
    this.onRouteChanged()
  }

  componentDidUpdate(prevProps) {
    if (this.props.location !== prevProps.location) {
      this.onRouteChanged()
    }
  }

  onRouteChanged() {
    this.defaultSet()
  }

  defaultSet() {
    // let nowPath = this.props.location.pathname
    // nowPath = nowPath.endsWith('/') ? nowPath : nowPath + '/'
    // let productId = parseInt(nowPath.split('/')[2])
    // let data = (productId%2) === 0 ? this.props.dataStore.sampleItemData1 : this.props.dataStore.sampleItemData2
    //
  }

  setPrice() {
    const productOption = this.props.stateStore.productOption
    let price = productOption.count * productOption.price
    this.props.stateStore.setProductOption("totalPrice", price)
  }
  onClickSchedTab = (e) => {
    const isOpenSched = !this.props.stateStore.productOption.isOpenSched
    this.props.stateStore.setProductOption("isOpenSched", isOpenSched)
  }
  onClickDefaultTab = (e) => {
    let isOpenDefault = !this.props.stateStore.productOption.isOpenDefault
    this.props.stateStore.setProductOption("isOpenDefault", isOpenDefault)
  }
  onClickSchedOption = (e, id) => {
    // let context = this
    // let nowPath = this.props.location.pathname
    // nowPath = nowPath.endsWith('/') ? nowPath : nowPath + '/'
    // let productId = parseInt(nowPath.split('/')[2])
    // let data = (productId%2) === 0 ? this.props.dataStore.sampleItemData1 : this.props.dataStore.sampleItemData2
    //
    // let options = {
    //   "isOpenSched" : false,
    //   "isOpenDefault" : true,
    //   "isSelectSched" : true,
    //   "isSelectDefault" : this.props.stateStore.productOption.isSelectDefault,
    //   "optionSched" : data.option_date[position],
    //   "optionDefault" : this.props.stateStore.productOption.optionDefault
    // }

    const dataProduct = this.props.dataStore.dataProduct
    let option
    for(let row of dataProduct.product_options) {
      if(row.id === id) {
        option = row
      }
    }

    // this.props.stateStore.setProductOption(options)
    this.props.stateStore.setProductOption("isOpenSched", false)
    this.props.stateStore.setProductOption("isOpenDefault", true)
    this.props.stateStore.setProductOption("isSelectSched", true)
    this.props.stateStore.setProductOption("optionSched", option)
    this.props.stateStore.setProductOption("parentId", id)
  }

  onClickDefaultOption = (e, id) => {
    let context = this
    let nowPath = this.props.location.pathname
    nowPath = nowPath.endsWith('/') ? nowPath : nowPath + '/'
    let productId = parseInt(nowPath.split('/')[2])
    const dataProduct = this.props.dataStore.dataProduct

    let data = (productId%2) === 0 ? this.props.dataStore.sampleItemData1 : this.props.dataStore.sampleItemData2
    const productOption = this.props.stateStore.productOption

    let option
    for(let row of dataProduct.product_options) {
      if(row.id === id) {
        option = row
      }
    }

    if(option.remaining_quantity <= 0) {
      return
    }

    this.props.stateStore.setProductOption("isOpenSched", false)
    this.props.stateStore.setProductOption("isOpenDefault", false)
    this.props.stateStore.setProductOption("isSelectSched", true)
    this.props.stateStore.setProductOption("isSelectDefault", true)
    this.props.stateStore.setProductOption("optionDefault", option)
    this.props.stateStore.setProductOption("price", option.price)
    this.props.stateStore.setProductOption("totalPrice", option.price)
    this.props.stateStore.setProductOption("count", 1)
    this.props.stateStore.setProductOption("remainingQuantity", option.remaining_quantity)
    this.props.stateStore.setProductOption("buyableQuantity", option.buyable_quantity_limit)
  }

  onClickRemoveSelect = (e) => {
    const dataProduct = this.props.dataStore.dataProduct
    if(dataProduct.product_options!=undefined && dataProduct.product_options.length>0) {
      let totalSteps = dataProduct.product_options[0].total_steps
      let isOpenSched = totalSteps === 2
      this.props.stateStore.setProductOption("totalSteps", totalSteps)
      this.props.stateStore.setProductOption("parentId", 0)
      this.props.stateStore.setProductOption("isOpenSched", isOpenSched)
      this.props.stateStore.setProductOption("isOpenDefault", !isOpenSched)
      this.props.stateStore.setProductOption("isSelectSched", false)
      this.props.stateStore.setProductOption("isSelectDefault", false)
      this.props.stateStore.setProductOption("optionSched", {})
      this.props.stateStore.setProductOption("optionDefault", {})
      this.props.stateStore.setProductOption("mobileSelOpt", false)
      // this.props.isMobile && this.props.stateStore.setProductOption("isOpenSched", false)
    }
  }

  onClickPlus = (e) => {
    const productOption = this.props.stateStore.productOption
    const product = this.props.dataStore.dataProduct.product
    if(productOption.count<productOption.buyableQuantity && product.frip_type != 'event') {
      this.props.stateStore.setProductOption("count", productOption.count + 1)
      this.setPrice()
    }
  }

  onClickMinus = (e) => {
    const productOption = this.props.stateStore.productOption
    const product = this.props.dataStore.dataProduct.product
    if(productOption.count>1 && product.frip_type != 'event') {
      this.props.stateStore.setProductOption("count", productOption.count - 1)
      this.setPrice()
    }
  }

  onClickParticipate = (e) => {
    const loginState = this.props.stateStore.loginState
    const dataUserInfo = this.props.dataStore.dataUserInfo
    const productOption = this.props.stateStore.productOption
    const product = this.props.dataStore.dataProduct.product
    let userLevel =
      (typeof  dataUserInfo.level === "undefined"
        || dataUserInfo.level === null
        || dataUserInfo.level === "")
        ? 0 : dataUserInfo.level
    if(product !== undefined) {
      if(loginState <= 0) {
        this.props.stateStore.setPopupOn('needLogin')
        return
      }
      if(product.frip_type === 'event' ||
        productOption.totalPrice <= 0) {
        if(userLevel < 120) { // 이벤트 참여일 때
          this.props.stateStore.setPopupOn('needAgree')
        } else {
          let pageId = parseInt(this.props.match.params.id)
          const dataProduct = this.props.dataStore.dataProduct
          const productOption = this.props.stateStore.productOption

          this.props.dataStore.setInitDataPaymentInfo()
          this.props.dataStore.setDataPaymentInfo('title', dataProduct.product.title)
          this.props.dataStore.setDataPaymentInfo('price', productOption.totalPrice)
          this.props.dataStore.setDataPaymentInfo('discountPrice', productOption.optionDefault.price)
          this.props.dataStore.setDataPaymentInfo('option1Name', productOption.optionDefault.option_text)
          this.props.dataStore.setDataPaymentInfo('option2Name', productOption.optionSched.option_text != undefined ? '' : productOption.optionSched.option_text)
          this.props.dataStore.setDataPaymentInfo('count', productOption.count)
          this.props.dataStore.setDataPaymentInfo('paymentId', 0)
          this.props.dataStore.setDataPaymentInfo('productId', dataProduct.product.id)
          this.props.dataStore.setDataPaymentInfo('option1Id', productOption.optionDefault.id)
          this.props.dataStore.setDataPaymentInfo('option2Id', productOption.optionSched.id != undefined ? -1 : productOption.optionSched.id)
          this.props.dataStore.setDataPaymentInfo('fripType', dataProduct.product.frip_type)
          this.postPurchases()
        }
        return
      }

      if(productOption.isSelectDefault === true) {
        let pageId = parseInt(this.props.match.params.id)
        const dataProduct = this.props.dataStore.dataProduct
        const productOption = this.props.stateStore.productOption

        this.props.dataStore.setInitDataPaymentInfo()
        this.props.dataStore.setDataPaymentInfo('title', dataProduct.product.title)
        this.props.dataStore.setDataPaymentInfo('price', productOption.totalPrice)
        this.props.dataStore.setDataPaymentInfo('discountPrice', productOption.optionDefault.price)
        this.props.dataStore.setDataPaymentInfo('option1Name', productOption.optionDefault.option_text)
        this.props.dataStore.setDataPaymentInfo('option2Name', productOption.optionSched.option_text != undefined ? '' : productOption.optionSched.option_text)
        this.props.dataStore.setDataPaymentInfo('count', productOption.count)
        this.props.dataStore.setDataPaymentInfo('paymentId', 0)
        this.props.dataStore.setDataPaymentInfo('productId', dataProduct.product.id)
        this.props.dataStore.setDataPaymentInfo('option1Id', productOption.optionDefault.id)
        this.props.dataStore.setDataPaymentInfo('option2Id', productOption.optionSched.id != undefined ? -1 : productOption.optionSched.id)
        this.props.dataStore.setDataPaymentInfo('fripType', dataProduct.product.frip_type)
        // this.props.history.push('/payment/' + pageId)
        this.postPurchases()
      }
    }
  }


  postPurchases() {
    const productOption = this.props.stateStore.productOption
    const dataPaymentInfo = this.props.dataStore.dataPaymentInfo
    const product = this.props.dataStore.dataProduct.product
    let payload = {}
    payload = {
      'product' : dataPaymentInfo.productId,
      'product_option' : dataPaymentInfo.option1Id,
      'amount' : dataPaymentInfo.count
    }

    let jwtToken = localStorage.getItem('jwt')
    this.props.apiStore.postPurchases(jwtToken, payload)
    // this.props.apiStore.postPurchases(jwtToken, payload)
        .then(res => {
        if(product.frip_type == 'event') {
          this.props.stateStore.setPopupOn("participateEvent")
        } else {
          if(productOption.totalPrice> 0) {
            let paymentId = res.data.id
            this.props.dataStore.setDataPaymentInfo('paymentId', paymentId)
            this.props.history.push('/payment/' + paymentId)
          } else {
            this.props.stateStore.setPopupOn("participateEvent")
          }
        }
      })
      .catch(err => {
        if(err.response.status === 400) {
          this.props.stateStore.setProductOption("callErrMsg", this.props.dataStore.str.S1022)
        } else if (err.response.status === 409) {
          this.props.stateStore.setProductOption("callErrMsg", this.props.dataStore.str.S1021)
        } else if (err.response.status === 412) {
          this.props.stateStore.setProductOption("callErrMsg", this.props.dataStore.str.S1057)
        } else {
          this.props.stateStore.setProductOption("callErrMsg", this.props.dataStore.str.S1006)
        }
        this.purchaseFailedPopup()
      })
  }

  onClickClose = (e) => {
    this.props.stateStore.setPopupOn("")
    window.location.reload()
  }

  onClickCloseMarketing = (e) => {
    this.props.stateStore.setPopupOn("")
    this.props.stateStore.setRegActiveState('regCase', 2)
    this.props.stateStore.setRegActiveState('isOn', true)
    this.props.stateStore.setRegActiveState('title', this.props.dataStore.str.W1024)
    this.props.stateStore.setRegActiveState('idTitle', this.props.dataStore.str.W1035)
    this.props.stateStore.setRegActiveState('mktTitle', this.props.dataStore.str.W1036)
  }

  renderOptionSched() {
    const dataProduct = this.props.dataStore.dataProduct
    const productOption = this.props.stateStore.productOption
    if(productOption.totalSteps < 2) {
      return (<Fragment></Fragment>)
    }

    // if(data.type != 0 && dataProduct.product_options != undefined) {
    //   return (<Fragment></Fragment>)
    // }

    let nowOptions = []
    let nowOptionGroup = {}
    for(let row of dataProduct.product_options) {
      if(row.parent_id === 0) {
        if(row.type === 'option') {
          nowOptions.push(row)
        }
        if(row.type === 'option_group') {
          nowOptionGroup = row
        }
      }
    }


    let options = []
    // let key = 0
    // for(let row of data.option_date) {
    //   let position = key
    //   options.push(
    //     <div className="select-option-schedule-item" key={key} onClick={(e)=>this.onClickSchedOption(e, position)}>
    //       <div className="select-option-schedule-date-con">
    //         <span className="select-option-schedule-date">{row.date}</span>
    //       </div>
    //       <ul className="select-option-schedule-detail">
    //         <li>{row.participate}/{row.count}{this.props.dataStore.str.W0028}</li>
    //         <li>|</li>
    //         <li>{this.props.dataStore.str.W0027}&nbsp;{row.due_date}</li>
    //       </ul>
    //     </div>
    //   )
    //   key = key + 1
    // }

    for(let row of nowOptions) {
      options.push(
        <div className="select-option-schedule-item" key={row.id} onClick={(e)=>this.onClickSchedOption(e, row.id)}>
          <div className="select-option-schedule-date-con">
            <span className="select-option-schedule-date">{row.option_text}</span>
          </div>
          <ul className="select-option-schedule-detail">
            {
            /*
              <li>{row.participate}/{row.count}{this.props.dataStore.str.W0028}</li>
              <li>|</li>
              <li>{this.props.dataStore.str.W0027}&nbsp;{row.due_date}</li>
            */
            }
          </ul>

        </div>
      )
    }

    return (
      <Fragment>
        <div className="select-option-schedule-entity" onClick={(e)=>this.onClickSchedTab(e)}>
          {
            /*옵션 선택여부에따른 탭 모양변화*/
            this.props.stateStore.productOption.isSelectSched
            ?
            <div className="select-option-schedule-option-selected">
              <div className="select-option-schedule">
                <span className="select-option-schedule-text">{nowOptionGroup.option_text}</span>
              </div>
              <div className="select-option-schedule-item-date-con">
                <span className="select-option-schedule-item-date">{this.props.stateStore.productOption.optionSched.option_text}</span>
              </div>
              <span className = {this.props.stateStore.productOption.isOpenSched? "open" : "closed"}></span>
            </div>
            :
            <div className="select-option-schedule-option-default">
              <div className="select-option-schedule">
                <span className="select-option-schedule-text">{nowOptionGroup.option_text}</span>
              </div>
              <span className = {this.props.stateStore.productOption.isOpenSched? "open" : "closed"}></span>
            </div>
          }
        </div>
        {
          this.props.stateStore.productOption.isOpenSched
          &&
          <div className="select-option-schedule-entity-item">{options}</div>
        }
      </Fragment>
    )
  }

  renderOptionDefault() {
    // let nowPath = this.props.location.pathname
    // nowPath = nowPath.endsWith('/') ? nowPath : nowPath + '/'
    // let productId = parseInt(nowPath.split('/')[2])
    // let data = (productId%2) === 0 ? this.props.dataStore.sampleItemData1 : this.props.dataStore.sampleItemData2
    //
    // if(data.type == 0
    //   &&this.props.stateStore.productOption.isSelectSched === false) {
    //   return (<Fragment></Fragment>)
    // }

    const dataProduct = this.props.dataStore.dataProduct
    const productOption = this.props.stateStore.productOption

    let nowOptions = []
    let nowOptionGroup = {}

    for(let row of dataProduct.product_options) {
      if(row.parent_id === productOption.parentId) {
        if(row.type === 'option') {
          nowOptions.push(row)
        }
      }
    }

    for(let row of dataProduct.product_options) {
      if(productOption.totalSteps == 1) {
        if(row.parent_id === 0) {
          if(row.type === 'option_group') {
            nowOptionGroup = row
          }
        }
      } else {
        if(row.parent_id != 0) {
          if(row.type === 'option_group') {
            nowOptionGroup = row
          }
        }
      }
    }

    let options = []
    let key = 0

    // for(let row of data.default_option) {
    //   let position = key
    //   options.push(
    //     <div className="select-option-participate-cost-container" key={key} onClick={(e)=>this.onClickDefaultOption(e, position)}>
    //       <div className="select-option-participate-title-con">
    //         <span className="select-option-participate-title">{row.title}</span>
    //       </div>
    //       <div className="select-option-quantity-con">
    //         <span className="select-option-quantity-text">
    //           {this.props.dataStore.str.W0029}
    //           &nbsp;
    //           {row.count}
    //           {this.props.dataStore.str.W0030}
    //         </span>
    //       </div>
    //       <div className="select-option-cost-con">
    //         <span className="select-option-cost">{row.price}</span>
    //       </div>
    //       <div className="select-option-discount-con">
    //         <span className="select-option-discount">{row.discount}</span>
    //       </div>
    //     </div>
    //   )
    //   key = key + 1
    // }



    for(let row of nowOptions) {
      options.push(
        <div className="select-option-participate-cost-container"
           key={row.id} onClick={(e)=>this.onClickDefaultOption(e, row.id)}>
          <div className="select-option-participate-title-con">
            <span className="select-option-participate-title">{row.option_text}</span>
          </div>
          <div className="select-option-quantity-con">
            {
              row.remaining_quantity != null
              &&
              <span className="select-option-quantity-text">
                {this.props.dataStore.str.W0029}
                &nbsp;
                {row.remaining_quantity}
                {this.props.dataStore.str.W0030}
              </span>
            }
          </div>
          {
            row.price != undefined
            &&
            <Fragment>
              {
                (row.original_price != undefined
                && row.original_price != null
                && row.original_price != row.price)
                &&
                <div className="select-option-cost-con">
                  <span className="select-option-cost">{this.props.apiStore.getFormatWon(row.original_price)}</span>
                </div>
              }
              <div className="select-option-discount-con">
                <span className="select-option-discount">{this.props.apiStore.getFormatWon(row.price)}</span>
              </div>
            </Fragment>
          }
        </div>
      )
    }

    return (
      <Fragment>
        <div className="select-option-basic-container" onClick={(e)=>this.onClickDefaultTab(e)}>
          {
            /*옵션 선택여부에따른 탭 모양변화*/
            this.props.stateStore.productOption.isSelectDefault
            ?
            <div className="select-option-basic-and-cost-con-selected">
              <div className="select-option-basic">
                <span
                  className={
                    this.props.stateStore.productOption.optionDefault.price === null
                    || this.props.stateStore.productOption.optionDefault.price === undefined
                    ?
                    "select-options-none-title" : "select-option-basic-title"}>{nowOptionGroup.option_text}</span>
              </div>
              <div className="select-option-basic-cost">
                <span className="select-option-cost-text">
                  {
                    this.props.apiStore.getFormatWon(this.props.stateStore.productOption.optionDefault.price)
                  }</span>
              </div>
              <span className = {this.props.stateStore.productOption.isOpenDefault? "open" : "closed"}></span>
            </div>
            :
            <div className="select-option-basic-and-cost-con-default">
              <div className="select-option-basic">
                <span className="select-option-basic-title">{nowOptionGroup.option_text}</span>
                <span className = {this.props.stateStore.productOption.isOpenDefault? "open" : "closed"}></span>
              </div>
            </div>
          }
        </div>
        {
          this.props.stateStore.productOption.isOpenDefault
          &&
          <div className="select-option-cost-and-quantity-entity-container">{options}</div>
        }
      </Fragment>
    )
  }

  renderSelectItem() {
    // let nowPath = this.props.location.pathname
    // nowPath = nowPath.endsWith('/') ? nowPath : nowPath + '/'
    // let productId = parseInt(nowPath.split('/')[2])
    // let data = (productId%2) === 0 ? this.props.dataStore.sampleItemData1 : this.props.dataStore.sampleItemData2
    const product = this.props.dataStore.dataProduct.product
    const productOption = this.props.stateStore.productOption
    if(productOption.isSelectDefault == true && product != undefined) {

      const classNameMinus = (product.frip_type == 'event' || productOption.count === 0) ? "disabled" : "enabled"
      const classNamePlus = (product.frip_type == 'event' || productOption.count === productOption.optionDefault.buyable_quantity_limit)
        ? "disabled" : "enabled"

      return (
        <Fragment>
          <div className="select-option-menu-result-container">
            <div className="select-option-menu-result-sche-title-price-con">
              {
                productOption.optionSched.option_text != undefined
                &&
                <span className="select-option-menu-sche">{productOption.optionSched.option_text}</span>
              }
              <span className="select-option-menu-title">{productOption.optionDefault.option_text}</span>
              <span className="select-option-menu-price">{this.props.apiStore.getFormatWon(productOption.totalPrice)}</span>
            </div>
            <div className="select-option-menu-remove-plus-minus-con">
              <span className="select-option-menu-remove" onClick={(e) => this.onClickRemoveSelect(e)}></span>
              <div className="select-option-menu-plus-minus-con">
                <span
                  className={"minus-btn "+classNameMinus}
                  onClick={(e) => this.onClickMinus(e)}></span>
                <span className="select-option-menu-quantity">{productOption.count}</span>
                <span
                  className={"plus-btn "+classNamePlus}
                  onClick={(e) => this.onClickPlus(e)}></span>
              </div>
            </div>
          </div>
        </Fragment>
      )
    }

  }

  renderParticipate() {
    const productOption = this.props.stateStore.productOption
    const product = this.props.dataStore.dataProduct.product
    let className = productOption.isSelectDefault === true ? "enabled" : "disabled"
    if(product != undefined) {
      return (
        <Fragment>
          <div className="select-option-menu-notice-container">
            {/*<span*/}
            {/*  className="select-option-menu-notice"*/}
            {/*  dangerouslySetInnerHTML={{__html: this.props.dataStore.str.S0012}}></span>*/}
            {/*/!*noti는 현재 분기처리안하고 일단 보여주게만 해놓음 됨 TODO logic은 추후처리*/}
            {/**!/*/}
            <span
              className={className}
              onClick={(e) => this.onClickParticipate(e)}>
              {this.props.dataStore.str.W0080}
            </span>
          </div>
        </Fragment>
      )
    }
  }

  renderDialog() {
    const popupOn = this.props.stateStore.popupOn
    if(popupOn === 'needAgree') {
      return (
        <OneBtn
          msg={this.props.dataStore.str.S0036}
          onClickClose={(e) => this.onClickCloseMarketing(e)}>
        </OneBtn>
      )
    }
    if(popupOn === 'participateEvent') {
      return (
        <OneBtn
          msg={this.props.dataStore.str.S0037}
          onClickClose={(e) => this.onClickClose(e)}>
        </OneBtn>
      )
    }
  }

  renderNoti() {
    const productOption = this.props.stateStore.productOption
    if(productOption.isOnNoti) {
      return (
          <span className="select-option-notice-msg">{productOption.callErrMsg}</span>
      )
    }
  }

  renderRegEndMsg() {
    if(this.props.stateStore.popupOn === 'regEndMsg') {
      return (
        <OneBtn
          msg={this.props.stateStore.regActiveState.regEndMsg}
          onClickClose={(e) => {
            this.props.stateStore.setPopupOn("")
          }}></OneBtn>
      )
    }
  }

  purchaseFailedPopup() {
    this.props.stateStore.setProductOption("isOnNoti", true)
    setTimeout(() => {
      this.props.stateStore.setProductOption("isOnNoti", false)
      this.props.stateStore.setProductOption("callErrMsg", "")
    }, 2000)
  }

  render() {
    if((this.props.stateStore.productOption.mobileSelOpt) &&
      window.innerWidth <= 480) {
      window.document.body.style.position = 'fixed'
      // window.document.body.style.overflow = 'hidden'
    } else {
      window.document.body.style.position = 'static'
      window.document.body.style.overflow = 'auto'
    }
    return (
      <Fragment>
        <div className="select-option-entity-container">
          <div className="select-option-entity-container-wrapper">
            <div className="select-option-backBtn-con">
              <span className="select-option-backBtn" onClick={(e) => {this.onClickRemoveSelect(e)}}></span>
            </div>
            <div className="select-option-title">{this.props.dataStore.str.W0024}</div>
            {this.renderOptionSched()}
            <div className="select-option-menu-container">{this.renderOptionDefault()}</div>
            {this.renderSelectItem()}
            {this.renderParticipate()}
            {this.renderDialog()}
          </div>
        </div>
        {this.renderNoti()}
        {this.renderRegEndMsg()}
      </Fragment>
    )
  }
}
export default SelectOption
