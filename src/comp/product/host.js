import React, { Component, Fragment } from 'react'
import { inject, observer } from 'mobx-react'
import { Link, withRouter } from 'react-router-dom'
@inject('stateStore', 'apiStore', 'dataStore')
@withRouter
@observer
class Host extends Component {

  renderHost() {
    let dataProduct = this.props.dataStore.dataProduct.product
    const imgUrl = this.props.dataStore.imageUrl
    if(dataProduct!=undefined) {
      return (
        <Fragment>
          <div className="host-name">{this.props.dataStore.str.W0019}</div>
          <div className="host-profile">
            <img
              src={dataProduct.host_image_url === undefined || dataProduct.host_image_url === null ? "" : dataProduct.host_image_url}
              onError={(e)=>e.target.setAttribute("src", imgUrl + "07_profile_user.png")}/>
            <div className="host-profile-info">
              <ul className="host-name-con">
                <li className="host-profile-name">{dataProduct.host_title}</li>
                <li className="host-profile-tag"><span className={dataProduct.host_level}></span></li>
              </ul>
              {/* TODO 현재 서버에서 미구현 된 사항. 나중에 수정할 예정. */}
              {/*<ul className="host-count-con">*/}
              {/*  <li className="host-frip-count">{this.props.dataStore.str.W0153}&nbsp;{dataProduct.no_product}</li>*/}
              {/*  <li className="host-like-count">{this.props.dataStore.str.W0021}&nbsp;{dataProduct.host_likes}</li>*/}
              {/*</ul>*/}
            </div>
          </div>
          <div className="host-info">
            <span>{dataProduct.host_body}</span>
          </div>
        </Fragment>
      )
    }
  }

  render() {
    return (
      <div className="host-entity-container">
        <div className="host-container">{this.renderHost()}</div>
      </div>
    )

  }
}
export default Host
