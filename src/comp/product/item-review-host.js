import React, { Component, Fragment } from 'react'
import { inject, observer } from 'mobx-react'

@inject('stateStore', 'apiStore', 'dataStore')
@observer
class ItemReviewHost extends Component {

  onClickImg = (e, position) => {
    this.props.stateStore.setLargerImgView('position', position)
    this.props.stateStore.setLargerImgView('images', this.props.thumnails)
    this.props.stateStore.setPopupOn('LargerImgView')
  }

  renderStar(point) {
    let star = []
    let key = 0

    for(var i=0; i<point; i++) {
      star.push(
        <li className="item-review-host-star" key={key}><span className="star"></span></li>
      )
      key = key + 1
    }


    for (var i=0; i<5-point; i++) {
      star.push(
        <li className="item-review-host-nonstar" key={key}><span className="not-star"></span></li>
      )
      key = key + 1
    }
    return (
      <ul className="item-review-host-star-list">{star}</ul>
    )
  }

  renderImg(imageList) {
    let images = []
    let key = 0
    if(imageList != undefined && imageList[0] != null) {
      for(var i=0; i<imageList.length; i++) {
        let image = this.props.dataStore.reviewImgUrl + imageList[i]
        let position = key
        images.push(
          <li className="item-review-host-image-item" key={key}>
            <img
              src={image}
              onClick={(e)=>{this.onClickImg(e, position)}}/>
          </li>
        )
        key = key + 1
      }
      return (
        <ul className="item-review-host-images-con">{images}</ul>
      )
    }

  }
  renderReview() {
    const data = this.props.reviewData
    const profileImg = this.props.profileImg
    const name = this.props.name
    const rate = this.props.rate
    const date = this.props.date
    const text = this.props.text
    const fripTitle = this.props.fripTitle
    const fripDate = this.props.fripDate == null ? '' : this.props.fripDate
    const fripOption = this.props.fripOption == null ? '' : this.props.fripOption
    const fripOptionCount = this.props.fripOptionCount == null ? 1 : this.props.fripOptionCount
    const thumnails = this.props.thumnails
    const imgUrl = this.props.dataStore.imageUrl
    return (
      <Fragment>
        <div className="item-review-host-container">
          <img
            src = {profileImg === undefined || profileImg === null ? "" : profileImg}
            alt = {''}
            onError={(e)=>e.target.setAttribute("src", imgUrl + "07_profile_user.png")}/>
          <div className="item-review-host-review-host-information">
            <div className="item-review-host-name">
              <span>{name}</span>
            </div>
            <ul className="item-review-host-date-star">
              <li className="item-review-host-star-con">{this.renderStar(rate)}</li>
              <li className="item-review-host-date">
                {this.props.apiStore.getFullTime(date)}
                &nbsp;
                {this.props.dataStore.str.W0073}
              </li>
            </ul>
          </div>
        </div>
        <div className="item-review-host-review-con">
          <div className="item-review-host-review">
            <span dangerouslySetInnerHTML={{__html: text}}></span>
          </div>
          <div className="item-review-host-info-name">
            <span>{fripTitle}</span>
          </div>
          <ul className="item-review-host-date-participate-fee-con">
            <li>{fripOption}</li>
            {/*
            <li>|</li>
            <li>{fripOption}({fripOptionCount}{this.props.dataStore.str.W0160})</li>
            */}

          </ul>
        </div>
        {this.renderImg(thumnails)}
      </Fragment>
    )
  }

  render() {
    return (
      <div className="item-review-host-entity-container">
        {this.renderReview()}
      </div>
    )

  }
}
export default ItemReviewHost
