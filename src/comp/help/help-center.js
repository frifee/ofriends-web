import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
@inject('stateStore', 'apiStore', 'dataStore')
@observer
class HelpCenter extends Component {

  render() {
    return (
      <div className="help-center">
        <div className="help-center-body">
          <div className="help-desk-heading center-text">
            <span>{this.props.dataStore.str.W0161}</span>
          </div>
          <div className="help-desk-title center-text">
            <span>{this.props.dataStore.str.W0162}</span>
          </div>
          <div className="help-desk-available-time-con">
            <span className="help-desk-available-time" dangerouslySetInnerHTML={{__html: this.props.dataStore.str.S1049}}></span>
          </div>
          <div className="help-desk-body">
            <a className="help-desk-kakao" href="https://goto.kakao.com/@ofriends">
              <span className="ico-help-desk kakao-help"></span>
              <div className="title-help-desk center-text">
                <span>{this.props.dataStore.str.W0163}</span>
              </div>
              <div className="info-help-desk center-text">
                <span>{this.props.dataStore.str.W0164}</span>
              </div>
            </a>
            <a className="help-desk-tel" href="tel:070-5175-6637">
              <span className="ico-help-desk phone-help"></span>
              <div className="title-help-desk center-text">
                <span>{this.props.dataStore.str.W0165}</span>
              </div>
              <div className="info-help-desk center-text">
                <span>{this.props.dataStore.str.W0166}</span>
              </div>
            </a>
            <a className="help-desk-email" href="mailto:cs@ofriends.co.kr">
              <span className="ico-help-desk email-help"></span>
              <div className="title-help-desk center-text">
                <span>{this.props.dataStore.str.W0167}</span>
              </div>
              <div className="info-help-desk center-text">
                <span>{this.props.dataStore.str.W0168}</span>
              </div>
            </a>
          </div>
        </div>

      </div>
    )

  }
}
export default HelpCenter
