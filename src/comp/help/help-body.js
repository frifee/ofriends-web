import React, { Component, Fragment } from 'react'
import { inject, observer } from 'mobx-react'
import Pagination from "react-js-pagination"
import HelpCenter from "./help-center";
@inject('stateStore', 'apiStore', 'dataStore')
@observer
class HelpBody extends Component {

  componentDidMount() {
    let data = this.props.dataStore.sampleHelpData.data
    let openList = {}
    for(var i=0; i<data.length; i++) {
      openList[data[i].id.toString()] = false
    }
    this.props.stateStore.setSampleHelpOpen(openList)
  }
  handlePageChange(pageNumber) {
    const faqState = this.props.stateStore.faqState

    if(pageNumber != faqState.page) {
      this.props.getMiscFaqs(faqState.tabPos, pageNumber)
    }
  }
  onClickTitle = (e, id) => {
    const openList = this.props.stateStore.faqState.itemOpen
    const isOpen = openList[id.toString()]
    let newList = {}
    for(var key in openList) {
      newList[key] = (id.toString()===key) ? !openList[key] : openList[key]
    }
    this.props.stateStore.setFaqState('itemOpen', newList)

  }

  renderHelp() {
    const data = this.props.dataStore.dataMiscFaqs
    let helpList = []
    for(var i=0; i<data.length; i++) {
      let row = data[i]

      const isOpen = this.props.stateStore.faqState.itemOpen[row.id.toString()]
      helpList.push(
        <Fragment key={row.id}>
          <div className="help-body-item" onClick={(e)=> this.onClickTitle(e, row.id)}>
            <div className="help-body-title-container">
              <ul className="help-body-item-icon-title-con">
                <li className="help-body-q-icon-con"><span className="help-body-icon"></span></li>
                <li className="help-body-title-con"><span className={isOpen ? "help-body-title-open" : "help-body-title-closed"}>{row.title}</span></li>
              </ul>
              <ul className="help-body-item-date-con">
                <li className="help-body-date-con"><span className="help-body-item-date">{this.props.apiStore.getFullTime(row.ct)}</span></li>
                <li className="help-body-arrow-con">
                  <span
                    className={
                      isOpen ? "content-open" : "content-closed"}>
                  </span>
                </li>
              </ul>
            </div>
            {
              isOpen
              &&
              <div className="help-body-content-container">
                <span className="help-body-content" dangerouslySetInnerHTML={{__html: row.body}}></span>
              </div>
            }
          </div>
        </Fragment>
      )
    }
    return (
      <Fragment>{helpList}</Fragment>
    )
  }

  renderPagination() {

    const faqState = this.props.stateStore.faqState
    let activePage = faqState.page
    let totalItemsCount = faqState.count
    let pageRangeDisplayed = (faqState.count/5) >= 5 ? 5 : parseInt(faqState.count/5 + 1)

    if(totalItemsCount>0) {
      return (
        <Pagination
          activePage={activePage}
          itemsCountPerPage={5}
          totalItemsCount={totalItemsCount}
          pageRangeDisplayed={pageRangeDisplayed+1}
          onChange={this.handlePageChange.bind(this)}
          firstPageText={""}
          prevPageText={""}
          nextPageText={""}
          lastPageText={""}
          itemClassFirst="first-link"
          itemClassPrev="prev-link"
          itemClassNext="next-link"
          itemClassLast="last-link"
          linkClass="page-link"
          innerClass="pagination"
          itemClass="pg"
          activeClass="current"
        />
      )
    }

  }

  render() {
    return (
      <div className="help-body-entity-container">
        <div className="help-body-help-items-con">{this.renderHelp()}</div>
        <div className="help-body-pagination">{this.renderPagination()}</div>
      </div>
    )

  }
}
export default HelpBody
