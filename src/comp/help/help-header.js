import React, { Component, Fragment } from 'react'
import { inject, observer } from 'mobx-react'

@inject('stateStore', 'apiStore', 'dataStore')
@observer
class HelpHeader extends Component {

  componentDidMount() {
    this.props.stateStore.setFaqState('tabPos', 0)
  }

  onClickTab = (e, position) => {
    if(this.props.stateStore.faqState.tabPos != position) {
      this.props.stateStore.setFaqState('tabPos', position)
      this.props.getMiscFaqs(position, 1)
    }
  }

  renderTab() {
    const faqState = this.props.stateStore.faqState
    let list = this.props.dataStore.helpTabList
    let tab = []
    for(var i=0; i<list.length; i++) {
      let row = list[i]
      tab.push(
        <li
          onClick={(e) => this.onClickTab(e, row.id)}
          key={row.id}
          className={
          faqState.tabPos === row.id
          ? "selected-tap" : "not-selected-tab"}
          id = {faqState.tabPos === row.id + 1
          ? "selected-prev" : ""}>
          <span>
            {row.name}
          </span>
        </li>
      )
    }
    return (
      <ul className="help-header-sub-tab-container">{tab}</ul>
    )
  }

  render() {
    return (
      <Fragment>
        <div className="help-header-entity-container">
          <span>{this.props.dataStore.str.W0005}</span>
        </div>
        <div className="help-header-sub-tab">{this.renderTab()}</div>
      </Fragment>
    )

  }
}
export default HelpHeader
