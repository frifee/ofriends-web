import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
import SpecialHeader from './special-header'
import SpecialBody from './special-body'
@inject('stateStore', 'apiStore', 'dataStore')
@observer
class Special extends Component {
  render() {
    return (
      <div className="body-special">
        <SpecialHeader></SpecialHeader>
        <SpecialBody></SpecialBody>
      </div>
    )

  }
}
export default Special
