import React, { Component, Fragment } from 'react'
import { inject, observer } from 'mobx-react'
import { Link, withRouter } from 'react-router-dom'
import { Carousel } from 'react-responsive-carousel'
import '../../static/scss/magazine.scss'
const data = [
  {
    'id' : 0,
    'name' : '2020년에 꼭 가봐야할 여행지',
    'img' : 'http://www.bloter.net/wp-content/uploads/2016/08/%EC%8A%A4%EB%A7%88%ED%8A%B8%ED%8F%B0-%EC%82%AC%EC%A7%84-765x519.jpg'
  },
  {
    'id' : 1,
    'name' : '2021년에 꼭 가봐야할 여행지',
    'img' : 'http://www.bloter.net/wp-content/uploads/2016/08/%EC%8A%A4%EB%A7%88%ED%8A%B8%ED%8F%B0-%EC%82%AC%EC%A7%84-765x519.jpg'
  },
  {
    'id' : 2,
    'name' : '2022년에 꼭 가봐야할 여행지',
    'img' : 'http://www.bloter.net/wp-content/uploads/2016/08/%EC%8A%A4%EB%A7%88%ED%8A%B8%ED%8F%B0-%EC%82%AC%EC%A7%84-765x519.jpg'
  },
  {
    'id' : 3,
    'name' : '2023년에 꼭 가봐야할 여행지',
    'img' : 'http://www.bloter.net/wp-content/uploads/2016/08/%EC%8A%A4%EB%A7%88%ED%8A%B8%ED%8F%B0-%EC%82%AC%EC%A7%84-765x519.jpg'
  }
]

@inject('stateStore', 'apiStore', 'dataStore')
@withRouter
@observer
class Magazine extends Component {

  constructor(props) {
    super(props)
    this.state = {
      mounted : false
    }
  }

  componentDidMount() {
    this.setState({
      mounted : true
    })
    this.onRouteChanged()
  }

  componentDidUpdate(prevProps) {

    if (this.props.location !== prevProps.location) {
      this.onRouteChanged()
    }
  }

  componentWillUnmount() {
    this.setState({
      mounted: false
    })
  }

  onRouteChanged() {


  }

  onClickCarousel = (e, id) => {
    this.props.history.push('/magazine/' + id)
    // window.location.href = dataMain.banners[position].link
  }

  renderCarousel() {
    if(this.state.mounted === true) {

      const dataMain = this.props.dataStore.dataMain
      if(dataMain.magazines != undefined) {
        let gallery = []
        let magazine = dataMain.magazines
        for(var i=0; i<magazine.length; i++) {

          let position = i
          let data = magazine[position]
          gallery.push(
            <div
              className="carousel-con"
              key={position}
              onClick={(e) => this.onClickCarousel(e, data.id)}>
              <div
                className="img-wrapper">
                <img
                  className="carousel-img"
                  src={'https://static.ofriends.co.kr/images/magazines/'+ data.id + '_0.jpg'}
                  onError={(e)=>e.target.setAttribute("src", this.props.dataStore.imageUrl + "06_detailpage_no_image.png")}/>
              </div>
              {/*
                <div
                  className="title-wrapper">
                  <span className="carousel-title">{data.title}</span>
                </div>
              */}

            </div>
          )
        }
        return (
          <Carousel
            className="mag-carousel"
            centerMode
            centerSlidePercentage = {92}
            showThumbs={false}
            swipeable={true}
            emulateTouch
            showStatus={false}
            infiniteLoop={true}>
            {gallery}
          </Carousel>
        )
      }

    }
  }

  renderItems() {
    const dataMain = this.props.dataStore.dataMain
    if(dataMain.magazines != undefined) {
      let view = []
      let magazine = dataMain.magazines.splice(0, 3)
      for(var i=0; i<magazine.length; i++) {

        let position = i
        let data = magazine[position]
        view.push(
          <div
            className="mag-item"
            key={position}
            onClick={(e) => this.onClickCarousel(e, data.id)}>
            <div className="img-wrapper">
              <img
                className="mag-image rounded-img"
                src={'https://static.ofriends.co.kr/images/magazines/'+ data.id + '_0.jpg'}
                onError={(e)=>e.target.setAttribute("src", this.props.dataStore.imageUrl + "06_detailpage_no_image.png")}/>
            </div>
            {/*
              <div className="title-wrapper">
                <span className="mag-title">{data.title}</span>
              </div>
            */}

          </div>
        )
      }

      return (
        <Fragment>{view}</Fragment>
      )
    }
  }

  render() {
    return (
      <div className="magazine">
        <div className="mag-sub">
          <span className="mag-title">{this.props.dataStore.str.W1040}</span>
          <Link className="mag-link" to="/magazine">{this.props.dataStore.str.W0013}</Link>
        </div>
        <div className="mobile">{this.renderCarousel()}</div>
        <div className="desktop">{this.renderItems()}</div>
      </div>
    )

  }
}
export default Magazine
