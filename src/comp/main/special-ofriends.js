import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
import ItemRisingOfriends from './item-rising-ofriends'
import { Link } from 'react-router-dom'
@inject('stateStore', 'apiStore', 'dataStore')
@observer
class SpecialOfriends extends Component {

  renderItems() {
    let view = []
    let data = this.props.dataStore.sampleRecomData
    const dataMain = this.props.dataStore.dataMain
    if(dataMain.new_products != undefined) {

      for(var i=0; i<dataMain.new_products.length; i++) {
        let position = i
        let item = dataMain.new_products[i]
        let tags = []
        if(item.is_hot === 1) {
          tags.push(this.props.dataStore.str.W0125)
        }
        if(item.is_new === 1) {
          tags.push(this.props.dataStore.str.W0126)
        }
        if(item.is_only === 1) {
          tags.push(this.props.dataStore.str.W0127)
        }
        view.push(
          <ItemRisingOfriends
            id = {item.id}
            key = {position}
            loc = {item.loc}
            isLike = {item.like}
            subTitle = {item.catch_phrase}
            title = {item.title}
            price = {item.original_price}
            discount = {item.price}
            rating = {item.rating}
            tags = {tags}
            ></ItemRisingOfriends>
        )

      }
      return (<div className="rising-ofriends-entity">{view}</div>)
    }
    // if(data != null) {
    //   let view = []
    //   for(var i=0; i<data.item.length; i++){
    //     let item = data.item[i]
    //     view.push(
    //       <ItemRisingOfriends
    //         id = {item.id}
    //         key = {i}
    //         loc = {item.loc}
    //         isLike = {item.isLike}
    //         subTitle = {item.subTitle}
    //         title = {item.title}
    //         price = {item.price}
    //         discount = {item.discount}
    //         rating = {item.rating}
    //         tags = {item.tags}
    //         ></ItemRisingOfriends>
    //     )
    //   }
    //   view = view.slice(0, 4)
    //
    // }
  }

  render() {
    return (
      <div className="rising-ofriends-entity-container">
        <div className="rising-ofriends-header">
          <span className="rising-ofriends-title">{ this.props.dataStore.str.W1050 }</span>
        </div>
        { this.renderItems() }
      </div>
    )

  }
}
export default SpecialOfriends
