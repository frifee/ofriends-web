import React, { Component, Fragment } from 'react'
import { inject, observer } from 'mobx-react'
import { Carousel } from 'react-responsive-carousel'
import Category from './category'
import RisingOfriends from './rising-ofriends'
import SpecialOfriends from './special-ofriends'
import Magazine from './magazine'
import Events from './events'
import Alliance from './alliance'
import { Link } from 'react-router-dom'
import TwoBtn from "../dialog/two-btn"
import ImagePopup from "../dialog/image-popup"
@inject('stateStore', 'apiStore', 'dataStore')
@observer
class Recom extends Component {
  constructor(props) {
    super(props)
    this.state = {
      mounted : false,
      popup: false
    }
  }
  componentDidMount() {
    this.mounted = true
    this.setState({
      mounted : true
    })
    this.onRouteChanged()
  }

  componentDidUpdate(prevProps) {

    if (this.props.location !== prevProps.location) {
      this.onRouteChanged()
    }
  }

  componentWillUnmount() {
    this.mounted = false
    this.setState({
      mounted: false
    })
  }

  onRouteChanged() {
    // alert('본 사이트는 현재 개발중인 테스트 페이지입니다. 테스트 페이지에서의 사용자정보를 포함한 모든 기록은 2019년 11월 25일에 삭제됩니다.')
    this.props.dataStore.setDataMain({})
    const token = localStorage.getItem('jwt')
    this.props.apiStore.getMain(token, []).then(res=>{
      this.props.dataStore.setDataMain({})
      this.props.dataStore.setDataMain(res.data)
      // this.props.stateStore.setMainPopupOn("main")
      this.setPopup()
    })
    .catch(err=> {
    })
  }

  setPopup() {
    const dataMain = this.props.dataStore.dataMain

    if(dataMain.popup != undefined) {
      // localStorage.removeItem('popup')
      if(localStorage.getItem('popup') == null || localStorage.getItem('popup')==='') {
        // console.log("hi")
        localStorage.setItem('popup', JSON.stringify({'result':[]}))
      }
      let popupCond = JSON.parse(localStorage.getItem('popup')).result
      // console.log(popupCond)
      if(popupCond.indexOf(dataMain.popup[0].id) === -1) {
        // console.log("")
        this.setState({
          popup : true
        })
      }
    }
  }

  onClickGallery = (e, position) => {
    const dataMain = this.props.dataStore.dataMain
    if(dataMain.banners != undefined) {
      // this.props.history.push(dataMain.banners[position].link)
      // this.props.history.push(dataMain.banners[position].link)
      window.location.href = dataMain.banners[position].link
    }
  }

  onClickPopupBtn1 = (e) => {
    const dataMain = this.props.dataStore.dataMain
    let popupCond = JSON.parse(localStorage.getItem('popup'))
    popupCond.result.push(dataMain.popup[0].id)
    localStorage.setItem('popup', JSON.stringify({'result':popupCond.result}))
    this.setState({
      popup : false
    })
  }

  onClickPopupBtn2 = (e) => {
    const dataMain = this.props.dataStore.dataMain
    // if(dataMain.popup[0].link != '') {
    //   window.location.href = dataMain.popup[0].link
    // }
    this.setState({
      popup : false
    })
  }

  renderCategory() {
    const data = this.props.dataStore.categories.mainCate
    let categories = []
    let key = 0
    for(var i=0; i<data.length; i++) {
      let row = data[i]
      categories.push(
        <Category key={row.id} catName={row.name} link={row.link} number={key}></Category>
      )
      key = key + 1
    }
    // for(var row = 0; row<data.length; row++) {
    //   categories.push(
    //     <Category key={row} catName={data[row]}></Category>
    //   )
    // }
    return (<div className="recom-categories">{categories}</div>)
  }

  renderCarousel() {
    const dataMain = this.props.dataStore.dataMain
    if(this.state.mounted === true) {
      let gallery = []
      if(dataMain.banners != undefined) {
        for(var i=0; i<dataMain.banners.length; i++) {
          let position = i
          let row = dataMain.banners[i]
          gallery.push(
            <div key={row.id} onClick={(e) => {this.onClickGallery(e, position)}}>
              <img
                className="recom-carousel-image-desktop desktop"
                src={row.image_desktop === undefined || row.image_desktop === null ? "" : row.image_desktop}
                onError={(e)=>e.target.setAttribute("src", this.props.dataStore.imageUrl + "06_detailpage_no_image.png")}/>
              <img
                className="recom-carousel-image-mobile mobile"
                src={row.image_mobile === undefined || row.image_mobile === null ? "" : row.image_mobile}
                onError={(e)=>e.target.setAttribute("src", this.props.dataStore.imageUrl + "06_detailpage_no_image.png")}/>
            </div>
          )
        }
      } else {
        return
      }
      return (
        <Fragment>
          <Carousel
            className="recom-carousel-container mobile"
            centerMode
            centerSlidePercentage = {92}
            autoPlay
            showThumbs={false}
            swipeable={true}
            emulateTouch
            showStatus={false}
            infiniteLoop={true}>
            {gallery}
          </Carousel>
          <Carousel
            className="recom-carousel-container desktop"
            autoPlay
            showThumbs={false}
            swipeable={true}
            emulateTouch
            showStatus={false}
            infiniteLoop={true}>
            {gallery}
          </Carousel>
        </Fragment>

      )
    }

  }
  renderPopup() {
    const dataMain = this.props.dataStore.dataMain
    if(this.state.popup === true && dataMain.popup != undefined) {
      // console.log(dataMain.popup)
      if(dataMain.popup[0].type == 'text') {
        return (
          <TwoBtn
            content={dataMain.popup[0].text}
            onClickBtn1={(e) => this.onClickPopupBtn1(e)}
            onClickBtn2={(e) => this.onClickPopupBtn2(e)}
            btn1 = {this.props.dataStore.str.W1037}
            btn2 = {this.props.dataStore.str.W0091}></TwoBtn>
        )
      } else {
        return (
          <ImagePopup
            image={dataMain.popup[0].link}
            onClickBtn1={(e) => this.onClickPopupBtn1(e)}
            onClickBtn2={(e) => this.onClickPopupBtn2(e)}
            btn1 = {this.props.dataStore.str.W1037}
            btn2 = {this.props.dataStore.str.W0091}></ImagePopup>
        )
      }
    }
  }
  render() {
    return (
      <div className="recom-body-recommended">
        <div className="recom-carousel-wrapper-container">
        { this.renderCarousel() }
        </div>
        { this.renderCategory() }
        <RisingOfriends {...this.props}></RisingOfriends>
        <Magazine></Magazine>
        <SpecialOfriends {...this.props}></SpecialOfriends>
        <Events></Events>
        <Alliance></Alliance>
        { this.renderPopup() }
      </div>
    )

  }
}
export default Recom
