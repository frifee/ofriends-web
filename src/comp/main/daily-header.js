import 'core-js'
import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
import { Link, withRouter } from 'react-router-dom'

@inject('stateStore', 'apiStore', 'dataStore')
@withRouter
@observer
class DailyHeader extends Component {

  renderTopList() {
    const categories = this.props.dataStore.categories
    let nowPath = this.props.location.pathname
    let tabs = []

    for(var i=0; i<categories.data.length; i++) {
      let row = categories.data[i]
      let className = nowPath.indexOf(row.link)>0 ? "activate top-list" : "none-activate top-list"
      tabs.push(
        <Link key = {row.id} to={'/daily' + row.link} className={className}>
          <div>
            <span>{row.name}</span>
          </div>
        </Link>
      )
    }
    return (
      <div className="daily-main-tab">{tabs}</div>
    )
  }

  renderBottomList() {
    let tabs = []
    let cate1 = this.props.match.params.cate1
    let cate2 = this.props.match.params.cate2
    const categories = this.props.dataStore.categories
    let catePos = categories.dict[cate1] == undefined ? 0 : categories.dict[cate1]

    for(var i=0; i<categories.data[catePos].sub.length; i++) {
      let row = categories.data[catePos].sub[i]
      let className = row.link.indexOf(cate2)>0 ? "activate sub-list" : "none-activate sub-list"
      tabs.push(
        <Link key = {row.id} to={'/daily/' + cate1 + row.link} className={className}>
          <div>
            <span>{row.name}</span>
          </div>
        </Link>
      )
    }
    return (
      <div className="daily-sub-tab">{tabs}</div>
    )
  }

  render() {
    return (
      <div className="body-daily-header">
        {this.renderTopList()}
        {this.renderBottomList()}
      </div>
    )

  }
}
export default DailyHeader
