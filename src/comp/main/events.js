import React, { Component, Fragment } from 'react'
import { inject, observer } from 'mobx-react'
import { Link, withRouter } from 'react-router-dom'
import { Carousel } from 'react-responsive-carousel'
import '../../static/scss/events-main.scss'
const data = [
  {
    'id' : 0,
    'name' : '2020년에 꼭 가봐야할 여행지',
    'img' : 'http://www.bloter.net/wp-content/uploads/2016/08/%EC%8A%A4%EB%A7%88%ED%8A%B8%ED%8F%B0-%EC%82%AC%EC%A7%84-765x519.jpg'
  },
  {
    'id' : 1,
    'name' : '2021년에 꼭 가봐야할 여행지',
    'img' : 'http://www.bloter.net/wp-content/uploads/2016/08/%EC%8A%A4%EB%A7%88%ED%8A%B8%ED%8F%B0-%EC%82%AC%EC%A7%84-765x519.jpg'
  },
  {
    'id' : 2,
    'name' : '2022년에 꼭 가봐야할 여행지',
    'img' : 'http://www.bloter.net/wp-content/uploads/2016/08/%EC%8A%A4%EB%A7%88%ED%8A%B8%ED%8F%B0-%EC%82%AC%EC%A7%84-765x519.jpg'
  },
  {
    'id' : 3,
    'name' : '2023년에 꼭 가봐야할 여행지',
    'img' : 'http://www.bloter.net/wp-content/uploads/2016/08/%EC%8A%A4%EB%A7%88%ED%8A%B8%ED%8F%B0-%EC%82%AC%EC%A7%84-765x519.jpg'
  }
]

@inject('stateStore', 'apiStore', 'dataStore')
@withRouter
@observer
class Events extends Component {

  constructor(props) {
    super(props)
    this.state = {
      mounted : false
    }
  }

  componentDidMount() {
    this.setState({
      mounted : true
    })
    this.onRouteChanged()
  }

  componentDidUpdate(prevProps) {

    if (this.props.location !== prevProps.location) {
      this.onRouteChanged()
    }
  }

  componentWillUnmount() {
    this.setState({
      mounted: false
    })
  }

  onRouteChanged() {

  }

  onClickCarousel = (e, type, link) => {
    if(type === 'internal') {
      this.props.history.push('/events')
    } else {
      window.location.href = link
    }

    // window.location.href = dataMain.banners[position].link
  }


  renderCarousel() {
    const eventBanners = this.props.dataStore.dataMain.event_banners
    if(eventBanners == undefined)
      return
    if(this.state.mounted === true) {

      let gallery = []
      for(let row of eventBanners) {
        gallery.push(
          <div
            className="ev-wrapper"
            key = {row.id}
            onClick={(e) => this.onClickCarousel(e, row.type, row.link)}>
            <img
              className="ev-image"
              onError={(e)=>e.target.setAttribute("src", this.props.dataStore.imageUrl + "06_detailpage_no_image.png")}
              src={row.image_link}/>
          </div>
        )
      }
      return (
        <Fragment>
          <Carousel
            className="ev-carousel desktop"
            showThumbs={false}
            showIndicators={true}
            swipeable={true}
            emulateTouch
            showStatus={false}
            showArrows={false}
            infiniteLoop={true}>
            {gallery}
          </Carousel>
          <Carousel
            className="ev-carousel mobile"
            centerMode
            centerSlidePercentage = {92}
            showThumbs={false}
            showIndicators={true}
            swipeable={true}
            emulateTouch
            showStatus={false}
            showArrows={false}
            infiniteLoop={true}>
            {gallery}
          </Carousel>
        </Fragment>

      )
    }
  }

  render() {
    return (
      <Fragment>
        <div className="event">
          <span className="ev-title">{this.props.dataStore.str.W1042}</span>
          {
            /*<Link className="ev-link" to="/events">{this.props.dataStore.str.W0013}</Link>*/
          }
          <Link className="ev-link" to="/events">{this.props.dataStore.str.W0013}</Link>
          {this.renderCarousel()}
        </div>
      </Fragment>
    )

  }
}
export default Events
