import React, {Component, Fragment} from 'react'
import { inject, observer } from 'mobx-react'
import { Link, withRouter } from 'react-router-dom'
import ItemRisingOfriends from './item-rising-ofriends'
import Pagination from "react-js-pagination"

@inject('stateStore', 'apiStore', 'dataStore')
@withRouter
@observer
class DailyBody extends Component {

  onClickSort = (e) => {
    e.stopPropagation()
    this.props.stateStore.setIsDailySortOpen(!this.props.stateStore.isDailySortOpen)
  }

  renderTab() {
    // let list = this.props.dataStore.sampleDailySubTabs2
    // let tabs = []
    // let key = 0
    // let nowPath = this.props.location.pathname
    // let path = nowPath.split('/')
    // if(path[3]!==undefined) {
    //   for(var i=0; i<list.item.length; i++) {
    //
    //     let link = '/daily' + '/' + path[2] + '/' + path[3] + list.item[i].link
    //     let className = nowPath.includes(list.item[i].link) ? "activate tap-list" : "none-activate tap-list"
    //     tabs.push(
    //       <Link key = {key} to={link} className={className}>
    //         <div>
    //           <span>{list.item[i].name}</span>
    //         </div>
    //       </Link>
    //     )
    //     key = key + 1
    //   }
    // }
    let sortList = this.props.dataStore.sortByItem
    let sortableList = []
    for(var i=0; i< sortList.length; i++) {

      let row = sortList[i]
      let link = this.props.location.pathname.split('/page')[0] + row.link
      sortableList.push(
        <Link key = {row.id} to={link}>
          <div>
            <span>{row.name}</span>
          </div>
        </Link>
      )
    }


    return (
      <div className="daily-body-tab-entity">
        {/* 마지막 category는 쓰이지 않지만 혹시 모르므로 남겨 둠
        <div className="daily-body-tab">{tabs}</div>
        */}
        <div className="daily-body-alignment-con" onClick={(e) => this.onClickSort(e)}>
          <span className="daily-body-alignment-icon"></span>
          <span className="daily-body-alignment-title">{this.props.dataStore.str.W0129}</span>
        </div>
        <div className="alignment-dropdown" style={{display:this.props.stateStore.isDailySortOpen === true ? 'flex' : 'none'}}>
          {this.props.stateStore.isDailySortOpen && sortableList}
        </div>
      </div>
    )
  }

  renderItems() {
    let data = this.props.dataStore.dataProductList
    if(data != null) {
      let view = []

      for(let item of data) {
        let tags = []
        if(item.is_hot === 1) {
          tags.push(this.props.dataStore.str.W0125)
        }
        if(item.is_new === 1) {
          tags.push(this.props.dataStore.str.W0126)
        }
        if(item.is_only === 1) {
          tags.push(this.props.dataStore.str.W0127)
        }
        view.push(
          <ItemRisingOfriends
            id = {item.id}
            key = {item.id}
            loc = {item.loc}
            isLike = {item.like}
            subTitle = {item.catch_phrase}
            title = {item.title}
            price = {item.original_price}
            discount = {item.price}
            rating = {item.rating}
            tags = {tags}
          ></ItemRisingOfriends>
        )
      }
      return (<div className="daily-entity">{view}</div>)
    }
  }
  handlePageChange(pageNumber) {
    let nowPath = this.props.location.pathname.split('/page')[0]
    nowPath = nowPath.endsWith('/') ? nowPath : nowPath + '/'
    let search = this.props.location.search
    if(search == "undefined" || search == null || search == "")
      search = ''
    this.props.history.push(nowPath + 'page/' + pageNumber + '/' + search)
  }
  renderPagination() {
    const dailyState = this.props.stateStore.dailyState
    let activePage = dailyState.page
    let totalItemsCount = dailyState.count
    let pageRangeDisplayed = (dailyState.count/5) >= 5 ? 5 : parseInt(dailyState.count/5 + 1)
    if(totalItemsCount>0) {
      return (
        <Pagination
          activePage={activePage}
          itemsCountPerPage={20}
          totalItemsCount={totalItemsCount}
          pageRangeDisplayed={pageRangeDisplayed + 1}
          onChange={this.handlePageChange.bind(this)}
          firstPageText={""}
          prevPageText={""}
          nextPageText={""}
          lastPageText={""}
          itemClassFirst="first-link"
          itemClassPrev="prev-link"
          itemClassNext="next-link"
          itemClassLast="last-link"
          linkClass="page-link"
          innerClass="pagination"
          itemClass="pg"
          activeClass="current"
        />
      )
    }
  }

  render() {
    return (
      <Fragment>
        <div className="body-daily-body">
          {this.renderTab()}
          {this.renderItems()}
        </div>
        {this.renderPagination()}
      </Fragment>
    )

  }
}
export default DailyBody
