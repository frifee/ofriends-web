import React, {Component, Fragment} from 'react'
import { inject, observer } from 'mobx-react'
import { Link } from 'react-router-dom'

@inject('stateStore', 'apiStore', 'dataStore')
@observer
class Category extends Component {
  render() {
    const imgUrl = this.props.dataStore.imageUrl;
    return (
      <Fragment>
        <div className="d-cat-item" id={"recom-categories-id-" + this.props.number}>
          <Link to={this.props.link}>
            <div className="d-cat-image-wrapper">
              <img
                className="d-cat-image"
                src={imgUrl + '01_new_main_category_0' + this.props.number + '.png'}
                alt={''}
                onError={(e)=>e.target.setAttribute("src", imgUrl + "m_menu_category.png")}/>
              {/*<div className="cat-name">{this.props.catName}</div>*/}
            </div>
          </Link>
        </div>
        <div className="m-cat-item" id={"m-recom-categories-id-" + this.props.number}>
          <Link to={this.props.link}>
            <div className="m-cat-image-wrapper">
              <img
                className="m-cat-image"
                src={imgUrl + 'mobile-menu-' + this.props.number + '.jpg'}
                alt={''}
                onError={(e)=>e.target.setAttribute("src", imgUrl + "m_menu_category.png")}/>
              {/*<div className="m-cat-name">{this.props.catName}</div>*/}
            </div>
          </Link>
        </div>
      </Fragment>
    )

  }
}
export default Category
