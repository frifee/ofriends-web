import React, { Component, Fragment } from 'react'
import { inject, observer } from 'mobx-react'
import { Link } from 'react-router-dom'
@inject('stateStore', 'apiStore', 'dataStore')
@observer
class ItemRisingOfriends extends Component {

  constructor(props) {
    super(props)
    this.state = {
      isLike : false
    }
  }

  componentDidMount() {
    this.setState({
      isLike : this.props.isLike
    })
  }

  onClickLike = (e) => {
    e.preventDefault()
    const loginState = this.props.stateStore.loginState
    if(loginState <= 0) {
      this.props.stateStore.setPopupOn('needLogin')
      return
    }
    this.likeChange()
    const token = this.props.apiStore.getJwtToken()
    if(token === null) {
      this.props.stateStore.setPopupOn('needLogin')
      return
    }
    if(this.state.isLike === 1) {
      this.props.apiStore.delLike(token, this.props.id).then((data) => {
        if(this.props.callback != undefined) {
          this.props.callback()
        }
      })
        .catch(err=> {
        })
    } else {
      this.props.apiStore.putLike(token, this.props.id).then((data) => {
      })
        .catch(err=> {
        })
    }
  }

  likeChange() {
    this.setState({
      isLike : 1 - this.state.isLike
    })
  }

  renderTagRow() {
    let view = []

    if(this.props.rating != null &&this.props.rating > 0) {
      view.push(
        <li key={-1}>
          <span className="rising-ofriends-item-star"></span>
          <span className="rising-ofriends-item-rating">{this.props.rating.toFixed(1)}</span>
        </li>
      )
    }
    if(this.props.tags != null) {
      let key = 0
      for(var i=0; i<this.props.tags.length; i++) {
        let tag = this.props.tags[i]
        view.push(
          <li key={key}>
            <span className={"rising-ofriends-item-tag-"+tag}></span>
          </li>
        )
        key = key + 1
      }
    }
    return (<Fragment>{view}</Fragment>)
  }

  render() {
    const linkTo = "/product/" + this.props.id
    const imgDUrl = this.props.dataStore.productsImgUrl + this.props.id + "_0.jpg"
    const price = this.props.price
    const discount = this.props.discount
    return (
      <div className= {this.props.eventFinished === true ? "event-finished-item" : "rising-ofriends-item"} >
        <Link className="rising-ofriends-item-link" to={linkTo}>
          <div className="rising-ofriends-item-thumb-container">
            <img className="rising-ofriends-item-thumb"
                 alt=''
                 src={imgDUrl === undefined || imgDUrl === null ? "" : imgDUrl}
                 onError={(e)=>e.target.setAttribute("src", this.props.dataStore.imageUrl + "01_main_thumbnail_error.png")}/>
            <div className="rising-ofriends-loc-like-container">
              <span className="rising-ofriends-item-location">{this.props.loc}</span>
              <span
                id="rising-ofriends-item-like"
                className={this.state.isLike === 1 ? "like": "unlike"}
                onClick={(e) => this.onClickLike(e)}/>
            </div>
          </div>
          {
            this.props.eventFinished &&
            (
              <span className="event-finished-title">{this.props.dataStore.str.S0164}</span>
            )
          }
          <div className="rising-ofriends-item-subTitle">{this.props.subTitle}</div>
          <div className="rising-ofriends-item-title" style={{'WebkitBoxOrient': 'vertical'}}>{this.props.title}</div>
          <ul className="rising-ofriends-item-pd">
            <li className="rising-ofriends-item-discount">
              {this.props.apiStore.getFormatWon(discount)}
            </li>
            {
              price !== discount
              &&
              <li className="rising-ofriends-item-price">
                {
                  this.props.apiStore.getFormatWon(price)
                }
              </li>
            }
          </ul>
          <ul className="rising-ofriends-item-tag-container">
            { this.renderTagRow() }
          </ul>
        </Link>
      </div>
    )
  }
}

export default ItemRisingOfriends
