import React, { Component, Fragment } from 'react'
import { inject, observer } from 'mobx-react'
import { Carousel } from 'react-responsive-carousel'
import { withRouter } from 'react-router-dom'
import '../../static/scss/alliance.scss'
@inject('stateStore', 'apiStore', 'dataStore')
@withRouter
@observer
class Alliance extends Component {

  constructor(props) {
    super(props)
    this.state = {
      mounted : false
    }
  }

  componentDidMount() {
    this.setState({
      mounted : true
    })
    this.onRouteChanged()
  }

  componentDidUpdate(prevProps) {

    if (this.props.location !== prevProps.location) {
      this.onRouteChanged()
    }
  }

  componentWillUnmount() {
    this.setState({
      mounted: false
    })
  }

  onRouteChanged() {

  }

  onClickCarousel = (e, type, link) => {

    if(type === 'internal') {
      this.props.history.push(link)
    } else {
      window.location.href = link
    }

    // window.location.href = dataMain.banners[position].link
  }

  renderCarousel() {
    const banners = this.props.dataStore.dataMain.collaboration_banners
    if(banners == undefined)
      return
    if(this.state.mounted === true) {
      let gallery = []
      for(let row of banners) {
        gallery.push(
          <div
            className="all-wrapper"
            key = {row.id}
            onClick={(e) => this.onClickCarousel(e, row.type, row.link)}>
            <img
              className="mobile"
              src={row.image_mobile}/>
            <img
              className="desktop"
              src={row.image_desktop}/>
          </div>
        )
      }
      return (
        <Carousel
          className="all-carousel"
          showThumbs={false}
          showIndicators={true}
          swipeable={true}
          emulateTouch
          showArrows={false}
          showStatus={false}
          infiniteLoop={true}>
          {gallery}
        </Carousel>
      )
    }
  }

  render() {
    return (
      <Fragment>
        <div className="alliance">
          {/*

          */}
          <span className="all-title">{this.props.dataStore.str.W1051}</span>
          {this.renderCarousel()}
        </div>
      </Fragment>
    )

  }
}
export default Alliance
