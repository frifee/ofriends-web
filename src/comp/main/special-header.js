import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
import { Link, withRouter } from 'react-router-dom'

@inject('stateStore', 'apiStore', 'dataStore')
@withRouter
@observer
class SpecialHeader extends Component {

  renderTopList() {
    let topList = this.props.dataStore.sampleDailyTabs
    let tabs = []
    let key = 0
    let nowPath = this.props.location.pathname

    for(var i=0; i<topList.item.length; i++){
      let row = topList.item[i]
      let link = '/daily' + row.link
      let className = nowPath.indexOf(row.link)>0 ? "activate top-list" : "none-activate top-list"
      tabs.push(
        <Link key = {key} to={link} className={className}>
          <div>
            <span>{row.name}</span>
          </div>
        </Link>
      )
      key = key + 1
    }
    return (
      <div className="special-main-tab">{tabs}</div>
    )
  }

  renderBottomList() {
    let bottomList = this.props.dataStore.sampleDailySubTabs1
    let tabs = []
    let key = 0
    let nowPath = this.props.location.pathname

    for(var i=0; i<bottomList.item.length; i++) {
      let row = bottomList.item[i]
      let path = nowPath.split('/')
      let link = '/daily' + '/' + path[2] + row.link
      let className = nowPath.indexOf(row.link)>0 ? "activate sub-list" : "none-activate sub-list"
      tabs.push(
        <Link key = {key} to={link} className={className}>
          <div>
            <span>{row.name}</span>
          </div>
        </Link>
      )
      key = key + 1
    }
    return (
      <div className="special-sub-tab">{tabs}</div>
    )
  }

  render() {
    return (
      <div className="body-special-header">
        {this.renderTopList()}
        {this.renderBottomList()}
      </div>
    )

  }
}
export default SpecialHeader
