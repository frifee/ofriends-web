import React, { Component, Fragment } from 'react'
import { inject, observer } from 'mobx-react'
import { Link, withRouter } from 'react-router-dom'


@inject('stateStore', 'apiStore', 'dataStore')
@withRouter
@observer
class MainHeader extends Component {
  componentDidMount() {
    this.setTabPos()
  }
  componentDidUpdate(prevProps) {
    this.setTabPos()
  }

  setTabPos() {
    let nowPath = this.props.location.pathname
    let tabPos = 0
    if(nowPath.indexOf('/daily')==0) {
      tabPos = 1
    } else if(nowPath.indexOf('/special')==0) {
      tabPos = 2
    }
    this.props.stateStore.setMainHeaderTab(tabPos)
  }

  renderHeaderTab() {

    return (
      <Fragment>
        <div className="main-header">
          <Link className="main-header-recommended" to="/">
            <div>
              <span className={this.props.stateStore.mainHeaderTab === 0 ? "selected" : "unselected"}>{this.props.dataStore.str.W0152}
                <span className="main-header-dot"/>
              </span>
            </div>
          </Link>
          <Link className="main-header-daily" to="/daily/play">
            <div >
              <span className={this.props.stateStore.mainHeaderTab === 1 ? "selected" : "unselected"}>{this.props.dataStore.str.W0153}
                <span className="main-header-dot"/>
              </span>
            </div>
          </Link>
          {/*
            <Link className="main-header-special" to="/special">
              <div >
                <span className={this.props.stateStore.mainHeaderTab === 2 ? "selected" : "unselected"}>{this.props.dataStore.str.W0011}
                  <span className="main-header-dot"/>
                </span>
              </div>
            </Link>
          */}

        </div>
      </Fragment>
    )
  }

  render() {
    return (
      <div className="main-header-entity-container">
        {this.renderHeaderTab()}
      </div>

    )

  }
}
export default MainHeader
