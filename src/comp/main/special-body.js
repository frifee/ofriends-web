import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
import { Link, withRouter } from 'react-router-dom'
import ItemRisingOfriends from './item-rising-ofriends'
import Pagination from "react-js-pagination"
@inject('stateStore', 'apiStore', 'dataStore')
@withRouter
@observer
class SpecialBody extends Component {

  onClickSort = (e) => {
    this.props.stateStore.setIsDailySortOpen(!this.props.stateStore.isDailySortOpen)
  }

  renderTab() {
    let list = this.props.dataStore.sampleDailySubTabs2
    let tabs = []
    let key = 0
    let nowPath = this.props.location.pathname
    let path = nowPath.split('/')
    if(path[3]!==undefined) {
      for(var i=0; i<list.item.length; i++) {
        let row = list.item[i]
        let link = '/daily' + '/' + path[2] + '/' + path[3] + row.link
        let className = nowPath.includes(row.link) ? "activate tap-list" : "none-activate tap-list"
        tabs.push(
          <Link key = {key} to={link} className={className}>
            <div>
              <span>{row.name}</span>
            </div>
          </Link>
        )
        key = key + 1
      }
    }

    let sortList = this.props.dataStore.sampleDailySortBy
    let sortableList = []
    let sortKey = 1

    for(var i=0; i< sortList.item.length; i++) {
      let row = sortList.item[i]
      let link = this.props.location.pathname.split('/page')[0] + '?sort=' + row.link
      sortableList.push(
        <Link key = {"sort-"+sortKey} to={link}>
          <div>
            <span>{row.name}</span>
          </div>
        </Link>
      )
      sortKey = sortKey + 1
    }


    return (
      <div className="special-body-tab-entity">
        <div className="special-body-tab">{tabs}</div>
        <span className="special-alignment" onClick={(e) => this.onClickSort(e)}>{this.props.dataStore.str.W0129}</span>
        <div
          className="alignment-dropdown"
          style={{display:this.props.stateStore.isDailySortOpen === true ? 'flex' : 'none'}}>
          {this.props.stateStore.isDailySortOpen && sortableList}
        </div>
      </div>
    )
  }

  renderItems() {
    let data = this.props.dataStore.sampleRecomData
    if(data != null) {
      let view = []
      let i = 0
      for(let item of data.item) {
        i = i + 1
        view.push(
          <ItemRisingOfriends
            id = {item.id}
            key = {i}
            loc = {item.loc}
            isLike = {item.isLike}
            subTitle = {item.subTitle}
            title = {item.title}
            price = {item.price}
            discount = {item.discount}
            rating = {item.rating}
            tags = {item.tags}
            ></ItemRisingOfriends>
        )
        // if(i>0 && i%4==0) {
        //   view.push(<div className="break-flex"></div>)
        // }
      }
      return (<div className="special-entity">{view}</div>)
    }
  }
  handlePageChange(pageNumber) {
    let nowPath = this.props.location.pathname.split('/page')[0]
    nowPath = nowPath.endsWith('/') ? nowPath : nowPath + '/'
    let search = this.props.location.search
    if(search == "undefined" || search == null || search == "")
      search = ''
    this.props.history.push(nowPath + 'page/' + pageNumber + '/' + search)
  }
  renderPagination() {
    let nowPath = this.props.location.pathname
    nowPath = nowPath.endsWith('/') ? nowPath.slice(0, -1) : nowPath
    let activePage
    try {
      activePage = parseInt(nowPath.split('/').pop())
      if(isNaN(activePage)) {
        activePage = 1
      }
    } catch (exception) {
      return
    }
    return (
      <Pagination
        activePage={activePage}
        itemsCountPerPage={10}
        totalItemsCount={450}
        pageRangeDisplayed={5}
        onChange={this.handlePageChange.bind(this)}
        firstPageText={""}
        prevPageText={""}
        nextPageText={""}
        lastPageText={""}
        itemClassFirst="first-link"
        itemClassPrev="prev-link"
        itemClassNext="next-link"
        itemClassLast="last-link"
        linkClass="page-link"
        innerClass="pagination"
        itemClass="pg"
        activeClass="current"
      />
    )
  }

  render() {
    return (
      <div className="body-special-body">
        {this.renderTab()}
        {this.renderItems()}
        {this.renderPagination()}
      </div>
    )

  }
}
export default SpecialBody
