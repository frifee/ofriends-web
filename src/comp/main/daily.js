import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
import { Link, withRouter } from 'react-router-dom'
import DailyHeader from './daily-header'
import DailyBody from './daily-body'

@inject('stateStore', 'apiStore', 'dataStore')
@withRouter
@observer
class Daily extends Component {

  componentDidMount() {

    this.props.stateStore.setDailyState('from', 0)
    this.props.stateStore.setDailyState('to', 0)
    this.props.stateStore.setDailyState('count', 0)
    this.props.stateStore.setDailyState('page', 1)
    this.onRouteChanged()
    // this.props.history.listen((location, action) => {
    //
    // });
  }

  componentDidUpdate(prevProps) {
    if (this.props.location !== prevProps.location) {
      this.onRouteChanged()
    }
  }


  onRouteChanged() {
    let cate1 = this.props.match.params.cate1
    let cate2 = this.props.match.params.cate2
    let page
    try {
      page = this.props.match.params.page == undefined ? 1 : parseInt(this.props.match.params.page)
      if(isNaN(page)) {
        this.props.history.push('/daily/play')
      }
    } catch (e) {
      this.props.history.push('/daily/play')
    }
    let queryParams = this.props.location.search
    this.props.stateStore.setDailyState('page', page)
    this.props.stateStore.setDailyState('queryParams', queryParams)
    this.getProductList(page)
  }

  getProductList(page) {
    let fromIdx = (page - 1) * 20
    let toIdx = fromIdx + 20 - 1
    let range = '[' + fromIdx + ',' + toIdx + ']'
    let params = []
    let cate1 = this.props.match.params.cate1
    let cate2 = this.props.match.params.cate2
    let queryParams = this.props.stateStore.dailyState.queryParams.replace("?", "")
    params.push({
      key: 'range',
      value: range
    })
    if(cate2 == undefined) {
      params.push({
        key: 'filter',
        value: "{\"cat_major\":\"" + cate1 + "\"}"
      })
    } else {
      params.push({
        key: 'filter',
        value: "{\"cat_minor\":\"" + cate2 + "\"}"
      })
    }

    let paramsDict = {}
    let queryParamsArr = queryParams.split("&")
    for(var i=0; i<queryParamsArr.length; i++) {
      let row = queryParamsArr[i].split("=")
      paramsDict[row[0]] = row[1]
    }

    if(paramsDict['sort'] != undefined) {
      if(paramsDict['sortby'] == undefined) {
        params.push({
          key: 'sort',
          value: "[\"" + paramsDict['sort'] + "\",\"DESC\"]"
        })
      } else {
        params.push({
          key: 'sort',
          value: "[\"" + paramsDict['sort'] + "\",\""+ paramsDict['sortby'].toUpperCase() + "\"]"
        })
      }
    }

    this.props.apiStore.getProductList(localStorage.getItem('jwt'), params).then(res=>{
      let range = res.headers['content-range'].split(' ')[1]
      let fromIdx = range.split('/')[0].split('-')[0]
      let toIdx = range.split('/')[0].split('-')[1]
      let count = range.split('/')[1]
      this.props.stateStore.setDailyState('from', fromIdx)
      this.props.stateStore.setDailyState('to', toIdx)
      this.props.stateStore.setDailyState('count', count)
      this.props.stateStore.setDailyState('page', page)
      this.props.dataStore.setDataProductList([])
      this.props.dataStore.setDataProductList(res.data)
    })
    .catch(err=> {
    })
  }

  render() {
    return (
      <div className="body-daily">
        <DailyHeader></DailyHeader>
        <DailyBody></DailyBody>
      </div>
    )

  }
}
export default Daily
