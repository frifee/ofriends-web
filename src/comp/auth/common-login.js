import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
import { Link } from 'react-router-dom'
@inject('stateStore', 'apiStore', 'dataStore')
@observer
class CommonLogin extends Component {

  render() {
    return (
      <div className="common-login-entity-container">
        <span className="common-login-title">{this.props.title}</span>
        <Link className="common-email-register" to={this.props.linkEmail}>{this.props.textEmail}</Link>
        <div className="common-fb-register-container" onClick={(e) => this.props.onClickFb(e)}>
          <span className="common-fb-register-icon"></span>
          <span className="common-fb-register-text" >{this.props.textFb}</span>
        </div>
        <div className="common-kakao-register-container" onClick={(e) => this.props.onClickKakao(e)}>
          <span className="common-kakao-register-icon"></span>
          <span className="common-kakao-register-text">{this.props.textKakao}</span>
        </div>
        <Link className="common-email-login" to={this.props.linkBottom}>{this.props.textBottom}</Link>
      </div>
    )

  }
}
export default CommonLogin
