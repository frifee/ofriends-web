import React, {Component, Fragment} from 'react'
import { inject, observer } from 'mobx-react'
import {withRouter} from 'react-router-dom'
import * as Util from '../../util'
import OneBtn from "./one-btn";

// Unused
@inject('stateStore', 'apiStore', 'dataStore')
@withRouter
@observer
class PhoneVerify extends Component {

  constructor(props) {
    super(props)
    this.state = {
      sendSentence : this.props.dataStore.str.S1026,
      sendBtnName : "phone-verify-send-number inActive"
    }
  }


  componentDidMount() {
    this.props.stateStore.setPhoneVerifyState('inputName', "")
    this.props.stateStore.setPhoneVerifyState('inputBirth', "")
    this.props.stateStore.setPhoneVerifyState('inputGender', "")
    this.props.stateStore.setPhoneVerifyState('inputPhoneNumber', "")
    this.props.stateStore.setPhoneVerifyState('inputVerifyNumber', "")
    this.props.stateStore.setPhoneVerifyState('verifyNumber', 0)
    this.props.stateStore.setPhoneVerifyState('isPhoneVerified', false)
    this.props.stateStore.setPhoneVerifyState('callErrMsg', "")
    this.props.stateStore.setPhoneVerifyState('name', "")
    this.props.stateStore.setPhoneVerifyState('phoneNumber', "")
    this.props.stateStore.setPhoneVerifyState('gender', "")
    this.props.stateStore.setPhoneVerifyState('birth', "")
  }

  onClickSendVerifyNumber = (e) => {
    let phoneVerifyState = this.props.stateStore.phoneVerifyState
    if(this.alertValidation() === false) {
      return
    }
    let jwtToken = this.props.apiStore.getJwtToken()
    if(jwtToken === null) {
      this.props.history.push('/login')
      return
    }
    // let phoneRegex = /^(?:(010\d{4})|(01[1|6|7|8|9]\d{3,4}))(\d{4})$/
    // let birthRegex = /^(?:[0-9]{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[1,2][0-9]|3[0,1]))$/
    // let genderRegex = /^[1-4]{1}$/
    // if(!phoneRegex.test(phoneVerifyState.inputPhoneNumber)) {
    //   alert(this.props.dataStore.str.S1032)
    //   return
    // }
    // if(!birthRegex.test(phoneVerifyState.inputBirth)) {
    //   alert(this.props.dataStore.str.S1045)
    //   return
    // }
    // if(!genderRegex.test(phoneVerifyState.inputGender)) {
    //   alert(this.props.dataStore.str.S1045)
    //   return
    // }
    this.setState({
      sendSentence : this.props.dataStore.str.S1027,
      sendBtnName : "phone-verify-send-number inActive"
    })
    let payload = {
      'phone' : phoneVerifyState.inputPhoneNumber,
    }
    this.props.apiStore.postRequestVerifyNumber(jwtToken, payload)
      .then((res) => {
        this.props.stateStore.setPhoneVerifyState("verifyNumber", res.data.id)
        this.props.stateStore.setPhoneVerifyState("isPhoneVerified", true)
        this.props.stateStore.setPhoneVerifyState("phoneNumber", phoneVerifyState.inputPhoneNumber)
        this.props.stateStore.setPhoneVerifyState("name", phoneVerifyState.inputName)
        this.setState({sendBtnName:"phone-verify-send-number active"})
      })
      .catch((err) => {
        this.props.stateStore.setPhoneVerifyState("isPhoneVerified", false)
        this.setState({sendBtnName:"phone-verify-send-number active"})
      })
  }

  onClickPhoneVerifyConfirm = (e) => {
    let phoneVerifyState = this.props.stateStore.phoneVerifyState
    if(this.alertValidation() === false) {
      return
    }
    let jwtToken = this.props.apiStore.getJwtToken()
    if(jwtToken === null) {
      this.props.history.push('/login')
      return
    }
    let genderNumber = this.props.stateStore.phoneVerifyState.inputGender
    if(genderNumber === '1' || genderNumber === '2') {
      this.props.stateStore.setPhoneVerifyState('birth', "19" + phoneVerifyState.inputBirth)
    } else if (genderNumber === '3' || genderNumber === '4') {
      this.props.stateStore.setPhoneVerifyState('birth', "20" + phoneVerifyState.inputBirth)
    }
    if(genderNumber === '1' || genderNumber === '3') {
      this.props.stateStore.setPhoneVerifyState('gender', 'male')
    } else if (genderNumber === '2' || genderNumber === '4') {
      this.props.stateStore.setPhoneVerifyState('gender', 'female')
    } else {
      this.props.stateStore.setPhoneVerifyState('gender', 'unknown')
    }
    let payload = {
      'name' : phoneVerifyState.name,
      'cert' : phoneVerifyState.inputVerifyNumber,
      'birthday' : phoneVerifyState.birth,
      'gender' : phoneVerifyState.gender,
      'phone' : phoneVerifyState.phoneNumber
    }
    this.props.apiStore.postVerifyConfirm(jwtToken, phoneVerifyState.verifyNumber, payload)
      .then(res => {
        this.props.stateStore.setRegActiveState('isConfirmSelf', true)
        this.props.stateStore.setPhoneVerifyState('isOn', false)
        // this.props.getUserInfo()
      })
      .catch(err => {
        this.props.stateStore.setPopupOn("phoneVerifyFailed")
        if(err.response.status === 409) {
          this.props.stateStore.setPhoneVerifyState('callErrMsg', this.props.dataStore.str.S1029)
        } else if(err.response.status === 412) {
          this.props.stateStore.setPhoneVerifyState('callErrMsg', this.props.dataStore.str.S1031)
          this.props.stateStore.setPhoneVerifyState("isPhoneVerified", false)
        } else {
          this.props.stateStore.setPhoneVerifyState('callErrMsg', this.props.dataStore.str.S1006)
        }
      })
  }

  onClickClosePhoneVerify = (e) => {
    this.props.stateStore.setPhoneVerifyState('isOn', false)
  }

  onChangeInputName = (e) => {
    let name = e.target.value
    this.props.stateStore.setPhoneVerifyState("inputName", name)
  }

  onChangeInputBirth = (e) => {
    let birth = e.target.value
    let numberRegex = /^[0-9]*$/
    if(birth.trim().length <= 6 && numberRegex.test(birth)) {
      this.props.stateStore.setPhoneVerifyState("inputBirth", e.target.value)
    }
  }

  onChangeInputGender = (e) => {
    let genderNumber = e.target.value
    let numberRegex = /^[0-9]*$/
    if(genderNumber.trim().length <= 1 && numberRegex.test(genderNumber)) {
      this.props.stateStore.setPhoneVerifyState("inputGender", e.target.value)
    }
  }

  onChangeInputPhoneNumber = (e) => {
    let phoneNumber = e.target.value
    let numberRegex = /^[0-9]*$/
    if(phoneNumber.trim().length <= 11 && numberRegex.test(phoneNumber)) {
      this.props.stateStore.setPhoneVerifyState("inputPhoneNumber", e.target.value)
    }
    if(phoneNumber.length <= 0) {
      this.setState({
        sendBtnName : "phone-verify-send-number inActive"
      })
    } else {
      this.setState({
        sendBtnName : "phone-verify-send-number active"
      })
    }
  }

  onChangeInputVerifyNumber = (e) => {
    let verifyNumber = e.target.value
    let numberRegex = /^[0-9]*$/
    if(verifyNumber.trim().length <= 6 && numberRegex.test(verifyNumber)) {
      this.props.stateStore.setPhoneVerifyState("inputVerifyNumber", e.target.value)
    }
  }

  alertValidation() {
    let nameRegex = /^[A-Z|a-z|가-힣]*$/
    let phoneVerifyState = this.props.stateStore.phoneVerifyState
    let phoneRegex = /^(?:(010\d{4})|(01[1|6|7|8|9]\d{3,4}))(\d{4})$/
    let birthRegex = /^(?:[0-9]{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[1,2][0-9]|3[0,1]))$/
    let genderRegex = /^[1-4]{1}$/

    if(phoneVerifyState.inputName.trim().length < 2) {
      alert(this.props.dataStore.str.S1053)
      return false
    }

    if(!nameRegex.test(phoneVerifyState.inputName)) {
      alert(this.props.dataStore.str.S1055)
      return false
    }

    if(!phoneRegex.test(phoneVerifyState.inputPhoneNumber)) {
      alert(this.props.dataStore.str.S1032)
      return false
    }

    if(!birthRegex.test(phoneVerifyState.inputBirth)) {
      alert(this.props.dataStore.str.S1045)
      return false
    }

    if(!genderRegex.test(phoneVerifyState.inputGender)) {
      alert(this.props.dataStore.str.S1045)
      return false
    }
  }

  renderPhoneVerify() {
    const phoneVerifyState = this.props.stateStore.phoneVerifyState
    let btnClassName = (phoneVerifyState !== undefined
      && phoneVerifyState.inputPhoneNumber.length !== 0
      && phoneVerifyState.inputName.length !== 0
      && phoneVerifyState.inputGender.length === 1
      && phoneVerifyState.inputVerifyNumber.length === 6
      && phoneVerifyState.inputBirth.length === 6
      && phoneVerifyState.isPhoneVerified !== false)
      ? "phone-verify-confirm-btn active" : "phone-verify-confirm-btn inActive"
    return (
      <Fragment>
        <div className="phone-verify-modal-container">
          <div className="phone-verify-entity-container">
            <div className="phone-verify-title-con">
              <span className="phone-verify-title">{this.props.dataStore.str.W1017}</span>
              <span className="phone-verify-close" onClick={(e) => {this.onClickClosePhoneVerify(e)}}></span>
            </div>
            <div className="phone-verify-name-con">
              <span className="phone-verify-name-title">{this.props.dataStore.str.W0037}</span>
              <input
                className="phone-verify-inputName"
                type="text"
                value = {phoneVerifyState.inputName}
                placeholder={this.props.dataStore.str.W0051}
                onChange = {(e) => this.onChangeInputName(e)}
              />
            </div>

            <div className="phone-verify-birth-con">
              <span className="phone-verify-birth-title">{this.props.dataStore.str.W1018}</span>
              <input
                className="phone-verify-inputBirth"
                type="password"
                value = {phoneVerifyState.inputBirth}
                onChange = {(e) => this.onChangeInputBirth(e)}
              />
              <span className="phone-verify-birth-dash">-</span>
              <input
                className="phone-verify-inputGender"
                type="password"
                value = {phoneVerifyState.inputGender}
                onChange = {(e) => this.onChangeInputGender(e)}
              />
              <span className="phone-verify-birth-dot"></span>
              <span className="phone-verify-birth-dot"></span>
              <span className="phone-verify-birth-dot"></span>
              <span className="phone-verify-birth-dot"></span>
              <span className="phone-verify-birth-dot"></span>
              <span className="phone-verify-birth-dot"></span>
            </div>

            <div className="phone-verify-phone-number-con">
              <span className="phone-verify-phone-number-title">{this.props.dataStore.str.W0040}</span>
              <input
                className="phone-verify-input-PhoneNumber"
                type="text"
                value = {phoneVerifyState.inputPhoneNumber}
                placeholder={this.props.dataStore.str.S1025}
                onChange = {(e) => this.onChangeInputPhoneNumber(e)}
              />
            </div>
            <span
              className={this.state.sendBtnName}
              onClick={(e) => {this.onClickSendVerifyNumber()}}>{this.state.sendSentence}</span>
            <div className="phone-verify-number-con">
              <span className="phone-verify-number-title">{this.props.dataStore.str.W1019}</span>
              <input
                className="phone-verify-inputVerifyNumber"
                type="text"
                value = {phoneVerifyState.inputVerifyNumber}
                placeholder={this.props.dataStore.str.S1028}
                onChange = {(e) => this.onChangeInputVerifyNumber(e)}
              />
            </div>
            <span
              className={btnClassName}
              onClick={(e) => {this.onClickPhoneVerifyConfirm()}}>{this.props.dataStore.str.W1020}</span>
          </div>
        </div>
      </Fragment>
    )
  }

  renderPhoneVerifyErrMsg() {
    if(this.props.stateStore.popupOn === 'phoneVerifyFailed') {
      return (
        <OneBtn
          msg={this.props.stateStore.phoneVerifyState.callErrMsg}
          onClickClose={(e) => {
            this.props.stateStore.setPopupOn("")
          }}></OneBtn>
      )
    }
  }

  render() {
    return (
      <div className="phone-verify-wrapper-container">
        {
          this.props.stateStore.phoneVerifyState.isOn === true &&
          this.renderPhoneVerify()
        }
        {
          this.renderPhoneVerifyErrMsg()
        }
      </div>
    );
  }

}

export default PhoneVerify
