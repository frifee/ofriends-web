import React, { Component, Fragment } from 'react'
import { inject, observer } from 'mobx-react'
import NoContents from '../my/no-contents'
import Pagination from "react-js-pagination"
import {withRouter} from 'react-router-dom'
import OneBtn from "./one-btn";
const sampleData = {
  "data" : [
    {
      "id" : 0,
      "title" : "coupon1",
      "due_date" : "2019-11-11",
      "option" : "100원이상 사용가능",
      "priceCut" : 1000
    },
    {
      "id" : 1,
      "title" : "coupon2",
      "due_date" : "2019-11-11",
      "option" : "100원이상 사용가능",
      "priceCut" : 2000
    },
    {
      "id" : 2,
      "title" : "coupon3",
      "due_date" : "2019-11-11",
      "option" : "100원이상 사용가능",
      "priceCut" : 100
    },
    {
      "id" : 3,
      "title" : "coupon4",
      "due_date" : "2019-11-11",
      "option" : "100원이상 사용가능",
      "priceCut" : 5000
    }
  ]
}

@inject('stateStore', 'apiStore', 'dataStore')
@withRouter
@observer
class CouponSelect extends Component {

  constructor(props) {
    super(props)
  }

  componentDidMount() {
    this.onRouteChanged()
    this.state = {
      msg : ""
    }
  }

  componentDidUpdate(prevProps) {
    if (this.props.location !== prevProps.location) {
      this.onRouteChanged()
    }
  }

  onRouteChanged() {
    let jwtToken = this.props.apiStore.getJwtToken()
    if(jwtToken === null) {
      this.props.history.push('/login')
      return
    }
    this.props.stateStore.setCouponPopupOn("")
    this.props.stateStore.setMyCouponState('from', 0)
    this.props.stateStore.setMyCouponState('to', 1)
    this.props.stateStore.setMyCouponState('count', 0)
    this.props.stateStore.setMyCouponState('page', 1)
    this.props.stateStore.setMyCouponState('done', false)
    this.props.stateStore.setMyCouponState('isCouponSelected', false)
    this.getCoupons(1)
  }



  getCoupons(page) {
    const myCouponState = this.props.stateStore.myCouponState

    let fromIdx = (page - 1) * 5
    let toIdx = fromIdx + 5
    let range = '[' + fromIdx + ',' + toIdx + ']'
    let params = []
    params.push({
      key: 'range',
      value: range
    })
    let jwtToken = this.props.apiStore.getJwtToken()
    if(jwtToken === null) {
      this.props.history.push('/login')
      return
    }
    this.props.apiStore.getMiscCoupons(jwtToken, params).then(res=>{
      let data = res.data
      this.props.dataStore.setDataMiscCoupons([])
      this.props.dataStore.setDataMiscCoupons(res.data)

      let range = res.headers['content-range'].split(' ')[1]
      let fromIdx = range.split('/')[0].split('-')[0]
      let toIdx = range.split('/')[0].split('-')[1]
      let count = range.split('/')[1]

      this.props.stateStore.setMyCouponState('from', fromIdx)
      this.props.stateStore.setMyCouponState('to', toIdx)
      this.props.stateStore.setMyCouponState('count', count)
      this.props.stateStore.setMyCouponState('page', page)
      this.props.stateStore.setMyCouponState('done', true)

      const dataMiscCoupons = this.props.dataStore.dataMiscCoupons
    })
    .catch(err=>{

    })
  }

  getExpectedPrice() {
    const samplePayment = this.props.stateStore.samplePayment
    let data = sampleData.data
    let priceCut = 0
    if(samplePayment.coupon>0) {
      priceCut = this.props.price - data[samplePayment.coupon].priceCut
    }
    return priceCut
  }

  getPriceCut() {
    let dataMiscCoupons = this.props.dataStore.dataMiscCoupons
    const paymentState = this.props.stateStore.paymentState
    let priceCut = 0
    let price = paymentState.price
    for(let row of dataMiscCoupons) {
      if(row.id === paymentState.coupon) {
        if(row.type==='same_amount') {
          priceCut = row.amount
        } else if(row.type==='same_rate') {
          let expectedCut = price * (row.amount)/(100* 100)
          expectedCut = parseInt(expectedCut.toFixed(0)) * 100
          if(row.amount === 50) {
            expectedCut = Math.min(expectedCut, 20000)
          }
          priceCut = expectedCut
        }
        break
      }
    }
    return priceCut
  }

  handlePageChange(pageNumber) {
    const paymentState = this.props.stateStore.paymentState
    if(pageNumber!= paymentState.page) {
      this.getCoupons(pageNumber)
    }

  }

  onClickClose = (e) => {
    this.props.stateStore.setPopupOn("")
    this.props.stateStore.setPaymentState("coupon", -1)
  }

  onClickClosePopup = (e) => {
    this.props.stateStore.setCouponPopupOn("")
    this.setState({
      msg : ""
    })
  }

  onClickCoupon = (e, id, coupon) => {
    const paymentState = this.props.stateStore.paymentState
    // return
    // console.log(coupon)
    if(coupon.type==='same_amount') {
      if(coupon.max_discount!=null && coupon.max_discount>paymentState.price) {
        this.props.stateStore.setCouponPopupOn("couponUseErr")
        this.setState({
          msg : this.props.apiStore.getFormatWon(coupon.max_discount).replace(' ', '')
            + this.props.dataStore.str.S0171
        })
        return
      }
    }
    this.props.stateStore.setPaymentState("coupon", id)
  }

  onClickApply = (e) => {
    // const myProfileState = this.props.stateStore.myProfileState
    // const samplePayment = this.props.stateStore.samplePayment
    // let data = sampleData.data
    // if(samplePayment.coupon>=0) {
    //   this.props.stateStore.setSamplePayment("priceCut", data[samplePayment.coupon].priceCut)
    // } else {
    //   this.props.stateStore.setSamplePayment("priceCut", 0)
    //   this.props.stateStore.setSamplePayment("coupon", -1)
    // }
    this.props.stateStore.setPaymentState('priceCut', this.getPriceCut())
    this.props.stateStore.setPaymentState('isCouponSelected', true)
    this.props.stateStore.setPopupOn("")
  }

  renderInfo() {
    return (
      <div className="coupon-select-date-option-price-amount-container">
        <div className="coupon-select-date-option-price-con">
          <div className="coupon-select-date-option-con">
            <span className="coupon-select-date">{this.props.title}</span>
            <span className="coupon-select-option">{this.props.option}&nbsp;x&nbsp;{this.props.count}{this.props.dataStore.str.W0086}</span>
          </div>
          <span className="coupon-select-price">{this.props.apiStore.getFormatWon(this.props.price)}</span>
        </div>
        <div className="coupon-select-priceCut-con">
          <span className="coupon-select-priceCut-title">{this.props.dataStore.str.W0084}</span>

          <span className="coupon-select-priceCut">-{this.props.apiStore.getFormatWon(this.getPriceCut())}</span>
        </div>
      </div>
    )
  }

  renderList() {
    // const samplePayment = this.props.stateStore.samplePayment
    const paymentState = this.props.stateStore.paymentState
    const myCouponState = this.props.stateStore.myCouponState
    let data = this.props.dataStore.dataMiscCoupons
    if(myCouponState.done === false) {
      return(<Fragment></Fragment>)
    }

    if(data.length==0 && myCouponState.done === true) {
      return (
        <div className="coupon-select-title-noContents-con">
          <span className="coupon-select-coupon-title">{this.props.dataStore.str.W0088}</span>
          <NoContents
            iconClass="icon-none-available"
            text={this.props.dataStore.str.S0015}>
          </NoContents>
        </div>
      )
    } else {
      let view = []
      for(var i=0; i<data.length; i++) {
        let row = data[i]
        const dueDate = this.props.apiStore.getTime(row.expiration)
        let className = row.id == paymentState.coupon ? "selected" : "not-selected"
        view.push(
          <li
            className={className}
            onClick={(e) => this.onClickCoupon(e, row.id, row)}
            key={row.id}>
            <span className="coupon-select-coupon-name">{row.name}</span>
            <span className="coupon-select-coupon-duedate-title">
              {this.props.dataStore.str.W0058}
              &nbsp;
              {dueDate}
              &nbsp;

            </span>
          </li>
        )
      }
      return (
        <div className="coupon-select-coupon-list-container">
          <span className="coupon-select-coupon-title">{this.props.dataStore.str.W0088}</span>
          <ul className="coupon-select-coupon-items">{view}</ul>
        </div>
      )
    }
  }

  renderPagination() {
    const myCouponState = this.props.stateStore.myCouponState
    let activePage = myCouponState.page
    let totalItemsCount = myCouponState.count
    let pageRangeDisplayed = (myCouponState.count/5) >= 5 ? 5 : parseInt(myCouponState.count/5 + 1)

    if(totalItemsCount == 0) {
      return
    } else {
      return (
        <Pagination
          activePage={activePage}
          itemsCountPerPage={5}
          totalItemsCount={totalItemsCount}
          pageRangeDisplayed={pageRangeDisplayed+1}
          onChange={this.handlePageChange.bind(this)}
          firstPageText={""}
          prevPageText={""}
          nextPageText={""}
          lastPageText={""}
          itemClassFirst="first-link"
          itemClassPrev="prev-link"
          itemClassNext="next-link"
          itemClassLast="last-link"
          linkClass="page-link"
          innerClass="pagination"
          itemClass="pg"
          activeClass="current"
        />
      )
    }
  }
  renderErr() {
    const couponPopupOn = this.props.stateStore.couponPopupOn
    if(couponPopupOn === 'couponUseErr') {
      return (
        <OneBtn
          msg={this.state.msg}
          onClickClose={(e) => this.onClickClosePopup(e)}>
        </OneBtn>
      )

    }
  }
  render() {
    return (
      <div className="coupon-select-entity-container">
        <div className="coupon-select-modal-container">
          <div className="coupon-select-coupon-title-con">
            <span className="coupon-select-coupon-title">{this.props.dataStore.str.W0034}</span>
            <span  className="coupon-select-coupon-remove" onClick={(e) => this.onClickClose(e)}></span>
          </div>
          {this.renderInfo()}
          {this.renderList()}
          {this.renderPagination()}
          <span className="coupon-select-couponApply" onClick={(e) => this.onClickApply(e)}>{this.props.dataStore.str.W0089}</span>
        </div>
        {this.renderErr()}
      </div>
    )

  }
}
export default CouponSelect
