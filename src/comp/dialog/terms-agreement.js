import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
import { Link } from 'react-router-dom'
import Agree from "./agree";

@inject('stateStore', 'apiStore', 'dataStore')
@observer
class TermsAgreement extends Component {

  constructor(props) {
    super(props)
    this.state = {
      'isAgree' : false
    }
  }

  componentDidMount() {

  }

  onClickAgree = (e) => {
    this.setState(
      {'isAgree' : !this.state.isAgree}
    )
  }

  onClickClose = (e) => {
    this.props.stateStore.setPopupOn("")
  }

  onClickRegister = (e) => {
    if(this.state.isAgree) {
      this.props.onClickRegister(e)
    }
  }

  onClickUseRegPrivacy  = (e) => {
    this.props.stateStore.setRegisterEmail("usePrivacyPopup", true)
  }

  onClickCloseAgree = (e) => {
    this.props.stateStore.setRegisterEmail("usePrivacyPopup", false)
  }

  renderUsePrivacy() {
    let registerEmail = this.props.stateStore.registerEmail
    if (registerEmail.usePrivacyPopup === true) {
      return (
        <Agree
          title={this.props.dataStore.str.S1033}
          content={this.props.dataStore.str.S1037}
          agreeSentence={this.props.dataStore.str.W0091}
          onClickClose={(e) => this.onClickCloseAgree(e)}>
        </Agree>
      )
    }
  }

  render() {
    let className = this.state.isAgree ? "checked" : "not-checked"
    let btnClassName = this.state.isAgree ? "enabled" : "disabled"
    return (
      <div className="terms-agreement-entity-container">
        <div className="terms-agreement-modal-container">
          <span className="terms-agreement-title">{this.props.dataStore.str.W0111}</span>
          <span
            className="terms-agreement-clickClose"
            onClick={(e) => this.onClickClose(e)}></span>
          <div className="terms-agreement-checkbox-con" onClick={(e) => this.onClickAgree(e)}>
            <span className={className}></span>
            <span className="terms-agreement-all-agreement">{this.props.dataStore.str.W1013}</span>
          </div>
          <div className="terms-agreement-clickAgree">
            <a className="terms-agreement-terms" target="_blank" href="/terms">
              {this.props.dataStore.str.S1016}
            </a>
            <span
              className="terms-agreement-personal-information"
              onClick={(e) => {this.onClickUseRegPrivacy(e)}}>
              {this.props.dataStore.str.S1017}
            </span>
          </div>
          <span className="terms-agreement-adult-agreement">{this.props.dataStore.str.S1018}</span>
          <span
            className={btnClassName}
            onClick={(e) => this.onClickRegister(e)}>
            {this.props.dataStore.str.W0107}
          </span>
        </div>
        {this.renderUsePrivacy()}
      </div>
    )

  }
}
export default TermsAgreement
