import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
import '../../static/scss/image-popup.scss'


@inject('stateStore', 'apiStore', 'dataStore')
@observer
class ImagePopup extends Component {

  onClickBtn1 = (e) => {
    this.props.stateStore.setMainPopupOn("")
  }

  onClickBtn2 = (e) => {
    this.props.stateStore.setMainPopupOn("")
  }

  render() {
    return (
      <div className="img-popup">
        <div className="p-container">
          <div className="p-img-wrapper">
            <img
              className="p-image"
              src={(this.props.image === undefined || this.props.image === null) ? "" : "https://" + this.props.image}
              alt=''
              onError={(e)=>e.target.setAttribute("src", this.props.dataStore.imageUrl + "01_main_thumbnail_error.png")}/>
            <div className="btn-con">
              <span className="never-show" onClick={(e)=>{this.props.onClickBtn1(e)}}>{this.props.btn1}</span>
              <span className="confirm" onClick={(e)=>{this.props.onClickBtn2(e)}}>{this.props.btn2}</span>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
export default ImagePopup
