import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'

@inject('stateStore', 'apiStore', 'dataStore')
@observer
class LargerImgView extends Component {

  onClickLeft = (e) => {
    const largerImgView = this.props.stateStore.largerImgView
    this.props.stateStore.setLargerImgView('position' , largerImgView.position - 1)
  }

  onClickRight = (e) => {
    const largerImgView = this.props.stateStore.largerImgView
    this.props.stateStore.setLargerImgView('position' , largerImgView.position + 1)
  }

  onClickClose = (e) => {
    this.props.stateStore.setPopupOn("")
  }

  getImgUrl(position) {
    const largerImgView = this.props.stateStore.largerImgView
    return this.props.dataStore.reviewImgUrl + largerImgView.images[position]
  }

  render() {
    const largerImgView = this.props.stateStore.largerImgView
    return (
      <div className="larger-image-view-entity-container">
        <div className="larger-image-view-modal-container">
          <div className="larger-image-view-con">
            <img
              src={this.getImgUrl(largerImgView.position) === undefined || this.getImgUrl(largerImgView.position) === null ? "" : this.getImgUrl(largerImgView.position)}
              alt=''
              onError={(e)=>e.target.setAttribute("src", this.props.dataStore.imageUrl + "06_detailpage_review_photo_error.png")}/>
            {
              largerImgView.position > 0
              && <span className="larger-img-view-leftClick" onClick={(e) => this.onClickLeft(e)}></span>
            }
            {
              largerImgView.position < largerImgView.images.length - 1
              &&
              <span className="larger-img-view-rightClick" onClick={(e) => this.onClickRight(e)}></span>
            }
            <span className="larger-img-view-length">{largerImgView.position + 1}/{largerImgView.images.length}</span>
            <span className="larger-img-view-closeClick" onClick={(e) => this.onClickClose(e)}></span>
          </div>
        </div>
      </div>
    )
  }
}
export default LargerImgView
