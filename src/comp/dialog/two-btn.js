import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'

@inject('stateStore', 'apiStore', 'dataStore')
@observer
class TwoBtn extends Component {

  onClickBtn1 = (e) => {
    this.props.stateStore.setMainPopupOn("")
  }

  onClickBtn2 = (e) => {

    this.props.stateStore.setMainPopupOn("")
  }

  render() {
    return (
      <div className="two-btn-entity-container">
        <div className="two-btn-wrapper-con">
          <div className="two-btn-modal-container">
            <div className="two-btn-content-con">
              <span className="two-btn-content" dangerouslySetInnerHTML={{__html: this.props.content}}></span>
            </div>
            <div className="two-btn-button-con">
              <span className="two-btn-first-btn" onClick={(e)=>{this.props.onClickBtn1(e)}}>{this.props.btn1}</span>
              <span className="two-btn-second-btn" onClick={(e)=>{this.props.onClickBtn2(e)}}>{this.props.btn2}</span>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
export default TwoBtn
