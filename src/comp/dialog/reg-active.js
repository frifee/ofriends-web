import React, {Component, Fragment} from 'react'
import { inject, observer } from 'mobx-react'
import {withRouter} from 'react-router-dom'
import Agree from './agree'
import * as Util from '../../util'
import OneBtn from "./one-btn";

@inject('stateStore', 'apiStore', 'dataStore')
@withRouter
@observer
class RegActive extends Component {

  componentDidMount() {
    this.preSet()
  }

  preSet() {
    const dataUserInfo = this.props.dataStore.dataUserInfo
    this.props.stateStore.setRegActiveState('isConfirmSelf', dataUserInfo.level>=120)
    this.props.stateStore.setRegActiveState('selUsePrivacy', false)
    this.props.stateStore.setRegActiveState('selOfferPrivacy', false)
    this.props.stateStore.setRegActiveState('isOnUsePrivacy', false)
    this.props.stateStore.setRegActiveState('isOnOfferPrivacy', false)
    this.props.stateStore.setRegActiveState('storageDuration', 5)
    this.props.stateStore.setRegActiveState('contactMethodNumber', true)
    this.props.stateStore.setRegActiveState('contactMethodSms', true)
    this.props.stateStore.setRegActiveState('contactMethodEmail', true)
    // const img = new Image()
    // img.src = "//track.buzzvil.com/action/pb/cpa/default/pixel.gif" + localStorage.BuzzAd
    // img.onload = this.onloadImg()
  }

  onloadImg() {
    localStorage.removeItem('BuzzAd')
  }

  callBackCertification(context, rsp) {
    if(rsp.success) {
      context.putUserCetification(rsp)
    } else {
      var msg = '인증에 실패하였습니다.'

      alert(msg)
    }
  }

  putUserCetification = (rsp) => {
    let payload = {
      'level' : 20,
      'uuid' : rsp.imp_uid
    }
    let jwtToken = this.props.apiStore.getJwtToken()
    if(jwtToken === null) {
      this.props.history.push('/login')
      return
    }
    this.props.apiStore.putUserSetting(jwtToken, payload)
      .then(res => {

        this.props.stateStore.setRegActiveState('isConfirmSelf', true)
        this.props.getUserInfo()
      })
      .catch(err => {
      })
  }

  isAvailableReg() {
    const regActiveState = this.props.stateStore.regActiveState
    let isAvailable = false
    switch(regActiveState.regCase) { // case 1(휴대폰 인증만, 결제의 경우), case2(본인인증 + 마케팅 동의)
      case 1:
        isAvailable = regActiveState.isConfirmSelf === true
        break
      case 2:
        isAvailable = regActiveState.isConfirmSelf === true
          &&regActiveState.selUsePrivacy === true
          &&regActiveState.selOfferPrivacy === true
          &&(
            regActiveState.contactMethodNumber === true
            ||
            regActiveState.contactMethodSms === true
            ||
            regActiveState.contactMethodEmail === true
          )
    }
    return isAvailable
  }

  onClickClose = (e) => {
    this.preSet()
    this.props.stateStore.setRegActiveState('isOn', false)
  }

  onClickVerify = (e) => {
    // this.props.stateStore.setPhoneVerifyState('isOn', true)
    //  TODO 2019-11-21 인증 과정의 변화로(일시적으로 폰 인증으로 전환),
    //  TODO phone-verify.js 파일 안의 onClickPhoneVerifyConfirm 메소드로 옮겨놓고, 잠시 주석 처리 함.


    // TODO 개인인증은 여기 두줄 풀면 됨
    Util.impInit()
    Util.impCertification(this, this.callBackCertification)
  }

  onClickUsePrivacy = (e) => {
    const regActiveState = this.props.stateStore.regActiveState
    if(regActiveState.selUsePrivacy) {
      this.props.stateStore.setRegActiveState('selUsePrivacy', false)
    }
  }

  onClickShowUsePrivacy = (e) => {
    const regActiveState = this.props.stateStore.regActiveState
    this.props.stateStore.setRegActiveState('isOnUsePrivacy', true)
    this.props.stateStore.setRegActiveState('selUsePrivacy', true)
    // if(regActiveState.selOfferPrivacy === false) {
    //   this.props.stateStore.setRegActiveState('isOnUsePrivacy', true)
    // } else {
    //   this.props.stateStore.setRegActiveState('selUsePrivacy', false)
    // }
  }

  onClickOfferPrivacy = (e) => {
    const regActiveState = this.props.stateStore.regActiveState
    if(regActiveState.selOfferPrivacy) {
      this.props.stateStore.setRegActiveState('selOfferPrivacy', false)
    }
  }

  onClickShowOfferPrivacy = (e) => {
    this.props.stateStore.setRegActiveState('isOnOfferPrivacy', true)
    this.props.stateStore.setRegActiveState('selOfferPrivacy', true)
    // const regActiveState = this.props.stateStore.regActiveState
    // if(regActiveState.selOfferPrivacy === false) {
    //   this.props.stateStore.setRegActiveState('isOnOfferPrivacy', true)
    // } else {
    //   this.props.stateStore.setRegActiveState('selOfferPrivacy', false)
    // }
  }

  onClickStorageDuration = (e, duration) => {
    this.props.stateStore.setRegActiveState('storageDuration', duration)
  }

  onClickContactMethod = (e, type) => {
    const regActiveState = this.props.stateStore.regActiveState
    switch (type) {
      case 0:
        this.props.stateStore.setRegActiveState('contactMethodNumber', !regActiveState.contactMethodNumber)

        break
      case 1:
        this.props.stateStore.setRegActiveState('contactMethodSms', !regActiveState.contactMethodSms)
        break
      case 2:
        this.props.stateStore.setRegActiveState('contactMethodEmail', !regActiveState.contactMethodEmail)
        break
      default:
        break
    }
  }

  onClickRegister = (e) => {
    if(this.isAvailableReg() === true) {
      const regActiveState = this.props.stateStore.regActiveState
      const phoneVerifyState = this.props.stateStore.phoneVerifyState
      let condition = regActiveState.isConfirmSelf === true
        &&regActiveState.selUsePrivacy === true
        &&regActiveState.selOfferPrivacy === true
        &&(
          regActiveState.contactMethodNumber === true
          ||
          regActiveState.contactMethodSms === true
          ||
          regActiveState.contactMethodEmail === true
        )
      if(condition === true) { // 본인확인 + 마케팅 동의
        if(regActiveState.contactMethodSms === false) {
          this.props.stateStore.setPopupOn("needSmsAgree")
          return
        }
        let mkt_contact = ((regActiveState.contactMethodNumber === true) ? 100 : 0)
          + ((regActiveState.contactMethodSms === true) ? 10 : 0)
          + ((regActiveState.contactMethodEmail === true) ? 1 : 0)
        let payload = {
          'mkt_consent' : 11,
          'mkt_year' : regActiveState.storageDuration,
          'mkt_contact' : mkt_contact,
          'level' : 120
        }
        let jwtToken = this.props.apiStore.getJwtToken()
        if(jwtToken === null) {
          this.props.history.push('/login')
          return
        }
        this.props.apiStore.putUserSetting(jwtToken, payload)
          .then(res => {
            this.preSet()
            this.props.stateStore.setPaymentState('activeUser', true)
            this.props.stateStore.setMyProfileState('activeCustomer', 2)
            this.props.stateStore.setRegActiveState('isOn', false)
            // this.props.stateStore.setRegActiveState('regEndMsg', regActiveState.regCase === 2 ? this.props.dataStore.str.S1043 : '')
            // this.props.stateStore.setSamplePayment("activeUser", true)
            this.props.getUserInfo()
            this.props.stateStore.setRegActiveState('regEndMsg', this.props.dataStore.str.S1043)
            this.props.stateStore.setPopupOn('regEndMsg')
            let length = localStorage.BuzzAd.length
            if(length>0) {
              if (localStorage.BuzzAd == null) { localStorage.BuzzAd = ""; }
              const img = new Image()
              img.src = "//track.buzzvil.com/action/pb/cpa/default/pixel.gif" + localStorage.BuzzAd
              img.onload = this.onloadImg()

            }
          })
          .catch(err => {
          })
      } else { // 본인 확인만
        this.props.stateStore.setPaymentState('activeUser', true)
        this.props.stateStore.setRegActiveState('isOn', false)
        this.props.getUserInfo()
        this.props.stateStore.setRegActiveState('regEndMsg', this.props.dataStore.str.S1013)
        this.props.stateStore.setPopupOn('regEndMsg')
      }
    }

  }

  onClickCloseAgree = (e, isAgree) => {
    const regActiveState = this.props.stateStore.regActiveState
    // if(regActiveState.isOnUsePrivacy === true && isAgree == true) {
    //   this.props.stateStore.setRegActiveState('selUsePrivacy', isAgree)
    // }
    // if(regActiveState.isOnOfferPrivacy === true && isAgree == true) {
    //   this.props.stateStore.setRegActiveState('selOfferPrivacy', isAgree)
    // }
    this.props.stateStore.setRegActiveState('isOnUsePrivacy', false)
    this.props.stateStore.setRegActiveState('isOnOfferPrivacy', false)
  }

  onClickCloseDialog = (e) => {
    this.props.stateStore.setPopupOn("")
  }

  renderRegBtn() {
    return (
      <span
        className = {this.isAvailableReg() === true ? 'enabled' : 'not-enabled'}
        onClick={(e) => this.onClickRegister(e)}>{this.props.dataStore.str.W0050}</span>
    )
  }

  renderAgree() {
    const regActiveState = this.props.stateStore.regActiveState
    if(regActiveState.isOnUsePrivacy == true) {
      return (
        <Agree
          title={this.props.dataStore.str.S1034}
          content={this.props.dataStore.str.S1038}
          agreeSentence={this.props.dataStore.str.W0151}
          onClickClose={(e, isAgree) => this.onClickCloseAgree(e, isAgree)}>
        </Agree>
      )
    }
    if(regActiveState.isOnOfferPrivacy == true) {
      return (
        <Agree
          title={this.props.dataStore.str.S1036}
          content={this.props.dataStore.str.S1039}
          agreeSentence={this.props.dataStore.str.W0151}
          onClickClose={(e, isAgree) => this.onClickCloseAgree(e, isAgree)}>
        </Agree>
      )
    }
  }


  render() {
    const regActiveState = this.props.stateStore.regActiveState
    return (
      <div className="reg-active-entity-container">
        <div className="reg-active-modal-container">
          <div className="reg-active-modal-wrapper">
            <div className="reg-active-modal-header">
              <span className="reg-active-modal-title">{regActiveState.title}</span>
              <span className="reg-active-modal-close-btn" onClick={(e) => this.onClickClose(e)}></span>
            </div>
            <div className="reg-active-event-image-wrapper">
              <span className="reg-active-event-image"></span>
            </div>
            <div className="reg-active-modal-body">
              {/*<span className="reg-active-modal-sub-title">{this.props.dataStore.str.W0133}</span>*/}
              {/*<span className="reg-active-modal-sub-desc" dangerouslySetInnerHTML={{__html: this.props.dataStore.str.S0032}}></span>*/}
              <span className="reg-active-modal-identify" dangerouslySetInnerHTML={{__html: regActiveState.idTitle}}></span>
              {
                regActiveState.isConfirmSelf
                  ?
                  <span className="reg-active-modal-identify-completed">{this.props.dataStore.str.W0149}</span>
                  :
                  <div className="reg-active-modal-phone-id-con" onClick={(e) => this.onClickVerify(e)}>
                    <span className="reg-active-modal-phone-id-icon"></span>
                    <span className="reg-active-modal-phone-id-title">{this.props.dataStore.str.W0148}</span>
                  </div>
              }
              <span
                className="reg-active-modal-marketing-agreement"
                dangerouslySetInnerHTML={{__html: regActiveState.mktTitle}}>
              </span>
              <div
                className="reg-active-modal-one-check-btn-con">
                <span
                  className = {regActiveState.selUsePrivacy ? "checked" : "unchecked"}
                  onClick= {regActiveState.selUsePrivacy ? (e) => this.onClickUsePrivacy(e) : (e) => {this.onClickShowUsePrivacy(e)}}>
                </span>
                <span
                  onClick={(e) => this.onClickShowUsePrivacy(e)}
                  className="reg-active-modal-one-check-btn">{this.props.dataStore.str.W0136}</span>
                <span
                  onClick={(e) => this.onClickShowUsePrivacy(e)}
                  className="reg-active-modal-one-check-arrow"></span>
              </div>
              <div
                className="reg-active-modal-two-check-btn-con">
                <span
                  className = {regActiveState.selOfferPrivacy ? "checked" : "unchecked"}
                  onClick= {regActiveState.selOfferPrivacy ? (e) => this.onClickOfferPrivacy(e) : (e) => {this.onClickShowOfferPrivacy(e)}}>
                </span>
                <span
                  onClick={(e) => this.onClickShowOfferPrivacy(e)}
                  className="reg-active-modal-two-check-btn">{this.props.dataStore.str.W0137}</span>
                <span
                  onClick={(e) => this.onClickShowOfferPrivacy(e)}
                  className="reg-active-modal-two-check-arrow"></span>
              </div>
              <span className="reg-active-modal-id-period-title">{this.props.dataStore.str.W0143}</span>
              <div className="reg-active-modal-id-period-container">
                <div
                  className = {regActiveState.storageDuration === 5
                  && regActiveState.selUsePrivacy === true
                  && regActiveState.selOfferPrivacy === true
                    ? "checked" : "unchecked"}
                  onClick={(e) => this.onClickStorageDuration(e, 5)}>
                  {
                    regActiveState.storageDuration == 5 &&
                    <span className="reg-active-modal-id-period-checkbox"></span>
                  }
                  <span className="reg-active-modal-id-period-year">{this.props.dataStore.str.W0138}</span>
                </div>
                <div
                  className = {regActiveState.storageDuration === 4
                  && regActiveState.selUsePrivacy === true
                  && regActiveState.selOfferPrivacy === true
                    ? "checked" : "unchecked"}
                  onClick={(e) => this.onClickStorageDuration(e, 4)}>
                  {
                    regActiveState.storageDuration == 4 &&
                    <span className="reg-active-modal-id-period-checkbox"></span>
                  }
                  <span className="reg-active-modal-id-period-year">{this.props.dataStore.str.W0139}</span>
                </div>
                <div
                  className = {regActiveState.storageDuration === 3
                  && regActiveState.selUsePrivacy === true
                  && regActiveState.selOfferPrivacy === true
                    ? "checked" : "unchecked"}
                  onClick={(e) => this.onClickStorageDuration(e, 3)}>
                  {
                    regActiveState.storageDuration == 3 &&
                    <span className="reg-active-modal-id-period-checkbox"></span>
                  }
                  <span className="reg-active-modal-id-period-year">{this.props.dataStore.str.W0140}</span>
                </div>
                <div
                  className = {regActiveState.storageDuration === 2
                  && regActiveState.selUsePrivacy === true
                  && regActiveState.selOfferPrivacy === true
                    ? "checked" : "unchecked"}
                  onClick={(e) => this.onClickStorageDuration(e, 2)}>
                  {
                    regActiveState.storageDuration == 2 &&
                    <span className="reg-active-modal-id-period-checkbox"></span>
                  }
                  <span className="reg-active-modal-id-period-year">{this.props.dataStore.str.W0141}</span>
                </div>
                <div
                  className = {regActiveState.storageDuration === 1
                  && regActiveState.selUsePrivacy === true
                  && regActiveState.selOfferPrivacy === true
                    ? "checked" : "unchecked"}
                  onClick={(e) => this.onClickStorageDuration(e, 1)}>
                  {
                    regActiveState.storageDuration == 1 &&
                    <span className="reg-active-modal-id-period-checkbox"></span>
                  }
                  <span className="reg-active-modal-id-period-year">{this.props.dataStore.str.W0142}</span>
                </div>
              </div>
              <span className="reg-active-modal-contact-method-title">{this.props.dataStore.str.W0144}</span>
              <div className="reg-active-modal-contact-method-container">
                <div
                  className = {regActiveState.contactMethodSms === true
                  && regActiveState.selUsePrivacy === true
                  && regActiveState.selOfferPrivacy === true
                    ? "checked" : "unchecked"}
                  onClick={(e) => this.onClickContactMethod(e, 1)}>
                  {
                    regActiveState.contactMethodSms == true &&
                    <span className="reg-active-modal-id-period-checkbox"></span>
                  }
                  <span className="reg-active-modal-contact-method-sms">{this.props.dataStore.str.W1047}</span>
                </div>
                <div
                  className = {regActiveState.contactMethodNumber === true
                  && regActiveState.selUsePrivacy === true
                  && regActiveState.selOfferPrivacy === true
                    ? "checked" : "unchecked"}
                  onClick={(e) => this.onClickContactMethod(e, 0)}>
                  {
                    regActiveState.contactMethodNumber == true &&
                    <span className="reg-active-modal-id-period-checkbox"></span>
                  }
                  <span className="reg-active-modal-contact-method-phone">{this.props.dataStore.str.W0145}</span>
                </div>

                <div
                  className = {regActiveState.contactMethodEmail === true
                  && regActiveState.selUsePrivacy === true
                  && regActiveState.selOfferPrivacy === true
                    ? "checked" : "unchecked"}
                  onClick={(e) => this.onClickContactMethod(e, 2)}>
                  {
                    regActiveState.contactMethodEmail == true &&
                    <span className="reg-active-modal-id-period-checkbox"></span>
                  }
                  <span className="reg-active-modal-contact-method-email">{this.props.dataStore.str.W0147}</span>
                </div>
              </div>
            </div>
            <div className="reg-info">
              <span>{this.props.dataStore.str.S0166}</span>
            </div>
            {this.renderRegBtn()}
          </div>
          {this.renderAgree()}
        </div>
      </div>
    )

  }
}
export default RegActive
