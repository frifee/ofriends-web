import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
import {withRouter} from 'react-router-dom'
import TwoBtn from './two-btn'

@inject('stateStore', 'apiStore', 'dataStore')
@withRouter
@observer
class DelRecomCode extends Component {

  onClickCancel = (e) => {
    this.props.stateStore.setMyProfileState('recomCode', 2)
  }

  onClickDelete = (e) => {

    let jwtToken = this.props.apiStore.getJwtToken()
    if(jwtToken === null) {
      this.props.history.push('/login')
      return
    }
    let payload = {
      'recommender' : null
    }
    this.props.apiStore.putUserSetting(jwtToken, payload)
      .then(res => {
        this.props.stateStore.setMyProfileState('recomCode', 0)
        this.props.getUserInfo()
      })
      .catch(err => {
      })
  }

  render() {
    return (
      <TwoBtn
        content={this.props.dataStore.str.S0006}
        onClickBtn1={(e) => this.onClickCancel(e)}
        onClickBtn2={(e) => this.onClickDelete(e)}
        btn1 = {this.props.dataStore.str.W0053}
        btn2 = {this.props.dataStore.str.W0052}></TwoBtn>
    )

  }
}
export default DelRecomCode
