import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'

@inject('stateStore', 'apiStore', 'dataStore')
@observer
class OneBtn extends Component {

  onClickClose = (e) => {
    this.props.stateStore.setPopupOn("")
  }

  render() {
    return (
      <div className="one-btn-entity-container">
        <div className="one-btn-wrapper-con">
          <div className="one-btn-modal-container">
            <div className="one-btn-msg-con">
              <span className="one-btn-msg" dangerouslySetInnerHTML={{__html: this.props.msg}}></span>
            </div>
            <div className="one-btn-confirm-con" onClick={(e) => this.props.onClickClose(e)}>
              <span className="one-btn-confirm">{this.props.dataStore.str.W0091}</span>
            </div>
          </div>
        </div>
      </div>
    )

  }
}
export default OneBtn
