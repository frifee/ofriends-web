import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'

@inject('stateStore', 'apiStore', 'dataStore')
@observer
class Agree extends Component {

  render() {
    return (
      <div className="agree-entity-container">
        <div className="agree-container">
          <div className="agree-title-close-con">
            <span className="agree-con-title">{this.props.title}</span>
            <span
              className="agree-con-close"
              onClick={(e) => this.props.onClickClose(e, false)}>
              {this.props.dataStore.str.W0150}
            </span>
          </div>
          <span
            className="agree-content"
            dangerouslySetInnerHTML={{__html: this.props.content}}></span>
          <span
            className="agree-con-agreement"
            onClick={(e) => this.props.onClickClose(e, true)}>
            {this.props.agreeSentence}
          </span>
        </div>
      </div>
    )

  }
}
export default Agree
