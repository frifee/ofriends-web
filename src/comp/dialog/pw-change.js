import React, { Component, Fragment } from 'react'
import { inject, observer } from 'mobx-react'
import {withRouter} from 'react-router-dom'
import { sha256 } from 'js-sha256'
@inject('stateStore', 'apiStore', 'dataStore')
@withRouter
@observer
class PwChange extends Component {

  componentDidMount() {
    this.props.stateStore.setPwChangeState("inputPrevPw", "")
    this.props.stateStore.setPwChangeState("inputNowPw", "")
    this.props.stateStore.setPwChangeState("inputRepeatPw", "")
    this.props.stateStore.setPwChangeState("inputErrMsg", "")
    this.props.stateStore.setPwChangeState("callErrMsg", "")
    this.props.stateStore.setMyProfileState('checkPw', 0)
  }

  onChangePrevPw = (e) => {
    this.props.stateStore.setPwChangeState("inputPrevPw", e.target.value)
  }

  onChangeNowPw = (e) => {
    this.props.stateStore.setPwChangeState("inputNowPw", e.target.value)
  }

  onChangeRepeatPw = (e) => {
    this.props.stateStore.setPwChangeState("inputRepeatPw", e.target.value)
  }

  onClickRemovePrevPw = (e) => {
    this.props.stateStore.setPwChangeState("inputPrevPw", "")
  }

  onClickRemovNowPw = (e) => {
    this.props.stateStore.setPwChangeState("inputNowPw", "")
  }

  onClickRemoveRepeatPw = (e) => {
    this.props.stateStore.setPwChangeState("inputRepeatPw", "")
  }

  onClickSave = (e) => {
    this.props.stateStore.setPwChangeState("isErrPw", false)
    this.props.stateStore.setPwChangeState("isErrPwConfirm", false)
    const pwChangeState = this.props.stateStore.pwChangeState
    const pwErrMsg = this.props.dataStore.str.S1003
    const pwNowPwErrMsg = this.props.dataStore.str.S1008
    const pwRepeatPwErrMsg = this.props.dataStore.str.S1014
    const pwRegex = /^((?=.*\d)(?=.*[a-z])|(?=.*\d)(?=.*[A-Z])|(?=.*[a-z])(?=.*[A-Z])|(?=.*\d)(?=.*\W)|(?=.*[a-z])(?=.*\W)|(?=.*[A-Z])(?=.*\W))[A-Za-z\d!@#$%^&*]{10,}$/
    if(pwChangeState.inputNowPw === pwChangeState.inputPrevPw) {
      this.props.stateStore.setPwChangeState("isErrPw", true)
      this.props.stateStore.setPwChangeState("inputErrMsg", pwNowPwErrMsg)
      this.props.stateStore.setMyProfileState('checkPw', 1)
      return
    }
    if(pwChangeState.inputNowPw !== pwChangeState.inputRepeatPw) {
      this.props.stateStore.setPwChangeState("isErrPwConfirm", true)
      this.props.stateStore.setPwChangeState("inputErrMsg", pwRepeatPwErrMsg)
      this.props.stateStore.setMyProfileState('checkPw', 1)
      return
    }
    if(!pwRegex.test(pwChangeState.inputNowPw)) { // 비밀번호 정규식
      this.props.stateStore.setPwChangeState("isErrPw", true)
      this.props.stateStore.setPwChangeState("inputErrMsg", pwErrMsg)
      // this.props.stateStore.setMyProfileState('checkPw', 1)
      return
    }
    this.changePw()
  }

  changePw = () => {
    const pwChangeState = this.props.stateStore.pwChangeState
    let jwtToken = this.props.apiStore.getJwtToken()
    if(jwtToken === null) {
      this.props.history.push('/login')
      return
    }
    let payload = {
      "old_password" : sha256(pwChangeState.inputPrevPw),
      "new_password" : sha256(pwChangeState.inputNowPw)
    }
    this.props.apiStore.putUserPassword(jwtToken, payload).then(res=>{
      this.props.stateStore.setMyProfileState('pwChange', 0)
    })
    .catch(err=> {
      if(err.response.status==409) {
        this.props.stateStore.setPwChangeState("callErrMsg", this.props.dataStore.str.S0010)
      } else {
        this.props.stateStore.setPwChangeState("callErrMsg", this.props.dataStore.str.S1006)
      }
      this.props.stateStore.setMyProfileState('checkPw', 1)
      this.pwChangeFailedPopup()
    })
  }

  onClickClose = (e) => {
    this.props.stateStore.setMyProfileState('pwChange', 0)
  }

  pwChangeFailedPopup() {
    this.props.stateStore.setPwChangeState("isOnNoti", true)
    setTimeout(() => {
      this.props.stateStore.setPwChangeState("isOnNoti", false)
      this.props.stateStore.setLoginEmail("callErrMsg", "")
    }, 2000)
  }

  renderNoti() {
    const pwChange = this.props.stateStore.pwChangeState
    if(pwChange.isOnNoti) {
      return (
          <div className="pw-change-notice-msg">{pwChange.callErrMsg}</div>
      )
    }
  }

  render() {
    const pwChangeState = this.props.stateStore.pwChangeState
    let btnClassName = (pwChangeState !== undefined
        && pwChangeState.inputNowPw.length !== 0
        && pwChangeState.inputPrevPw.length !== 0
        && pwChangeState.inputRepeatPw.length !== 0)
        ? "pw-change-button-active" : "pw-change-button-inactive"
    return (
      <Fragment>
        <div className="pw-change-modal-entity-container">
          <div className="pw-change-modal-container">
            <div className="pw-change-title-con">
              <div>
                <span>{this.props.dataStore.str.W0054}</span>
              </div>
              <span className="pw-change-popup-x" onClick={(e) => this.onClickClose(e)}></span>
            </div>
            <div className="pw-change-input-con">
              <div className="pw-change-prev-password-con">
                <input
                  type="password"
                  value={pwChangeState.inputPrevPw}
                  className="pw-change-prev-password"
                  onChange={(e) => this.onChangePrevPw(e)}
                  placeholder={this.props.dataStore.str.S0007}/>
                {
                  pwChangeState.inputPrevPw.length > 0
                  && <span className="pw-change-pw-prev-remove-btn" onClick={(e) => this.onClickRemovePrevPw(e)}></span>
                }
              </div>
              <div className="pw-change-now-password-con">
                <input
                  type="password"
                  value={pwChangeState.inputNowPw}
                  className="pw-change-now-password"
                  onChange={(e) => this.onChangeNowPw(e)}
                  placeholder={this.props.dataStore.str.S0008}/>
                {
                  pwChangeState.inputNowPw.length > 0
                  && <span className="pw-change-pw-now-remove-btn" onClick={(e) => this.onClickRemovNowPw(e)}></span>
                }
                {
                  pwChangeState.isErrPw === true
                  && <span className="pw-change-pw-now-err-msg">{pwChangeState.inputErrMsg}</span>
                }
              </div>
              <div className="pw-change-repeat-password-con">
                <input
                  type="password"
                  value={pwChangeState.inputRepeatPw}
                  className="pw-change-repeat-password"
                  onChange={(e) => this.onChangeRepeatPw(e)}
                  placeholder={this.props.dataStore.str.S0009}/>
                {
                  pwChangeState.inputRepeatPw.length > 0
                  && <span className="pw-change-pw-repeat-remove-btn" onClick={(e) => this.onClickRemoveRepeatPw(e)}></span>
                }
                {
                  pwChangeState.isErrPwConfirm === true
                  && <span className="pw-change-pw-repeat-err-msg">{pwChangeState.inputErrMsg}</span>
                }
              </div>
            </div>
            <div className="pw-change-button-con">
              <span className={btnClassName} onClick={(e) => this.onClickSave(e)}>{this.props.dataStore.str.W0048}</span>
            </div>
          </div>
          {this.renderNoti()}
        </div>
      </Fragment>

    )

  }
}
export default PwChange
