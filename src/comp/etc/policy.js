import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
import '../../static/scss/policy.scss'

@inject('stateStore', 'apiStore', 'dataStore')
@observer
class Policy extends Component {

  render() {
    return (
      <div className="policy-privacy-entity-container">
        <div className="policy-privacy-title">{this.props.title}</div>
        <div className="policy-privacy-contents">{this.props.contents}</div>
      </div>
    )

  }
}
export default Policy
