import React, {Component, Fragment} from 'react'
import { inject, observer } from 'mobx-react'
import queryString from 'query-string'
import Err from './err'
import OneBtn from "../dialog/one-btn";
import '../../static/scss/finish-payment-mobile.scss'

@inject('stateStore', 'apiStore', 'dataStore')
@observer
class FinishPaymentMobile extends Component {

  constructor(props) {
    super(props)
    this.state = {
      isErrPage : false,
      isSuccess: false,
      isErr : false,
      resMsg : '',
      errMsg : ''
    }
  }


  componentDidMount() {
    this.onRouteChanged()
  }

  onRouteChanged() {
    const parsed = queryString.parse(this.props.location.search)
    const impUid = parsed.imp_uid
    const impSuccess = parsed.imp_success
    const errorMsg = parsed.error_msg === undefined ? this.props.dataStore.str.S1058 : parsed.error_msg
    const merchantUid = parsed.merchant_uid
    let originalPrice = parsed.original_price === undefined ? 0 : Number(parsed.original_price)
    let coupon = parsed.coupon === undefined ? 0 : Number(parsed.coupon)
    let payload = {
      'coupon' : coupon,
      'imp_uid' : impUid,
      'original_price' : originalPrice
    }
    if(impUid !== undefined && merchantUid !== undefined && impSuccess === 'true') {
      let jwtToken = this.props.apiStore.getJwtToken()
      // if(jwtToken === null) {
      //   this.props.history.push('/login')
      //   return
      // }
      this.props.apiStore.postMobilePayComplete(jwtToken, merchantUid, payload)
        .then(res => {
          if(res.status === 201) {
            this.setState({resMsg : this.props.dataStore.str.S1062})
          } else {
            this.setState({resMsg : this.props.dataStore.str.S1061})
          }
          this.setState({isSuccess : true})
        })
        .catch(err => {
          this.setState({
            isErr : true,
            errMsg : this.props.dataStore.str.S1058
          })
        })
    } else {
      if(impSuccess === 'false') {
        this.setState({
          isErr : true,
          errMsg : errorMsg
        })
      } else {
        this.setState({'isErrPage' : true})
      }
    }
  }

  renderSuccessMsg() {
    if(this.state.isSuccess) {
      return (
        <OneBtn
          msg={this.state.resMsg}
          onClickClose={(e) => {
            this.setState({isSuccess : false})
            this.props.history.push("/")
          }}></OneBtn>
      )
    }
  }

  renderErrMsg() {
    if(this.state.isErr) {
      return (
        <OneBtn
          msg={this.state.errMsg}
          onClickClose={(e) => {
            this.setState({isErr : false})
            this.props.history.push("/")
          }}></OneBtn>
      )
    }
  }

  render() {
    return (
      <div className="finish-payment-mobile-entire">
        {
          this.state.isErrPage &&
          <Err></Err>
        }
        {this.renderErrMsg()}
        {this.renderSuccessMsg()}
      </div>
    )

  }
}
export default FinishPaymentMobile
