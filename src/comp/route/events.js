import React, { Component, Fragment } from 'react'
import { inject, observer } from 'mobx-react'
import ItemRisingOfriends from '../main/item-rising-ofriends'
import Pagination from "react-js-pagination"
import '../../static/scss/events-route.scss'
import NoContents from "../my/no-contents";
const data = [
  {
    "id":123843,
    "frip_type":"ticket",
    "title":"[동반할인] 아늑한 다이닝이있는 쿠킹 클래스 (예약 가능)",
    "catch_phrase":"요리도 배우고 식사도하고, 1인 1실습",
    "price":0,
    "original_price":39000,
    "rating":0,
    "location":"서울 강서구 마곡동",
    "is_hot":0,
    "is_only":0,
    "is_new":0,
    "like":0
  },
  {
    "id":123843,
    "frip_type":"ticket",
    "title":"[동반할인] 아늑한 다이닝이있는 쿠킹 클래스 (예약 가능)",
    "catch_phrase":"요리도 배우고 식사도하고, 1인 1실습",
    "price":0,
    "original_price":39000,
    "rating":0,
    "location":"서울 강서구 마곡동",
    "is_hot":0,
    "is_only":0,
    "is_new":0,
    "like":0
  },
  {
    "id":123843,
    "frip_type":"ticket",
    "title":"[동반할인] 아늑한 다이닝이있는 쿠킹 클래스 (예약 가능)",
    "catch_phrase":"요리도 배우고 식사도하고, 1인 1실습",
    "price":0,
    "original_price":39000,
    "rating":0,
    "location":"서울 강서구 마곡동",
    "is_hot":0,
    "is_only":0,
    "is_new":0,
    "like":0
  },
  {
    "id":123843,
    "frip_type":"ticket",
    "title":"[동반할인] 아늑한 다이닝이있는 쿠킹 클래스 (예약 가능)",
    "catch_phrase":"요리도 배우고 식사도하고, 1인 1실습",
    "price":0,
    "original_price":39000,
    "rating":0,
    "location":"서울 강서구 마곡동",
    "is_hot":0,
    "is_only":0,
    "is_new":0,
    "like":0
  },
  {
    "id":123843,
    "frip_type":"ticket",
    "title":"[동반할인] 아늑한 다이닝이있는 쿠킹 클래스 (예약 가능)",
    "catch_phrase":"요리도 배우고 식사도하고, 1인 1실습",
    "price":0,
    "original_price":39000,
    "rating":0,
    "location":"서울 강서구 마곡동",
    "is_hot":0,
    "is_only":0,
    "is_new":0,
    "like":0
  },
  {
    "id":123843,
    "frip_type":"ticket",
    "title":"[동반할인] 아늑한 다이닝이있는 쿠킹 클래스 (예약 가능)",
    "catch_phrase":"요리도 배우고 식사도하고, 1인 1실습",
    "price":0,
    "original_price":39000,
    "rating":0,
    "location":"서울 강서구 마곡동",
    "is_hot":0,
    "is_only":0,
    "is_new":0,
    "like":0
  }
]
@inject('stateStore', 'apiStore', 'dataStore')
@observer
class Events extends Component {

  constructor(props) {
    super(props)
    this.state = {
      mounted : false
    }
  }

  componentDidMount() {
    this.setState({
      mounted : true
    })
    this.onRouteChanged()
  }

  componentDidUpdate(prevProps) {

    if (this.props.location !== prevProps.location) {
      this.onRouteChanged()
    }
  }

  componentWillUnmount() {
    this.setState({
      mounted: false
    })
  }

  onRouteChanged() {
    this.props.stateStore.setEventsState("fromO", 0)
    this.props.stateStore.setEventsState("toO", 1)
    this.props.stateStore.setEventsState("countO", 0)
    this.props.stateStore.setEventsState("pageO", 0)
    this.props.stateStore.setEventsState("fromF", 0)
    this.props.stateStore.setEventsState("toF", 1)
    this.props.stateStore.setEventsState("countF", 0)
    this.props.stateStore.setEventsState("pageF", 0)
    this.props.stateStore.setEventsState("done", false)
    // this.getEvents(0)

    this.props.dataStore.setDataEventRunning([])
    this.props.dataStore.setDataEventFinished([])
    this.getEventListFinished(1)
    this.getEventListRunning(1)
  }

  // getEvents(page) {
  //   this.props.stateStore.setEventsState("done", true)
  //   this.props.stateStore.setEventsState("fromO", 0)
  //   this.props.stateStore.setEventsState("toO", 1)
  //   this.props.stateStore.setEventsState("countO", data.length)
  //   this.props.stateStore.setEventsState("pageO", page)
  //   this.props.stateStore.setEventsState("fromF", 0)
  //   this.props.stateStore.setEventsState("toF", 1)
  //   this.props.stateStore.setEventsState("countF", data.length)
  //   this.props.stateStore.setEventsState("pageF", page)
  // }

  getEventListFinished(page) {
    let fromIdx = (page - 1) * 8
    let toIdx = fromIdx + 8 - 1
    let range = '[' + fromIdx + ',' + toIdx + ']'
    let params = []
    params.push({
      key: 'range',
      value: range
    })
    params.push({
      key: 'filter',
      value: "{\"status\":\"finished\",\"event\":1}"
    })
    this.props.apiStore.getProductList(localStorage.getItem('jwt'), params).then(res=>{
      let range = res.headers['content-range'].split(' ')[1]
      let fromIdx = range.split('/')[0].split('-')[0]
      let toIdx = range.split('/')[0].split('-')[1]
      let count = range.split('/')[1]

      this.props.stateStore.setEventsState("fromF", fromIdx)
      this.props.stateStore.setEventsState("toF", toIdx)
      this.props.stateStore.setEventsState("countF", count)
      this.props.stateStore.setEventsState("pageF", page)
      this.props.dataStore.setDataEventFinished(res.data)
    })
    .catch(err => {

    })
  }

  getEventListRunning(page) {
    let fromIdx = (page - 1) * 8
    let toIdx = fromIdx + 8 - 1
    let range = '[' + fromIdx + ',' + toIdx + ']'
    let params = []
    params.push({
      key: 'range',
      value: range
    })
    params.push({
      key: 'filter',
      value: "{\"status\":\"running\",\"event\":1}"
    })
    this.props.apiStore.getProductList(localStorage.getItem('jwt'), params).then(res=>{
      let range = res.headers['content-range'].split(' ')[1]
      let fromIdx = range.split('/')[0].split('-')[0]
      let toIdx = range.split('/')[0].split('-')[1]
      let count = range.split('/')[1]

      this.props.stateStore.setEventsState("done", true)
      this.props.stateStore.setEventsState("fromO", 0)
      this.props.stateStore.setEventsState("toO", 1)
      this.props.stateStore.setEventsState("countO", data.length)
      this.props.stateStore.setEventsState("pageO", page)
      this.props.dataStore.setDataEventRunning(res.data)
    })
    .catch(err => {

    })
  }

  renderOngoing() {

    let eventsState = this.props.stateStore.eventsState
    const dataEventRunning = this.props.dataStore.dataEventRunning
    if(eventsState.done===true && data.length===0) {
      //TODO 진행중 이벤트가 없습니다 페이지 띄울것
      return (
        <Fragment>
          <NoContents
            iconClass="icon-none-available"
            text={this.props.dataStore.str.S0165}>
          </NoContents>
        </Fragment>
      )
    }
    let view = []
    for(let row of dataEventRunning) {
      let item = row
      let tags = []
      if(item.is_hot === 1) {
        tags.push(this.props.dataStore.str.W0125)
      }
      if(item.is_new === 1) {
        tags.push(this.props.dataStore.str.W0126)
      }
      if(item.is_only === 1) {
        tags.push(this.props.dataStore.str.W0127)
      }
      view.push(
        <ItemRisingOfriends
          id = {item.id}
          key = {item.id}
          loc = {item.loc}
          isLike = {item.like}
          subTitle = {item.catch_phrase}
          title = {item.title}
          price = {item.original_price}
          discount = {item.price}
          rating = {item.rating}
          tags = {tags}>
        </ItemRisingOfriends>
      )
    }
    return (
      <Fragment>
        <div className="ongoing-items">
          {view}
        </div>
      </Fragment>
    )
  }

  renderOngoingPagination() {

    let eventsState = this.props.stateStore.eventsState
    let activePage = eventsState.pageO
    let totalItemsCount = eventsState.countO
    let pageRangeDisplayed = (eventsState.countO/5) >= 5 ? 5 : parseInt(eventsState.countO/5 + 1)

    if(eventsState.done===true && data.length===0) {
      return (
        <Fragment></Fragment>
      )
    } else {
      return (
        <Pagination
          activePage={activePage}
          itemsCountPerPage={8}
          totalItemsCount={totalItemsCount}
          pageRangeDisplayed={pageRangeDisplayed+1}
          onChange={this.handleOngoingPage.bind(this)}
          firstPageText={""}
          prevPageText={""}
          nextPageText={""}
          lastPageText={""}
          itemClassFirst="first-link"
          itemClassPrev="prev-link"
          itemClassNext="next-link"
          itemClassLast="last-link"
          linkClass="page-link"
          innerClass="pagination"
          itemClass="pg"
          activeClass="current"
        />
      )
    }
  }

  renderFinish() {
    let view = []

    const dataEventFinished = this.props.dataStore.dataEventFinished
    for(let row of dataEventFinished) {
      let item = row
      let tags = []
      if(item.is_hot === 1) {
        tags.push(this.props.dataStore.str.W0125)
      }
      if(item.is_new === 1) {
        tags.push(this.props.dataStore.str.W0126)
      }
      if(item.is_only === 1) {
        tags.push(this.props.dataStore.str.W0127)
      }
      view.push(
        <ItemRisingOfriends
          id = {item.id}
          key = {item.id}
          loc = {item.loc}
          isLike = {item.like}
          subTitle = {item.catch_phrase}
          title = {item.title}
          price = {item.original_price}
          discount = {item.price}
          rating = {item.rating}
          tags = {tags}
          eventFinished = {true}>
        </ItemRisingOfriends>
      )
    }
    return (
      <div className="finished-items">
        {view}
      </div>
    )
  }

  renderFinishPagination() {
    let eventsState = this.props.stateStore.eventsState
    let activePage = eventsState.pageF
    let totalItemsCount = eventsState.countF
    let pageRangeDisplayed = (eventsState.countF/5) >= 5 ? 5 : parseInt(eventsState.countF/5 + 1)
    if(eventsState.done===true && data.length===0) {
      return (
        <Fragment></Fragment>
      )
    } else {
      return (
        <Pagination
          activePage={activePage}
          itemsCountPerPage={8}
          totalItemsCount={totalItemsCount}
          pageRangeDisplayed={pageRangeDisplayed+1}
          onChange={this.handleFinishPage.bind(this)}
          firstPageText={""}
          prevPageText={""}
          nextPageText={""}
          lastPageText={""}
          itemClassFirst="first-link"
          itemClassPrev="prev-link"
          itemClassNext="next-link"
          itemClassLast="last-link"
          linkClass="page-link"
          innerClass="pagination"
          itemClass="pg"
          activeClass="current"
        />
      )
    }
  }

  handleOngoingPage(pageNumber) {
    this.props.stateStore.setEventsState("pageO", pageNumber)
    this.getEventListRunning(pageNumber)
  }

  handleFinishPage(pageNumber) {
    this.props.stateStore.setEventsState("pageF", pageNumber)
    this.getEventListFinished(pageNumber)
  }

  render() {
    return (
      <Fragment>
        <div className="ev-page">
          <span className="ev-title">{this.props.dataStore.str.W1043}</span>
          <div className="ev-ongoing">
            <span className="ongoing-title">{this.props.dataStore.str.W1044}</span>
            {this.renderOngoing()}
            {this.renderOngoingPagination()}
          </div>
          <div className="ev-finished">
            <span className="finished-title">{this.props.dataStore.str.W1045}</span>
            {this.renderFinish()}
            {this.renderFinishPagination()}
          </div>
        </div>
      </Fragment>
    )

  }
}
export default Events
