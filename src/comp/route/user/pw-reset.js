import React, { Component, Fragment } from 'react'
import { inject, observer } from 'mobx-react'
import { Link } from 'react-router-dom'
import '../../../static/scss/pw-reset.scss'
@inject('stateStore', 'apiStore', 'dataStore')
@observer
class PwReset extends Component {
  constructor(props) {
    super(props)
  }

  componentDidMount() {
    this.props.stateStore.setDefaultOpen()
    this.onRouteChanged()
  }

  componentDidUpdate(prevProps) {
    if (this.props.location !== prevProps.location) {
      this.onRouteChanged()
    }
  }

  onRouteChanged() {
    let exp = localStorage.getItem('exp')
    let jwtToken = localStorage.getItem('jwt')
    if(exp != null && jwtToken != null) {
      this.props.history.goBack()
    }
    this.props.stateStore.setPwReset("inputEmail", "")
    this.props.stateStore.setPwReset("isErrEmail", false)
    this.props.stateStore.setPwReset("inputErrMsg", "")
    this.props.stateStore.setPwReset("isOnNoti", false)
    this.props.stateStore.setPwReset("callErrMsg", "")
    this.props.stateStore.setPwReset("step", 0)
  }

  putUserPasswordLost() {
    const pwReset = this.props.stateStore.pwReset
    let payload = {
      'email' : pwReset.inputEmail
    }
    this.props.apiStore.putUserPasswordLost(null, payload)
      .then(res => {
        this.props.stateStore.setPwReset("step", 1)
      })
      .catch(err=> {
        this.pwResetFailedPopup()
        if(err.response.status === 409) {
          this.props.stateStore.setPwReset("callErrMsg", this.props.dataStore.str.S1015)
        } else {
          this.props.stateStore.setPwReset("callErrMsg", this.props.dataStore.str.S1006)
        }
      })
  }

  renderNoti() {
    const pwReset = this.props.stateStore.pwReset
    if(pwReset.isOnNoti) {
      return (
          <span className="pw-reset-notice-msg">{pwReset.callErrMsg}</span>
      )
    }
  }

  pwResetFailedPopup() {
    this.props.stateStore.setPwReset("isOnNoti", true)
    setTimeout(() => {
      this.props.stateStore.setPwReset("isOnNoti", false)
      this.props.stateStore.setPwReset("callErrMsg", "")
    }, 2000)
  }

  onChangeEmail = (e) => {
    const pwReset = this.props.stateStore.pwReset
    this.props.stateStore.setPwReset("inputEmail", e.target.value)
  }

  onClickRemoveEmail = (e) => {
    this.props.stateStore.setPwReset("inputEmail", "")
  }

  onClickSendEmail = (e) => {
    const pwReset = this.props.stateStore.pwReset
    const emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    this.props.stateStore.setPwReset("isErrEmail", false)

    let emptyErrMsg = this.props.dataStore.str.S1011
    let invalidErrMsg = this.props.dataStore.str.S0041

    if(pwReset.inputEmail.length===0) {
      this.props.stateStore.setPwReset("isErrEmail", true)
      this.props.stateStore.setPwReset("inputErrMsg", emptyErrMsg)
      return
    }
    if(emailRegex.test(pwReset.inputEmail) === false) {
      this.props.stateStore.setPwReset("isErrEmail", true)
      this.props.stateStore.setPwReset("inputErrMsg", invalidErrMsg)
      return
    }

    this.putUserPasswordLost()
  }

  renderSendEmail() {
    const pwReset = this.props.stateStore.pwReset
    let btnClassName = pwReset.inputEmail.length > 0 ? "enabled" : "disabled"
    if(pwReset.step === 0) {
      return (
        <Fragment>
          <span className="pw-reset-email-send-title">{this.props.dataStore.str.W0109}</span>
          <span className="pw-reset-email-send-desc">{this.props.dataStore.str.S0024}</span>
          <div className="pw-reset-email-input-err-con">
            <input
              className="pw-reset-email-input"
              type="email"
              value = {pwReset.inputEmail}
              placeholder={this.props.dataStore.str.W0099}
              onChange = {(e) => this.onChangeEmail(e)}/>
            {
              pwReset.inputEmail.length>0
              && <span className="pw-reset-email-remove-btn" onClick={(e) => this.onClickRemoveEmail(e)}></span>
            }
            {
              pwReset.isErrEmail == true
              && <span className="pw-reset-email-err-msg">{pwReset.inputErrMsg}</span>
            }
          </div>
          <span
            className = {btnClassName}
            onClick={(e) => this.onClickSendEmail(e)}>
            {this.props.dataStore.str.W0109}
          </span>
        </Fragment>
      )
    }

  }

  renderSendPw() {
    const pwReset = this.props.stateStore.pwReset
    if(pwReset.step === 1) {
      return (
        <Fragment>
          <span className="pw-reset-sending-email-title">{this.props.dataStore.str.W0108}</span>
          <span className="pw-reset-temp-pw-desc" dangerouslySetInnerHTML={{__html: this.props.dataStore.str.S0025}}></span>
          <Link className="pw-reset-login-with-tempPw" to="/login/email">{this.props.dataStore.str.W0110}</Link>
        </Fragment>
      )
    }

  }

  render() {
    return (
      <div className="pw-reset-entity-container">
        {this.renderSendEmail()}
        {this.renderSendPw()}
        {this.renderNoti()}
      </div>
    )

  }
}
export default PwReset
