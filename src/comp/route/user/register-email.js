import React, {Component, Fragment} from 'react'
import {inject, observer} from 'mobx-react'
import {Link} from 'react-router-dom'
import '../../../static/scss/register-email.scss'
import forge from 'node-forge'
import jwt from 'jwt-decode'
import {sha256} from 'js-sha256'
import Agree from "../../dialog/agree";

@inject('stateStore', 'apiStore', 'dataStore')
@observer
class RegisterEmail extends Component {

  constructor(props) {
    super(props)
  }

  componentDidMount() {
    this.props.stateStore.setRegisterEmail("inputEmail", "")
    this.props.stateStore.setRegisterEmail("inputPw", "")
    this.props.stateStore.setRegisterEmail("inputPwConfirm", "")
    this.props.stateStore.setRegisterEmail("inputRecomCode", "")
    this.props.stateStore.setRegisterEmail("isErrEmail", false)
    this.props.stateStore.setRegisterEmail("isErrPw", false)
    this.props.stateStore.setRegisterEmail("isErrPwConfirm", false)
    this.props.stateStore.setRegisterEmail("isErrRecomCode", false)
    this.props.stateStore.setRegisterEmail("isOnTerms", false)
    this.props.stateStore.setRegisterEmail("isOnNoti", false)
    this.props.stateStore.setRegisterEmail("inputErrMsg", "")
    this.props.stateStore.setRegisterEmail("callErrMsg", "")
    this.props.stateStore.setRegisterEmail("usePrivacyPopup", false)
  }

  register() {
    let encryptPK = this.props.dataStore.encryptPK
    let registerEmail = this.props.stateStore.registerEmail
    const inputEmail = registerEmail.inputEmail
    const inputPw = registerEmail.inputPw
    const recomCode = registerEmail.inputRecomCode
    let publicKey = forge.pki.publicKeyFromPem(forge.util.encodeUtf8(encryptPK))
    let payload = {
      "pf": "email",
      // "password": forge.util.encode64(publicKey.encrypt(forge.util.encodeUtf8(registerEmail.pw), 'RSA-OAEP')),
      "password": sha256(inputPw),
      "email": inputEmail,
      // "recommender": registerEmail.inputRecomCode.length > 5 ? registerEmail.inputRecomCode : ""
      "recommender": recomCode
    }
    this.props.apiStore.postUserSignup(payload).then(res => {
      this.login(inputEmail, inputPw)
    })
      .catch(err => {
        this.registerFailedPopup()
        if (err.response.status === 409) {
          this.props.stateStore.setRegisterEmail("callErrMsg", this.props.dataStore.str.S0021)
        } else if(err.response.status === 412) {
          this.props.stateStore.setRegisterEmail("callErrMsg", this.props.dataStore.str.S1046)
        } else {
          this.props.stateStore.setRegisterEmail("callErrMsg", this.props.dataStore.str.S1006)
        }
      })
  }

  login(email, pw) {
    let encryptPK = this.props.dataStore.encryptPK
    let publicKey = forge.pki.publicKeyFromPem(forge.util.encodeUtf8(encryptPK))
    let payload = {
      'pf': 'email',
      'email': email,
      // 'password' : forge.util.encode64(publicKey.encrypt(forge.util.encodeUtf8(pw), 'RSA-OAEP'))
      'password': sha256(pw)
    }
    this.props.apiStore.postAuthenticate(payload)
      .then(res => {
      var token = jwt(res.data.token)
      localStorage.setItem('exp', token.exp)
      localStorage.setItem('jwt', res.data.token)
      this.props.dataStore.setDataAuthenticate(res.data)
      this.props.dataStore.setDataJwt(res.data.token)
      this.getUserInfo()
    })
      .catch(err => {
        this.registerFailedPopup()
        if(err.response.status === 409) {
          this.props.stateStore.setLoginEmail("callErrMsg", this.props.dataStore.str.S1007)
        } else if(err.response.status === 412) {
          this.props.stateStore.setLoginEmail("callErrMsg", this.props.dataStore.str.S1030)
        } else {
          this.props.stateStore.setLoginEmail("callErrMsg", this.props.dataStore.str.S1006)
        }
      })
  }

  getUserInfo() {
    this.props.apiStore.getUserInfo(this.props.dataStore.dataJwt).then(res => {
      this.props.dataStore.setDataUserInfo(res.data)
      this.props.stateStore.setLoginState(1)
      this.props.history.goBack()
    })
      .catch(err => {
      })
  }

  registerEmailFailedPopup() {
    this.props.stateStore.setRegisterEmail("isOnNoti", true)
    setTimeout(() => {
      this.props.stateStore.setRegisterEmail("isOnNoti", false)
      this.props.stateStore.setRegisterEmail("callErrMsg", "")
    }, 2000)
  }


  onClickTerms = (e) => {
    const registerEmail = this.props.stateStore.registerEmail
    this.props.stateStore.setRegisterEmail("isOnTerms", !registerEmail.isOnTerms)
  }

  onChangeEmail = (e) => {
    const registerEmail = this.props.stateStore.registerEmail
    this.props.stateStore.setRegisterEmail("inputEmail", e.target.value)
  }

  onChangePw = (e) => {
    const registerEmail = this.props.stateStore.registerEmail
    this.props.stateStore.setRegisterEmail("inputPw", e.target.value)
  }

  onChangePwConfirm = (e) => {
    const registerEmail = this.props.stateStore.registerEmail
    this.props.stateStore.setRegisterEmail("inputPwConfirm", e.target.value)
  }

  onChangeRecomCode = (e) => {
    const registerEmail = this.props.stateStore.registerEmail
    this.props.stateStore.setRegisterEmail("inputRecomCode", e.target.value)
  }

  onClickRemoveEmail = (e) => {
    this.props.stateStore.setRegisterEmail("inputEmail", "")
  }

  onClickRemovePw = (e) => {
    this.props.stateStore.setRegisterEmail("inputPw", "")
  }

  onClickRemoveRepeat = (e) => {
    this.props.stateStore.setRegisterEmail("inputPwConfirm", "")
  }

  onClickUseRegPrivacy  = (e) => {
    this.props.stateStore.setRegisterEmail("usePrivacyPopup", true)
  }

  onClickCloseAgree = (e) => {
    this.props.stateStore.setRegisterEmail("usePrivacyPopup", false)
  }

  renderUsePrivacy() {
    let registerEmail = this.props.stateStore.registerEmail
    if (registerEmail.usePrivacyPopup === true) {
      return (
        <Agree
          title={this.props.dataStore.str.S1033}
          content={this.props.dataStore.str.S1037}
          agreeSentence={this.props.dataStore.str.W0091}
          onClickClose={(e) => this.onClickCloseAgree(e)}>
        </Agree>
      )
    }
  }

  onClickRegister = (e) => {
    const pwRegex = /^((?=.*\d)(?=.*[a-z])|(?=.*\d)(?=.*[A-Z])|(?=.*[a-z])(?=.*[A-Z])|(?=.*\d)(?=.*\W)|(?=.*[a-z])(?=.*\W)|(?=.*[A-Z])(?=.*\W))[A-Za-z\d!@#$%^&*]{10,}$/
    const emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    const registerEmail = this.props.stateStore.registerEmail

    this.props.stateStore.setRegisterEmail("isErrEmail", false)
    this.props.stateStore.setRegisterEmail("isErrPw", false)
    this.props.stateStore.setRegisterEmail("isErrPwConfirm", false)
    this.props.stateStore.setRegisterEmail("isErrRecomCode", false)

    let writeEmailErrMsg = this.props.dataStore.str.S1011
    let invalidEmailErrmsg = this.props.dataStore.str.S0041
    let pwErrmsg = this.props.dataStore.str.S1003
    let confirmErrmsg = this.props.dataStore.str.S1014
    let termsErrmsg = this.props.dataStore.str.S1004
    let pwnotValueMsg = this.props.dataStore.str.S1019;

    if (registerEmail.inputEmail.trim().length === 0) {
      this.props.stateStore.setRegisterEmail("isErrEmail", true)
      this.props.stateStore.setRegisterEmail("inputErrMsg", writeEmailErrMsg)
      return
    }
    if (!emailRegex.test(registerEmail.inputEmail)) {
      this.props.stateStore.setRegisterEmail("isErrEmail", true)
      this.props.stateStore.setRegisterEmail("inputErrMsg", invalidEmailErrmsg)
      return
    }
    if (registerEmail.inputPw.trim().length === 0) {
      this.props.stateStore.setRegisterEmail("isErrPw", true)
      this.props.stateStore.setRegisterEmail("inputErrMsg", pwnotValueMsg)
      return
    }
    if (registerEmail.inputPwConfirm.trim().length === 0) {
      this.props.stateStore.setRegisterEmail("isErrPwConfirm", true)
      this.props.stateStore.setRegisterEmail("inputErrMsg", pwnotValueMsg)
      return
    }
    if (!pwRegex.test(registerEmail.inputPw)) { // 비밀번호 정규식
      this.props.stateStore.setRegisterEmail("isErrPw", true)
      this.props.stateStore.setRegisterEmail("inputErrMsg", pwErrmsg)
      return
    }
    if (registerEmail.isOnTerms === false) {
      this.props.stateStore.setRegisterEmail("callErrMsg", termsErrmsg)
      this.registerEmailFailedPopup()
      return
    }
    if (registerEmail.inputPw !== registerEmail.inputPwConfirm) { // 앞 비밀번호와 뒷 비밀번호의 일치하지 않음
      this.props.stateStore.setRegisterEmail("isErrPwConfirm", true)
      this.props.stateStore.setRegisterEmail("inputErrMsg", confirmErrmsg)
      return
    }

    // this.props.stateStore.setLoginState(1)
    // this.props.history.push('/')
    this.register()
  }

  renderInput() {
    const registerEmail = this.props.stateStore.registerEmail
    return (
      <Fragment>
        <div className="register-email-inputEmail-con">
          <input
            className="register-email-inputEmail"
            type="email"
            value={registerEmail.inputEmail}
            placeholder={this.props.dataStore.str.W0099}
            onChange={(e) => this.onChangeEmail(e)}/>
          {
            registerEmail.inputEmail.length > 0
            && <span className="register-email-remove-btn" onClick={(e) => this.onClickRemoveEmail(e)}></span>
          }
          {
            registerEmail.isErrEmail == true
            && <span className="register-email-inputErrMsg">{registerEmail.inputErrMsg}</span>
          }
        </div>
        <div className="register-email-inputPw-con">
          <input
            className="register-email-inputPw"
            type="password"
            value={registerEmail.inputPw}
            placeholder={this.props.dataStore.str.W0100}
            onChange={(e) => this.onChangePw(e)}/>
          {
            registerEmail.inputPw.length > 0
            && <span className="register-email-removePw" onClick={(e) => this.onClickRemovePw(e)}></span>
          }
          {
            registerEmail.isErrPw == true
            && <span className="register-email-pwErrMsg">{registerEmail.inputErrMsg}</span>
          }
        </div>
        <div className="register-email-inputPwConfirm-con">
          <input
            className="register-email-inputPwConfirm"
            type="password"
            value={registerEmail.inputPwConfirm}
            placeholder={this.props.dataStore.str.W0101}
            onChange={(e) => this.onChangePwConfirm(e)}/>
          {
            registerEmail.inputPwConfirm.length > 0
            && <span className="register-email-removeRepeat" onClick={(e) => this.onClickRemoveRepeat(e)}></span>
          }
          {
            registerEmail.isErrPwConfirm == true
            && <span className="register-email-confirmErrMsg">{registerEmail.inputErrMsg}</span>
          }
        </div>
        <div className="register-email-recomCode-con">
          <span className="register-email-recomCode-title">{this.props.dataStore.str.W0102}</span>
          <input
            className="register-email-inputRecomCode"
            type="text"
            value={registerEmail.inputRecomCode}
            placeholder={this.props.dataStore.str.W0106}
            onChange={(e) => this.onChangeRecomCode(e)}/>
          {
            registerEmail.isErrRecomCode == true
            && <span className="register-email-input-RecomErrMsg">{registerEmail.inputErrMsg}</span>
          }
        </div>
      </Fragment>

    )

  }

  renderTerms() {
    const registerEmail = this.props.stateStore.registerEmail
    let className = registerEmail.isOnTerms ? "checked" : "not-checked"
    return (
      <Fragment>
        <div className="register-email-terms-agreement-container" onClick={(e) => this.onClickTerms(e)}>
          <span className={className}></span>
          <span className="register-email-all-agreement">{this.props.dataStore.str.W1013}</span>
        </div>
        <div className="register-email-terms-container">
          <a className="register-email-terms-agreement-terms" target="_blank" href="/terms">
            {this.props.dataStore.str.S1016}
          </a>
          <span className="register-email-terms-agreement-personal-information" onClick={(e) => {this.onClickUseRegPrivacy(e)}}>
            {this.props.dataStore.str.S1017}
          </span>
        </div>
        <span className="register-email-terms-adult-agreement">{this.props.dataStore.str.S1018}</span>
      </Fragment>
    )
  }

  renderNoti() {
    const registerEmail = this.props.stateStore.registerEmail
    if (registerEmail.isOnNoti) {
      return (
        <span className="register-email-notice-msg">{registerEmail.callErrMsg}</span>
      )
    }
  }

  registerFailedPopup() {
    this.props.stateStore.setRegisterEmail("isOnNoti", true)
    setTimeout(() => {
      this.props.stateStore.setRegisterEmail("isOnNoti", false)
      this.props.stateStore.setRegisterEmail("callErrMsg", "")
    }, 2000)
  }

  render() {
    const registerEmail = this.props.stateStore.registerEmail
    let btnClassName = (registerEmail !== undefined
      && registerEmail.inputEmail.length !== 0
      && registerEmail.inputPw.length !== 0
      && registerEmail.inputPwConfirm.length !== 0
      && registerEmail.isOnTerms !== false)
      ? "register-email-register-btn-active" : "register-email-register-btn-inactive"
    return (
      <div className="register-email-entity-container">
        <span className="register-email-title">{this.props.dataStore.str.W0105}</span>
        {this.renderInput()}
        {this.renderTerms()}
        <span
          className={btnClassName}
          onClick={(e) => this.onClickRegister(e)}>
          {this.props.dataStore.str.W0107}
        </span>
        {this.renderNoti()}
        {this.renderUsePrivacy()}
      </div>
    )

  }
}

export default RegisterEmail
