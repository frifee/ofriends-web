import React, { Component, Fragment } from 'react'
import { inject, observer } from 'mobx-react'
import { Link } from 'react-router-dom'
import jwt from 'jwt-decode'
import '../../../static/scss/login-email.scss'
import forge from 'node-forge'
import { sha256 } from 'js-sha256'
@inject('stateStore', 'apiStore', 'dataStore')
@observer
class LoginEmail extends Component {

  constructor(props) {
    super(props)
  }
  // 'password' : forge.util.encode64(publicKey.encrypt(forge.util.encodeUtf8(loginEmail.inputPw), 'RSA-OAEP'))
  login() {
    const loginEmail = this.props.stateStore.loginEmail
    let encryptPK = this.props.dataStore.encryptPK
    let publicKey = forge.pki.publicKeyFromPem(forge.util.encodeUtf8(encryptPK))
    let payload = {
      'pf' : 'email',
      'email' : loginEmail.inputEmail,
      // 'password' : forge.util.encode64(publicKey.encrypt(forge.util.encodeUtf8(loginEmail.inputPw), 'RSA-OAEP'))
      'password' : sha256(loginEmail.inputPw)
    }
    this.props.apiStore.postAuthenticate(payload)
      .then(res => {
      var token = jwt(res.data.token)
      localStorage.setItem('exp', token.exp)
      localStorage.setItem('jwt', res.data.token)
      this.props.dataStore.setDataAuthenticate(res.data)
      this.props.dataStore.setDataJwt(res.data.token)
      this.getUserInfo()
    })
    .catch((err) => {
      if(err.response.status === 409) {
        this.props.stateStore.setLoginEmail("callErrMsg", this.props.dataStore.str.S1007)
      } else if(err.response.status === 412) {
        this.props.stateStore.setLoginEmail("callErrMsg", this.props.dataStore.str.S1030)
      } else {
        this.props.stateStore.setLoginEmail("callErrMsg", this.props.dataStore.str.S1006)
      }
      this.loginFailedPopup()
    })

  }

  getUserInfo() {
    let jwt = localStorage.getItem('jwt')
    this.props.apiStore.getUserInfo(localStorage.getItem('jwt')).then(res=>{
      this.props.dataStore.setDataUserInfo(res.data)
      this.props.stateStore.setLoginState(1)
      this.props.history.goBack()
    })
    .catch(err=> {
    })
  }

  renderNoti() {
    const loginEmail = this.props.stateStore.loginEmail
    if(loginEmail.isOnNoti) {
      return (
        <span className="login-email-notice-msg">{loginEmail.callErrMsg}</span>
      )
    }
  }

  componentDidMount() {
    this.props.stateStore.setLoginEmail("inputEmail", "")
    this.props.stateStore.setLoginEmail("inputPw", "")
    this.props.stateStore.setLoginEmail("isErrEmail", false)
    this.props.stateStore.setLoginEmail("isErrPw", false)
    this.props.stateStore.setLoginEmail("inputErrMsg", "")
    this.props.stateStore.setLoginEmail("isOnNoti", false)
    this.props.stateStore.setLoginEmail("callErrMsg", "")
  }
  onChangeEmail = (e) => {
    const loginEmail = this.props.stateStore.loginEmail
    this.props.stateStore.setLoginEmail("inputEmail", e.target.value)
  }

  onChangePw = (e) => {
    const loginEmail = this.props.stateStore.loginEmail
    this.props.stateStore.setLoginEmail("inputPw", e.target.value)
  }

  onClickRemoveEmail = (e) => {
    this.props.stateStore.setLoginEmail("inputEmail", "")
  }

  onClickRemovePw = (e) => {
    this.props.stateStore.setLoginEmail("inputPw", "")
  }

  onClickLogin = (e) => {
    const loginEmail = this.props.stateStore.loginEmail
    const pwRegex = /^((?=.*\d)(?=.*[a-z])|(?=.*\d)(?=.*[A-Z])|(?=.*[a-z])(?=.*[A-Z])|(?=.*\d)(?=.*\W)|(?=.*[a-z])(?=.*\W)|(?=.*[A-Z])(?=.*\W))[A-Za-z\d!@#$%^&*]{10,}$/

    this.props.stateStore.setLoginEmail("isErrEmail", false)
    this.props.stateStore.setLoginEmail("isErrPw", false)

    let pwErrmsg = this.props.dataStore.str.S1003
    let confirmErrmsg = this.props.dataStore.str.S0010
    let emailErrmsg = this.props.dataStore.str.S1006

    if(loginEmail.inputEmail === 'test06@frifee.co')
      this.login()

    if(loginEmail.inputEmail.length===0) {
      this.props.stateStore.setLoginEmail("isErrEmail", true)
      this.props.stateStore.setLoginEmail("inputErrMsg", emailErrmsg)
      return
    }

    if(loginEmail.inputPw.length===0) {
      this.props.stateStore.setLoginEmail("isErrPw", true)
      this.props.stateStore.setLoginEmail("inputErrMsg", pwErrmsg)
      return
    }

    // if(!pwRegex.test(loginEmail.inputPw)) { // 비밀번호 정규식
    //   this.props.stateStore.setLoginEmail("isErrPw", true)
    //   this.props.stateStore.setLoginEmail("inputErrMsg", pwErrmsg)
    //   return
    // }

    this.login()

  }

  loginFailedPopup() {
    this.props.stateStore.setLoginEmail("isOnNoti", true)
    setTimeout(() => {
      this.props.stateStore.setLoginEmail("isOnNoti", false)
      this.props.stateStore.setLoginEmail("callErrMsg", "")
    }, 2000)
  }

  render() {
    const loginEmail = this.props.stateStore.loginEmail
    let btnClassName = (loginEmail !== undefined
        && loginEmail.inputEmail.length !== 0
        && loginEmail.inputPw.length !== 0)
        ? "login-email-login-click-btn-active" : "login-email-login-click-btn-inactive"
    return (
    <Fragment>
      <div className="login-email-entity-container">
        <span className="login-email-login-title">{this.props.dataStore.str.W0096}</span>
        <div className="login-email-email-err-con">
          <input
            className="login-email-email-input"
            type="email"
            value = {loginEmail.inputEmail}
            placeholder={this.props.dataStore.str.W0099}
            onChange = {(e) => this.onChangeEmail(e)}/>
          {
            loginEmail.inputEmail.length>0
            && <span className="login-email-email-remove-btn" onClick={(e) => this.onClickRemoveEmail(e)}></span>
          }
          {
            loginEmail.isErrEmail == true
            && <span className="login-email-email-err-msg">{loginEmail.inputErrMsg}</span>
          }
        </div>
        <div className="login-email-pw-err-con">
          <input
            className="login-email-pw-input"
            type="password"
            value = {loginEmail.inputPw}
            placeholder={this.props.dataStore.str.W0100}
            onChange = {(e) => this.onChangePw(e)}/>
          {
            loginEmail.inputPw.length>0
            && <span className="login-email-pw-remove-btn" onClick={(e) => this.onClickRemovePw(e)}></span>
          }
          {
            loginEmail.isErrPw == true
            && <span className="login-email-pw-err-msg">{loginEmail.inputErrMsg}</span>
          }
        </div>
        <span
          className={btnClassName}
          onClick={(e) => this.onClickLogin(e)}>
          {this.props.dataStore.str.W0002}
          </span>
        <Link className="login-email-find-pw" to="/pw-reset">{this.props.dataStore.str.S0023}</Link>
      </div>
      {this.renderNoti()}
    </Fragment>
    )

  }
}
export default LoginEmail
