import React, { Component, Fragment } from 'react'
import { inject, observer } from 'mobx-react'
import jwt from 'jwt-decode'
import CommonLogin from '../../auth/common-login'
@inject('stateStore', 'apiStore', 'dataStore')
@observer
class Login extends Component {

  constructor(props) {
    super(props)
    this.state = {
      pf: "",
      pfId: "",
      pfUsername: "",
      pfToken: "",
      image_url: "",
      gender: "",
      ageRange: "",
      birthday: "",
      isErrRegister: false,
      errMsg: ""
    }
  }

  componentDidMount() {
    this.props.stateStore.setDefaultOpen()
    this.onRouteChanged()

  }

  componentDidUpdate(prevProps) {
    if (this.props.location !== prevProps.location) {
      this.onRouteChanged()
    }
  }

  onRouteChanged() {
    let exp = localStorage.getItem('exp')
    let jwtToken = localStorage.getItem('jwt')
    if(exp != null && jwtToken != null) {
      this.props.history.goBack()
    }

  }

  onClickFb = (e) => {
    window.FB.login(loginResponse=> {
      if (loginResponse.status === 'connected') {
        this.setState({pfId:loginResponse.authResponse.userID})
        this.onSuccessFacebookLogin(loginResponse)
      } else {
        this.setState({'errMsg' : this.props.dataStore.str.S1005})
        this.loginFailedPopup()
        return
      }
    },
    {
      scope: 'public_profile',
      // scope: 'public_profile,email',
      // scope: 'public_profile,email,user_age_range,user_birthday,user_gender',
      // return_scopes: true,
    })
  }

  onClickKakao = (e) => {
    if(window.Kakao.isInitialized() === true) {
      window.Kakao.cleanup()
    }
    window.Kakao.init(process.env.REACT_APP_KAKAO_APP_ID)
    window.Kakao.Auth.login({
      success: (authObj)=> {
        this.onSuccessKakaoLogin(authObj)
      },
      fail: (err)=> {
        this.setState({'errMsg' : this.props.dataStore.str.S1005})
        this.loginFailedPopup()
        return
      }
    })
  }

  onSuccessFacebookLogin(loginResponse) {
    window.FB.api('/'+this.state.pfId,
      'GET',
      // {"fields":"name,birthday,age_range,gender,picture"},
      // {"fields":"name,picture,email"},
      {"fields":"name,picture"},
        (response) => {
        this.props.stateStore.setPopupOn("termsAgreement")
        this.setState({
          'pf': "facebook",
          "pfId": response.id,
          "pfUsername": response.name,
          "pfToken": loginResponse.authResponse.accessToken,
          'image_url': response.picture.data.url,
          // "email": response.email,
          // 'gender': response.gender,
          // 'ageRange': response.age_range.min,
          // 'birthday': response.birthday
        })
      this.submitDb()
    });
  }

  onSuccessKakaoLogin(authObj) {
    window.Kakao.API.request({
      url: '/v2/user/me',
      success: (response)=> {
        this.setState({
          'pf': "kakao",
          'pfId': response.id,
          'pfUsername': response.kakao_account.profile.nickname,
          'pfToken': authObj.access_token,
          'image_url': response.kakao_account.profile.thumbnail_image_url,
          'gender': response.kakao_account.gender,
          'ageRange': response.kakao_account.age_range,
          'birthday': response.kakao_account.birthday
        })
        this.submitDb()
      },
      fail: (err)=> {
        this.setState({'errMsg' : this.props.dataStore.str.S1005})
        this.loginFailedPopup()
        return
      }
    })
  }

  submitDb() {
    let payload = {
      'pf': this.state.pf,
      'pfId': this.state.pfId,
      'pfUsername': this.state.pfUsername,
      'pfToken': this.state.pfToken,
      'image_url': this.state.image_url
    }
    this.props.apiStore.postAuthenticate(payload)
      .then(res=> {
        let token = jwt(res.data.token)
        localStorage.setItem('exp', token.exp)
        localStorage.setItem('jwt', res.data.token)
        this.props.dataStore.setDataAuthenticate(token)
        this.props.dataStore.setDataJwt(res.data.token)
        this.getUserInfo()
      })
      .catch(err=> {
        if(err.response.status === 409) {
          this.setState({'errMsg' : this.props.dataStore.str.S1007})
        } else if(err.response.status === 412) {
          this.setState({'errMsg' : this.props.dataStore.str.S1030})
        } else {
          this.setState({'errMsg' : this.props.dataStore.str.S1006})
        }
        this.loginFailedPopup()
      })
  }

  getUserInfo() {
    this.props.apiStore.getUserInfo(this.props.dataStore.dataJwt).then(res=>{
      this.props.dataStore.setDataUserInfo(res.data)
      this.props.stateStore.setLoginState(1)
      this.props.history.goBack()
    })
    .catch(err=> {
    })
  }

  loginFailedPopup() {
    this.setState({"isErrRegister": true})
    setTimeout(() => {
      this.setState({"isErrRegister": false})
    }, 2000)
  }

  render() {
    return (
      <Fragment>
      <CommonLogin
        title={this.props.dataStore.str.W0002}
        textEmail={this.props.dataStore.str.W0096}
        textFb={this.props.dataStore.str.W0097}
        textKakao={this.props.dataStore.str.W0098}
        textBottom={this.props.dataStore.str.W0001}
        linkEmail={'/login/email'}
        linkBottom={'/register'}
        onClickFb={(e)=>this.onClickFb(e)}
        onClickKakao={(e)=>this.onClickKakao(e)}></CommonLogin>
        {
          this.state.isErrRegister
          &&
          <span className="terms-agreement-register-failed">{this.state.errMsg}</span>
        }
      </Fragment>
    )

  }
}
export default Login
