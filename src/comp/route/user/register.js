import React, { Component, Fragment } from 'react'
import { inject, observer } from 'mobx-react'
import { Link } from 'react-router-dom'
import CommonLogin from '../../auth/common-login'
import TermsAgreement from '../../dialog/terms-agreement'
import '../../../static/scss/register.scss'
import forge from "node-forge";
import jwt from "jwt-decode";
import Agree from "../../dialog/agree";
@inject('stateStore', 'apiStore', 'dataStore')
@observer
class Register extends Component {

  constructor(props) {
    super(props)
    this.state = {
      pf: "",
      pfId: "",
      pfUsername: "",
      pfToken: "",
      image_url: "",
      gender: "",
      ageRange: "",
      birthday: "",
      isErrRegister: false,
      errMsg: ""
    }
  }

  componentDidMount() {
    this.props.stateStore.setDefaultOpen()
    this.onRouteChanged()
  }

  componentDidUpdate(prevProps) {
    if (this.props.location !== prevProps.location) {
      this.onRouteChanged()
    }
  }

  onRouteChanged() {
    let exp = localStorage.getItem('exp')
    let jwtToken = localStorage.getItem('jwt')
    if(exp != null && jwtToken != null) {
      this.props.history.goBack()
    }
  }

  onClickFb = (e) => {
    window.FB.login((loginResponse)=> {
      if (loginResponse.status === 'connected') {
        this.setState({pfId:loginResponse.authResponse.userID})
        this.onSuccessFacebookLogin(loginResponse)
      } else {
        this.setState({'errMsg' : this.props.dataStore.str.S0026})
        this.registerFailedPopup()
        return
      }
    },
    {
      scope: 'public_profile',
      // scope: 'public_profile,email',
      // scope: 'public_profile,email,user_age_range,user_birthday,user_gender',
      // return_scopes: true,
    })
  }

  onSuccessFacebookLogin(loginResponse) {
    window.FB.api('/'+this.state.pfId,
      'GET',
      // {"fields":"name,birthday,age_range,gender,picture"},
      // {"fields":"name,picture,email"},
      {"fields":"name,picture"},
      (response) => {
        this.props.stateStore.setPopupOn("termsAgreement")
        this.setState({
          'pf': "facebook",
          "pfId": response.id,
          "pfUsername": response.name,
          "pfToken": loginResponse.authResponse.accessToken,
          'image_url': response.picture.data.url,
          // "email": response.email,
          // 'gender': response.gender,
          // 'ageRange': response.age_range.min,
          // 'birthday': response.birthday
      })
    })
  }

  onClickKakao = (e) => {
    if (window.Kakao.isInitialized() === true) {
      window.Kakao.cleanup()
    }
    window.Kakao.init(process.env.REACT_APP_KAKAO_APP_ID)
    window.Kakao.Auth.login({
      // throughTalk: false, // 카카오 간편 로그인 문제를 해결하기 위함(수동 로그인)
      success: (authObj)=> {
        this.onSuccessKakaoLogin(authObj)
      },
      fail: (err)=> {
        this.setState({'errMsg' : this.props.dataStore.str.S0026})
        this.registerFailedPopup()
        return
      }
    })

    // window.Kakao.Auth.createLoginButton({
    //   container: '.common-kakao-register-container',
    //   success: (authObj) => {
    //     this.onSuccessKakaoLogin(authObj)
    //   },
    //   fail: (err) => {
    //     this.setState({'errMsg': this.props.dataStore.str.S0026})
    //     this.registerFailedPopup()
    //     return
    //   }
    // })

  }


  onSuccessKakaoLogin(authObj) {
    window.Kakao.API.request({
      url: '/v2/user/me',
      success: (response)=> {
        this.props.stateStore.setPopupOn("termsAgreement")
        this.setState({
          'pf': "kakao",
          'pfId': response.id,
          'pfUsername': response.kakao_account.profile.nickname,
          'pfToken': authObj.access_token,
          'image_url': response.kakao_account.profile.thumbnail_image_url,
          'gender': response.kakao_account.gender,
          'ageRange': response.kakao_account.age_range,
          'birthday': response.kakao_account.birthday
        })
      },
      fail: (err)=> {
        this.setState({'errMsg' : this.props.dataStore.str.S0026})
        this.registerFailedPopup()
        return
      }
    })
  }

  onClickPopupBtn = (e) => {
    let payload = {
      'pf': this.state.pf,
      'pfId': this.state.pfId,
      'pfUsername': this.state.pfUsername,
      'pfToken': this.state.pfToken,
      'image_url': this.state.image_url
    }

    this.props.apiStore.postAuthenticate(payload)
      .then(res=> {
        let token = jwt(res.data.token)
        localStorage.setItem('exp', token.exp)
        localStorage.setItem('jwt', res.data.token)
        this.props.dataStore.setDataAuthenticate(token)
        this.props.dataStore.setDataJwt(res.data.token)
        this.props.stateStore.setPopupOn("")
        this.getUserInfo()
      })
      .catch(err=> {
        if(err.response.status === 409) {
          this.setState({'errMsg' : this.props.dataStore.str.S1007})
        } else if(err.response.status === 412) {
          this.setState({'errMsg' : this.props.dataStore.str.S1030})
        } else {
          this.setState({'errMsg' : this.props.dataStore.str.S1006})
        }
        this.registerFailedPopup()
      })
  }

  getUserInfo() {
    this.props.apiStore.getUserInfo(this.props.dataStore.dataJwt).then(res=>{
      this.props.dataStore.setDataUserInfo(res.data)
      this.props.stateStore.setLoginState(1)
      this.props.history.push('/')
    })
      .catch(err=> {
        this.setState({'errMsg' : this.props.dataStore.str.S1052})
        this.registerFailedPopup()
        return
      })
  }

  renderTermsAgreement() {
    if(this.props.stateStore.popupOn === 'termsAgreement') {
      return (
        <TermsAgreement
          onClickRegister={(e) => this.onClickPopupBtn(e)}>
        </TermsAgreement>
      )
    }
  }

  registerFailedPopup() {
    this.setState({"isErrRegister": true})
    setTimeout(() => {
      this.setState({"isErrRegister": false})
    }, 2000)
  }

  render() {
    return (
      <Fragment>
        <CommonLogin
          title={this.props.dataStore.str.W0001}
          textEmail={this.props.dataStore.str.W0093}
          textFb={this.props.dataStore.str.W0094}
          textKakao={this.props.dataStore.str.W0095}
          textBottom={this.props.dataStore.str.W0002}
          linkEmail={'/register/email'}
          linkBottom={'/login'}
          onClickFb={(e)=>this.onClickFb(e)}
          onClickKakao={(e)=>this.onClickKakao(e)}>
        </CommonLogin>
        {this.renderTermsAgreement()}
        {
          this.state.isErrRegister
          &&
          <span className="terms-agreement-register-failed">{this.state.errMsg}</span>
        }
      </Fragment>

    )

  }
}
export default Register
