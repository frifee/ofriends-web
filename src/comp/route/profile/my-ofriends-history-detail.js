import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
import {withRouter} from 'react-router-dom'
import ProfileTab from '../../my/profile-tab'
import UserInfo from '../../my/user-info'
import MyOfriendsTab from '../../my/my-ofriends-tab'
import ItemMyOfriends from '../../my/item-my-ofriends'
import OneBtn from '../../dialog/one-btn'
import TwoBtn from '../../dialog/two-btn'
import '../../../static/scss/my-ofriends-history-detail.scss'
const sampleData = {
  "data" : [
    {
      "id" : 0,
      "count" : 3,
      "available" : 0,
      "cancel" : 1,
      "title" : "미니사과따고 사과잼도 만들고1",
      "option" : "A-1 소인 1인 입장권/B-1 소인2인 입장권",
      "date" : "2019-10-08",
      "due_date" : "2020-12-21",
      "location" : "서울",
      "materials" : "공병",
      "imageUrl" : "https://t1.daumcdn.net/cfile/tistory/2652163B558289520C"
    },
    {
      "id" : 1,
      "count" : 3,
      "available" : 2,
      "cancel" : 0,
      "title" : "미니사과따고 사과잼도 만들고2",
      "option" : "A-1 소인 1인 입장권/B-1 소인2인 입장권",
      "date" : "2019-10-08",
      "due_date" : "2020-12-21",
      "location" : "서울",
      "materials" : "공병",
      "imageUrl" : "https://t1.daumcdn.net/cfile/tistory/2652163B558289520C"
    },
    {
      "id" : 2,
      "count" : 3,
      "available" : 0,
      "cancel" : 3,
      "title" : "미니사과따고 사과잼도 만들고3",
      "option" : "A-1 소인 1인 입장권/B-1 소인2인 입장권",
      "date" : "2019-10-08",
      "due_date" : "2020-12-21",
      "location" : "서울",
      "materials" : "공병",
      "imageUrl" : "https://blog.toss.im/wp-content/uploads/2019/06/on_demand_insurance_travel.png"
    },
    {
      "id" : 3,
      "count" : 5,
      "available" : 1,
      "cancel" : 0,
      "title" : "미니사과따고 사과잼도 만들고4",
      "option" : "A-1 소인 1인 입장권/B-1 소인2인 입장권",
      "date" : "2019-10-08",
      "due_date" : "2020-12-21",
      "location" : "서울",
      "materials" : "공병"
    },
    {
      "id" : 4,
      "count" : 6,
      "available" : 0,
      "cancel" : 2,
      "title" : "미니사과따고 사과잼도 만들고5",
      "option" : "A-1 소인 1인 입장권/B-1 소인2인 입장권",
      "date" : "2019-10-08",
      "due_date" : "2020-12-21",
      "location" : "서울",
      "materials" : "공병",
      "imageUrl" : "https://t1.daumcdn.net/cfile/tistory/99128B3E5AD978AF20"
    }
  ]
}


@inject('stateStore', 'apiStore', 'dataStore')
@withRouter
@observer
class MyOfriendsHistoryDetail extends Component {

  constructor(props) {
    super(props)
    this.state = {
      callErrMsg: "",
      isErrCancel: false,
    }
  }

  componentDidMount() {
    this.props.stateStore.setDefaultOpen()
    this.onRouteChanged()
  }

  componentDidUpdate(prevProps) {
    if (this.props.location !== prevProps.location) {
      this.onRouteChanged()
    }
  }

  onRouteChanged() {
    let jwtToken = this.props.apiStore.getJwtToken()
    if(jwtToken === null) {
      this.props.history.push('/login')
      return
    }
    this.props.stateStore.setDefaultOpen()
    this.props.dataStore.setSampleMyOfriendsHistory(sampleData)
    this.props.dataStore.setDataPurchaseEach(undefined)
    this.getPurchaseEach()
  }

  getPurchaseEach() {
    const purchaseId = this.props.match.params.id
    let jwtToken = this.props.apiStore.getJwtToken()
    this.props.apiStore.getPurchaseEach(jwtToken, purchaseId)
      .then(res => {
        this.props.dataStore.setDataPurchaseEach(res.data)
      })
      .catch(err => {

      })
  }

  onClickCancelSched = (e) => {
    this.props.stateStore.setPopupOn('prePurchaseCancel')
  }

  postPurchasesCancel() {
    const dataPurchaseEach = this.props.dataStore.dataPurchaseEach
    if(dataPurchaseEach != undefined && dataPurchaseEach != null) {
      // this.props.stateStore.setPopupOn('postPurchaseCancel')
      let jwtToken = localStorage.getItem('jwt')
      let payload = {
        "purchase": dataPurchaseEach.id,
        "cancel_request_amount": dataPurchaseEach.tx_paid_amount,
        "reason": ""//,
        // "refund_holder": "string",
        // "refund_bank": "string",
        // "refund_account": "string"
      }
      this.props.apiStore.postPurchasesCancel(jwtToken, dataPurchaseEach.id, payload)
        .then(res => {
          this.getPurchaseEach()
          this.props.stateStore.setPopupOn('postPurchaseCancel')
          // this.props.dataStore.setDataPurchaseEach(res.data)
        })
        .catch(err => {
          if(err.response.status === 400) {
            this.setState({callErrMsg: this.props.dataStore.str.S1022})
          } else if (err.response.status === 409) {
            this.setState({callErrMsg: this.props.dataStore.str.S1023})
          } else {
            this.setState({callErrMsg: this.props.dataStore.str.S1006})
          }
          this.cancelFailedPopup()
        })
    }
  }

  onClickDeny = (e) => {
    this.props.stateStore.setPopupOn("")
  }

  onClickProcess = (e) => {
    this.props.stateStore.setPopupOn("")
    this.postPurchasesCancel()
  }

  onClickClose = (e) => {
    this.props.stateStore.setPopupOn("")
  }

  renderDialog() {
    const popupOn = this.props.stateStore.popupOn
    if(popupOn === 'prePurchaseCancel') {
      return (
        <TwoBtn
          content={this.props.dataStore.str.S0039}
          onClickBtn1={(e) => this.onClickDeny(e)}
          onClickBtn2={(e) => this.onClickProcess(e)}
          btn1 = {this.props.dataStore.str.W0170}
          btn2 = {this.props.dataStore.str.W0169}></TwoBtn>
      )
    }
    if(popupOn === 'postPurchaseCancel') {
      return (
        <OneBtn
          msg={this.props.dataStore.str.S0038}
          onClickClose={(e) => this.onClickClose(e)}>
        </OneBtn>
      )
    }
  }

  renderDetail() {

    if(this.props.dataStore.sampleMyOfriendsHistory.data === undefined)
      return
    let pageId = parseInt(this.props.match.params.id)%2
    let data = this.props.dataStore.sampleMyOfriendsHistory.data[pageId]
    const dataPurchaseEach = this.props.dataStore.dataPurchaseEach
    if(dataPurchaseEach != undefined) {
      return (
        <div className="my-ofriends-history-detail-container">
          <div className="my-ofriends-history-detail-title">
            <span>{this.props.dataStore.str.W0062}</span>
          </div>
          <ItemMyOfriends
            type={"detail"}
            itemId={dataPurchaseEach.id}
            imgUrl={dataPurchaseEach.product}
            title={dataPurchaseEach.title}
            location={dataPurchaseEach.location}
            materials={dataPurchaseEach.stuff_to_prepare}
            dueDate={dataPurchaseEach.close_date}
            date={dataPurchaseEach.ct}
            option={dataPurchaseEach.option_text}
            count={dataPurchaseEach.amount}
            available={dataPurchaseEach.status == 'confirmed'}
            cancel={dataPurchaseEach.status != 'cancelled'}
            status={dataPurchaseEach.status}
            onClickCancelSched = {(e) => this.onClickCancelSched(e)}>
          </ItemMyOfriends>
        </div>
      )
    }
  }

  renderPayment() {
    const dataPurchaseEach = this.props.dataStore.dataPurchaseEach
    if(dataPurchaseEach != undefined) {
      return (
        <div className="my-ofriends-history-detail-payment-container">
          <div className="my-ofriends-history-detail-payment-information">
            <span>{this.props.dataStore.str.W0064}</span>
          </div>
          <div className="my-ofriends-history-detail-payment-price">
            <span className="my-ofriends-history-detail-price-title">{this.props.dataStore.str.W0065}</span>
            <span className="my-ofriends-history-detail-price">{this.props.apiStore.getFormatWon(dataPurchaseEach.tx_original_paid_amount)}</span>
          </div>
          <div className="my-ofriends-history-detail-payment-discount">
            <span className="my-ofriends-history-detail-discount-title">{this.props.dataStore.str.W0066}</span>
            <span className="my-ofriends-history-detail-discount">{this.props.apiStore.getFormatWon(dataPurchaseEach.tx_original_paid_amount-dataPurchaseEach.tx_paid_amount)}</span>
          </div>
          <div className="my-ofriends-history-detail-payment-pay">
            <span className="my-ofriends-history-detail-pay-title">{this.props.dataStore.str.W0067}</span>
            <span className="my-ofriends-history-detail-pay">{this.props.apiStore.getFormatWon(dataPurchaseEach.tx_paid_amount)}</span>
          </div>
          <div className="my-ofriends-history-detail-payment-method-con">
            <span className="my-ofriends-history-detail-payment-method-title">{this.props.dataStore.str.W0068}</span>
            <span className="my-ofriends-history-detail-payment-method">{dataPurchaseEach.tx_pay_method}</span>
          </div>
        </div>
      )
    }


  }

  renderNoti() {
    if(this.state.isErrCancel) {
      return (
        <span className="my-ofriends-history-notice-msg">{this.state.callErrMsg}</span>
      )
    }
  }

  cancelFailedPopup() {
    this.setState({isErrCancel:true})
    setTimeout(() => {
      this.setState({isErrCancel:false, callErrMsg:""})
    }, 2000)
  }

  render() {
    return (
      <div className="my-ofriends-history-detail-entity-container">
        <UserInfo></UserInfo>
        <ProfileTab tabPos={2}></ProfileTab>
        <MyOfriendsTab tabPos={1}></MyOfriendsTab>
        {this.renderDetail()}
        {this.renderPayment()}
        {this.renderDialog()}
        {this.renderNoti()}
      </div>
    )

  }
}
export default MyOfriendsHistoryDetail
