import React, {Component, Fragment} from 'react'
import { inject, observer } from 'mobx-react'
import Pagination from "react-js-pagination"
import ProfileTab from '../../my/profile-tab'
import UserInfo from '../../my/user-info'
import MyOfriendsTab from '../../my/my-ofriends-tab'
import ItemMyOfriends from '../../my/item-my-ofriends'
import NoContents from '../../my/no-contents'
import '../../../static/scss/my-ofriends-available.scss'
import {withRouter} from 'react-router-dom'

const sampleData = {
  "data" : [
    {
      "id" : 0,
      "title" : "미니사과따고 사과잼도 만들고1",
      "option" : "A-1 소인 1인 입장권/B-1 소인2인 입장권",
      "date" : "2019-10-08",
      "due_date" : "2020-12-21",
      "location" : "서울",
      "materials" : "공병",
      "imageUrl" : "https://t1.daumcdn.net/cfile/tistory/2652163B558289520C"
    },
    {
      "id" : 1,
      "title" : "미니사과따고 사과잼도 만들고2",
      "option" : "A-1 소인 1인 입장권/B-1 소인2인 입장권",
      "date" : "2019-10-08",
      "due_date" : "2020-12-21",
      "location" : "서울",
      "materials" : "공병",
      "imageUrl" : "https://t1.daumcdn.net/cfile/tistory/2652163B558289520C"
    },
    {
      "id" : 2,
      "title" : "미니사과따고 사과잼도 만들고3",
      "option" : "A-1 소인 1인 입장권/B-1 소인2인 입장권",
      "date" : "2019-10-08",
      "due_date" : "2020-12-21",
      "location" : "서울",
      "materials" : "공병",
      "imageUrl" : "https://blog.toss.im/wp-content/uploads/2019/06/on_demand_insurance_travel.png"
    },
    {
      "id" : 3,
      "title" : "미니사과따고 사과잼도 만들고4",
      "option" : "A-1 소인 1인 입장권/B-1 소인2인 입장권",
      "date" : "2019-10-08",
      "due_date" : "2020-12-21",
      "location" : "서울",
      "materials" : "공병"
    },
    {
      "id" : 4,
      "title" : "미니사과따고 사과잼도 만들고5",
      "option" : "A-1 소인 1인 입장권/B-1 소인2인 입장권",
      "date" : "2019-10-08",
      "due_date" : "2020-12-21",
      "location" : "서울",
      "materials" : "공병",
      "imageUrl" : "https://t1.daumcdn.net/cfile/tistory/99128B3E5AD978AF20"
    }
  ]
}


@inject('stateStore', 'apiStore', 'dataStore')
@withRouter
@observer
class MyOfriendsAvailable extends Component {

  componentDidMount() {
    this.props.stateStore.setDefaultOpen()
    this.onRouteChanged()
  }

  componentDidUpdate(prevProps) {
    if (this.props.location !== prevProps.location) {
      this.onRouteChanged()
    }
  }

  onRouteChanged() {
    let jwtToken = this.props.apiStore.getJwtToken()
    if(jwtToken === null) {
      this.props.history.push('/login')
      return
    }
    this.props.stateStore.setDefaultOpen()
    this.props.dataStore.setDataPurchases([])
    this.props.stateStore.setMyOfrindsAvailState('done', false)
    this.getPurchase(1)
  }

  getPurchase(page) {
    const myOfrindsAvailState = this.props.stateStore.myOfrindsAvailState
    let jwtToken = this.props.apiStore.getJwtToken()
    if(jwtToken === null) {
      this.props.history.push('/login')
      return
    }

    let params = []
    let fromIdx = (page - 1) * 8
    let toIdx = fromIdx + 8
    let range = '[' + fromIdx + ',' + toIdx + ']'
    params.push({
      key: 'range',
      value: range
    })
    params.push({
      key: 'filter',
      value: "{\"status\": [\"confirmed\"]}"
    })
    this.props.apiStore.getPurchase(jwtToken, params)
      .then(res => {
        let range = res.headers['content-range'].split(' ')[1]
        let fromIdx = range.split('/')[0].split('-')[0]
        let toIdx = range.split('/')[0].split('-')[1]
        let count = range.split('/')[1]
        this.props.stateStore.setMyOfrindsAvailState('from', fromIdx)
        this.props.stateStore.setMyOfrindsAvailState('to', toIdx)
        this.props.stateStore.setMyOfrindsAvailState('count', count)
        this.props.stateStore.setMyOfrindsAvailState('page', page)
        this.props.stateStore.setMyOfrindsAvailState('done', true)
        this.props.dataStore.setDataPurchases([])
        this.props.dataStore.setDataPurchases(res.data)
      })
      .catch(err => {

      })
  }

  handlePageChange(pageNumber) {
    const myOfrindsAvailState = this.props.stateStore.myOfrindsAvailState
    if(pageNumber != myOfrindsAvailState.page) {
      this.getPurchase(pageNumber)
    }
  }
  renderBody() {
    const dataPurchases = this.props.dataStore.dataPurchases
    const myOfrindsAvailState = this.props.stateStore.myOfrindsAvailState
    if(dataPurchases != undefined) {
      if(dataPurchases.length==0 &&myOfrindsAvailState.done === true) {
        return (
          <NoContents
            iconClass="icon-none-available"
            text={this.props.dataStore.str.S0011}>
          </NoContents>
        )
      } else {
        let items = []
        for(let row of dataPurchases) {
          items.push(
            <li className="my-ofriends-available-item" key={row.id}>
              <ItemMyOfriends
                type={"available"}
                itemId={row.id}
                imgUrl={row.product}
                title={row.title}
                location={row.location}
                materials={row.stuff_to_prepare}
                dueDate={row.close_date}
                date={row.ct}
                option={row.option_text}
                count={row.amount}
                available={row.status}
                cancel={row.status}
                status={row.status}>
              </ItemMyOfriends>
            </li>
          )
        }

        return (
          <ul className="my-ofriends-available-container">{items}</ul>
        )
      }
    }
  }

  renderPagination() {

    const myOfrindsAvailState = this.props.stateStore.myOfrindsAvailState
    let activePage = myOfrindsAvailState.page
    let totalItemsCount = myOfrindsAvailState.count
    let pageRangeDisplayed = (myOfrindsAvailState.count/8) >= 5 ? 5 : parseInt(myOfrindsAvailState.count/8 + 1)

    if(myOfrindsAvailState == undefined || myOfrindsAvailState.count === "0") {
      return(
          <Fragment></Fragment>
      )
    } else {
      return (
        <Pagination
          activePage={activePage}
          itemsCountPerPage={10}
          totalItemsCount={totalItemsCount}
          pageRangeDisplayed={pageRangeDisplayed + 1}
          onChange={this.handlePageChange.bind(this)}
          firstPageText={""}
          prevPageText={""}
          nextPageText={""}
          lastPageText={""}
          itemClassFirst="first-link"
          itemClassPrev="prev-link"
          itemClassNext="next-link"
          itemClassLast="last-link"
          linkClass="page-link"
          innerClass="pagination"
          itemClass="pg"
          activeClass="current"
        />
      )
    }
  }

  render() {
    return (
      <div className="my-ofriends-available-entity-container">
        <UserInfo></UserInfo>
        <ProfileTab tabPos={2}></ProfileTab>
        <MyOfriendsTab tabPos={0}></MyOfriendsTab>
        {this.renderBody()}
        {this.renderPagination()}
      </div>
    )

  }
}
export default MyOfriendsAvailable
