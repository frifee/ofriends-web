import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'

import ProfileTab from '../../my/profile-tab'
import UserInfo from '../../my/user-info'
import LikeBody from '../../my/like-body'
import '../../../static/scss/my-like.scss'

@inject('stateStore', 'apiStore', 'dataStore')
@observer
class MyLike extends Component {

  componentDidMount() {
    this.props.stateStore.setDefaultOpen()
    this.onRouteChanged()
  }

  componentDidUpdate(prevProps) {
    if (this.props.location !== prevProps.location) {
      this.onRouteChanged()
    }
  }

  onRouteChanged() {
    let jwtToken = this.props.apiStore.getJwtToken()
    if(jwtToken === null) {
      this.props.history.push('/login')
      return
    }
  }

  render() {
    return (
      <div className="my-like-entity-container">
        <UserInfo {...this.props}></UserInfo>
        <ProfileTab tabPos={1} {...this.props}></ProfileTab>
        <LikeBody {...this.props}></LikeBody>
      </div>
    )

  }
}
export default MyLike
