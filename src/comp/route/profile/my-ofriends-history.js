import React, {Component, Fragment} from 'react'
import { inject, observer } from 'mobx-react'
import Pagination from "react-js-pagination"
import ProfileTab from '../../my/profile-tab'
import UserInfo from '../../my/user-info'
import MyOfriendsTab from '../../my/my-ofriends-tab'
import ItemMyOfriends from '../../my/item-my-ofriends'
import NoContents from '../../my/no-contents'
import {withRouter} from 'react-router-dom'
import '../../../static/scss/my-ofriends-history.scss'
const sampleData = {
  "data" : [
    {
      "id" : 0,
      "count" : 3,
      "available" : 0,
      "cancel" : 1,
      "title" : "미니사과따고 사과잼도 만들고1",
      "option" : "A-1 소인 1인 입장권/B-1 소인2인 입장권",
      "date" : "2019-10-08",
      "due_date" : "2020-12-21",
      "location" : "서울",
      "materials" : "공병",
      "imageUrl" : "https://t1.daumcdn.net/cfile/tistory/2652163B558289520C"
    },
    {
      "id" : 1,
      "count" : 3,
      "available" : 2,
      "cancel" : 0,
      "title" : "미니사과따고 사과잼도 만들고2",
      "option" : "A-1 소인 1인 입장권/B-1 소인2인 입장권",
      "date" : "2019-10-08",
      "due_date" : "2020-12-21",
      "location" : "서울",
      "materials" : "공병",
      "imageUrl" : "https://t1.daumcdn.net/cfile/tistory/2652163B558289520C"
    },
    {
      "id" : 2,
      "count" : 3,
      "available" : 0,
      "cancel" : 3,
      "title" : "미니사과따고 사과잼도 만들고3",
      "option" : "A-1 소인 1인 입장권/B-1 소인2인 입장권",
      "date" : "2019-10-08",
      "due_date" : "2020-12-21",
      "location" : "서울",
      "materials" : "공병",
      "imageUrl" : "https://blog.toss.im/wp-content/uploads/2019/06/on_demand_insurance_travel.png"
    },
    {
      "id" : 3,
      "count" : 5,
      "available" : 1,
      "cancel" : 0,
      "title" : "미니사과따고 사과잼도 만들고4",
      "option" : "A-1 소인 1인 입장권/B-1 소인2인 입장권",
      "date" : "2019-10-08",
      "due_date" : "2020-12-21",
      "location" : "서울",
      "materials" : "공병"
    },
    {
      "id" : 4,
      "count" : 6,
      "available" : 0,
      "cancel" : 2,
      "title" : "미니사과따고 사과잼도 만들고5",
      "option" : "A-1 소인 1인 입장권/B-1 소인2인 입장권",
      "date" : "2019-10-08",
      "due_date" : "2020-12-21",
      "location" : "서울",
      "materials" : "공병",
      "imageUrl" : "https://t1.daumcdn.net/cfile/tistory/99128B3E5AD978AF20"
    }
  ]
}

@inject('stateStore', 'apiStore', 'dataStore')
@withRouter
@observer
class MyOfriendsHistory extends Component {
  componentDidMount() {
    this.props.stateStore.setDefaultOpen()
    this.onRouteChanged()
  }

  componentDidUpdate(prevProps) {
    if (this.props.location !== prevProps.location) {
      this.onRouteChanged()
    }
  }

  onRouteChanged() {
    let jwtToken = this.props.apiStore.getJwtToken()
    if(jwtToken === null) {
      this.props.history.push('/login')
      return
    }
    this.props.stateStore.setDefaultOpen()
    this.props.dataStore.setDataPurchases([])
    this.props.stateStore.setMyOfriendsHisState('done', false)
    this.getPurchase(1)
  }

  getPurchase(page) {
    const myOfriendsHisState = this.props.stateStore.myOfriendsHisState
    let jwtToken = this.props.apiStore.getJwtToken()
    if(jwtToken === null) {
      this.props.history.push('/login')
      return
    }

    let params = []
    let fromIdx = (page - 1) * 8
    let toIdx = fromIdx + 8
    let range = '[' + fromIdx + ',' + toIdx + ']'
    params.push({
      key: 'range',
      value: range
    })
    params.push({
      key: 'filter',
      value: "{\"status\": [\"finished\", \"reviewed\", \"cancelled\", \"confirmed\"]}"
    })
    this.props.apiStore.getPurchase(jwtToken, params)
      .then(res => {
        let range = res.headers['content-range'].split(' ')[1]
        let fromIdx = range.split('/')[0].split('-')[0]
        let toIdx = range.split('/')[0].split('-')[1]
        let count = range.split('/')[1]
        this.props.stateStore.setMyOfriendsHisState('from', fromIdx)
        this.props.stateStore.setMyOfriendsHisState('to', toIdx)
        this.props.stateStore.setMyOfriendsHisState('count', count)
        this.props.stateStore.setMyOfriendsHisState('page', page)
        this.props.stateStore.setMyOfriendsHisState('done', true)
        this.props.dataStore.setDataPurchases([])
        this.props.dataStore.setDataPurchases(res.data)
      })
      .catch(err => {

      })
  }

  handlePageChange(pageNumber) {

    const myOfriendsHisState = this.props.stateStore.myOfriendsHisState
    if(pageNumber != myOfriendsHisState.page) {
      this.getPurchase(pageNumber)
    }
  }

  renderBody() {
    let historyList = this.props.dataStore.sampleMyOfriendsHistory
    const myOfriendsHisState = this.props.stateStore.myOfriendsHisState
    const dataPurchases = this.props.dataStore.dataPurchases
    if(dataPurchases != undefined && myOfriendsHisState.done === true) {
      if(dataPurchases.length==0) {
        return (
          <NoContents
            iconClass="icon-none-available"
            text={this.props.dataStore.str.S1050}>
          </NoContents>
        )
      } else {
        let items = []
        for(let row of dataPurchases) {
          items.push(
            <li className="my-ofriends-history-item" key={row.id}>
              <ItemMyOfriends
                type={"history"}
                itemId={row.id}
                imgUrl={row.product}
                title={row.title}
                location={row.location}
                materials={row.stuff_to_prepare}
                dueDate={row.close_date}
                date={row.ct}
                option={row.option_text}
                count={row.amount}
                available={row.status}
                cancel={row.status}
                status={row.status}>
              </ItemMyOfriends>
            </li>
          )
        }
        return (
          <ul className="my-ofriends-history-container">{items}</ul>
        )
      }
    }
  }

  renderPagination() {

    const myOfriendsHisState = this.props.stateStore.myOfriendsHisState
    let activePage = myOfriendsHisState.page
    let totalItemsCount = myOfriendsHisState.count
    let pageRangeDisplayed = (myOfriendsHisState.count/8) >= 5 ? 5 : parseInt(myOfriendsHisState.count/8 + 1)

    if(myOfriendsHisState == undefined || myOfriendsHisState.count === "0") {
      return(
        <Fragment></Fragment>
      )
    } else {
      return (
        <Pagination
          activePage={myOfriendsHisState.page}
          itemsCountPerPage={8}
          totalItemsCount={myOfriendsHisState.count}
          pageRangeDisplayed={pageRangeDisplayed}
          onChange={this.handlePageChange.bind(this)}
          firstPageText={""}
          prevPageText={""}
          nextPageText={""}
          lastPageText={""}
          itemClassFirst="first-link"
          itemClassPrev="prev-link"
          itemClassNext="next-link"
          itemClassLast="last-link"
          linkClass="page-link"
          innerClass="pagination"
          itemClass="pg"
          activeClass="current"
        />
      )
    }
  }

  render() {
    return (
      <div className="my-ofriends-history-entity-container">
        <UserInfo></UserInfo>
        <ProfileTab tabPos={2}></ProfileTab>
        <MyOfriendsTab tabPos={1}></MyOfriendsTab>
        {this.renderBody()}
        {this.renderPagination()}
      </div>
    )

  }
}
export default MyOfriendsHistory
