import React, { Component, Fragment } from 'react'
import { inject, observer } from 'mobx-react'
import {withRouter} from 'react-router-dom'
import ProfileTab from '../../my/profile-tab'
import UserInfo from '../../my/user-info'
import MyOfriendsTab from '../../my/my-ofriends-tab'
import ItemReviews from '../../my/item-review'
import Pagination from "react-js-pagination"
import NoContents from '../../my/no-contents'
import '../../../static/scss/my-ofriends-review.scss'

const myHistory = {
  "data" : [
    {
      "id" : 0,
      "rate" : 4,
      "isReview" : true,
      "date" : "2019-10-30",
      "text" : "뭐 이딴게 다있어 맛은 있는데 뭔가 쫌 이상한것 같음",
      "frip_info" : {
        "name" : "미니사과 따고 빵 만들기1",
        "date" : "2019-11-13",
        "option" : "참가비(1인)"
      },
      "images" : [
        "https://res.cloudinary.com/frientrip/image/upload/c_limit,w_600,h_600,dpr_3.0/736936_cropped4037585446048028594",
        "https://res.cloudinary.com/frientrip/image/upload/c_limit,w_600,h_600,dpr_3.0/736936_cropped4037585446048028594",
        "https://res.cloudinary.com/frientrip/image/upload/c_limit,w_600,h_600,dpr_3.0/736936_cropped4037585446048028594"
      ]
    },
    {
      "id" : 1,
      "rate" : 3,
      "isReview" : true,
      "date" : "2019-10-30",
      "text" : "뭐 이딴게 다있어 맛은 있는데 뭔가 쫌 이상한것 같음",
      "frip_info" : {
        "name" : "미니사과 따고 빵 만들기2",
        "date" : "2019-11-13",
        "option" : "참가비(1인)"
      },
      "images" : [ ]
    },
    {
      "id" : 2,
      "rate" : 5,
      "isReview" : true,
      "date" : "2019-10-30",
      "text" : "뭐 이딴게 다있어 맛은 있는데 뭔가 쫌 이상한것 같음",
      "frip_info" : {
        "name" : "미니사과 따고 빵 만들기3",
        "date" : "2019-11-13",
        "option" : "참가비(1인)"
      },
      "images" : [
        "https://res.cloudinary.com/frientrip/image/upload/c_limit,w_600,h_600,dpr_3.0/736936_cropped4037585446048028594"
      ]
    },
    {
      "id" : 3,
      "rate" : 4,
      "isReview" : false,
      "date" : "2019-10-30",
      "text" : "뭐 이딴게 다있어 맛은 있는데 뭔가 쫌 이상한것 같음",
      "frip_info" : {
        "name" : "미니사과 따고 빵 만들기4",
        "date" : "2019-11-13",
        "option" : "참가비(1인)"
      },
      "images" : [
        "https://res.cloudinary.com/frientrip/image/upload/c_limit,w_600,h_600,dpr_3.0/736936_cropped4037585446048028594",
        "https://res.cloudinary.com/frientrip/image/upload/c_limit,w_600,h_600,dpr_3.0/736936_cropped4037585446048028594",
        "https://res.cloudinary.com/frientrip/image/upload/c_limit,w_600,h_600,dpr_3.0/736936_cropped4037585446048028594"
      ]
    },
    {
      "id" : 4,
      "rate" : 4,
      "isReview" : true,
      "date" : "2019-10-30",
      "text" : "뭐 이딴게 다있어 맛은 있는데 뭔가 쫌 이상한것 같음",
      "frip_info" : {
        "name" : "미니사과 따고 빵 만들기5",
        "date" : "2019-11-13",
        "option" : "참가비(1인)"
      },
      "images" : [ ]
    },
    {
      "id" : 5,
      "rate" : 4,
      "isReview" : false,
      "date" : "2019-10-30",
      "text" : "뭐 이딴게 다있어 맛은 있는데 뭔가 쫌 이상한것 같음",
      "frip_info" : {
        "name" : "미니사과 따고 빵 만들기6",
        "date" : "2019-11-13",
        "option" : "참가비(1인)"
      },
      "images" : [
        "https://res.cloudinary.com/frientrip/image/upload/c_limit,w_600,h_600,dpr_3.0/736936_cropped4037585446048028594"
      ]
    }
  ]
}

@inject('stateStore', 'apiStore', 'dataStore')
@withRouter
@observer
class MyOfriendsReview extends Component {

  componentDidMount() {
    this.props.stateStore.setDefaultOpen()
    this.onRouteChanged()
  }

  componentDidUpdate(prevProps) {
    if (this.props.location !== prevProps.location) {
      this.onRouteChanged()
    }
  }

  onRouteChanged() {

    this.props.stateStore.setDefaultOpen()
    // this.props.dataStore.setSampleMyReviews(myHistory)
    this.props.dataStore.setDataPurchases([])
    this.getPurchase(1)
  }

  getPurchase(page) {
    const myReviewState = this.props.stateStore.myReviewState
    let jwtToken = this.props.apiStore.getJwtToken()
    if(jwtToken === null) {
      this.props.history.push('/login')
      return
    }

    let params = []
    let fromIdx = (page - 1) * 8
    let toIdx = fromIdx + 8
    let range = '[' + fromIdx + ',' + toIdx + ']'
    params.push({
      key: 'range',
      value: range
    })
    params.push({
      key: 'filter',
      value: "{\"review\":" + 1 + "\,\"status\": [\"finished\", \"reviewed\"]}"
    })

    this.props.apiStore.getPurchase(jwtToken, params)
      .then(res => {

        let range = res.headers['content-range'].split(' ')[1]
        let fromIdx = range.split('/')[0].split('-')[0]
        let toIdx = range.split('/')[0].split('-')[1]
        let count = range.split('/')[1]
        this.props.stateStore.setMyOfriendsHisState('from', fromIdx)
        this.props.stateStore.setMyOfriendsHisState('to', toIdx)
        this.props.stateStore.setMyOfriendsHisState('count', count)
        this.props.stateStore.setMyOfriendsHisState('page', page)
        this.props.stateStore.setMyOfriendsHisState('done', true)

        this.props.dataStore.setDataPurchases([])
        this.props.dataStore.setDataPurchases(res.data)
      })
      .catch(err => {

      })
  }

  handlePageChange(pageNumber) {

  }

  renderReviewHost() {
    // if(this.props.dataStore.sampleMyReviews.data == undefined)
    //   return
    const dataPurchases = this.props.dataStore.dataPurchases
    let data = this.props.dataStore.sampleMyReviews.data
    if(dataPurchases.length==0) {
      return (
        <NoContents
          iconClass="icon-none-available"
          text={this.props.dataStore.str.S1051}>
        </NoContents>
      )
    } else {
      let reviews = []
      const dataPurchases = this.props.dataStore.dataPurchases

      for(let row of dataPurchases) {
        let isReview = row.review_id != null
        reviews.push(
          <div className="my-ofriends-review-item"
            key={row.id}>
            <ItemReviews
              key={'review:'+row.id}
              id={row.id}
              reviewId={row.review_id}
              hostId={row.host}
              productId={row.product}
              rate={row.review_rating}
              isReview={isReview}
              date={row.ut}
              text={row.review_body}
              title={row.title}
              catchPhrase={row.catch_phrase}
              optionName={row.option_name}
              optionNumber={row.option_number}
              optionDate={row.option_time}
              optionId={row.product_option}
              images={row.image_files}></ItemReviews>
          </div>
        )
      }
      return (
        <Fragment>
          <div className="my-ofriends-review-reviews-container">
            {reviews}
          </div>
        </Fragment>)
    }

  }

  renderPagination() {

    const myOfriendsHisState = this.props.stateStore.myOfriendsHisState
    let activePage = myOfriendsHisState.page
    let totalItemsCount = myOfriendsHisState.count
    let pageRangeDisplayed = (myOfriendsHisState.count/8) >= 5 ? 5 : parseInt(myOfriendsHisState.count/8 + 1)

    if(myOfriendsHisState == undefined || myOfriendsHisState.count === "0") {
      return(
          <Fragment></Fragment>
      )
    } else {
      return (
        <Pagination
          activePage={myOfriendsHisState.page}
          itemsCountPerPage={8}
          totalItemsCount={myOfriendsHisState.count}
          pageRangeDisplayed={pageRangeDisplayed}
          onChange={this.handlePageChange.bind(this)}
          firstPageText={""}
          prevPageText={""}
          nextPageText={""}
          lastPageText={""}
          itemClassFirst="first-link"
          itemClassPrev="prev-link"
          itemClassNext="next-link"
          itemClassLast="last-link"
          linkClass="page-link"
          innerClass="pagination"
          itemClass="pg"
          activeClass="current"
        />
      )
    }
  }

  render() {
    const dataPurchases = this.props.dataStore.dataPurchases
    return (
      <div className="my-ofriends-review-entity-container" data={dataPurchases.length.toString()}>
        <UserInfo></UserInfo>
        <ProfileTab tabPos={2}></ProfileTab>
        <MyOfriendsTab tabPos={2}></MyOfriendsTab>
        {this.renderReviewHost()}
        {this.renderPagination()}
      </div>
    )

  }
}
export default MyOfriendsReview
