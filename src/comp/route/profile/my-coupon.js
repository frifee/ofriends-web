import React, { Component, Fragment } from 'react'
import { inject, observer } from 'mobx-react'
import {withRouter} from 'react-router-dom'
import ProfileTab from '../../my/profile-tab'
import UserInfo from '../../my/user-info'
import Pagination from "react-js-pagination"
import NoContents from '../../my/no-contents'
import ItemCoupon from '../../my/item-coupon'
import '../../../static/scss/my-coupon.scss'



@inject('stateStore', 'apiStore', 'dataStore')
@withRouter
@observer
class MyCoupon extends Component {

  componentDidMount() {
    this.props.stateStore.setDefaultOpen()
    this.onRouteChanged()
  }

  componentDidUpdate(prevProps) {
    if (this.props.location !== prevProps.location) {
      this.onRouteChanged()
    }
  }

  onRouteChanged() {
    let jwtToken = this.props.apiStore.getJwtToken()
    if(jwtToken === null) {
      this.props.history.push('/login')
      return
    }
    this.props.stateStore.setMyCouponState('from', 0)
    this.props.stateStore.setMyCouponState('to', 1)
    this.props.stateStore.setMyCouponState('count', 0)
    this.props.stateStore.setMyCouponState('page', 1)
    this.props.stateStore.setMyCouponState('done', false)
    this.getCoupons(1)
  }

  getCoupons(page) {
    const myCouponState = this.props.stateStore.myCouponState

    let fromIdx = (page - 1) * 8
    let toIdx = fromIdx + 8
    let range = '[' + fromIdx + ',' + toIdx + ']'
    let params = []
    params.push({
      key: 'range',
      value: range
    })
    let jwtToken = this.props.apiStore.getJwtToken()
    if(jwtToken === null) {
      this.props.history.push('/login')
      return
    }
    this.props.apiStore.getMiscCoupons(jwtToken, params).then(res=>{
      let data = res.data

      this.props.dataStore.setDataMiscCoupons([])
      this.props.dataStore.setDataMiscCoupons(res.data)

      let range = res.headers['content-range'].split(' ')[1]
      let fromIdx = range.split('/')[0].split('-')[0]
      let toIdx = range.split('/')[0].split('-')[1]
      let count = range.split('/')[1]

      this.props.stateStore.setMyCouponState('from', fromIdx)
      this.props.stateStore.setMyCouponState('to', toIdx)
      this.props.stateStore.setMyCouponState('count', count)
      this.props.stateStore.setMyCouponState('page', page)
      this.props.stateStore.setMyCouponState('done', true)

      const dataMiscCoupons = this.props.dataStore.dataMiscCoupons
    })
    .catch(err=>{

    })
  }

  handlePageChange(pageNumber) {

  }

  renderCoupon() {
    const dataMiscCoupons = this.props.dataStore.dataMiscCoupons
    const myCouponState = this.props.stateStore.myCouponState
    if(myCouponState.done === false) {
      return(<Fragment></Fragment>)
    }
    if(dataMiscCoupons.length==0) {
      return (
        <NoContents
          iconClass="icon-none-available"
          text={this.props.dataStore.str.S0037}>
        </NoContents>
      )
    } else {
      let coupons = []
      for(let row of dataMiscCoupons) {
        coupons.push(
          <ItemCoupon
            key={row.id}
            id={row.id}
            title={row.name}
            dueDate={row.expiration}
            option={row.type}></ItemCoupon>
        )
      }
      return (
        <Fragment>
          <div className="my-coupon-coupon-list-container">{coupons}</div>
          {this.renderPagination()}
        </Fragment>
      )
    }
  }

  renderPagination() {
    const myCouponState = this.props.stateStore.myCouponState
    const data = this.props.dataStore.dataMiscCoupons
    let activePage = myCouponState.page
    let totalItemsCount = myCouponState.count
    let pageRangeDisplayed = (myCouponState.count/5) >= 5 ? 5 : parseInt(myCouponState.count/5 + 1)
    if(data == undefined || data.length===0) {
      return(
        <Fragment></Fragment>
      )
    } else {
      return (
        <Pagination
          activePage={activePage}
          itemsCountPerPage={8}
          totalItemsCount={totalItemsCount}
          pageRangeDisplayed={pageRangeDisplayed+1}
          onChange={this.handlePageChange.bind(this)}
          firstPageText={""}
          prevPageText={""}
          nextPageText={""}
          lastPageText={""}
          itemClassFirst="first-link"
          itemClassPrev="prev-link"
          itemClassNext="next-link"
          itemClassLast="last-link"
          linkClass="page-link"
          innerClass="pagination"
          itemClass="pg"
          activeClass="current"
        />
      )
    }
  }

  render() {
    const dataMiscCoupons = this.props.dataStore.dataMiscCoupons
    const myCouponState = this.props.stateStore.myCouponState
    return (
      <div className="my-coupon-entity-container">
        <UserInfo></UserInfo>
        <ProfileTab tabPos={4}></ProfileTab>
        {this.renderCoupon()}
      </div>
    )

  }
}
export default MyCoupon
