import React, { Component, Fragment } from 'react'
import { inject, observer } from 'mobx-react'
import {withRouter} from 'react-router-dom'
import ProfileTab from '../../my/profile-tab'
import UserInfo from '../../my/user-info'
import ProfileBody from '../../my/profile-body'
import PwChange from '../../dialog/pw-change'
import DelRecomCode from '../../dialog/del-recom-code'
import '../../../static/scss/my-profile.scss'
@inject('stateStore', 'apiStore', 'dataStore')
@withRouter
@observer
class MyProfile extends Component {

  componentDidMount() {
    this.props.stateStore.setDefaultOpen()
    this.onRouteChanged()
  }

  componentDidUpdate(prevProps) {
    if (this.props.location !== prevProps.location) {
      this.onRouteChanged()
    }
  }

  onRouteChanged() {
    let jwtToken = this.props.apiStore.getJwtToken()
    if(jwtToken === null) {
      this.props.history.push('/login')
      return
    }
  }

  renderDialogDelRecomCode() {
    if(this.props.stateStore.myProfileState.recomCode === 3) {
      return (
        <DelRecomCode
          getUserInfo={() => this.getUserInfo()}></DelRecomCode>
      )
    }
  }

  renderPwChange() {
    if(this.props.stateStore.myProfileState.pwChange === 1) {
      return (
        <PwChange></PwChange>
      )
    }
  }

  getUserInfo() {
    let jwtToken = this.props.apiStore.getJwtToken()
    if(jwtToken === null) {
      this.props.history.push('/login')
      return
    }
    this.props.apiStore.getUserInfo(jwtToken).then(res=>{
      this.props.dataStore.setDataUserInfo(res.data)
    })
    .catch(err=> {
    })
  }

  render() {
    return (
      <div className="my-profile-entity-container">
        <UserInfo></UserInfo>
        <ProfileTab tabPos={0}></ProfileTab>
        <ProfileBody getUserInfo={() => this.getUserInfo()}></ProfileBody>
        {this.renderDialogDelRecomCode()}
        {this.renderPwChange()}
      </div>
    )

  }
}
export default MyProfile
