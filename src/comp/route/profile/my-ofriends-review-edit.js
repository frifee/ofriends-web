import React, { Component, useEffect } from 'react'
import { inject, observer } from 'mobx-react'

import ProfileTab from '../../my/profile-tab'
import UserInfo from '../../my/user-info'
import MyOfriendsTab from '../../my/my-ofriends-tab'
import ViewModeReview from '../../my/view-mode-review'
import EditModeReview from '../../my/edit-mode-review'
import {withRouter} from 'react-router-dom'
import '../../../static/scss/my-ofriends-review-edit.scss'
const myHistory = {
  "data" : [
    {
      "id" : 0,
      "rate" : 4,
      "isReview" : true,
      "date" : "2019-10-30",
      "text" : "뭐 이딴게 다있어 맛은 있는데 뭔가 쫌 이상한것 같음",
      "frip_info" : {
        "name" : "미니사과 따고 빵 만들기1",
        "date" : "2019-11-13",
        "option" : "참가비(1인)"
      },
      "images" : [
        "https://res.cloudinary.com/frientrip/image/upload/c_limit,w_600,h_600,dpr_3.0/736936_cropped4037585446048028594",
        "https://res.cloudinary.com/frientrip/image/upload/c_limit,w_600,h_600,dpr_3.0/736936_cropped4037585446048028594",
        "https://res.cloudinary.com/frientrip/image/upload/c_limit,w_600,h_600,dpr_3.0/736936_cropped4037585446048028594"
      ]
    },
    {
      "id" : 1,
      "rate" : 3,
      "isReview" : true,
      "date" : "2019-10-30",
      "text" : "뭐 이딴게 다있어 맛은 있는데 뭔가 쫌 이상한것 같음",
      "frip_info" : {
        "name" : "미니사과 따고 빵 만들기2",
        "date" : "2019-11-13",
        "option" : "참가비(1인)"
      },
      "images" : [ ]
    },
    {
      "id" : 2,
      "rate" : 5,
      "isReview" : true,
      "date" : "2019-10-30",
      "text" : "뭐 이딴게 다있어 맛은 있는데 뭔가 쫌 이상한것 같음",
      "frip_info" : {
        "name" : "미니사과 따고 빵 만들기3",
        "date" : "2019-11-13",
        "option" : "참가비(1인)"
      },
      "images" : [
        "https://res.cloudinary.com/frientrip/image/upload/c_limit,w_600,h_600,dpr_3.0/736936_cropped4037585446048028594"
      ]
    },
    {
      "id" : 3,
      "rate" : 4,
      "isReview" : false,
      "date" : "2019-10-30",
      "text" : "뭐 이딴게 다있어 맛은 있는데 뭔가 쫌 이상한것 같음",
      "frip_info" : {
        "name" : "미니사과 따고 빵 만들기4",
        "date" : "2019-11-13",
        "option" : "참가비(1인)"
      },
      "images" : [
        "https://res.cloudinary.com/frientrip/image/upload/c_limit,w_600,h_600,dpr_3.0/736936_cropped4037585446048028594",
        "https://res.cloudinary.com/frientrip/image/upload/c_limit,w_600,h_600,dpr_3.0/736936_cropped4037585446048028594",
        "https://res.cloudinary.com/frientrip/image/upload/c_limit,w_600,h_600,dpr_3.0/736936_cropped4037585446048028594"
      ]
    },
    {
      "id" : 4,
      "rate" : 4,
      "isReview" : true,
      "date" : "2019-10-30",
      "text" : "뭐 이딴게 다있어 맛은 있는데 뭔가 쫌 이상한것 같음",
      "frip_info" : {
        "name" : "미니사과 따고 빵 만들기5",
        "date" : "2019-11-13",
        "option" : "참가비(1인)"
      },
      "images" : [ ]
    },
    {
      "id" : 5,
      "rate" : 4,
      "isReview" : false,
      "date" : "2019-10-30",
      "text" : "뭐 이딴게 다있어 맛은 있는데 뭔가 쫌 이상한것 같음",
      "frip_info" : {
        "name" : "미니사과 따고 빵 만들기6",
        "date" : "2019-11-13",
        "option" : "참가비(1인)"
      },
      "images" : [
        "https://res.cloudinary.com/frientrip/image/upload/c_limit,w_600,h_600,dpr_3.0/736936_cropped4037585446048028594"
      ]
    }
  ]
}

@inject('stateStore', 'apiStore', 'dataStore')
@withRouter
@observer
class MyOfriendsReviewEdit extends Component {

  componentDidMount() {

    this.unblock = this.props.history.block((location, action) => {
      const myReviewEditState = this.props.stateStore.myReviewEditState
      if(myReviewEditState.state === 1) {
        return this.props.dataStore.str.S0033
        // return false
      }

    })

    this.onRouteChanged()

  }

  componentDidUpdate(prevProps) {
    if (this.props.location !== prevProps.location) {
      this.onRouteChanged()
    }
  }

  onRouteChanged() {
    let jwtToken = this.props.apiStore.getJwtToken()
    if(jwtToken === null) {
      this.props.history.push('/login')
      return
    }

    let ids = this.props.match.params.id.split('-')
    if(ids.length !=4)
      return

    let optionId = parseInt(ids[3])
    this.props.stateStore.setMyReviewEditState("optionId", optionId)

    let productId = parseInt(ids[2])
    this.props.stateStore.setMyReviewEditState("productId", productId)
    this.getProduct(productId)

    let hostId = parseInt(ids[1])
    this.props.stateStore.setMyReviewEditState("id", hostId)

    if(ids[0] === 'new') {
      this.setEditMode()
    } else {
      let reviewId = parseInt(ids[0])
      this.getHostReview(hostId, reviewId)
    }

    // let hostId = parseInt(ids[0])
    // let productId = parseInt(ids[1])
    // this.props.stateStore.setMyReviewEditState("id", hostId)
    // this.props.stateStore.setMyReviewEditState("productId", productId)
    //
    // this.getHostReview(hostId)
    // this.getProduct(productId)
    //
    // this.props.stateStore.setDefaultOpen()
    // this.props.dataStore.setSampleMyReviews(myHistory)
    // let pageId = parseInt(this.props.match.params.id)


    // let data = this.props.dataStore.sampleMyReviews.data[pageId]
    // let state = data.isReview === true ? 0 : 1
    // let id = data.id
    // let rate = data.isReview === true ? data.rate : 0
    // let text = data.isReview === true ? data.text : ""
    // let images = data.isReview === true ? data.images : []
    // this.props.stateStore.setMyReviewEditState("id", id)
    // this.props.stateStore.setMyReviewEditState("state", state)
    // this.props.stateStore.setMyReviewEditState("rate", rate)
    // this.props.stateStore.setMyReviewEditState("inputText", text)
    // this.props.stateStore.setMyReviewEditState("thumnails", images)

  }

  componentWillUnmount() {
    this.unblock()
  }

  getHostReview(hostId, reviewId) {
    let jwtToken = this.props.apiStore.getJwtToken()
    if(jwtToken === null) {
      this.props.history.push('/login')
      return
    }
    this.props.apiStore.getHostReviewEach(jwtToken, hostId, reviewId)
      .then(res => {
        let ids = this.props.match.params.id.split('-')
        if(ids.length !=4)
          return
        let hostId = parseInt(ids[0])
        let productId = parseInt(ids[1])
        let hostData = res.data[0]
        let inputText = hostData.body.replace(/<br\/>/g, '\n')
                            .replace(/<br>/g, '\n')
                            .replace(/&#34;/g, "\"")
                            .replace(/&#39;/g, "\'")

        this.props.stateStore.setMyReviewEditState("state", 0)
        this.props.stateStore.setMyReviewEditState("rate", hostData.rating)
        this.props.stateStore.setMyReviewEditState("inputText", inputText)
        this.props.stateStore.setMyReviewEditState("inputFormatText", hostData.body)
        this.props.stateStore.setMyReviewEditState("thumbnails", JSON.parse(hostData.image_files))
        this.props.stateStore.setMyReviewEditState("reviewId", hostData.id)
      })
      .catch(err => {
        if(err.response.status === 404) {
          this.setEditMode()

        }
      })
  }

  setEditMode() {
    this.props.stateStore.setMyReviewEditState("reviewId", -1)
    this.props.stateStore.setMyReviewEditState("state", 1)
    this.props.stateStore.setMyReviewEditState("rate", 0)
    this.props.stateStore.setMyReviewEditState("inputText", '')
    this.props.stateStore.setMyReviewEditState("thumbnails", [])
  }

  getProduct(productId) {
    this.props.apiStore.getProduct(null, productId)
    .then(res => {
      this.props.stateStore.setMyReviewEditState("productTitle", res.data.product.title)
    })
    .catch(err => {
    })
  }

  renderReview() {
    if(this.props.stateStore.myReviewEditState.state == 0) {
      return (
        <ViewModeReview></ViewModeReview>
      )
    } else {
      return (
        <EditModeReview></EditModeReview>
      )
    }
  }

  render() {
    return (
      <div className="my-ofriends-review-edit-entity-container">
        <UserInfo></UserInfo>
        <ProfileTab tabPos={2}></ProfileTab>
        <MyOfriendsTab tabPos={2}></MyOfriendsTab>
        <div className="my-ofriends-review-edit-title">{this.props.dataStore.str.W0074}</div>
        <div className="my-ofriends-review-edit-container">{this.renderReview()}</div>
      </div>
    )

  }
}
export default MyOfriendsReviewEdit
