import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
import {withRouter} from 'react-router-dom'
import ProfileTab from '../../my/profile-tab'
import UserInfo from '../../my/user-info'
import RowMyInterest from '../../my/row-my-interest'
import '../../../static/scss/my-interest.scss'

@inject('stateStore', 'apiStore', 'dataStore')
@withRouter
@observer
class MyInterest extends Component {

  constructor(props) {
    super(props)
    this.state = {
      modalUp: false,
    }
  }

  componentDidMount() {
    this.props.stateStore.setDefaultOpen()
    this.onRouteChanged()
  }

  componentDidUpdate(prevProps) {
    if (this.props.location !== prevProps.location) {
      this.onRouteChanged()
    }
  }

  onRouteChanged() {
    let jwtToken = this.props.apiStore.getJwtToken()
    if(jwtToken === null) {
      this.props.history.push('/login')
      return
    }
    this.props.stateStore.clearMyInterestSelected()
    this.getUserFavorite()
  }

  getUserFavorite() {
    let jwtToken = this.props.apiStore.getJwtToken()
    if(jwtToken === null) {
      this.props.history.push('/login')
      return
    }
    this.props.apiStore.getUserFavorite(jwtToken).then(res => {

      const favorites = res.data.favorite.split(',')
      this.props.stateStore.clearMyInterestSelected()
      for(let row of favorites) {
        this.props.stateStore.setMyInterestSelected(row)
      }
    })
    .catch(err => {

    })
  }

  renderNoti() {
    if(this.state.modalUp === true) {
      return (
          <span className="my-interest-save-msg">{this.props.dataStore.str.S1020}</span>
      )
    }
  }

  putUserFavorite() {
    const myInterestSelected = this.props.stateStore.myInterestSelected
    let jwtToken = this.props.apiStore.getJwtToken()
    if(jwtToken === null) {
      this.props.history.push('/login')
      return
    }
    let payload = {
      favorite : myInterestSelected.join()
    }
    this.props.apiStore.putUserFavorite(jwtToken, payload)
      .then(res => {
      })
      .catch(err => {

      })
  }

  onClickSave = (e) => {
    this.putUserFavorite()
    this.setState({"modalUp": true})
    setTimeout(() => {
      this.setState({"modalUp": false})
    }, 2000)
  }

  renderBody() {
    const myInterestSelected = this.props.stateStore.myInterestSelected
    let data = this.props.dataStore.interests.data
    let categories = []
    for(let row of data) {
      categories.push(
        <RowMyInterest
          selected={myInterestSelected}
          id={row.id}
          category = {row.category}
          key={row.id}
          item={row.item}></RowMyInterest>
      )
    }

    return (
      <div className="my-interest-save-categories-container">
        <div className="my-interest-save-con">
          <span onClick={(e) => {this.onClickSave(e)}}>{this.props.dataStore.str.W0075}</span>
        </div>
        <div className="my-interest-categories-con">{categories}</div>
      </div>
    )
  }

  render() {
    return (
      <div className="my-interest-entity-container">
        <UserInfo></UserInfo>
        <ProfileTab tabPos={3}></ProfileTab>
        {this.renderBody()}
        {this.renderNoti()}
      </div>
    )

  }
}
export default MyInterest
