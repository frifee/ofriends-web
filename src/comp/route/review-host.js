import React, { Component, Fragment, useEffect } from 'react'
import { inject, observer } from 'mobx-react'
import ItemReviewHost from '../product/item-review-host'
import LargerImgView from '../dialog/larger-img-view'
import Pagination from "react-js-pagination"
import {withRouter} from 'react-router-dom'
import '../../static/scss/review-host.scss'

@inject('stateStore', 'apiStore', 'dataStore')
@withRouter
@observer
class ReviewHost extends Component {

  componentDidMount() {
    this.onRouteChanged()
  }

  componentDidUpdate(prevProps) {
    if (this.props.location !== prevProps.location) {
      this.onRouteChanged()
    }
  }

  onRouteChanged() {
    this.getHostReview(1)
  }

  getHostReview(page) {
    const reviewHostState = this.props.stateStore.reviewHostState
    let jwtToken = this.props.apiStore.getJwtToken()
    let params = []
    let fromIdx = (page - 1) * 10
    let toIdx = fromIdx + 10
    let range = '[' + fromIdx + ',' + toIdx + ']'
    let hostId = this.props.match.params.id
    params.push({
      key: 'range',
      value: range
    })
    this.props.apiStore.getHostReview(jwtToken, hostId, params)
      .then(res => {
        let range = res.headers['content-range'].split(' ')[1]
        let fromIdx = range.split('/')[0].split('-')[0]
        let toIdx = range.split('/')[0].split('-')[1]
        let count = range.split('/')[1]
        this.props.stateStore.setReviewHostState('from', fromIdx)
        this.props.stateStore.setReviewHostState('to', toIdx)
        this.props.stateStore.setReviewHostState('count', count)
        this.props.stateStore.setReviewHostState('page', page)
        this.props.dataStore.setDataHostReviews({})
        this.props.dataStore.setDataHostReviews(res.data)
      })
      .catch(err => {

      })
  }

  handlePageChange(pageNumber) {
    const reviewHostState = this.props.stateStore.reviewHostState
    if(pageNumber != reviewHostState.page) {
      this.getHostReview(pageNumber)
    }
  }
  renderReviewHeader() {
    const reviewHostState = this.props.stateStore.reviewHostState
    const dataHostReviews = this.props.dataStore.dataHostReviews
    let star = []
    let key = 0
    let point = dataHostReviews.host_rating
    let pos = parseInt(point)
    for(let i=0; i<pos; i++) {
      star.push(
        <li key={key}><span className="star"></span></li>
      )
      key = key + 1
    }

    let half = point - parseInt(point) > 0
    if(half === true) {
      star.push(
        <li key={key}><span className="half-star"></span></li>
      )
      key = key + 1
      pos = pos + 1
    }

    for (let i=0; i<5-pos; i++) {
      star.push(
        <li key={key}><span className="not-star"></span></li>
      )
      key = key + 1
    }

    return (
      <Fragment>
        <div className="route-review-host-reviews-entity-con">
          <div className="route-review-host-reviews-number-star">
            <span className="route-review-host-reviews-number-con">
              {this.props.dataStore.str.W0022}
              &nbsp;
              <span className="route-review-host-reviews-number">{reviewHostState.count}</span>
            </span>
          </div>
          <ul className="route-review-host-star-con">{star}</ul>
        </div>
      </Fragment>
    )

  }

  renderReviews() {
    let data = this.props.dataStore.sampleReviewHost.data
    const dataHostReviews = this.props.dataStore.dataHostReviews
    let reviews = []
    let key = 0
    if(dataHostReviews.reviews != undefined) {
      for(let row of dataHostReviews.reviews) {
        reviews.push(
          <ItemReviewHost
            key = {row.id}
            profileId = {row.user}
            name = {row.user}
            rate = {row.rating}
            date = {row.ct}
            text = {row.body}
            fripTitle = {row.product_title}
            fripDate = {row.option_text}
            fripOption = {row.option_text}
            fripOptionCount = {row.option_number}
            thumnails = {JSON.parse(row.image_files)}
            ></ItemReviewHost>
        )
      }
    }


    return (
      <Fragment>
        {reviews}
      </Fragment>
    )
  }

  renderPagination() {
    const reviewHostState = this.props.stateStore.reviewHostState
    let activePage = reviewHostState.page
    let totalItemsCount = reviewHostState.count
    let pageRangeDisplayed = (reviewHostState.count / 10) >= 5 ? 5 : parseInt(reviewHostState.count / 10 + 1)
    if (totalItemsCount > 0) {
      return (
        <Pagination
          activePage={reviewHostState.page}
          itemsCountPerPage={10}
          totalItemsCount={reviewHostState.count}
          pageRangeDisplayed={pageRangeDisplayed}
          onChange={this.handlePageChange.bind(this)}
          firstPageText={""}
          prevPageText={""}
          nextPageText={""}
          lastPageText={""}
          itemClassFirst="first-link"
          itemClassPrev="prev-link"
          itemClassNext="next-link"
          itemClassLast="last-link"
          linkClass="page-link"
          innerClass="pagination"
          itemClass="pg"
          activeClass="current"
        />
      )
    }
  }

  renderLargerImg() {
    if(this.props.stateStore.popupOn === 'LargerImgView') {
      return (
        <LargerImgView></LargerImgView>
      )
    }
  }

  render() {
    return (
      <div className="route-review-host-all-reviews">
        {this.renderReviewHeader()}
        <div className="route-review-host-all-review-items">{this.renderReviews()}</div>
        <div className="route-review-pagination">{this.renderPagination()}</div>
        {this.renderLargerImg()}
      </div>
    )

  }
}
export default ReviewHost
