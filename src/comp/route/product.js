import React, { Component, Fragment } from 'react'
import { inject, observer } from 'mobx-react'
import { Link, withRouter } from 'react-router-dom'

import Frip from '../product/frip'
import Host from '../product/host'
import Info from '../product/info'
import ReviewHost from '../product/review-host'
import SelectOption from '../product/select-option'
import LargerImgView from '../dialog/larger-img-view'
import '../../static/scss/product.scss'
import {Helmet} from "react-helmet";

@inject('stateStore', 'apiStore', 'dataStore')
@withRouter
@observer
class Product extends Component {

  componentDidMount() {
    this.onRouteChanged()
  }

  componentDidUpdate(prevProps) {
    if (this.props.location !== prevProps.location) {
      this.onRouteChanged()
    }
  }

  onRouteChanged() {
    let productId = this.props.match.params.id

    let isOpenSched = false
    this.props.dataStore.setDataProduct({})
    this.props.stateStore.setProductOption("isOpenSched", isOpenSched)
    this.props.stateStore.setProductOption("isOpenDefault", !isOpenSched)
    this.props.stateStore.setProductOption("isSelectSched", false)
    this.props.stateStore.setProductOption("isSelectDefault", false)
    this.props.stateStore.setProductOption("optionSched", {})
    this.props.stateStore.setProductOption("optionDefault", {})
    this.props.stateStore.setProductOption("mobileSelOpt", false)

    this.props.stateStore.setProductOption("isEventProduct", false) // ※ 모바일 - 이벤트 자동참여 전용. 다른 용도로 쓰면 절대로 안 됨!!
    this.props.stateStore.setProductOption("eventProductOpt", {}) // ※ 모바일 - 이벤트 자동참여 전용. 다른 용도로 쓰면 절대로 안 됨!!
    this.getProduct(productId)
    if (/bz_tracking_id/.test(window.location.search)) { localStorage.BuzzAd = window.location.search }

  }

  getProduct(productId) {
    const token = this.props.apiStore.getJwtToken()
    this.props.apiStore.getProduct(token, productId).then(res=>{
      try {
        this.props.dataStore.setDataProduct({})
        this.props.dataStore.setDataProduct(res.data)
        if(res.data.product_options!==undefined && res.data.product_options.length>0) {
          let totalSteps = res.data.product_options[0].total_steps

          let isOpenSched = totalSteps === 2

          let isEventPrd = (res.data.product_options.lastIndex === 1) &&
            (res.data.product_options[1].price === 0 ||
              res.data.product_options[1].price === null ||
              res.data.product_options[1].price === undefined)

          this.props.stateStore.setProductOption("totalSteps", totalSteps)
          this.props.stateStore.setProductOption("parentId", 0)
          this.props.stateStore.setProductOption("isOpenSched", isOpenSched)
          this.props.stateStore.setProductOption("isOpenDefault", !isOpenSched)
          this.props.stateStore.setProductOption("isSelectSched", false)
          this.props.stateStore.setProductOption("isSelectDefault", false)
          this.props.stateStore.setProductOption("optionSched", {})
          this.props.stateStore.setProductOption("optionDefault", {})

          if(isEventPrd) {
            this.props.stateStore.setProductOption("isEventProduct", isEventPrd)
            this.props.stateStore.setProductOption("eventProductOpt", res.data.product_options[1])
          }
        }
        this.renderVenueMap()
        this.renderGatheringMap()
      } catch(e) {

      }

    })
      .catch(err=> {
        this.props.history.push('/error')
      })
  }

  renderMap(id, lat, lng, posName, addr) {
    let container = window.document.getElementById(id)
    container.style.width = '500px'
    container.style.height = '500px'

    let options = {
      center: new window.kakao.maps.LatLng(lat, lng),
      level: 3
    }

    let map = new window.kakao.maps.Map(container, options)

    let markerPosition  = new window.kakao.maps.LatLng(lat, lng);

    let marker = new window.kakao.maps.Marker({
      position: markerPosition
    })

    marker.setMap(map)

    let href = 'https://map.kakao.com/link/map/'+ posName +  ',' + lat + ',' + lng
    let iwContent = '<div style="padding:5px;">' + posName + '<br><a href="' + href + '" style="color:blue" target="_blank">큰지도보기</a></div>'

    let iwPosition = new window.kakao.maps.LatLng(lat, lng)

    let infowindow = new window.kakao.maps.InfoWindow({
      position : iwPosition,
      content : iwContent
    })
    infowindow.open(map, marker)
  }

  renderVenueMap() {
    const product = this.props.dataStore.dataProduct.product
    let venue = product.venue
    let lat = product.venue_lat
    let lng = product.venue_lng
    if(venue != null) {
      this.renderMap('venue_map', lat, lng, '진행 장소', venue)
      this.renderMap('venue_map_m', lat, lng, '진행 장소', venue)
    }
  }

  renderGatheringMap() {
    const product = this.props.dataStore.dataProduct.product
    let gatheringPlace = product.gathering_place
    let lat = product.geo_lat
    let lng = product.geo_lng
    if(gatheringPlace != null
      && lat!=product.venue_lat
      && lng!=product.venue_lng) {
      this.renderMap('gathering_map', lat, lng, '모이는 장소', gatheringPlace)
      this.renderMap('gathering_map_m', lat, lng, '모이는 장소', gatheringPlace)
    }
  }

  renderLargerImg() {
    if(this.props.stateStore.popupOn === 'LargerImgView') {
      return (
        <LargerImgView></LargerImgView>
      )
    }
  }

  render() {
    let product = this.props.dataStore.dataProduct.product
    if(product == undefined) {
      return (<Fragment></Fragment>)
    }

    return (
      <Fragment>
        <div className="product-page desktop">
          <div className="product-intro">
            <Frip></Frip>
            {
              product.frip_type != 'event'
              &&
              <Fragment>
                <Host></Host>
                <ReviewHost></ReviewHost>
              </Fragment>
            }
            <Info isMobile={false}></Info>
          </div>
          <div className="product-select">
            <SelectOption isMobile={false}></SelectOption>
          </div>
        </div>
        <div className="product-page mobile">
          <div className="product-intro">
            <Frip isEvent={product.frip_type === 'event' ? true : false}></Frip>
            {
              product.frip_type != 'event'
              &&
              <Fragment>
                <Host></Host>
                <ReviewHost></ReviewHost>
              </Fragment>
            }
            <Info isMobile={true}></Info>
          </div>
          <div
            className="product-select"
            style={{display : this.props.stateStore.productOption.mobileSelOpt === true ? 'flex' : 'none'}}>
            <SelectOption isMobile={true}></SelectOption>
          </div>
        </div>
        {this.renderLargerImg()}
      </Fragment>
    )
  }
}
export default Product
