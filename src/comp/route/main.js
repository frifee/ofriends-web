import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
import { Carousel } from 'react-responsive-carousel'
import { Link, withRouter } from 'react-router-dom'
import MainHeader from '../main/main-header'
import Daily from '../main/daily'
import Recom from '../main/recom'
import Special from '../main/special'
import '../../static/scss/main.scss'
import OneBtn from "../dialog/one-btn"
import TwoBtn from "../dialog/two-btn"
import {HOME_URL} from "../../store/api-store"

@inject('stateStore', 'apiStore', 'dataStore')
@withRouter
@observer
class Main extends Component {

  constructor(props) {
    super(props)
    this.state = {
      'eventPopup' : true
    }
  }

  componentDidMount() {
  }

  // 이벤트 기간 팝업(임시)
  // renderEventNoti() {
  //   return (
  //     <OneBtn
  //       msg={this.props.dataStore.str.S1048}
  //       onClickClose={(e) => {
  //         this.setState({'eventPopup' : false})
  //       }}></OneBtn>
  //   )
  // }

  renderEventNoti() {
    let cookie = this.getCookie('neverShowPopup')
    if((cookie === 'false' || cookie === null || cookie === undefined) && this.state.eventPopup === true) {
      return (
        <TwoBtn
          content={this.props.dataStore.str.S1048}
          onClickBtn1={(e) => this.onClickNeverShow(e)}
          onClickBtn2={(e) => {this.setState({'eventPopup' : false})}}
          btn1 = {this.props.dataStore.str.W1037}
          btn2 = {this.props.dataStore.str.W0091}></TwoBtn>
      )
    }
  }

  onClickNeverShow = (e) => {
    this.setCookie('neverShowPopup', 'true', 100000)
    this.setState({'eventPopup' : false})
  }


  setCookie(name, value, days) {
    let expires = ""
    if (days) {
      let date = new Date()
      date.setTime(date.getTime() + (days*24*60*60*1000))
      expires = "; expires=" + date.toUTCString()
    }
    document.cookie = name + "=" + (value || "")  + expires + "; path=/"
  }

  getCookie(name) {
    let nameEQ = name + "=";
    let ca = document.cookie.split(';')
    for(let i=0; i < ca.length; i++) {
      let c = ca[i];
      while (c.charAt(0)===' ') c = c.substring(1,c.length)
      if (c.indexOf(nameEQ) === 0) return c.substring(nameEQ.length,c.length)
    }
    return null;
  }

  eraseCookie(name) {
    document.cookie = name+'=; Max-Age=-99999999;'
  }


  renderHeaderTab() {
    switch (this.props.stateStore.mainHeaderTab) {
      case 0:
        return (<Recom></Recom>)
        break
      case 1:
        return (<Daily></Daily>)
        break
      case 2:
        return (<Special></Special>)
        break
      default:
        return null
    }
  }
  render() {
    return (
      <div className="main-entity-container">
        <MainHeader></MainHeader>
        { this.renderHeaderTab() }
      </div>
    )
  }
}
export default Main
