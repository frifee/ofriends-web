import React, { Component, Fragment } from 'react'
import { inject, observer } from 'mobx-react'
import ItemRisingOfriends from '../main/item-rising-ofriends'
import { Link, withRouter } from 'react-router-dom'
import Pagination from "react-js-pagination"

@inject('stateStore', 'apiStore', 'dataStore')
@withRouter
@observer
class Search extends Component {
  componentDidMount() {
    this.props.stateStore.setSearchState('from', 0)
    this.props.stateStore.setSearchState('to', 0)
    this.props.stateStore.setSearchState('count', 0)
    this.props.stateStore.setSearchState('page', 1)
    this.props.stateStore.setSearchState('keyword', "")
    this.props.stateStore.setSearchState('done', false)
    this.onRouteChanged()
  }

  componentDidUpdate(prevProps) {
    if (this.props.location !== prevProps.location) {
      this.onRouteChanged()
    }
  }

  onRouteChanged() {

    let page
    try {
      page = this.props.match.params.page == undefined ? 1 : parseInt(this.props.match.params.page)
      if(isNaN(page)) {
        this.props.history.push('/')
      }
    } catch (e) {
      this.props.history.push('/')
    }
    let keyword = this.props.match.params.keyword
    this.props.stateStore.setSearchState('page', page)
    this.props.stateStore.setSearchState('keyword', keyword)
    this.props.stateStore.setSearchState('done', false)
    this.getProductList(page)
  }

  getProductList(page) {
    this.props.dataStore.setDataProductList([])
    const searchState = this.props.stateStore.searchState
    let fromIdx = (page - 1) * 20
    let toIdx = fromIdx + 20 - 1
    let range = '[' + fromIdx + ',' + toIdx + ']'
    let params = []
    let keyword = searchState.keyword
    params.push({
      key: 'range',
      value: range
    })
    params.push({
      key: 'filter',
      value: "{\"q\":\"" + keyword + "\"}"
    })
    this.props.apiStore.getProductList(localStorage.getItem('jwt'), params).then(res=>{
      let range = res.headers['content-range'].split(' ')[1]
      let fromIdx = range.split('/')[0].split('-')[0]
      let toIdx = range.split('/')[0].split('-')[1]
      let count = range.split('/')[1]
      this.props.stateStore.setSearchState('from', fromIdx)
      this.props.stateStore.setSearchState('to', toIdx)
      this.props.stateStore.setSearchState('count', count)
      this.props.stateStore.setSearchState('page', page)
      this.props.stateStore.setSearchState('done', true)
      this.props.dataStore.setDataProductList(res.data)
    })
    .catch(err=> {
      this.props.stateStore.setSearchState('done', true)
      this.props.dataStore.setDataProductList([])
    })
  }

  handlePageChange(pageNumber) {
    const searchState = this.props.stateStore.searchState
    this.props.history.push('/search/'+ searchState.keyword + '/page/' + pageNumber)
  }
  renderItems() {
    const data = this.props.dataStore.dataProductList
    const searchState = this.props.stateStore.searchState
    if(data != null) {
      if(searchState.done===true && data.length===0) {
        return (
          <div className="search-no-contents-container">
            <span className="search-no-contents-icon"></span>
            <span className="search-no-contents-title">{this.props.dataStore.str.S0029}</span>
          </div>)
      } else {
        let view = []

        for(let item of data) {
          let tags = []
          if(item.is_hot === 1) {
            tags.push(this.props.dataStore.str.W0125)
          }
          if(item.is_new === 1) {
            tags.push(this.props.dataStore.str.W0126)
          }
          if(item.is_only === 1) {
            tags.push(this.props.dataStore.str.W0127)
          }
          view.push(
            <ItemRisingOfriends
              {...this.props}
              id = {item.id}
              key = {item.id}
              loc = {item.loc}
              isLike = {item.like}
              subTitle = {item.catch_phrase}
              title = {item.title}
              price = {item.original_price}
              discount = {item.price}
              rating = {item.rating}
              tags = {tags}
              ></ItemRisingOfriends>
          )
        }
        return (<div className="route-recommend-rising-ofriends-item-list">{view}</div>)
      }

    }
  }
  renderPagination() {
    // let nowPath = this.props.location.pathname
    // let activePage
    // try {
    //   activePage = nowPath.replace('/recommend/', '')
    //   if(activePage === '') {
    //     activePage = 1
    //   } else {
    //     activePage = parseInt(activePage)
    //   }
    // } catch (exception) {
    //   return
    // }
    const searchState = this.props.stateStore.searchState
    const data = this.props.dataStore.dataProductList
    let activePage = searchState.page
    let totalItemsCount = searchState.count
    let pageRangeDisplayed = (searchState.count/5) >= 5 ? 5 : parseInt(searchState.count/5 + 1)
    if(data == undefined || data.length===0) {
      return(
        <Fragment></Fragment>
      )
    } else {
      return (
        <Pagination
          activePage={activePage}
          itemsCountPerPage={20}
          totalItemsCount={totalItemsCount}
          pageRangeDisplayed={pageRangeDisplayed+1}
          onChange={this.handlePageChange.bind(this)}
          firstPageText={""}
          prevPageText={""}
          nextPageText={""}
          lastPageText={""}
          itemClassFirst="first-link"
          itemClassPrev="prev-link"
          itemClassNext="next-link"
          itemClassLast="last-link"
          linkClass="page-link"
          innerClass="pagination"
          itemClass="pg"
          activeClass="current"
        />
      )
    }

  }
  render() {
    const data = this.props.dataStore.dataProductList
    const searchState = this.props.stateStore.searchState
    let dataCount = data == undefined ? 0 : data.length
    return (
      <div className="route-recommend-entity-container">
        {/*타이틀*/}
        <div className="route-recommend-title-con">
          <span className="route-recommend-title">&quot;{searchState.keyword}&quot;&nbsp;{this.props.dataStore.str.W0130}</span>
          <span className="route-recommend-number">{searchState.count}</span>
        </div>
        {/*item element*/}
        <div className="route-recommend-rising-ofriends-items">{this.renderItems()}</div>
        {/*pagination*/}
        <div className="route-recommend-pagination">
          { this.renderPagination() }
        </div>
      </div>
    )

  }
}
export default Search
