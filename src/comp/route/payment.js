import React, { Component, Fragment } from 'react'
import { inject, observer } from 'mobx-react'

import ActiveUser from '../payment/active-user'
import FinishPayment from '../payment/finish-payment'
import PaymentInfo from '../payment/payment-info'
import PaymentMethod from '../payment/payment-method'

import OneBtn from '../dialog/one-btn'
import CouponSelect from '../dialog/coupon-select'
import '../../static/scss/payment.scss'
import * as Util from '../../util'
import Agree from "../dialog/agree";
import TwoBtn from "../dialog/two-btn";
import {HOME_URL} from "../../store/api-store";
const sampleData = {
  'data' : {
    "title" : "[프립버스1] 미니사과 따고 빵 만들고 사과따고 빵 사과 빵 미니사과 따고 빵 만들고",
    "price" : 36000,
    "image" : "https://res.cloudinary.com/frientrip/image/upload/ar_4:3,c_fill,dpr_1.0,g_auto,q_auto,r_0,w_930/product_banner_1568007404390_684866",
    "date" : "2019년 10월 6일(일요일) 10:00",
    "option" : "참가비 (1인)",
    "count" : 1,
  }
}


@inject('stateStore', 'apiStore', 'dataStore')
@observer
class Payment extends Component {

  constructor(props) {
    super(props)
    this.state = {
      location: '',
    }
  }

  componentDidMount() {
    this.onRouteChanged()
    this.unblock = this.props.history.block((location, action) => {
      if(this.props.stateStore.paymentState.state !== 1) {
        this.props.stateStore.setPopupOn('purchaseCancel')
        this.setState({location: location.pathname})
        return false
      }
    })

  }

  componentDidUpdate(prevProps) {
    if (this.props.location !== prevProps.location) {
      this.onRouteChanged()
    }
  }

  componentWillUnmount() {
    this.unblock()
  }

  onRouteChanged() {
    const dataPaymentInfo = this.props.dataStore.dataPaymentInfo
    if(dataPaymentInfo.productId === undefined || dataPaymentInfo.productId === -1) {
      this.props.history.push('/')
    }
    const dataUserInfo = this.props.dataStore.dataUserInfo

    this.props.stateStore.setPaymentState("activeUser", dataUserInfo.level >= 20)
    this.props.stateStore.setPaymentState("isOpenSelectedOf", true)
    this.props.stateStore.setPaymentState("isOpenSelectedOfFinish", true)
    this.props.stateStore.setPaymentState("price", dataPaymentInfo.price)
    this.props.stateStore.setPaymentState("priceCut", 0)
    this.props.stateStore.setPaymentState("isSelectTerms", false)
    this.props.stateStore.setPaymentState("paymentMethod", -1)
    this.props.stateStore.setPaymentState("coupon", -1)
    this.props.stateStore.setPaymentState("state", 0)
    this.props.stateStore.setPaymentState("offerPrivacyPopup", false)
  }

  paymentCallback = (payload) => {
    if(payload.success === true) {
      this.postPurchasesSettlement(payload)
    } else {
      alert('결제실패 : ' + payload.error_msg)
    }
  }

  postPurchasesSettlement(payload) {
    const dataPaymentInfo = this.props.dataStore.dataPaymentInfo
    const paymentState = this.props.stateStore.paymentState
    payload['original_paid_amount'] = paymentState.price
    payload['quantity'] = dataPaymentInfo.count
    if(paymentState.coupon > 0) {
      payload['coupon'] = paymentState.coupon
    }
    let jwtToken = localStorage.getItem('jwt')
    this.props.apiStore.postPurchasesSettlement(jwtToken, dataPaymentInfo.paymentId, payload)
      .then(res => {
        this.props.stateStore.setPaymentState("state", 1)
      })
      .error(err => {
        if(err.response.status === 400) {
          this.props.stateStore.setPopupOn("unknownError")
          // alert(this.props.dataStore.str.S1058)
        } else if (err.response.status === 409 || err.response.status === 412) {
          this.props.stateStore.setPopupOn("duplicatedCoupon")
          // alert(this.props.dataStore.str.S1059)
        } else {
          this.props.stateStore.setPopupOn("unknown")
          // alert(this.props.dataStore.str.S1006)
        }
      })
  }

  callPayment() {
    const dataPaymentInfo = this.props.dataStore.dataPaymentInfo
    const paymentState = this.props.stateStore.paymentState
    const dataUserInfo = this.props.dataStore.dataUserInfo
    Util.impInit()

    let pg = 'nice'
    let payMethod
    switch(paymentState.paymentMethod) {
      case 0:
        payMethod = 'card'
        break
      case 1:
        payMethod = 'trans'
        break
      case 2:
        payMethod = 'vbank'
        break
      case 3:
        payMethod = 'trans'
        break
    }
    let merchantUid = dataPaymentInfo.paymentId
    let name = dataPaymentInfo.title
    let amount = paymentState.price - paymentState.priceCut
    // let amount = 1000
    let buyerEmail = dataUserInfo.email
    let buyerName = dataUserInfo.name
    // let buyerName = '테스트'
    let buyerTel = dataUserInfo.phone
    // let buyerTel = '01012345678'

    let quantity = paymentState.count
    let coupon = 0
    if(paymentState.coupon > 0) {
      coupon = paymentState.coupon
    }

    let originalPrice = paymentState.price

    let options = {
      "pg" : pg, //아임포트 관리자에서 danal_tpay를 기본PG로 설정하신 경우는 생략 가능
      "pay_method" : payMethod, //card(신용카드), trans(실시간계좌이체), vbank(가상계좌), phone(휴대폰소액결제)
      "merchant_uid" : merchantUid, //상점에서 관리하시는 고유 주문번호를 전달
      "name" : name,
      "amount" : amount,
      "buyer_email" : buyerEmail,
      "buyer_name" : buyerName,
      "buyer_tel" : buyerTel,
      "m_redirect_url" : HOME_URL + "mobilepay?coupon=" + coupon + "&original_price=" + originalPrice
    }
    Util.impPayment(options, this.paymentCallback)
  }


  processPurchaseAttempt() {
    const dataPaymentInfo = this.props.dataStore.dataPaymentInfo
    const paymentState = this.props.stateStore.paymentState
    let purchaseId = dataPaymentInfo.paymentId
    let originalPrice = paymentState.price
    let price = paymentState.price - paymentState.priceCut
    let coupon = 0
    if(paymentState.coupon > 0) {
      coupon = paymentState.coupon
    }
    let jwtToken = this.props.apiStore.getJwtToken()
    if(jwtToken === null) {
      this.props.history.push('/login')
      return
    }
    if(originalPrice === undefined || coupon === undefined || price === undefined) {
      this.props.stateStore.setPopupOn("attemptFailed")
      return
    }
    let payload = {
      'coupon' : Number(coupon),
      'original_price' : Number(originalPrice),
      'price' : Number(price)
    }
    this.props.apiStore.postPurchaseAttempt(jwtToken, purchaseId, payload)
      .then( res => {
        this.callPayment()
      })
      .catch( err => {
        this.props.stateStore.setPopupOn("attemptFailed")
      })
  }


  onClickPayment = (e) => {
    const regActiveState = this.props.stateStore.regActiveState
    const paymentState = this.props.stateStore.paymentState
    if(paymentState.isSelectTerms === false) {
      this.props.stateStore.setPopupOn("notSelectTerms")
      return
    } else if (paymentState.paymentMethod === -1) {
      this.props.stateStore.setPopupOn("noPaymentMethod")
      return
    } else if (paymentState.isCouponSelected === true && paymentState.activeUser === false) {
      this.props.stateStore.setPopupOn("needAgree")
      return
    } else if (paymentState.activeUser === false && regActiveState.isConfirmSelf === false) {
      this.props.stateStore.setPopupOn("notActiveUser")
      return
    }
    this.processPurchaseAttempt()
    // this.props.stateStore.setPaymentState("state", 1)
  }

  onClickTermsCheck = (e) => {
    const paymentState = this.props.stateStore.paymentState
    if(paymentState.isSelectTerms === true) {
      this.props.stateStore.setPaymentState("isSelectTerms", false)
      return
    }
    this.props.stateStore.setPaymentState("offerPrivacyPopup", true)
  }

  onClickCloseAgree = (e, isAgree) => {
    let paymentState = this.props.stateStore.paymentState
    if(paymentState.isSelectTerms === false && isAgree === true) {
      this.props.stateStore.setPaymentState("isSelectTerms", true)
    }
    this.props.stateStore.setPaymentState("offerPrivacyPopup", false)
  }

  onClickCancel = (e) => {
    this.props.stateStore.setPopupOn('')
  }

  onClickPurchaseCancel = (e) => {
    const dataPaymentInfo = this.props.dataStore.dataPaymentInfo
    if(dataPaymentInfo != undefined
      && dataPaymentInfo != null
      && this.props.stateStore.paymentState.state !== 1) {
      let jwtToken = localStorage.getItem('jwt')
      let payload = {
        "purchase": dataPaymentInfo.paymentId,
        "cancel_request_amount": dataPaymentInfo.count,
        "reason": ""
      }
      this.props.apiStore.postPurchasesCancel(jwtToken, dataPaymentInfo.paymentId, payload)
        .then(res => {
          this.unblock()
          this.props.history.push(this.state.location)
        })
        .catch(err => {
          this.unblock()
          this.props.history.push(this.state.location)
        })
    }
  }

  renderByState() {
    const paymentState = this.props.stateStore.paymentState
    const dataPaymentInfo = this.props.dataStore.dataPaymentInfo
    switch(paymentState.state) {
      case 0:
        return (
          <Fragment>
            {this.renderActiveUser()}
            {this.renderPaymentInfo()}
            {this.renderPaymentMethod()}
            {this.renderCheckTerms()}
            {this.renderCouponSelect()}
            <span className="payment-payment-btn" onClick={(e)=>{this.onClickPayment(e)}}>
              {this.props.apiStore.getFormatWon(paymentState.price - paymentState.priceCut)}
              &nbsp;
              {this.props.dataStore.str.W0090}
            </span>
          </Fragment>
        )
        break;
      case 1:
        return (
          <FinishPayment
            productId={dataPaymentInfo.productId}
            title={dataPaymentInfo.title}
            image={sampleData.data.image}
            date={dataPaymentInfo.option2Name}
            option={dataPaymentInfo.option1Name}
            count={dataPaymentInfo.count}
            price={paymentState.price}
            priceCut={paymentState.priceCut}
            paymentPrice={paymentState.price - paymentState.priceCut}
            method={this.props.dataStore.paymentMethod[paymentState.paymentMethod]}></FinishPayment>
        )
        break;
    }
  }

  renderActiveUser() {
    const paymentState = this.props.stateStore.paymentState
    if(paymentState.activeUser === false) {
      return (
        <ActiveUser></ActiveUser>
      )
    }
  }

  renderPaymentInfo() {
    const dataPaymentInfo = this.props.dataStore.dataPaymentInfo
    const paymentState = this.props.stateStore.paymentState
    return (
      <PaymentInfo
        productId={dataPaymentInfo.productId}
        title={dataPaymentInfo.title}
        image={sampleData.data.image}
        date={dataPaymentInfo.option2Name}
        option={dataPaymentInfo.option1Name}
        count={dataPaymentInfo.count}
        price={dataPaymentInfo.price}>
      </PaymentInfo>
    )
  }

  renderPaymentMethod() {
    return (
      <PaymentMethod></PaymentMethod>
    )
  }

  renderCheckTerms() {
    const paymentState = this.props.stateStore.paymentState
    let className = paymentState.isSelectTerms === true ? "unchecked" : "checked"
    return (
      <div className="payment-checkTerms-entity-container" onClick={(e) => this.onClickTermsCheck(e)}>
        <span className={className}></span>
        <span className="payment-terms-text">{this.props.dataStore.str.S0014}</span>
      </div>
    )
  }

  renderCouponSelect() {
    const dataPaymentInfo = this.props.dataStore.dataPaymentInfo
    const paymentState = this.props.stateStore.paymentState
    if(this.props.stateStore.popupOn === 'couponSelect') {
      return (
        <CouponSelect
          productId={dataPaymentInfo.productId}
          title={dataPaymentInfo.title}
          image={sampleData.data.image}
          date={dataPaymentInfo.option2Name}
          option={dataPaymentInfo.option1Name}
          count={dataPaymentInfo.count}
          price={paymentState.price}></CouponSelect>
      )
    }
  }

  renderErrMsg() {
    switch(this.props.stateStore.popupOn) {
      case 'notActiveUser':
        return (
          <OneBtn
            msg={this.props.dataStore.str.S0018}
            onClickClose={(e) => {
              this.props.stateStore.setPopupOn("")
              this.props.stateStore.setRegActiveState('regCase', 1)
              this.props.stateStore.setRegActiveState('isOn', true)
              this.props.stateStore.setRegActiveState("title", this.props.dataStore.str.W1026)
            }}></OneBtn>
        )
        break
      case 'noPaymentMethod':
        return (
          <OneBtn
            msg={this.props.dataStore.str.S0017}
            onClickClose={(e) => {
              this.props.stateStore.setPopupOn("")
            }}></OneBtn>
        )
        break
      case 'needAgree':
        return (
          <OneBtn
            msg={this.props.dataStore.str.S1012}
            onClickClose={(e) => {
              this.props.stateStore.setPopupOn("")
              this.props.stateStore.setRegActiveState('regCase', 2)
              this.props.stateStore.setRegActiveState('isOn', true)
              this.props.stateStore.setRegActiveState('title', this.props.dataStore.str.W1024)
              this.props.stateStore.setRegActiveState('idTitle', this.props.dataStore.str.W1035)
              this.props.stateStore.setRegActiveState('mktTitle', this.props.dataStore.str.W1036)
            }}></OneBtn>
        )
        break
      case 'notSelectTerms':
        return (
          <OneBtn
            msg={this.props.dataStore.str.S0016}
            onClickClose={(e) => {
              this.props.stateStore.setPopupOn("")
            }}></OneBtn>
        )
        break
      case 'unknownError':
        return (
          <OneBtn
            msg={this.props.dataStore.str.S1058}
            onClickClose={(e) => {
              this.props.stateStore.setPopupOn("")
            }}></OneBtn>
        )
        break
      case 'duplicatedCoupon':
        return (
          <OneBtn
            msg={this.props.dataStore.str.S1059}
            onClickClose={(e) => {
              this.props.stateStore.setPopupOn("")
            }}></OneBtn>
        )
        break
      case 'attemptFailed':
        return (
          <OneBtn
            msg={this.props.dataStore.str.S1058}
            onClickClose={(e) => {
              this.props.stateStore.setPopupOn("")
            }}></OneBtn>
        )
        break
      case 'unknown':
        return (
          <OneBtn
            msg={this.props.dataStore.str.S1006}
            onClickClose={(e) => {
              this.props.stateStore.setPopupOn("")
              this.props.history.push("/")
            }}></OneBtn>
        )
        break
    }
  }

  renderOfferPrivacyPopup() {
    let paymentState = this.props.stateStore.paymentState
    if(paymentState.offerPrivacyPopup === true) {
      return (
        <Agree
          title={this.props.dataStore.str.S1035}
          content={this.props.dataStore.str.S1040}
          agreeSentence={this.props.dataStore.str.W0151}
          onClickClose={(e, isAgree) => this.onClickCloseAgree(e, isAgree)}>
        </Agree>
      )
    }
  }

  renderRegEndMsg() {
    if(this.props.stateStore.popupOn === 'regEndMsg') {
      return (
        <OneBtn
          msg={this.props.stateStore.regActiveState.regEndMsg}
          onClickClose={(e) => {
            this.props.stateStore.setPopupOn("")
          }}></OneBtn>
      )
    }
  }

  renderPurchaseCancelPopup() {
    if(this.props.stateStore.popupOn === 'purchaseCancel') {
      return (
        <TwoBtn
          content={this.props.dataStore.str.S1060}
          onClickBtn1={(e) => this.onClickCancel(e)}
          onClickBtn2={(e) => this.onClickPurchaseCancel(e)}
          btn1={this.props.dataStore.str.W0053}
          btn2={this.props.dataStore.str.W0091}></TwoBtn>
      )
    }
  }


  render() {
    return (
      <div className="payment-entity-container">
        {this.renderByState()}
        {this.renderErrMsg()}
        {this.renderOfferPrivacyPopup()}
        {this.renderRegEndMsg()}
        {this.renderPurchaseCancelPopup()}
      </div>
    )

  }
}
export default Payment
