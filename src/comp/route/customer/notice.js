import React, { Component, Fragment } from 'react'
import { inject, observer } from 'mobx-react'
import Pagination from "react-js-pagination"
import '../../../static/scss/notice.scss'
import HelpCenter from '../../help/help-center'

@inject('stateStore', 'apiStore', 'dataStore')
@observer
class Notice extends Component {

  componentDidMount() {
    const noticeState = this.props.stateStore.noticeState

    this.props.stateStore.setNoticeState('from', 0)
    this.props.stateStore.setNoticeState('to', 0)
    this.props.stateStore.setNoticeState('count', 0)
    this.props.stateStore.setNoticeState('page', 1)
    this.props.stateStore.setNoticeState('noticeOpen', {})
    this.props.dataStore.setDataMiscNotice([])
    this.getMiscNotice(noticeState.page)

  }

  handlePageChange(pageNumber) {
    const noticeState = this.props.stateStore.noticeState
    if(pageNumber != noticeState.page) {
      this.getMiscNotice(pageNumber)
    }

  }
  onClickTitle = (e, id) => {
    const openList = this.props.stateStore.noticeState.noticeOpen
    const isOpen = openList[id.toString()]

    let newList = {}
    for(var key in openList) {
      newList[key] = (id.toString()===key) ? !openList[key] : openList[key]
    }
    this.props.stateStore.setNoticeState('noticeOpen', newList)
  }
  getMiscNotice(page) {
    const noticeState = this.props.stateStore.noticeState

    let fromIdx = (page - 1) * 10
    let toIdx = fromIdx + 9
    let range = '[' + fromIdx + ',' + toIdx + ']'
    let params = []
    params.push({
      key: 'range',
      value: range
    })

    this.props.apiStore.getMiscNotice(params).then(res=>{
      let data = res.data
      for(let row of res.data) {
        row.body = row.body.replace(/\n/g, '<br>')
      }
      this.props.dataStore.setDataMiscNotice(res.data)
      let range = res.headers['content-range'].split(' ')[1]
      let fromIdx = range.split('/')[0].split('-')[0]
      let toIdx = range.split('/')[0].split('-')[1]
      let count = range.split('/')[1]
      this.props.stateStore.setNoticeState('from', fromIdx)
      this.props.stateStore.setNoticeState('to', toIdx)
      this.props.stateStore.setNoticeState('count', count)
      this.props.stateStore.setNoticeState('page', page)
      let noticeOpen = {}
      for(var i=0; i<res.data.length; i++) {
        let row = res.data[i]
        noticeOpen[row.id.toString()] = false
      }
      this.props.stateStore.setNoticeState('noticeOpen', noticeOpen)
    })
    .catch(err=> {
    })
  }
  renderNotice() {
    const data = this.props.dataStore.dataMiscNotice
    let noticeList = []
    for(let row of data) {
      const isOpen = this.props.stateStore.noticeState.noticeOpen[(row.id).toString()]
      noticeList.push(
        <Fragment key={row.id}>
          <div className="notice-item" onClick={(e)=> this.onClickTitle(e, row.id)}>
            <div className="notice-title-container">
              <ul className="notice-item-icon-title-con">
                {/*<li><span className="notice-item-icon">[Q]</span></li>*/}
                <li>
                  <span
                    className={isOpen ? "notice-item-title-open" : "notice-item-title-closed"}>
                    {row.title}
                  </span>
                </li>
              </ul>
              <ul className="notice-item-date-arrow-con">
                <li className="notice-item-date-con"><span className="notice-item-date">{this.props.apiStore.getFullTime(row.ct)}</span></li>
                <li className="notice-item-arrow-con">
                  <span
                    className={
                      isOpen ? "content-open" : "content-closed"}>
                  </span>
                </li>
              </ul>
            </div>
            {
              isOpen
              &&
              <div className="notice-item-content-con">
                <span className="notice-item-content" dangerouslySetInnerHTML={{__html: row.body}}></span>
              </div>
            }
          </div>
        </Fragment>
      )
    }
    return (
      <Fragment>{noticeList}</Fragment>
    )
  }

  renderPagination() {
    const noticeState = this.props.stateStore.noticeState
    let activePage = noticeState.page
    let totalItemsCount = noticeState.count
    let pageRangeDisplayed = (noticeState.count / 10) >= 5 ? 5 : parseInt(noticeState.count / 10 + 1)
    if (totalItemsCount > 0) {
      return (
        <Pagination
          activePage={noticeState.page}
          itemsCountPerPage={10}
          totalItemsCount={noticeState.count}
          pageRangeDisplayed={pageRangeDisplayed}
          onChange={this.handlePageChange.bind(this)}
          firstPageText={""}
          prevPageText={""}
          nextPageText={""}
          lastPageText={""}
          itemClassFirst="first-link"
          itemClassPrev="prev-link"
          itemClassNext="next-link"
          itemClassLast="last-link"
          linkClass="page-link"
          innerClass="pagination"
          itemClass="pg"
          activeClass="current"
        />
      )
    }
  }

  render() {
    return (
      <div className="notice-entity-container">
        <div className="notice-title">{this.props.dataStore.str.W0004}</div>
        <div className="notice-items-container">{this.renderNotice()}</div>
        {this.renderPagination()}
      </div>
    )

  }
}
export default Notice
