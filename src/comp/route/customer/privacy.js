import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
import Policy from '../../etc/policy'
@inject('stateStore', 'apiStore', 'dataStore')
@observer
class Privacy extends Component {

  render() {
    return (
      <Policy
        title={this.props.dataStore.str.W0131}
        contents={this.props.dataStore.str.S0031}>
      </Policy>
    )

  }
}
export default Privacy
