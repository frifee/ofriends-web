import React, {Component, Fragment} from 'react'
import { inject, observer } from 'mobx-react'
import HelpHeader from '../../help/help-header'
import HelpBody from '../../help/help-body'
import '../../../static/scss/help.scss'
import HelpCenter from "../../help/help-center";
@inject('stateStore', 'apiStore', 'dataStore')
@observer
class Help extends Component {


  componentDidMount() {
    this.props.stateStore.setFaqState('from', 0)
    this.props.stateStore.setFaqState('to', 0)
    this.props.stateStore.setFaqState('count', 0)
    this.props.stateStore.setFaqState('page', 1)
    this.props.stateStore.setFaqState('itemOpen', {})
    this.props.stateStore.setFaqState('tabPos', 0)
    this.getMiscFaqs(0, 1)
  }

  getMiscFaqs(tabPos, page) {
    const helpTabList = this.props.dataStore.helpTabList
    let fromIdx = (page - 1) * 5
    let toIdx = fromIdx + 4
    let range = '[' + fromIdx + ',' + toIdx + ']'
    let params = []
    if(helpTabList[tabPos].category!=null) {
      params.push({
        key: 'filter',
        value : "{\"category\":\"" + helpTabList[tabPos].category + "\"}"
      })
    }
    params.push({
      key: 'range',
      value: range
    })
    this.props.apiStore.getMiscFaqs(params).then(res=>{
      let data = res.data
      for(let row of res.data) {
        row.body = row.body.replace(/\n/g, '<br>')
      }
      this.props.dataStore.setDataMiscFaqs(res.data)
      let range = res.headers['content-range'].split(' ')[1]
      let fromIdx = range.split('/')[0].split('-')[0]
      let toIdx = range.split('/')[0].split('-')[1]
      let count = range.split('/')[1]
      this.props.stateStore.setFaqState('from', fromIdx)
      this.props.stateStore.setFaqState('to', toIdx)
      this.props.stateStore.setFaqState('count', count)
      this.props.stateStore.setFaqState('page', page)
      let itemOpen = {}
      let noticeOpen = {}
      for(var i=0; i<res.data.length; i++) {
        let row = res.data[i]
        itemOpen[row.id.toString()] = false
      }
      this.props.stateStore.setFaqState('itemOpen', itemOpen)
    })
    .catch(err=> {
    })
  }

  render() {
    return (
      <div className="help-entity-container">
        <div className="help-header-body-con">
          <HelpHeader
            getMiscFaqs={(tabPos, page) => this.getMiscFaqs(tabPos, page)}></HelpHeader>
          <HelpBody
            getMiscFaqs={(tabPos, page) => this.getMiscFaqs(tabPos, page)}></HelpBody>
        </div>
        {/*<HelpCenter></HelpCenter>*/}
      </div>
    )
  }
}
export default Help
