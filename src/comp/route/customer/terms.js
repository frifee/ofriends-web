import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
import Policy from '../../etc/policy'
@inject('stateStore', 'apiStore', 'dataStore')
@observer
class Terms extends Component {

  render() {
    return (
      <Policy
        title={this.props.dataStore.str.W0014}
        contents={this.props.dataStore.str.S1041}>
      </Policy>
    )

  }
}
export default Terms
