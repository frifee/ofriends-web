import React, {Component, Fragment} from 'react'
import { inject, observer } from 'mobx-react'
import ItemRisingOfriends from '../main/item-rising-ofriends'
import { Link, withRouter } from 'react-router-dom'
import Pagination from "react-js-pagination"
import '../../static/scss/recommend.scss'
import MainHeader from "../main/main-header";
@inject('stateStore', 'apiStore', 'dataStore')
@withRouter
@observer
class Exhibit extends Component {
  componentDidMount() {
    this.props.stateStore.setExhibitState('from', 0)
    this.props.stateStore.setExhibitState('to', 0)
    this.props.stateStore.setExhibitState('count', 0)
    this.props.stateStore.setExhibitState('page', 1)

    this.onRouteChanged()
  }

  componentDidUpdate(prevProps) {
    if (this.props.location !== prevProps.location) {
      this.onRouteChanged()
    }
  }

  onRouteChanged() {
    let page
    let id
    try {
      page = this.props.match.params.page == undefined ? 1 : parseInt(this.props.match.params.page)

      if(isNaN(page)) {
        this.props.history.push('/')
      }
    } catch (e) {
      this.props.history.push('/')
    }
    this.props.stateStore.setExhibitState('page', page)
    this.props.stateStore.setExhibitState('id', parseInt(this.props.match.params.id))
    this.getProductList(page)
  }

  getProductList(page) {
    let fromIdx = (page - 1) * 20
    let toIdx = fromIdx + 20 - 1
    let range = '[' + fromIdx + ',' + toIdx + ']'
    let params = []
    params.push({
      key: 'range',
      value: range
    })
    params.push({
      key: 'filter',
      value: "{\"exhibit\":\"" + this.props.stateStore.exhibitState.id + "\"}"
    })

    this.props.apiStore.getProductList(localStorage.getItem('jwt'), params).then(res=>{
      try {
        let range = res.headers['content-range'].split(' ')[1]
        let fromIdx = range.split('/')[0].split('-')[0]
        let toIdx = range.split('/')[0].split('-')[1]
        let count = range.split('/')[1]
        this.props.stateStore.setExhibitState('from', fromIdx)
        this.props.stateStore.setExhibitState('to', toIdx)
        this.props.stateStore.setExhibitState('count', count)
        this.props.stateStore.setExhibitState('page', page)
        this.props.dataStore.setDataProductList([])
        this.props.dataStore.setDataProductList(res.data)
      } catch(e) {}
    })
    .catch(err=> {
      this.props.dataStore.setDataProductList([])
      this.props.history.push('/error')
    })
  }

  handlePageChange(pageNumber) {
    let id = this.props.match.params.id
    this.props.history.push('/exhibit/'+ id + '/' + pageNumber)
  }

  renderItems() {
    const data = this.props.dataStore.dataProductList
    if(data != null) {
      let view = []

      for(let item of data) {
        let tags = []
        if(item.is_hot === 1) {
          tags.push(this.props.dataStore.str.W0125)
        }
        if(item.is_new === 1) {
          tags.push(this.props.dataStore.str.W0126)
        }
        if(item.is_only === 1) {
          tags.push(this.props.dataStore.str.W0127)
        }
        view.push(
          <ItemRisingOfriends
            id = {item.id}
            key = {item.id}
            loc = {item.loc}
            isLike = {item.like}
            subTitle = {item.catch_phrase}
            title = {item.title}
            price = {item.original_price}
            discount = {item.price}
            rating = {item.rating}
            tags = {tags}
            ></ItemRisingOfriends>
        )
      }
      return (<div className="route-recommend-rising-ofriends-item-list">{view}</div>)
    }
  }
  renderPagination() {
    // let nowPath = this.props.location.pathname
    // let activePage
    // try {
    //   activePage = nowPath.replace('/recommend/', '')
    //   if(activePage === '') {
    //     activePage = 1
    //   } else {
    //     activePage = parseInt(activePage)
    //   }
    // } catch (exception) {
    //   return
    // }
    const exhibitState = this.props.stateStore.exhibitState
    let activePage = exhibitState.page
    let totalItemsCount = exhibitState.count
    let pageRangeDisplayed = (exhibitState.count/5) >= 5 ? 5 : parseInt(exhibitState.count/5 + 1)
    if(totalItemsCount>0) {
      return (
        <Pagination
          activePage={activePage}
          itemsCountPerPage={20}
          totalItemsCount={totalItemsCount}
          pageRangeDisplayed={pageRangeDisplayed + 1}
          onChange={this.handlePageChange.bind(this)}
          firstPageText={""}
          prevPageText={""}
          nextPageText={""}
          lastPageText={""}
          itemClassFirst="first-link"
          itemClassPrev="prev-link"
          itemClassNext="next-link"
          itemClassLast="last-link"
          linkClass="page-link"
          innerClass="pagination"
          itemClass="pg"
          activeClass="current"
        />
      )
    }
  }
  render() {
    const data = this.props.dataStore.dataProductList
    const id = this.props.match.params.id
    let dataCount = data == undefined ? 0 : data.length
    let bannerDUrl = this.props.dataStore.exhibitImgUrl + id + '_d.jpg'
    let bannerMUrl = this.props.dataStore.exhibitImgUrl + id + '_m.jpg'
    return (
      <div className="route-recommend-entity-container">
        <MainHeader></MainHeader>
        <div className="route-recommend-exhibit-banner-wrapper">
          <img
            className="route-recommend-exhibit-image-d"
            alt = ''
            src={bannerMUrl === undefined || bannerMUrl === null ? "" : bannerDUrl}
            onError={(e)=>e.target.setAttribute("src", this.props.dataStore.imageUrl + "06_detailpage_no_image.png")}/>
          <img
            className="route-recommend-exhibit-image-m"
            alt = ''
            src={bannerDUrl === undefined || bannerDUrl === null ? "" : bannerMUrl}
            onError={(e)=>e.target.setAttribute("src", this.props.dataStore.imageUrl + "06_detailpage_no_image.png")}/>
        </div>

        {/*타이틀
        <div className="route-recommend-title-con">
          <span className="route-recommend-title">{this.props.dataStore.str.W0012}</span>
          <span className="route-recommend-number">{dataCount}</span>
        </div>
        */}
        {/*item element*/}
        <div className="route-recommend-rising-ofriends-items">{this.renderItems()}</div>
        {/*pagination*/}
        <div className="route-recommend-pagination">
          { this.renderPagination() }
        </div>
      </div>
    )

  }
}
export default Exhibit
