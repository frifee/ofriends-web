import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
import '../../static/scss/err.scss'

@inject('stateStore', 'apiStore', 'dataStore')
@observer
class Err extends Component {

  render() {
    return (
      <div className="err-container">
        <span className="err-image"></span>
        <span className="err-title">{this.props.dataStore.str.S0040}</span>
      </div>
    )

  }
}
export default Err
