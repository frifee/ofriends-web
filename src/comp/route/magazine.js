import React, { Component, Fragment } from 'react'
import { inject, observer } from 'mobx-react'
import ItemRisingOfriends from '../main/item-rising-ofriends'
import '../../static/scss/magazine-detail.scss'
@inject('stateStore', 'apiStore', 'dataStore')
@observer
class Magazine extends Component {

  componentDidMount() {

    this.onRouteChanged()
  }

  componentDidUpdate(prevProps) {
    if (this.props.location !== prevProps.location) {
      this.onRouteChanged()
    }
  }

  onRouteChanged() {
    let magazineId = this.props.match.params.id
    this.getMagazine(magazineId)
  }

  getMagazine(magazineId) {
    this.props.apiStore.getMagazine(localStorage.getItem('jwt'), magazineId)
      .then(res => {
        this.props.dataStore.setDataMagazine(res.data)
      })
      .catch(err => {

      })
  }

  renderItems() {
    const dataMagazineProducts = this.props.dataStore.dataMagazineProducts
    let view = []
    for(var i=0; i<dataMagazineProducts.length; i++) {
      let item = dataMagazineProducts[i]
      let tags = []
      if(item.is_hot === 1) {
        tags.push(this.props.dataStore.str.W0125)
      }
      if(item.is_new === 1) {
        tags.push(this.props.dataStore.str.W0126)
      }
      if(item.is_only === 1) {
        tags.push(this.props.dataStore.str.W0127)
      }
      view.push(
        <ItemRisingOfriends
          id = {item.id}
          key = {item.id}
          loc = {item.loc}
          isLike = {item.like}
          subTitle = {item.catch_phrase}
          title = {item.title}
          price = {item.original_price}
          discount = {item.price}
          rating = {item.rating}
          tags = {tags}
          ></ItemRisingOfriends>
      )
    }
    return (<div className="relate-items">{view}</div>)
  }



  render() {
    const dataMagazine = this.props.dataStore.dataMagazine
    const dataMagazineProducts = this.props.dataStore.dataMagazineProducts
    if(dataMagazine != undefined
      && dataMagazineProducts != undefined) {
      return (
        <div className="magazine-detail">
          <div className="date-con">
            <span className="date">{this.props.apiStore.getMagazineTime(dataMagazine.ct)}</span>
          </div>
          <div className="title-con">
            <span className="title">{dataMagazine.title}</span>
          </div>
          <div className="desc-con">
            <span className="desc">{dataMagazine.catch_phrase}</span>
          </div>
          <div className="body-con" dangerouslySetInnerHTML={{__html: dataMagazine.body}}>
          </div>
          {
            dataMagazineProducts.length > 0
            &&
            <Fragment>
              <div className="relate-con">
                <span className="relate-title">{this.props.dataStore.str.W1041}</span>
              </div>
              {this.renderItems()}
            </Fragment>
          }

        </div>
      )
    }

    else {
      return (<Fragment></Fragment>)
    }



  }
}
export default Magazine
