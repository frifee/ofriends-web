import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'

@inject('stateStore', 'apiStore', 'dataStore')
@observer
class DailyActivity extends Component {

  render() {
    return (
      <div>
        <div>일상페이지</div>
        <div>{this.props.stateStore.isLoading ? '로딩중' : '로딩아닌중'}</div>
        <div>{this.props.apiStore.sample}</div>
      </div>
    )

  }
}
export default DailyActivity
