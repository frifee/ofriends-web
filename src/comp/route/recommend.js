import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
import ItemRisingOfriends from '../main/item-rising-ofriends'
import { Link, withRouter } from 'react-router-dom'
import Pagination from "react-js-pagination"
import '../../static/scss/recommend.scss'
@inject('stateStore', 'apiStore', 'dataStore')
@withRouter
@observer
class Recommend extends Component {
  componentDidMount() {
    this.props.stateStore.setRecommendState('from', 0)
    this.props.stateStore.setRecommendState('to', 0)
    this.props.stateStore.setRecommendState('count', 0)
    this.props.stateStore.setRecommendState('page', 1)

    this.onRouteChanged()
  }

  componentDidUpdate(prevProps) {
    if (this.props.location !== prevProps.location) {
      this.onRouteChanged()
    }
  }

  onRouteChanged() {
    let page
    try {
      page = this.props.match.params.page == undefined ? 1 : parseInt(this.props.match.params.page)
      if(isNaN(page)) {
        this.props.history.push('/recommend')
      }
    } catch (e) {
      this.props.history.push('/recommend')
    }
    this.props.stateStore.setRecommendState('page', page)
    this.getProductList(page)
  }

  onClickSort = (e) => {
    e.stopPropagation()
    this.props.stateStore.setIsDailySortOpen(!this.props.stateStore.isDailySortOpen)
  }

  getProductList(page) {
    let fromIdx = (page - 1) * 20
    let toIdx = fromIdx + 20 - 1
    let range = '[' + fromIdx + ',' + toIdx + ']'
    let params = []
    let queryParams = this.props.location.search.replace("?", "")
    let paramsDict = {}

    let queryParamsArr = queryParams.split("&")
    for(var i=0; i<queryParamsArr.length; i++) {
      let row = queryParamsArr[i].split("=")
      paramsDict[row[0]] = row[1]
    }

    if(paramsDict['sort'] != undefined) {
      if(paramsDict['sortby'] == undefined) {
        params.push({
          key: 'sort',
          value: "[\"" + paramsDict['sort'] + "\",\"DESC\"]"
        })
      } else {
        params.push({
          key: 'sort',
          value: "[\"" + paramsDict['sort'] + "\",\""+ paramsDict['sortby'].toUpperCase() + "\"]"
        })
      }
    }
    params.push({
      key: 'range',
      value: range
    })
    params.push({
      key: 'filter',
      value: "{\"promo\":\"" + 1 + "\"}"
    })

    
    this.props.apiStore.getProductList(localStorage.getItem('jwt'), params).then(res=>{

      let range = res.headers['content-range'].split(' ')[1]
      let fromIdx = range.split('/')[0].split('-')[0]
      let toIdx = range.split('/')[0].split('-')[1]
      let count = range.split('/')[1]
      this.props.stateStore.setRecommendState('from', fromIdx)
      this.props.stateStore.setRecommendState('to', toIdx)
      this.props.stateStore.setRecommendState('count', count)
      this.props.stateStore.setRecommendState('page', page)
      this.props.dataStore.setDataProductList([])
      this.props.dataStore.setDataProductList(res.data)
    })
      .catch(err=> {
        this.props.dataStore.setDataProductList([])
      })
  }

  handlePageChange(pageNumber) {
    this.props.history.push('/recommend/' + pageNumber)
  }
  renderItems() {
    const data = this.props.dataStore.dataProductList
    if(data != null) {
      let view = []
      for(let item of data) {
        let tags = []
        if(item.is_hot === 1) {
          tags.push(this.props.dataStore.str.W0125)
        }
        if(item.is_new === 1) {
          tags.push(this.props.dataStore.str.W0126)
        }
        if(item.is_only === 1) {
          tags.push(this.props.dataStore.str.W0127)
        }
        view.push(
          <ItemRisingOfriends
            id = {item.id}
            key = {item.id}
            loc = {item.loc}
            isLike = {item.like}
            subTitle = {item.catch_phrase}
            title = {item.title}
            price = {item.original_price}
            discount = {item.price}
            rating = {item.rating}
            tags = {tags}
          ></ItemRisingOfriends>
        )
      }
      return (<div className="route-recommend-rising-ofriends-item-list">{view}</div>)
    }
  }
  renderPagination() {
    const recommendState = this.props.stateStore.recommendState
    let activePage = recommendState.page
    let totalItemsCount = recommendState.count
    let pageRangeDisplayed = (recommendState.count/5) >= 5 ? 5 : parseInt(recommendState.count/5 + 1)
    if(totalItemsCount>0) {
      return (
        <Pagination
          activePage={activePage}
          itemsCountPerPage={20}
          totalItemsCount={totalItemsCount}
          pageRangeDisplayed={pageRangeDisplayed+1}
          onChange={this.handlePageChange.bind(this)}
          firstPageText={""}
          prevPageText={""}
          nextPageText={""}
          lastPageText={""}
          itemClassFirst="first-link"
          itemClassPrev="prev-link"
          itemClassNext="next-link"
          itemClassLast="last-link"
          linkClass="page-link"
          innerClass="pagination"
          itemClass="pg"
          activeClass="current"
        />
      )
    }
  }

  renderDropdown() {
    let sortList = this.props.dataStore.sortByItem
    let sortableList = []
    for(var i=0; i< sortList.length; i++) {
      let row = sortList[i]
      let link = this.props.location.pathname.split('/page')[0] + row.link
      sortableList.push(
        <Link key = {row.id} to={link}>
          <div>
            <span>{row.name}</span>
          </div>
        </Link>
      )
    }
    return (
      <div className="rec-align-dropdown" style={{display:this.props.stateStore.isDailySortOpen === true ? 'flex' : 'none'}}>
        {this.props.stateStore.isDailySortOpen && sortableList}
      </div>
    )
  }

  render() {
    // const data = this.props.dataStore.dataProductList
    const recommendState = this.props.stateStore.recommendState
    let dataCount = recommendState === undefined ? 0 : recommendState.count

    return (
      <div className="route-recommend-entity-container">
        {/*타이틀*/}
        <div className="route-recommend-title-con">
          <span className="route-recommend-title">{this.props.dataStore.str.W0012}</span>
          <span className="route-recommend-number">{dataCount}</span>
          <div className="rec-align" onClick={(e) => this.onClickSort(e)}>
            <span className="rec-align-icon"></span>
            <span className="rec-align-title">{this.props.dataStore.str.W0129}</span>
          </div>
          {this.renderDropdown()}
        </div>
        {/*item element*/}
        <div className="route-recommend-rising-ofriends-items">{this.renderItems()}</div>
        {/*pagination*/}
        { this.renderPagination() }
      </div>
    )

  }
}
export default Recommend
