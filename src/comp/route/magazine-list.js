import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
import InfiniteScroll from 'react-infinite-scroller'
import { Link, withRouter } from 'react-router-dom'
import '../../static/scss/magazine-list.scss'


@inject('stateStore', 'apiStore', 'dataStore')
@observer
class MagazineList extends Component {

  constructor(props) {
    super(props)
    this.state = {
      loadMore : true,
      lastCt : 0
    }
  }

  componentDidMount() {

    this.onRouteChanged()
  }

  componentDidUpdate(prevProps) {
    if (this.props.location !== prevProps.location) {
      this.onRouteChanged()
    }
  }

  onRouteChanged() {
    this.props.dataStore.initDataMagazineList()
  }



  loadItems(page) {
    this.setState({
      loadMore: false
    })
    let params = ''
    if(this.state.lastCt != 0) {
      params = '[\"' + this.props.apiStore.getMonth(this.state.lastCt) + '\"]'
    }


    let queryParams = []
    if(params!='') {
      queryParams.push({
        key: 'range',
        value: params
      })
    }
    this.props.apiStore.getMagazineList(localStorage.getItem('jwt'), queryParams)
      .then((res) => {
        this.props.dataStore.addDataMagazineList(res.data)
        this.setState({
          lastCt : res.data[res.data.length - 1].ct
        })
        this.setState({
          loadMore: res.data.length > 0
        })
      })
      .catch((err) => {

      })

    if(page>10) {
      this.setState({
        loadMore: false
      })
      return
    }


    const data = {
      'month' : '2020-11',
      'item' : [
        {
          'id' : 0,
          'img' : 'http://www.bloter.net/wp-content/uploads/2016/08/%EC%8A%A4%EB%A7%88%ED%8A%B8%ED%8F%B0-%EC%82%AC%EC%A7%84-765x519.jpg',
          'text' : '나만의 커피 어쩌고저쩌고'
        },
        {
          'id' : 0,
          'img' : 'http://www.bloter.net/wp-content/uploads/2016/08/%EC%8A%A4%EB%A7%88%ED%8A%B8%ED%8F%B0-%EC%82%AC%EC%A7%84-765x519.jpg',
          'text' : '나만의 커피 어쩌고저쩌고'
        },
        {
          'id' : 0,
          'img' : 'http://www.bloter.net/wp-content/uploads/2016/08/%EC%8A%A4%EB%A7%88%ED%8A%B8%ED%8F%B0-%EC%82%AC%EC%A7%84-765x519.jpg',
          'text' : '나만의 커피 어쩌고저쩌고'
        },
        {
          'id' : 0,
          'img' : 'http://www.bloter.net/wp-content/uploads/2016/08/%EC%8A%A4%EB%A7%88%ED%8A%B8%ED%8F%B0-%EC%82%AC%EC%A7%84-765x519.jpg',
          'text' : '나만의 커피 어쩌고저쩌고'
        }
      ]
    }

    //loading 구현을 위해 임시로 delay를 줌
    setTimeout(() => {
      this.props.dataStore.addSampleMagazines(data)
    }, 1000)

  }

  renderMonth(key, nowMonth, rowItems) {
    return (
      <div className="mag-bundle" key={key}>
        <span className="mag-month">{nowMonth}</span>
        <div className="mag-items">
          {rowItems}
        </div>
      </div>
    )
  }

  renderMagazine() {
    const data = this.props.dataStore.dataMagazineList
    let nowMonth = ''
    let rowMagazines = []
    let rowMonth = []
    let rowItems = []
    let key = 0
    for(var i = 0; i<data.length; i++) {
      let magazine = data[i]
      let thisMonth = this.props.apiStore.getMonth(magazine.ct)

      if(nowMonth !== thisMonth && (nowMonth !== '' && nowMonth !== undefined)) {
        rowMonth.push(
          this.renderMonth(key, nowMonth, rowItems)
        )
        rowItems = []
        key = key + 1
      }
      nowMonth = thisMonth
      rowItems.push(
        <div className="mag-item" key={magazine.id}>
          <Link className="mag-link" to={'/magazine/' + magazine.id}>
            <div className="img-wrapper">
              <img
                className="mag-image"
                src={'https://static.ofriends.co.kr/images/magazines/'+ magazine.id + '_0.jpg'}
                onError={(e)=>e.target.setAttribute("src", this.props.dataStore.imageUrl + "06_detailpage_no_image.png")}/>
            </div>
            <div className="title-wrapper">
              <span className="mag-title">{magazine.title}</span>
            </div>
          </Link>
        </div>
      )

    }

    rowMonth.push(
      this.renderMonth(key, nowMonth, rowItems)
    )

    return (
      <div className="mag-entity">
        <span className="magazine-title">{this.props.dataStore.str.W1040}</span>
        {rowMonth}
      </div>
    )
  }

  renderMagazines() {
    const data = this.props.dataStore.dataSampleMagazineList
    let rowMonth = []
    for(var i = 0; i<data.length; i++) {
      let rowData = data[i]
      let rowItems = []
      for(var j = 0; j<rowData.item.length; j++) {

        let item = rowData.item[j]
        rowItems.push(
          <div className="mag-item">
            <Link className="mag-link" to={'/magazine/' + item.id}>
              <div className="img-wrapper">
                <img
                  className="mag-image"
                  src={item.img}
                  onError={(e)=>e.target.setAttribute("src", this.props.dataStore.imageUrl + "06_detailpage_no_image.png")}/>
              </div>
              <div className="title-wrapper">
                <span className="mag-title">{item.text}</span>
              </div>
            </Link>
          </div>
        )
      }

      rowMonth.push(
        <div className="mag-bundle" key={i}>
          <span className="mag-month">{data[i].month}</span>
          <div className="mag-items">
            {rowItems}
          </div>
        </div>
      )
    }
    return (
      <div className="mag-entity">
        <span className="magazine-title">{this.props.dataStore.str.W1040}</span>
        {rowMonth}
      </div>
    )
  }

  render() {
    return (
      <div className="magazine-list">
        <InfiniteScroll
          className="mag-scroll"
          pageStart={0}
          loadMore={this.loadItems.bind(this)}
          hasMore={this.state.loadMore}
          loader={<div className="loader" key={0}>Loading ...</div>}>
          {this.renderMagazine()}
        </InfiniteScroll>
      </div>
    )

  }
}
export default MagazineList
