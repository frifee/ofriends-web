import { observable, action, computed } from 'mobx'
import axios from 'axios'
import str from '../static/string.json'
import { Link } from 'react-router-dom'
import * as Const from '../const'
export class DataStore {
  @observable data
  @observable str
  @observable imageUrl

  @observable myProfileTabs
  @observable myOfriendsTab
  @observable paymentMethod

  @observable sampleRecomData
  @observable sampleDailyTabs
  @observable sampleDailySubTabs1
  @observable sampleDailySubTabs2
  @observable sampleDailySortBy
  @observable sampleItemData1
  @observable sampleItemData2
  @observable sampleHost
  @observable sampleReviewHost
  @observable sampleHelpTabList
  @observable sampleHelpData
  @observable sampleNotices
  @observable sampleMyProfile
  @observable sampleMyOfriendsAvailable
  @observable sampleMyOfriendsHistory
  @observable sampleMyReviews

  @observable categories
  @observable interests

  @observable dataMiscNotice
  @observable dataMiscFaqs
  @observable dataMiscCoupons
  @observable dataMain
  @observable helpTabList
  @observable dataProductList
  @observable dataProduct
  @observable dataMagazineList
  @observable dataAuthenticate
  @observable dataJwt
  @observable dataUserInfo
  @observable sortByItem
  @observable encryptPK
  @observable dataPurchases
  @observable reviewImgUrl
  @observable dataHostReviews
  @observable dataPaymentInfo
  @observable dataPurchaseEach
  @observable dataSampleMagazineList
  @observable dataMagazine
  @observable dataMagazineProducts
  @observable dataEventFinished
  @observable dataEventRunning
  constructor() {
    this.dataSampleMagazineList = []
    this.data = "data"
    this.str = str
    this.categories = Const.CATEGORY
    this.interests = Const.InterestList
    this.imageUrl = "https://static.ofriends.co.kr/images/static/"
    this.reviewImgUrl = "https://static.ofriends.co.kr/images/reviews/"
    this.userImgUrl = "https://static.ofriends.co.kr/images/users/"
    this.productsImgUrl = "https://static.ofriends.co.kr/images/products/"
    this.exhibitImgUrl = "https://static.ofriends.co.kr/images/exhibits/"
    this.dataMiscNotice = []
    this.dataMiscFaqs = []
    this.dataMiscCoupons = []
    this.dataMain = {}
    this.dataProductList = []
    this.dataProduct = {}
    this.dataMagazineList = []
    this.dataAuthenticate = {}
    this.dataJwt = ''
    this.dataUserInfo = {}
    this.dataPurchases = []
    this.dataHostReviews = {}
    this.dataMagazine = {}
    this.dataMagazineProducts = []
    this.dataEventFinished = []
    this.dataEventRunning = []
    this.dataPaymentInfo = {
      'title' : '',
      'price' : 0,
      'discountPrice' : 0,
      'image' : '',
      'option1Name' : '',
      'option2Name' : '',
      'count' : 0,
      'paymentId' : 0,
      'productId' : -1,
      'option1Id' : 0,
      'option2Id' : 0,
      'fripType' : ''
    }
    this.encryptPK = Const.EncryptPK
    this.sortByItem = Const.SORT_BY_ITEM.item
    this.sampleHelpTabList = ['전체', '이용안내', '회원정보', '결제/환불', '쿠폰']
    this.helpTabList = [
      {
        id: 0,
        category: null,
        name: '전체'
      },
      {
        id: 1,
        category: 'usage',
        name: '이용안내'
      },
      {
        id: 2,
        category: 'user',
        name: '회원정보'
      },
      {
        id: 3,
        category: 'payment',
        name: '결제/환불'
      },
      {
        id: 4,
        category: 'coupon',
        name: '쿠폰'
      }
    ]

    this.sampleMyOfriendsAvailable = {}
    this.sampleMyOfriendsHistory = {}
    this.sampleMyReviews = {}

    // this.paymentMethod = ['신용/체크카드', '핸드폰 결제', '무통장 입금', '실시간 계좌이체']
    this.paymentMethod = ['신용/체크카드', '실시간 계좌이체']
    this.sampleMyProfile = {
      "id" : 0,
      "nickName" : null,
      "isActiveUser" : false,
      "name" : "이름입력",
      "birth" : "1900-00-00",
      "gender" : "남자",
      "cellNum" : "01012345678",
      "email" : "raul@frifee.co",
      "password" : "1111",
      "recomCode" : null
    }
    this.myProfileTabs = [
      {
        "title" : this.str.W0008,
        "url" : "/my/profile",
        "id" : 0,
      },
      {
        "title" : this.str.W0021,
        "url" : "/my/like",
        "id" : 1,
      },
      {
        "title" : this.str.W0032,
        "url" : "/my/ofriends/available",
        "id" : 2,
      },
      {
        "title" : this.str.W0033,
        "url" : "/my/interest",
        "id" : 3,
      },
      {
        "title" : this.str.W0034,
        "url" : "/my/coupon",
        "id" : 4,
      }
    ]
    this.myOfriendsTab = [
      {
        "title" : this.str.W0055,
        "url" : "/my/ofriends/available",
        "id" : 0,
      },
      {
        "title" : this.str.W0056,
        "url" : "/my/ofriends/history",
        "id" : 1,
      },
      {
        "title" : this.str.W0057,
        "url" : "/my/ofriends/review",
        "id" : 2,
      }
    ]

    this.sampleHelpData = {
      "data" : [
        {
          "title" : "예약일 당일에 취소가 가능한가요?",
          "content" : "안녕하세요 오프렌즈 고객센터입니다. 어쩌고 저쩌고",
          "id" : 0,
          "date" : "2019-10-08 14:00"
        },
        {
          "title" : "예약일 당일에 환불이 가능한가요?",
          "content" : "안녕하세요 오프렌즈 고객센터입니다. 어쩌고 저쩌고",
          "id" : 1,
          "date" : "2019-10-08 15:00"
        },
        {
          "title" : "예약일 당일에 XX가 가능한가요?",
          "content" : "안녕하세요 오프렌즈 고객센터입니다. 어쩌고 저쩌고",
          "id" : 2,
          "date" : "2019-10-08 16:00"
        },
        {
          "title" : "예약일 당일에 뭐가 가능한가요?",
          "content" : "안녕하세요 오프렌즈 고객센터입니다. 어쩌고 저쩌고",
          "id" : 3,
          "date" : "2019-10-08 17:00"
        },
        {
          "title" : "예약일 당일에 취소안하기가 가능한가요?",
          "content" : "안녕하세요 오프렌즈 고객센터입니다. 어쩌고 저쩌고",
          "id" : 4,
          "date" : "2019-10-08 18:00"
        }
      ]
    }
    this.sampleHelpTabList = ['전체', '이용안내', '회원정보', '결제/환불', '쿠폰']
    this.sampleHost = {
      "name" : "라울컬렉션",
      "tag" : "super",
      "frip_count" : 2,
      "like_count" : 20,
      "info" : "홍천 과수원 근문 프립 대원분들을 위해 라울이 알찬 일정을 준비했습니다!!",
      "image" : "https://res.cloudinary.com/frientrip/image/upload/%EB%B8%8C%EB%A1%9C%EC%B9%98_%EB%94%94%EC%9E%90%EC%9D%B8_pwfdg7"
    }
    this.sampleReviewHost = {
      "rate" : 3,
      "data" : [
        {
          "id" : 0,
          "name" : "김정훈1",
          "rate" : 4,
          "date" : "2019-10-30",
          "text" : "뭐 이딴게 다있어 맛은 있는데 뭔가 쫌 이상한것 같음",
          "frip_info" : {
            "name" : "미니사과 따고 빵 만들기",
            "date" : "2019-11-13",
            "option" : "참가비(1인)"
          },
          "profile_img" : "https://res.cloudinary.com/frientrip/image/upload/%EB%B8%8C%EB%A1%9C%EC%B9%98_%EB%94%94%EC%9E%90%EC%9D%B8_pwfdg7",
          "images" : [
            "https://res.cloudinary.com/frientrip/image/upload/c_limit,w_600,h_600,dpr_3.0/736936_cropped4037585446048028594",
            "https://res.cloudinary.com/frientrip/image/upload/c_limit,w_600,h_600,dpr_3.0/736936_cropped4037585446048028594",
            "https://res.cloudinary.com/frientrip/image/upload/c_limit,w_600,h_600,dpr_3.0/736936_cropped4037585446048028594"
          ]
        },
        {
          "id" : 1,
          "name" : "김정훈2",
          "rate" : 5,
          "date" : "2019-10-30",
          "text" : "뭐 이딴게 다있어 맛은 있는데 뭔가 쫌 이상한것 같음",
          "frip_info" : {
            "name" : "미니사과 따고 빵 만들기",
            "date" : "2019-11-13",
            "option" : "참가비(1인)"
          },
          "profile_img" : "https://res.cloudinary.com/frientrip/image/upload/%EB%B8%8C%EB%A1%9C%EC%B9%98_%EB%94%94%EC%9E%90%EC%9D%B8_pwfdg7",
          "images" : [ ]
        },
        {
          "id" : 2,
          "name" : "김정훈3",
          "rate" : 1,
          "date" : "2019-10-30",
          "text" : "뭐 이딴게 다있어 맛은 있는데 뭔가 쫌 이상한것 같음",
          "frip_info" : {
            "name" : "미니사과 따고 빵 만들기",
            "date" : "2019-11-13",
            "option" : "참가비(1인)"
          },
          "images" : [
            "https://res.cloudinary.com/frientrip/image/upload/c_limit,w_600,h_600,dpr_3.0/736936_cropped4037585446048028594"
          ]
        }
      ]
    }
    this.sampleItemData1 = {
      "info" : "이거? 내 살냄새야!",
      "title" : "[프립버스1] 미니사과 따고 빵 만들고 사과따고 빵 사과 빵 미니사과 따고 빵 만들고",
      "price" : 36000,
      "discount" : 27360,
      "discount_rate" : 24,
      "is_like" : 1,
      "type" : 0,
      "images" : [
        "https://res.cloudinary.com/frientrip/image/upload/ar_4:3,c_fill,dpr_1.0,g_auto,q_auto,r_0,w_930/product_banner_1568007404390_684866",
        "https://res.cloudinary.com/frientrip/image/upload/ar_4:3,c_fill,dpr_1.0,g_auto,q_auto,r_0,w_930/product_banner_1568007404390_684866",
        "https://res.cloudinary.com/frientrip/image/upload/ar_4:3,c_fill,dpr_1.0,g_auto,q_auto,r_0,w_930/product_banner_1568007404390_684866",
        "https://res.cloudinary.com/frientrip/image/upload/ar_4:3,c_fill,dpr_1.0,g_auto,q_auto,r_0,w_930/product_banner_1568007404390_684866"
      ],
      "like_count" : 7000,
      "rate" : 4.3,
      "location" : "서울",
      "option_date" : [
        {
          "date" : "2019년 10월 6일(일요일) 10:00",
          "due_date" : "2019년 10월 5일(일요일) 10:00",
          "count" : 10,
          "participate" : 2
        },
        {
          "date" : "2019년 10월 7일(일요일) 10:00",
          "due_date" : "2019년 10월 6일(일요일) 10:00",
          "count" : 10,
          "participate" : 2
        },
        {
          "date" : "2019년 10월 8일(일요일) 10:00",
          "due_date" : "2019년 10월 7일(일요일) 10:00",
          "count" : 10,
          "participate" : 2
        },
        {
          "date" : "2019년 10월 6일(일요일) 10:00",
          "due_date" : "2019년 10월 5일(일요일) 10:00",
          "count" : 10,
          "participate" : 10
        }
      ],
      "default_option" : [
        {
          "title" : "참가비 (1인)",
          "count" : 90,
          "price" : 30000,
          "discount" : 24000
        },
        {
          "title" : "참가비 (2인)",
          "count" : 90,
          "price" : 50000,
          "discount" : 40000
        }
      ]
    }
    this.sampleItemData2 = {
      "info" : "이거? 내 살냄새야!",
      "title" : "[프립버스2] 미니사과 따고 빵 만들고 사과따고 빵 사과 빵 미니사과 따고 빵 만들고",
      "price" : 36000,
      "discount" : 27360,
      "discount_rate" : 24,
      "is_like" : 0,
      "type" : 1,
      "images" : [
        "https://res.cloudinary.com/frientrip/image/upload/ar_4:3,c_fill,dpr_1.0,g_auto,q_auto,r_0,w_930/product_banner_1571113880754_331625",
        "https://res.cloudinary.com/frientrip/image/upload/ar_4:3,c_fill,dpr_1.0,g_auto,q_auto,r_0,w_930/product_banner_1571113880754_331625",
        "https://res.cloudinary.com/frientrip/image/upload/ar_4:3,c_fill,dpr_1.0,g_auto,q_auto,r_0,w_930/product_banner_1571113880754_331625"
      ],
      "like_count" : 7000,
      "rate" : 3.7,
      "location" : "경기 홍천",
      "option_date" : [
        {
          "date" : "2019년 10월 6일(일요일) 10:00",
          "due_date" : "2019년 10월 5일(일요일) 10:00",
          "count" : 10,
          "participate" : 2
        },
        {
          "date" : "2019년 10월 7일(일요일) 10:00",
          "due_date" : "2019년 10월 6일(일요일) 10:00",
          "count" : 10,
          "participate" : 1
        },
        {
          "date" : "2019년 10월 8일(일요일) 10:00",
          "due_date" : "2019년 10월 7일(일요일) 10:00",
          "count" : 10,
          "participate" : 3
        }
      ],
      "default_option" : [
        {
          "title" : "참가비 (1인)",
          "count" : 90,
          "price" : 30000,
          "discount" : 24000
        },
        {
          "title" : "참가비 (2인)",
          "count" : 90,
          "price" : 50000,
          "discount" : 40000
        }
      ]
    }
    this.sampleDailySortBy = {
      "item" : [
        {
          "name" : "인기순",
          "link" : "pop"
        },
        {
          "name" : "등록순",
          "link" : "time"
        },
        {
          "name" : "평점순",
          "link" : "rate"
        },
        {
          "name" : "가격 높은순",
          "link" : "highprice"
        },
        {
          "name" : "가격 낮은순",
          "link" : "lowprice"
        }
      ]
    }
    this.sampleDailyTabs = {
      "item" : [
        {
          "name" : "액티비티",
          "link" : "/activity"
        },
        {
          "name" : "배움",
          "link" : "/learning"
        },
        {
          "name" : "키즈",
          "link" : "/kids"
        }
      ]
    }
    this.sampleDailySubTabs1 = {
      "item" : [
        {
          "name" : "스포츠",
          "link" : "/activity"
        },
        {
          "name" : "투어/관람",
          "link" : "/tour"
        },
        {
          "name" : "대회/축제",
          "link" : "/festival"
        },
        {
          "name" : "먹거리",
          "link" : "/foods"
        },
        {
          "name" : "건강/뷰티",
          "link" : "/health"
        },
        {
          "name" : "실내체험",
          "link" : "/inside"
        },
        {
          "name" : "자원봉사",
          "link" : "/volunteer"
        }
      ]
    }
    this.sampleDailySubTabs2 = {
      "item" : [
        {
          "name" : "전체",
          "link" : "/"
        },
        {
          "name" : "라켓스포츠",
          "link" : "/racket"
        },
        {
          "name" : "구기스포츠",
          "link" : "/ballgame"
        },
        {
          "name" : "사격·양궁",
          "link" : "/shooting"
        },
        {
          "name" : "승마",
          "link" : "/riding"
        }
      ]
    }
    this.sampleRecomData = {
      "item" : [
        {
          "id": 1,
          "loc": "김포",
          "isLike": 1,
          "subTitle": "부제목",
          "title": "[제목]어쩌고저쩌고",
          "price": 50000,
          "discount": 30000,
          "rating": 4.3,
          "tags" : ["new", "only"]
        },
        {
          "id": 2,
          "loc": "김포",
          "isLike": 1,
          "subTitle": "부제목",
          "title": "[제목]어쩌고저쩌고",
          "price": 50000,
          "discount": 30000,
          "rating": 4.3,
          "tags" : ["only"]
        },
        {
          "id": 3,
          "loc": "서울",
          "isLike": 0,
          "subTitle": "부제목",
          "title": "[제목]어쩌고저쩌고",
          "price": 50000,
          "discount": 30000,
          "rating": 4.3,
          "tags" : ["new"]
        },
        {
          "id": 4,
          "loc": "인천",
          "isLike": 1,
          "subTitle": "부제목",
          "title": "[제목]어쩌고저쩌고",
          "price": 50000,
          "discount": 30000,
          "rating": 4.3,
          "tags" : []
        },
        {
          "id": 5,
          "loc": "춘천",
          "isLike": 1,
          "subTitle": "부제목",
          "title": "[제목]어쩌고저쩌고",
          "price": 50000,
          "discount": 30000,
          "rating": 4.3,
          "tags" : ["only"]
        },
        {
          "id": 6,
          "loc": "대구",
          "isLike": 0,
          "subTitle": "부제목",
          "title": "[제목]어쩌고저쩌고",
          "price": 50000,
          "discount": 30000,
          "rating": 4.3,
          "tags" : ["new"]
        }
      ]
    }
  }
  @action
  setData = (data) => {
    this.data = data
  }
  @action
  setDataMain = (data) => {
    this.dataMain = data
  }

  @action
  setDataProductList = (data) => {
    this.dataProductList = data
  }

  @action
  setDataPurchaseEach = (data) => {
    this.dataPurchaseEach = data
  }

  @action
  setDataProduct = (data) => {
    this.dataProduct = data
  }

  @action
  setDataProductLike = (value) => {
    this.dataProduct.product.like = value
  }

  @action
  setDataMiscNotice = (data) => {
    this.dataMiscNotice = data
  }

  @action
  setDataMiscFaqs = (data) => {
    this.dataMiscFaqs = data
  }

  @action
  setDataMiscCoupons = (data) => {
    this.dataMiscCoupons = data
  }

  @action
  setDataAuthenticate = (data) => {
    this.dataAuthenticate = data
  }

  @action
  setDataJwt = (data) => {
    this.dataJwt = data
  }

  @action
  setDataUserInfo = (data) => {
    this.dataUserInfo = data
  }

  @action
  setDataPurchases = (data) => {
    this.dataPurchases = data
  }

  @action
  setDataHostReviews = (data) => {
    this.dataHostReviews = data
  }

  @action
  setSampleMyProfile = (key, value) => {
    this.sampleMyProfile[key] = value
  }

  @action
  setSampleMyOfriendsAvailable = (data) => {
    this.sampleMyOfriendsAvailable = data
  }

  @action
  setSampleMyOfriendsHistory = (data) => {
    this.sampleMyOfriendsHistory = data
  }

  @action
  setSampleMyReviews = (data) => {
    this.sampleMyReviews = data
  }

  @action
  setDataPaymentInfo = (key, value) => {
    this.dataPaymentInfo[key] = value
  }

  @action
  setInitDataPaymentInfo = () => {
    this.dataPaymentInfo = {}
  }

  @action
  addSampleMagazines = (item) => {
    this.dataSampleMagazineList.push(item)
  }

  @action
  initDataMagazineList = () => {
    this.dataMagazineList = []
  }

  @action
  addDataMagazineList = (items) => {
    for(let row of items) {
      this.dataMagazineList.push(row)
    }
  }

  @action
  setDataMagazine = (data) => {
    this.dataMagazine = data.magazine
    this.dataMagazineProducts = []
    for(let row of data.magazine_products) {
      this.dataMagazineProducts.push(row)
    }
  }

  @action
  setDataEventFinished = (items) => {
    this.dataEventFinished = items
  }

  @action
  setDataEventRunning = (items) => {
    this.dataEventRunning = items
  }
 }

export default new DataStore()
