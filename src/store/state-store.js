import { observable, action, computed, runInAction } from 'mobx';

export class StateStore {

  @observable appVal
  @observable isLoading
  @observable loginState
  @observable mMenuOpen
  @observable isHeaderSearch
  @observable isHelpMenuOpen

  @observable isProfileMenuOpen
  @observable isDailySortOpen


  @observable sampleHelpOpen

  @observable productOption

  //tab position관련
  @observable helpHeaderTab
  @observable myProfileTab
  @observable mainHeaderTab
  @observable myOfriendsTab
  //my/profile
  @observable myProfileState
  @observable myReviewEditState
  @observable myInterestSelected

  @observable registerEmail
  @observable loginEmail
  @observable pwReset
  //popupOn
  @observable popupOn

  @observable largerImgView

  @observable samplePayment

  @observable noticeState
  @observable faqState
  @observable myCouponState
  @observable myReviewState
  @observable myOfrindsAvailState
  @observable myOfriendsHisState
  @observable reviewHostState
  @observable recommendState
  @observable exhibitState
  @observable searchState
  @observable dailyState
  @observable headerState
  @observable regActiveState
  @observable pwChangeState
  @observable likeBodyState
  @observable paymentState
  @observable phoneVerifyState
  @observable eventsState
  @observable mainPopupOn
  @observable couponPopupOn
  constructor() {

    this.isLoading = true
    this.mainPopupOn = "main"
    this.loginState = 0

    this.mainHeaderTab = 0
    this.helpHeaderTab = 0
    this.myProfileTab = 0
    this.myOfriendsTab = 0

    this.setDefaultOpen()
    this.nowPathName = '/'
    this.sampleHelpOpen = {}
    this.productOption = {
      "isOpenSched": false, //
      "isOpenDefault": false, // 1개일 때, 기본 옵션 창.. 2개일 때, 추가 옵션 창 > 여닫음 여부
      "isSelectSched": false, // 참가비(1인), X버튼, 가격, 수량(+,-) 창 뜨는 것
      "isSelectDefault": false,
      "optionSched": {},
      "optionDefault": {},
      "totalPrice": 0,
      "count": 0,
      "isShareOn": false,
      "totalSteps": 0,
      "parentId": 0,
      "isOnNoti": false,
      "callErrMsg": "",
      "mobileSelOpt": false,
      "isEventProduct": false,
      "eventProduct": {}, // ※ 모바일 - 이벤트 자동참여 전용. 다른 용도로 쓰면 절대로 안 됨!!
      "eventProductOpt": {} // ※ 모바일 - 이벤트 자동참여 전용. 다른 용도로 쓰면 절대로 안 됨!!
    }

    this.myProfileState = {
      "nickChange" : 0, //0: 저장상태, 1: 수정중
      "activeCustomer" : 0, //0: 등록안됨, 1: 등록중(popup on), 2: 등록완료
      "userLevel" : 0,
      "pwChange" : 0, //0: 저장중, 1: 수정중
      "recomCode" : 0, //0: 정보없음, 1: 변경중, 2: 삭제, 3: 삭제팝업
      "inputNick" : "",
      "inputRecomCode" : "",
      "checkPw" : 0, //0:문제없음, 1: 비번오류
    }
    this.myReviewEditState = {
      "id" : 0,
      "state" : 0, //0 : viewmode, 1: editmode
      "rate" : 0,
      "productId" : 0,
      "optionId" : 0,
      "productTitle" : "",
      "inputText" : "",
      "inputFormatText" : "",
      "thumbnails" : [],
      "reviewId" : -1,
    }
    this.myInterestSelected = []
    this.popupOn = ""
    this.couponPopupOn = ""
    this.largerImgView = {
      "position" : 0,
      "images" : []
    }
    this.registerEmail = {
      "inputEmail" : "",
      "inputPw" : "",
      "inputPwConfirm" : "",
      "inputRecomCode" : "",
      "isErrEmail" : false,
      "isErrPw" : false,
      "isErrPwConfirm" : false,
      "isErrRecomCode" : false,
      "isOnTerms" : false,
      "isOnNoti" : false,
      "inputErrMsg" : "",
      "callErrMsg" : "",
      "usePrivacyPopup": false
    }
    this.loginEmail = {
      "inputEmail" : "",
      "inputPw" : "",
      "isErrEmail" : false,
      "isErrPw" : false,
      "inputErrMsg" : "",
      "isOnNoti" : false,
      "callErrMsg" : ""
    }
    this.pwReset = {
      "inputEmail" : "",
      "isErrEmail" : false,
      "inputErrMsg" : "",
      "isOnNoti" : false,
      "callErrMsg" : "",
      "step" : 0 //0:이메일입력, 1: 발송완료
    }
    this.samplePayment = {

    }

    this.paymentState = {
      "activeUser" : false,
      "isOpenSelectedOf" : true,
      "isOpenSelectedOfFinish" : true,
      "price" : 0,
      "priceCut" : 0,
      "isSelectTerms" : false,
      "paymentMethod" : -1,
      "coupon" : -1,
      "state" : 0, //0: 결제준비, 1: 결제완료
      "offerPrivacyPopup" : false,
      "isCouponSelected" : false
    }

    this.noticeState = {
      "from" : 0,
      "to" : 1,
      "count" : 0,
      "page" : 0,
      "noticeOpen" : {}
    }

    this.likeBodyState = {
      "from" : 0,
      "to" : 1,
      "count" : 0,
      "page" : 0,
      "done" : false
    }

    this.faqState = {
      "from" : 0,
      "to" : 1,
      "count" : 0,
      "page" : 0,
      "itemOpen" : {},
      "tabPos" : 0
    }

    this.myCouponState = {
      "from" : 0,
      "to" : 1,
      "count" : 0,
      "page" : 0,
      "done" : false
    }

    this.myReviewState = {
      "from" : 0,
      "to" : 1,
      "count" : 0,
      "page" : 0,
      "done" : false
    }

    this.myOfrindsAvailState = {
      "from" : 0,
      "to" : 1,
      "count" : 0,
      "page" : 0,
      "done" : false
    }

    this.myOfriendsHisState = {
      "from" : 0,
      "to" : 1,
      "count" : 0,
      "page" : 0,
      "done" : false
    }

    this.reviewHostState = {
      "from" : 0,
      "to" : 1,
      "count" : 0,
      "page" : 0,
      "done" : false
    }

    this.recommendState = {
      "from" : 0,
      "to" : 1,
      "count" : 0,
      "page" : 0
    }

    this.exhibitState = {
      "id" : 0,
      "from" : 0,
      "to" : 1,
      "count" : 0,
      "page" : 0
    }

    this.searchState = {
      "from" : 0,
      "to" : 1,
      "count" : 0,
      "page" : 0,
      "keyword" : ""
    }

    this.dailyState = {
      "from" : 0,
      "to" : 1,
      "count" : 0,
      "page" : 0,
      "queryParams" : "",
      "done": false
    }

    this.headerState = {
      "inputSearch" : "",
      "isOnSearch" : false
    }

    this.eventsState = {
      "fromO" : 0,
      "toO" : 1,
      "countO" : 0,
      "pageO" : 0,
      "fromF" : 0,
      "toF" : 1,
      "countF" : 0,
      "pageF" : 0,
      "done": false
    }

    this.regActiveState = {
      "regCase" : -1, // 1(휴대폰 인증만, 결제의 경우), 2(본인인증 + 마케팅 동의), 3(본인인증 + 마케팅 동의, 내 오프렌즈 등업할 때)
      "isOn" : false,
      "isConfirmSelf" : false,
      "selUsePrivacy" : false,
      "selOfferPrivacy" : false,
      "isOnUsePrivacy" : false,
      "isOnOfferPrivacy" : false,
      "storageDuration" : -1,
      "contactMethodNumber" : false,
      "contactMethodSms" : false,
      "contactMethodEmail" : false,
      "title" : "",
      "idTitle" : "",
      "mktTitle" : "",
      "endMsg" : ""
    }

    this.pwChangeState = {
      "inputPrevPw" : "",
      "inputNowPw" : "",
      "inputRepeatPw" : "",
      "isErrPw" : false,
      "isErrPwConfirm" : false,
      "inputErrMsg" : "",
      "isOnNoti" : false,
      "callErrMsg" : ""
    }

    this.phoneVerifyState = {
      "isOn" : false,
      "inputName" : "",
      "inputBirth" : "",
      "inputGender" : "",
      "inputPhoneNumber" : "",
      "inputVerifyNumber" : "",
      "verifyNumber" : 0,
      "isPhoneVerified" : false,
      "callErrMsg" : "",
      "birth" : "",
      "gender" : "",
      "name" : "",
      "phoneNumber" : ""
    }
  }

  @action
  setIsLoading(state) {
    this.isLoading = state
  }

  @action
  setLoginState(state) {
    this.loginState = state
  }

  @action
  setDefaultOpen() {
    this.isHeaderSearch = false
    this.isHelpMenuOpen = false
    this.isProfileMenuOpen = false
    this.isDailySortOpen = false
  }

  @action
  setMMenuOpen = (isOpen) => {
    this.setDefaultOpen()
    this.mMenuOpen = isOpen
  }

  @action
  setIsHeaderSearch = (isOn) => {
    this.setDefaultOpen()
    this.isHeaderSearch = isOn
  }

  @action
  setMainHeaderTab = (pos) => {
    this.setDefaultOpen()
    this.mainHeaderTab = pos
  }

  @action
  setIsHelpMenuOpen = (isOn) => {
    this.setDefaultOpen()
    this.isHelpMenuOpen = isOn
  }

  @action
  setIsProfileMenuOpen = (isOn) => {
    this.setDefaultOpen()
    this.isProfileMenuOpen = isOn
  }

  @action
  setIsDailySortOpen = (isOpen) => {
    this.setDefaultOpen()
    this.isDailySortOpen = isOpen
  }

  @action
  setProductOption = (key, value) => {
    this.productOption[key] = value
  }

  @action
  setHelpHeaderTab = (position) => {
    this.helpHeaderTab = position
  }

  @action
  setSampleHelpOpen = (data) => {
    this.sampleHelpOpen = data
  }

  @action
  setNoticeState = (key, value) => {
    this.noticeState[key] = value
  }

  @action
  setLikeBodyState = (key, value) => {
    this.likeBodyState[key] = value
  }

  @action
  setFaqState = (key, value) => {
    this.faqState[key] = value
  }

  @action
  setMyCouponState = (key, value) => {
    this.myCouponState[key] = value
  }

  @action
  setMyReviewState = (key, value) => {
    this.myReviewState[key] = value
  }

  @action
  setMyOfrindsAvailState = (key, value) => {
    this.myOfrindsAvailState[key] = value
  }

  @action
  setMyOfriendsHisState = (key, value) => {
    this.myOfriendsHisState[key] = value
  }

  @action
  setReviewHostState = (key, value) => {
    this.reviewHostState[key] = value
  }

  @action
  setHeaderState = (key, value) => {
    this.headerState[key] = value
  }

  @action
  setRecommendState = (key, value) => {
    this.recommendState[key] = value
  }

  @action
  setExhibitState = (key, value) => {
    this.exhibitState[key] = value
  }

  @action
  setSearchState = (key, value) => {
    this.searchState[key] = value
  }

  @action
  setDailyState = (key, value) => {
    this.dailyState[key] = value
  }

  @action
  setRegActiveState = (key, value) => {
    this.regActiveState[key] = value
  }

  @action
  setMyProfileTab = (pos) => {
    this.myProfileTab = pos
  }

  @action
  setMyProfileState = (key, value) => {
    this.myProfileState[key] = value
  }

  @action
  setMyReviewEditState = (key, value) => {
    this.myReviewEditState[key] = value
  }

  @action
  setPwChangeState = (key, value) => {
    this.pwChangeState[key] = value
  }

  @action
  setMyInterestSelected = (id) => {
    if(!this.myInterestSelected.includes(id)) {
      this.myInterestSelected.push(id)
    } else {
      let index = this.myInterestSelected.indexOf(id)
      if(index > -1) {
        this.myInterestSelected.splice(index, 1)
      }
    }
  }

  @action
  clearMyInterestSelected = () => {
    this.myInterestSelected = []
  }

  @action
  setMyOfriendsTab = (pos) => {
    this.myOfriendsTab = pos
  }

  @action
  setPopupOn = (key) => {
    this.popupOn = key
  }

  @action
  setCouponPopupOn = (key) => {
    this.couponPopupOn = key
  }

  @action
  setMainPopupOn = (key) => {
    this.setMainPopupOn = key
  }

  @action
  setLargerImgView = (key, value) => {
    this.largerImgView[key] = value
  }

  @action
  setSamplePayment = (key, value) => {
    this.samplePayment[key] = value
  }

  @action
  setPaymentState = (key, value) => {
    this.paymentState[key] = value
  }

  @action
  setInitPayment = () => {
    this.paymentState = {
      "activeUser" : false,
      "isOpenSelectedOf" : true,
      "isOpenSelectedOfFinish" : true,
      "price" : 0,
      "priceCut" : 0,
      "isSelectTerms" : false,
      "paymentMethod" : -1,
      "coupon" : -1,
      "state" : 0, //0: 결제준비, 1: 결제완료
      "offerPrivacyPopup" : false,
      "isCouponSelected" : false
    }
  }

  @action
  setRegisterEmail = (key, value) => {
    this.registerEmail[key] = value
  }
  @action
  setLoginEmail = (key, value) => {
    this.loginEmail[key] = value
  }

  @action
  setPwReset = (key, value) => {
    this.pwReset[key] = value
  }

  @action
  setPhoneVerifyState = (key, value) => {
    this.phoneVerifyState[key] = value
  }

  @action
  setEventsState = (key, value) => {
    this.eventsState[key] = value
  }

}

export default new StateStore()
