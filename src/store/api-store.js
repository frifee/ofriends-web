import { observable, action, computed } from 'mobx';
import axios from 'axios';
import jwt from 'jwt-decode'

const API_DEV = "https://apidev.ofriends.co.kr/"
const API_MAIN = "https://api.ofriends.co.kr/"

const HOME_DEV = "https://dev.ofriends.co.kr/"
const HOME_MAIN = "https://ofriends.co.kr/"

let API_URL = ""
export let HOME_URL = ""

let isDev = false

export class ApiStore {
  @observable sample

  constructor() {
    this.sample = "sample"
    API_URL = isDev ? API_DEV : API_MAIN
    HOME_URL = isDev ? HOME_DEV : HOME_MAIN
  }

  getFormatWon(number) {
    try {
      return number.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,') + ' 원'
    }catch(e) {
      return ''
    }
  }
  getQueryParams = (params) => {

    let queryParams = []
    for(var i=0; i<params.length; i++) {
      queryParams.push(params[i].key + "=" + params[i].value)
    }
    if(queryParams.length> 0)
      return '?' + encodeURI(queryParams.join("&")).replace(/"/g, '%22')
    else
      return ''
  }

  getSample = () => {
    // let url = 'https://api.frientrip.com/Categories/v5/list'
    // return axios.get(url)
  }

  timePad = (number) => {
    return number<10 ? '0' + number : number
  }

  getTime = (timestamp) => {
    let d = new Date(timestamp*1000)
    let year = d.getFullYear()
    let month  = this.timePad(d.getMonth()+1)
    let date = this.timePad(d.getDate())
    let hours = this.timePad(d.getHours())
    let minutes = this.timePad(d.getMinutes())
    let seconds = this.timePad(d.getSeconds())
    return year + '-' + month + '-' + date
  }

  getFullTime = (timestamp) => {
    let d = new Date(timestamp*1000)
    let year = d.getFullYear()
    let month  = this.timePad(d.getMonth()+1)
    let date = this.timePad(d.getDate())
    let hours = this.timePad(d.getHours())
    let minutes = this.timePad(d.getMinutes())
    let seconds = this.timePad(d.getSeconds())
    return year + '-' + month + '-' + date + ' ' + hours + ':' + minutes
  }

  getMonth = (timestamp) => {
    let d = new Date(timestamp*1000)
    let year = d.getFullYear()
    let month  = this.timePad(d.getMonth()+1)
    return year + '-' + month
  }

  getMagazineTime = (timestamp) => {
    let d = new Date(timestamp*1000)
    let year = d.getFullYear()
    let month  = this.timePad(d.getMonth()+1)
    return year + '년 ' + month + '월'
  }

  getScheTime = (timestamp) => {
    let d = new Date(timestamp*1000)
    let year = d.getFullYear()
    let month  = this.timePad(d.getMonth()+1)
    let date = this.timePad(d.getDate())
    let hours = this.timePad(d.getHours())
    let minutes = this.timePad(d.getMinutes())
    let week = ['일', '월', '화', '수', '목', '금', '토'];
    let yyyyMMDD = year + '-' + month + '-' + date
    let dayOfWeek = week[new Date(yyyyMMDD).getDay()];
    return year + '년 ' + month + '월 ' + date + '일' + '(' + dayOfWeek + '요일) ' + hours + ':' + minutes
  }

  getJwtToken = () => {
    let jwtToken = localStorage.getItem('jwt')
    if(jwtToken != null) {
      let token = jwt(jwtToken)
      jwtToken = parseInt(token.exp) < Math.floor(Date.now() / 1000) ? null : jwtToken
    }

    return jwtToken
  }

  chkToken = (jwtToken) => {
    return (jwtToken != null)
      && parseInt(jwt(jwtToken).exp) > Math.floor(Date.now() / 1000)
  }

  getSamplePopular = () => {
    // let url = 'https://api.frientrip.com/SearchLogs/v5/popular'
    // return axios.get(url)
  }
  //get api
  getMiscNotice = (params) => {
    let queryParams = this.getQueryParams(params)
    let url = API_URL + 'misc/notices' + queryParams
    return axios.get(url)
  }

  getMiscFaqs = (params) => {
    let queryParams = this.getQueryParams(params)
    let url = API_URL + 'misc/faqs' + queryParams
    return axios.get(url)
  }

  getMiscCoupons = (token, params) => {
    let queryParams = this.getQueryParams(params)
    let url = API_URL + 'misc/coupons' + queryParams
    let authHeader = {}
    if(this.chkToken(token) === true) {
      authHeader = {'Authorization': 'Bearer ' + token}
    }
    return axios.create({
      headers: authHeader
    }).get(url)
  }

  getMain = (token, params) => {
    let queryParams = this.getQueryParams(params)
    let url = API_URL + 'products/main' + queryParams
    let authHeader = {}
    if(this.chkToken(token) === true) {
      authHeader = {'Authorization': 'Bearer ' + token}
    }
    return axios.create({
      headers: authHeader
    }).get(url)
  }

  getProductList = (token, params) => {
    let queryParams = this.getQueryParams(params)
    let url = API_URL + 'products' + queryParams
    let authHeader = {}
    if(this.chkToken(token) === true) {
      authHeader = {'Authorization': 'Bearer ' + token}
    }
    return axios.create({
      headers: authHeader
    }).get(url)
  }

  getProduct = (token, productId) => {
    let url = API_URL + 'products/' + productId
    let authHeader = {}
    if(this.chkToken(token) === true) {
      authHeader = {'Authorization': 'Bearer ' + token}
    }
    return axios.create({
      headers: authHeader
    }).get(url)
  }

  getMagazineList = (token, params) => {
    let queryParams = this.getQueryParams(params)
    let url = API_URL + 'magazines' + queryParams
    let authHeader = {}
    if(this.chkToken(token) === true) {
      authHeader = {'Authorization': 'Bearer ' + token}
    }
    return axios.create({
      headers: authHeader
    }).get(url)
  }

  getMagazine = (token, magazineId) => {
    let url = API_URL + 'magazines/' + magazineId
    let authHeader = {}
    if(this.chkToken(token) === true) {
      authHeader = {'Authorization': 'Bearer ' + token}
    }
    return axios.create({
      headers: authHeader
    }).get(url)
  }

  getUserInfo = (token) => {
    let url = API_URL + 'users/info'
    let authHeader = {}
    if(this.chkToken(token) === true) {
      authHeader = {'Authorization': 'Bearer ' + token}
    }
    return axios.create({
      headers: authHeader
    }).get(url)
  }

  getUserFavorite = (token) => {
    let url = API_URL + 'users/favorites'
    let authHeader = {}
    if(this.chkToken(token) === true) {
      authHeader = {'Authorization': 'Bearer ' + token}
    }
    return axios.create({
      headers: authHeader
    }).get(url)
  }

  getPurchase = (token, params) => {
    let queryParams = this.getQueryParams(params)
    let url = API_URL + 'purchases' + queryParams
    let authHeader = {}
    if(this.chkToken(token) === true) {
      authHeader = {'Authorization': 'Bearer ' + token}
    }
    return axios.create({
      headers: authHeader
    }).get(url)
  }

  getPurchaseEach = (token, purchaseId) => {
    let url = API_URL + 'purchases/' + purchaseId
    let authHeader = {}
    if(this.chkToken(token) === true) {
      authHeader = {'Authorization': 'Bearer ' + token}
    }
    return axios.create({
      headers: authHeader
    }).get(url)
  }

  getHostReview = (token, hostId, params) => {
    let queryParams = this.getQueryParams(params)
    let url = API_URL + 'hosts/' + hostId + "/reviews" + queryParams
    let authHeader = {}
    if(this.chkToken(token) === true) {
      authHeader = {'Authorization': 'Bearer ' + token}
    }
    return axios.create({
      headers: authHeader
    }).get(url)
  }

  getHostReviewEach = (token, hostId, reviewId) => {
    let url = API_URL + 'hosts/' + hostId + "/reviews/" + reviewId
    let authHeader = {}
    if(this.chkToken(token) === true) {
      authHeader = {'Authorization': 'Bearer ' + token}
    }
    return axios.create({
      headers: authHeader
    }).get(url)
  }

  //post api
  postAuthenticate = (payload) => {
    let url = API_URL + 'users/authenticate'
    return axios.post(url, payload)
  }

  postUserSignup = (payload) => {
    let url = API_URL + 'users/signup'
    return axios.post(url, payload)
  }

  postImageUpload = (token, formData) => {
    let url = API_URL + 'images/upload/reviews'
    let authHeader = {'Content-Type':'multipart/form-data'}
    if(this.chkToken(token) === true) {
      authHeader = {
        'Authorization': 'Bearer ' + token,
        'Content-Type':'multipart/form-data'
      }
    }
    return axios.create({
      headers: authHeader,
      data: formData
    }).post(url, formData)
  }

  postImageUploadUsers = (token, formData) => {
    let url = API_URL + 'images/upload/users'
    let authHeader = {'Content-Type':'multipart/form-data'}
    if(this.chkToken(token) === true) {
      authHeader = {
        'Authorization': 'Bearer ' + token,
        'Content-Type':'multipart/form-data'
      }
    }
    return axios.create({
      headers: authHeader,
      data: formData
    }).post(url, formData)
  }

  postHostReview = (token, hostId, payload) => {
    let url = API_URL + 'hosts/' + hostId + '/reviews'
    let authHeader = {}
    if(this.chkToken(token) === true) {
      authHeader = {'Authorization': 'Bearer ' + token}
    }
    return axios.create({
      headers: authHeader
    }).post(url, payload)
  }

  postPurchases = (token, payload) => {
    let url = API_URL + 'purchases'
    let authHeader = {}
    if(this.chkToken(token) === true) {
      authHeader = {'Authorization': 'Bearer ' + token}
    }
    return axios.create({
      headers: authHeader
    }).post(url, payload)
  }

  postPurchasesSettlement = (token, purchaseId, payload) => {
    let url = API_URL + 'purchases/' + purchaseId + '/settlement'
    let authHeader = {}
    if(this.chkToken(token) === true) {
      authHeader = {'Authorization': 'Bearer ' + token}
    }
    return axios.create({
      headers: authHeader
    }).post(url, payload)
  }

  postPurchasesCancel = (token, purchaseId, payload) => {
    let url = API_URL + 'purchases/' + purchaseId + '/cancel'
    let authHeader = {}
    if(this.chkToken(token) === true) {
      authHeader = {'Authorization': 'Bearer ' + token}
    }
    return axios.create({
      headers: authHeader
    }).post(url, payload)
  }

  postWithdrawal = (token) => {
    let url = API_URL + 'users/withdrawal'
    let authHeader = {}
    if(this.chkToken(token) === true) {
      authHeader = {'Authorization': 'Bearer ' + token}
    }
    return axios.create({
      headers: authHeader
    }).post(url)
  }

  postRequestVerifyNumber = (token, payload) => {
    let url = API_URL + 'users/phone/cert'
    let authHeader = {}
    if(this.chkToken(token) === true) {
      authHeader = {'Authorization': 'Bearer ' + token}
    }
    return axios.create({
      headers: authHeader
    }).post(url, payload)
  }

  postVerifyConfirm = (token, certId, payload) => {
    let url = API_URL + 'users/phone/cert/' + certId + '/check'
    let authHeader = {}
    if(this.chkToken(token) === true) {
      authHeader = {'Authorization': 'Bearer ' + token}
    }
    return axios.create({
      headers: authHeader
    }).post(url, payload)
  }


  postMobilePayComplete = (token, merchantId, payload) => {
    let url = API_URL + 'purchases/' + merchantId + '/complete'
    let authHeader = {}
    if(this.chkToken(token) === true) {
      authHeader = {'Authorization': 'Bearer ' + token}
    }
    return axios.create({
      headers: authHeader
    }).post(url, payload)
  }

  postPurchaseAttempt = (token, purchaseId, payload) => {
    let url = API_URL + 'purchases/' + purchaseId + '/attempt'
    let authHeader = {}
    if(this.chkToken(token) === true) {
      authHeader = {'Authorization': 'Bearer ' + token}
    }
    return axios.create({
      headers: authHeader
    }).post(url, payload)
  }


  //put api
  putLike = (token, productId) => {
    let url = API_URL + 'products/' + productId + '/like'
    let authHeader = {}
    if(this.chkToken(token) === true) {
      authHeader = {'Authorization': 'Bearer ' + token}
    }
    return axios.create({
      headers: authHeader
    }).put(url)
  }

  putUserPassword = (token, payload) => {
    let url = API_URL + 'users/password'
    let authHeader = {}
    if(this.chkToken(token) === true) {
      authHeader = {'Authorization': 'Bearer ' + token}
    }
    return axios.create({
      headers: authHeader
    }).put(url, payload)
  }

  putUserPasswordLost = (token, payload) => {
    let url = API_URL + 'users/password/lost'
    let authHeader = {}
    if(this.chkToken(token) === true) {
      authHeader = {'Authorization': 'Bearer ' + token}
    }
    return axios.create({
      headers: authHeader
    }).put(url, payload)
  }

  putUserSetting = (token, payload) => {
    let url = API_URL + 'users/setting'
    let authHeader = {}
    if(this.chkToken(token) === true) {
      authHeader = {'Authorization': 'Bearer ' + token}
    }
    return axios.create({
      headers: authHeader
    }).put(url, payload)
  }

  putUserFavorite = (token, payload) => {
    let url = API_URL + 'users/favorites'
    let authHeader = {}
    if(this.chkToken(token) === true) {
      authHeader = {'Authorization': 'Bearer ' + token}
    }
    return axios.create({
      headers: authHeader
    }).put(url, payload)
  }

  putHostReview = (token, hostId, reviewId, payload) => {
    let url = API_URL + 'hosts/' + hostId + '/reviews/' + reviewId
    let authHeader = {}
    if(this.chkToken(token) === true) {
      authHeader = {'Authorization': 'Bearer ' + token}
    }
    return axios.create({
      headers: authHeader
    }).put(url, payload)
  }

  //del api
  delLike = (token, productId) => {
    let url = API_URL + 'products/' + productId + '/like'
    let authHeader = {}
    if(this.chkToken(token) === true) {
      authHeader = {'Authorization': 'Bearer ' + token}
    }
    return axios.create({
      headers: authHeader
    }).delete(url)
  }

}

export default new ApiStore()
