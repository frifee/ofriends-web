import React, { Component, Fragment } from 'react';

import { inject, observer } from 'mobx-react';

import Head from './comp/common/head'
import Header from './comp/common/header'
import Body from './comp/common/body'
import Footer from './comp/common/footer'
import { withRouter, Link } from 'react-router-dom'
@inject('stateStore', 'apiStore', 'dataStore')
@withRouter
@observer
class App extends Component {

  constructor(props) {
    super(props)
    this.state = {
      mounted : false
    }
  }
  componentDidMount() {
    this.setState({
      mounted : true
    })
  }
  onClickOut = (e) => {
    this.props.stateStore.setDefaultOpen()
  }

  render() {
    return (
      <Fragment>
        {
          this.state.mounted
          &&
          <div onClick={(e) => this.onClickOut(e)}>
            <Head></Head>
            <Header></Header>
            <Body></Body>
            <Footer></Footer>
          </div>
        }
      </Fragment>


    )

  }
}
export default App
