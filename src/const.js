export const SORT_BY_ITEM = {
  "item" : [
    {
      "id" : 0,
      "name" : "인기순",
      "link" : "?sort=likes"
    },
    {
      "id" : 1,
      "name" : "등록순",
      "link" : "?sort=ct"
    },
    {
      "id" : 2,
      "name" : "평점순",
      "link" : "?sort=rating"
    },
    {
      "id" : 3,
      "name" : "가격 높은순",
      "link" : "?sort=price"
    },
    {
      "id" : 4,
      "name" : "가격 낮은순",
      "link" : "?sort=price&sortby=asc"
    }
  ]
}

export const CATEGORY = {
  "dict" : {
    "play" : 0,
    "learn" : 1,
    "share" : 2
  },
  "mainCate" : [
    {
      "id": 0,
      "link" : "/daily/play",
      "name" : "놀기"
    },
    {
      "id": 1,
      "link" : "/daily/learn",
      "name" : "배우기"
    },
    {
      "id": 2,
      "link" : "/daily/share",
      "name" : "함께하기"
    }
  ],
  "data" : [
    {
      "id" : 0,
      "name" : "놀기",
      "link" : "/play",
      "sub" : [
        {
          "id" : 0,
          "name" : "Eat&Drink",
          "link" : "/food"
        },
        {
          "id" : 1,
          "name" : "아웃도어",
          "link" : "/outdoor"
        },
        {
          "id" : 2,
          "name" : "건강/뷰티",
          "link" : "/beauty"
        },
        {
          "id" : 3,
          "name" : "투어/관람",
          "link" : "/tour"
        },
        {
          "id" : 4,
          "name" : "체험",
          "link" : "/experience"
        },
        {
          "id" : 5,
          "name" : "키즈",
          "link" : "/kids"
        }
      ]
    },
    {
      "id" : 1,
      "name" : "배우기",
      "link" : "/learn",
      "sub" : [
        {
          "id" : 0,
          "name" : "재테크",
          "link" : "/money"
        },
        {
          "id" : 1,
          "name" : "스포츠",
          "link" : "/sports"
        },
        {
          "id" : 2,
          "name" : "쿠킹/베이킹",
          "link" : "/cooking"
        },
        {
          "id" : 3,
          "name" : "음악/예술",
          "link" : "/art"
        },
        {
          "id" : 4,
          "name" : "공예DIY",
          "link" : "/craft"
        },
        {
          "id" : 5,
          "name" : "잡코칭",
          "link" : "/job"
        }
      ]
    },
    {
      "id" : 2,
      "name" : "함께하기",
      "link" : "/share",
      "sub" : [
        {
          "id" : 0,
          "name" : "스터디/소셜",
          "link" : "/study"
        },
        {
          "id" : 1,
          "name" : "라이프케어",
          "link" : "/lifecare"
        },
        {
          "id" : 2,
          "name" : "이벤트",
          "link" : "/event"
        }
      ]
    }
  ]
}

export const InterestList = {
  "data" : [
    {
      "id" : 0,
      "category" : "Sports",
      "item" : [
        {
          "id" : 1010,
          "name" : "러닝/워킹"
        },
        {
          "id" : 1020,
          "name" : "등산/클라이밍"
        },
        {
          "id" : 1030,
          "name" : "라이딩"
        },
        {
          "id" : 1040,
          "name" : "골프"
        },
        {
          "id" : 1050,
          "name" : "스키/보드"
        },
        {
          "id" : 1060,
          "name" : "서핑"
        },
        {
          "id" : 1070,
          "name" : "스쿠버"
        },
        {
          "id" : 1080,
          "name" : "구기 스포츠"
        },
        {
          "id" : 1090,
          "name" : "라켓 스포츠"
        },
        {
          "id" : 1100,
          "name" : "헬스"
        },
        {
          "id" : 1110,
          "name" : "요가"
        },
        {
          "id" : 1120,
          "name" : "댄스"
        }
      ]
    },
    {
      "id" : 1,
      "category" : "Art&Culture",
      "item" : [
        {
          "id" : 2010,
          "name" : "공방"
        },
        {
          "id" : 2020,
          "name" : "미술"
        },
        {
          "id" : 2030,
          "name" : "음악"
        },
        {
          "id" : 2040,
          "name" : "사진"
        },
        {
          "id" : 2050,
          "name" : "관람"
        },
        {
          "id" : 2060,
          "name" : "영화"
        },
        {
          "id" : 2070,
          "name" : "독서"
        },
        {
          "id" : 2080,
          "name" : "쿠킹"
        },
        {
          "id" : 2090,
          "name" : "술/음료"
        }
      ]
    },
    {
      "id" : 2,
      "category" : "Study",
      "item" : [
        {
          "id" : 3010,
          "name" : "자녀교육"
        },
        {
          "id" : 3020,
          "name" : "재테크"
        },
        {
          "id" : 3030,
          "name" : "금융상품"
        },
        {
          "id" : 3040,
          "name" : "주식"
        },
        {
          "id" : 3050,
          "name" : "부동산"
        },
        {
          "id" : 3060,
          "name" : "취업/이직"
        },
        {
          "id" : 3070,
          "name" : "퇴직"
        },
        {
          "id" : 3080,
          "name" : "노후준비"
        },
        {
          "id" : 3090,
          "name" : "투잡"
        },
        {
          "id" : 3100,
          "name" : "건강"
        }
      ]
    }
  ]
}

export const EncryptPK = '-----BEGIN PUBLIC KEY-----\nMFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBAKNKCQ+aAqn7Z4Sm+QEp2blKCm9JA7+Nrh62yfY0ENS0xMy1UgumuICnzkiHIm8dHEEZRkyxGWMncn5CfBNieKUCAwEAAQ==\n-----END PUBLIC KEY-----'
