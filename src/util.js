import axios from "axios";

export function impInit() {
  window.IMP.init('imp61039100')
}

export function impCertification(context, callback) {
  window.IMP.certification({
    merchant_uid : 'merchant_' + new Date().getTime() //본인인증과 연관된 가맹점 내부 주문번호가 있다면 넘겨주세요

  }, function(rsp) {
    // console.log(rsp)
    callback(context, rsp)
    // if(rsp.success) {
    // 	 // 인증성공
    //   window.$.ajax({
  	// 	  type : 'POST',
  	// 	  url : 'https://api.iamport.kr/certifications/' + rsp.imp_uid,
  	// 	  dataType : 'json',
  	// 	  data : {
  	// 		  imp_uid : rsp.imp_uid
  	// 	  }
		//   }).done(function(rsp) {
    //     console.log(rsp)
		//   });
    //
    // } else {
    // 	 // 인증취소 또는 인증실패
    //
    // }
  })
}

export function impPayment(options, paymentCallback) {
  window.IMP.request_pay(options, function(data) {
    let payload = {
      'apply_num': data.apply_num,
      'bank_name': data.bank_name,
      'buyer_addr': data.buyer_addr,
      'buyer_email': data.buyer_email,
      'buyer_name': data.buyer_name,
      'buyer_postcode': data.buyer_postcode,
      'buyer_tel': data.buyer_tel,
      'card_name': data.card_name,
      'card_quota': data.card_quota,
      'currency': data.currency,
      'custom_data': data.custom_data,
      'imp_uid': data.imp_uid,
      'merchant_uid': data.merchant_uid,
      'name': data.name,
      'paid_amount': data.paid_amount,
      'paid_at': data.paid_at,
      'pay_method': data.pay_method,
      'pg_provider': data.pg_provider,
      'pg_tid': data.pg_tid,
      'pg_type': data.pg_type,
      'receipt_url': data.receipt_url,
      'request_id': data.request_id,
      'status': data.status,
      'success': data.success,
      'error_msg': data.error_msg
    }
    paymentCallback(payload)
    if ( data.success ) {
    	//[1] 서버단에서 결제정보 조회를 위해 jQuery ajax로 imp_uid 전달하기

    	// window.$.ajax({
    	// 	url: "/payments/complete", //cross-domain error가 발생하지 않도록 주의해주세요
    	// 	type: 'POST',
    	// 	dataType: 'json',
    	// 	data: {
	    // 		imp_uid : rsp.imp_uid
	    // 		//기타 필요한 데이터가 있으면 추가 전달
    	// 	}
    	// }).done(function(data) {
      //
    	// 	//[2] 서버에서 REST API로 결제정보확인 및 서비스루틴이 정상적인 경우
    	// 	if ( true ) {
    	// 		var msg = '결제가 완료되었습니다.';
    	// 		msg += '\n고유ID : ' + rsp.imp_uid;
    	// 		msg += '\n상점 거래ID : ' + rsp.merchant_uid;
    	// 		msg += '\결제 금액 : ' + rsp.paid_amount;
    	// 		msg += '카드 승인번호 : ' + rsp.apply_num;
      //
    	// 		alert(msg);
    	// 	} else {
    	// 		//[3] 아직 제대로 결제가 되지 않았습니다.
    	// 		//[4] 결제된 금액이 요청한 금액과 달라 결제를 자동취소처리하였습니다.
    	// 	}
    	// });
    } else {
        // var msg = '결제에 실패하였습니다.';
        // msg += '에러내용 : ' + rsp.error_msg;
        //
        // alert(msg);
        // paymentCallback(false, rsp)
    }
  });
}
