#react app 세팅하는법

1. git clone
2. npm install
3. npm start

##how to mobx
- 앱 데이터 및 상태관리
- 참고: https://github.com/mobxjs/mobx
- rest call, api 결과값 저장, app 상태 클래스 세개로 나눠둠

##app 구조

- comp : only react component
- comp/common : 앱 전체 화면구성으로 쪼개둠
- comp/dialog : 앱 내 필요한 팝업 처리
- comp/etc : 필요한 ux 구현(ex pagination, calandar)은 여기서
- comp/route : routing된 body page 구현체
- store : mobx 구조체
- store/api-store : rest api구현 (Promise type reture)
- store/data-store : rest api결과값 저장
- store/state-store : 앱 상태 클래스
- style.sass : sass 적용은 여기서
- App.js : head, header, body, footer 로 나눠져있음
- comp/common/body.js : comp/route/ 디렉터리 내의 component routing을 처리
